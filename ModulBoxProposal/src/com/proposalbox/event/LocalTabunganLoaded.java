package com.proposalbox.event;

import java.util.ArrayList;
import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.CollateralTabungan;

public class LocalTabunganLoaded implements BprEvent {
	public List<CollateralTabungan> listTabungan = new ArrayList<CollateralTabungan>();

	public LocalTabunganLoaded(List<CollateralTabungan> listTabungan) {
		super();
		this.listTabungan.addAll(listTabungan);
	}
}

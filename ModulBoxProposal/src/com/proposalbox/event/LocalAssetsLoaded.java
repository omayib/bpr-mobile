package com.proposalbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Assets;

public class LocalAssetsLoaded implements BprEvent {
	public List<Assets> assets;

	public LocalAssetsLoaded(List<Assets> assets) {
		super();
		this.assets = assets;
	}

}

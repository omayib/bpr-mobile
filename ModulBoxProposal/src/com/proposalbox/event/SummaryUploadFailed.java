package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class SummaryUploadFailed implements BprEvent {
	public String exception;

	public SummaryUploadFailed(String exception) {
		super();
		this.exception = exception;
	}

}

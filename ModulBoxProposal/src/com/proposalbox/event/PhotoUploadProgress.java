package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class PhotoUploadProgress implements BprEvent {
	public long progress;
	public String name;

	public PhotoUploadProgress(long progress, String name) {
		super();
		this.progress = progress;
		this.name = name;
	}

}

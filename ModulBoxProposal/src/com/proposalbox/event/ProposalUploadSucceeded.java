package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Summary;

public class ProposalUploadSucceeded implements BprEvent {
	public Summary summary;

	public ProposalUploadSucceeded(Summary summary) {
		super();
		this.summary = summary;
	}

}

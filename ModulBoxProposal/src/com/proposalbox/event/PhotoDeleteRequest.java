package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Photo;

public class PhotoDeleteRequest implements BprEvent {
	public Photo photo;
	public String token;
	public PhotoDeleteRequest(Photo photo, String token) {
		super();
		this.photo = photo;
		this.token = token;
	}

	
}

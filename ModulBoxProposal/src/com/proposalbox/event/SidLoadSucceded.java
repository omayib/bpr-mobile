package com.proposalbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Sid;

public class SidLoadSucceded implements BprEvent {
	public List<Sid> sids;

	public SidLoadSucceded(List<Sid> sids) {
		super();
		this.sids = sids;
	}

}

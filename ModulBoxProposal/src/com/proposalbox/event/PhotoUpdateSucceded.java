package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.PhotoCandidate;

public class PhotoUpdateSucceded implements BprEvent {
	public PhotoCandidate latestCandidate;

	public PhotoUpdateSucceded(PhotoCandidate latestCandidate) {
		super();
		this.latestCandidate = latestCandidate;
	}

}

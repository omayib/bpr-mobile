package com.proposalbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Purposes;

public class LocalPurposesLoaded implements BprEvent {
	public List<Purposes> purposes;

	public LocalPurposesLoaded(List<Purposes> purposes) {
		super();
		this.purposes = purposes;
	}

}

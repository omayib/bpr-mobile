package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.CapacityCollection;

public class LocalCapacityLoaded implements BprEvent {
	public CapacityCollection capacityCollection;

	public LocalCapacityLoaded(CapacityCollection capacityCollection) {
		super();
		this.capacityCollection = capacityCollection;
	}

}

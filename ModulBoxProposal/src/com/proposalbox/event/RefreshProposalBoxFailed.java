package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class RefreshProposalBoxFailed implements BprEvent {

	public String exception;

	public RefreshProposalBoxFailed(String exception) {
		super();
		this.exception = exception;
	}

}

package com.proposalbox.event;

import java.util.ArrayList;
import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;

public class LocalExpenditureLoaded implements BprEvent {
	public List<CapacityExpenditure> expenditures= new ArrayList<CapacityExpenditure>();

	public LocalExpenditureLoaded(List<CapacityExpenditure> incomes) {
		super();
		this.expenditures = incomes;
	}

}

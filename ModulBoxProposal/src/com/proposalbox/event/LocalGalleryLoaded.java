package com.proposalbox.event;

import java.util.ArrayList;
import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Photo;

public class LocalGalleryLoaded implements BprEvent {
	public List<Photo> listPhoto = new ArrayList<Photo>();

	public LocalGalleryLoaded(List<Photo> listPhoto) {
		super();
		this.listPhoto = listPhoto;
	}

}

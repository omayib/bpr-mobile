package com.proposalbox.event;

import java.util.ArrayList;
import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.CapacityIncome;

public class LocalIncomeLoaded implements BprEvent {
	public List<CapacityIncome> incomes = new ArrayList<CapacityIncome>();

	public LocalIncomeLoaded(List<CapacityIncome> incomes) {
		super();
		this.incomes = incomes;
	}

}

package com.proposalbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.PhotoCandidate;

public class PhotoCandidateToUpload implements BprEvent {
	public List<PhotoCandidate> listPhotoCandidates;

	public PhotoCandidateToUpload(List<PhotoCandidate> listPhotoCandidates) {
		super();
		this.listPhotoCandidates = listPhotoCandidates;
	}

}

package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.SubmissionCredit;

public class LocalSubmissionLoaded implements BprEvent {
	public SubmissionCredit submissionCredit;

	public LocalSubmissionLoaded(SubmissionCredit submissionCredit) {
		super();
		this.submissionCredit = submissionCredit;
	}

}

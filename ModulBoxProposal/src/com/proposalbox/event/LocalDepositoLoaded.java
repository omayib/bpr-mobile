package com.proposalbox.event;

import java.util.ArrayList;
import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.CollateralDeposito;

public class LocalDepositoLoaded implements BprEvent {
	public List<CollateralDeposito> listDeposito = new ArrayList<CollateralDeposito>();

	public LocalDepositoLoaded(List<CollateralDeposito> listDeposito) {
		super();
		this.listDeposito.addAll(listDeposito);
	}
}

package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class PhotoUploadS3Succeded implements BprEvent {
	public String uniqueId;
	public String url;

	public PhotoUploadS3Succeded(String uniqueId, String url) {
		super();
		this.uniqueId = uniqueId;
		this.url = url;
	}

}

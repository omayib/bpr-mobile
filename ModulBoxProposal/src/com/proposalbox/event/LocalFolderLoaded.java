package com.proposalbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.PhotoFolder;

public class LocalFolderLoaded implements BprEvent {
	public List<PhotoFolder> listFolder;

	public LocalFolderLoaded(List<PhotoFolder> listFolder) {
		super();
		this.listFolder = listFolder;
	};

}

package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.PhotoFolder;

public class FolderGalleryCreated implements BprEvent {
	public PhotoFolder photoGallery;

	public FolderGalleryCreated(PhotoFolder photoGallery) {
		super();
		this.photoGallery = photoGallery;
	}

}

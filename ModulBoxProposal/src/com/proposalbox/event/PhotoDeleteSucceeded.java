package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Photo;

public class PhotoDeleteSucceeded implements BprEvent {
	public Photo photo;

	public PhotoDeleteSucceeded(Photo photo) {
		super();
		this.photo = photo;
	}
}

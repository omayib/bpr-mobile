package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Job;

public class LocalJobLoaded implements BprEvent {
	public Job job;

	public LocalJobLoaded(Job job) {
		super();
		this.job = job;
	}

}

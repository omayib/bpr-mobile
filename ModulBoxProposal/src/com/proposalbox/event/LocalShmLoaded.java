package com.proposalbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.CollateralShm;

public class LocalShmLoaded implements BprEvent {
	public List<CollateralShm> listShm;

	public LocalShmLoaded(List<CollateralShm> listShm) {
		super();
		this.listShm = listShm;
	}

}

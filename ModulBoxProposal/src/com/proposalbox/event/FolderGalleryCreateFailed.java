package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class FolderGalleryCreateFailed implements BprEvent {
	public String exception;

	public FolderGalleryCreateFailed(String exception) {
		super();
		this.exception = exception;
	}

}

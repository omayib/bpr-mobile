package com.proposalbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.event.BoxData;

public class RefreshProposalBoxSucceeded implements BprEvent {
	public List<BoxData> response;

	public RefreshProposalBoxSucceeded(List<BoxData> response) {
		super();
		this.response = response;
	}
}

package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class MarkProposalAsCompleted implements BprEvent {
	public int idProposal;
	public String token;

	public MarkProposalAsCompleted(int i, String token) {
		super();
		this.token = token;
		this.idProposal = i;
	}

}

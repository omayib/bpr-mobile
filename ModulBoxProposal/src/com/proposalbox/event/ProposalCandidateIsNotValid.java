package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class ProposalCandidateIsNotValid implements BprEvent {
	public String exception;

	public ProposalCandidateIsNotValid(String exception) {
		super();
		this.exception = exception;
	}

}

package com.proposalbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.EnvironmentalCheck;

public class LocalEnvLoaded implements BprEvent {
	public List<EnvironmentalCheck> listEnv;

	public LocalEnvLoaded(List<EnvironmentalCheck> listEnv) {
		super();
		this.listEnv = listEnv;
	}

}

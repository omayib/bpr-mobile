package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.SummaryCandidate;

public class MarkSummaryAsCompleted implements BprEvent {
	public SummaryCandidate summaryCandidate;

	public MarkSummaryAsCompleted(SummaryCandidate summaryCandidate) {
		super();
		this.summaryCandidate = summaryCandidate;
	}

}

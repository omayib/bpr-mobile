package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class SummaryUploadSucceded implements BprEvent {
	public int idProposal;

	public SummaryUploadSucceded(int idProposal) {
		super();
		this.idProposal = idProposal;
	}

}

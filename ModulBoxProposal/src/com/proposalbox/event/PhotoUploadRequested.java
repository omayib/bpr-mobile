package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.PhotoCandidate;

public class PhotoUploadRequested implements BprEvent {
	public PhotoCandidate photoCandidate;

	public PhotoUploadRequested(PhotoCandidate photoCandidate) {
		super();
		this.photoCandidate = photoCandidate;
	}

}

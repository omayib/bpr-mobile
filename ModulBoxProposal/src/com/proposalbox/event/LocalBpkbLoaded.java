package com.proposalbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.model.CollateralBpkb;

public class LocalBpkbLoaded implements BprEvent {
	public List<CollateralBpkb> listBpkb;

	public LocalBpkbLoaded(List<CollateralBpkb> listBpkb) {
		super();
		this.listBpkb = listBpkb;
	}

}

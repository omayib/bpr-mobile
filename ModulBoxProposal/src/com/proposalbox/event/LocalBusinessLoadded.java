package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.BusinessCollection;

public class LocalBusinessLoadded implements BprEvent {
	public BusinessCollection businessCollection;

	public LocalBusinessLoadded(BusinessCollection businessCollection) {
		super();
		this.businessCollection = businessCollection;
	}

}

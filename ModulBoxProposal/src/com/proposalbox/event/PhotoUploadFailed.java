package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class PhotoUploadFailed implements BprEvent {

	public String exception;

	public PhotoUploadFailed(String exception) {
		super();
		this.exception = exception;
	}

}

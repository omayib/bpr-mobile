package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;

public class ProposalUploadFailed implements BprEvent {
	public String exception;

	public ProposalUploadFailed(String exception) {
		super();
		this.exception = exception;
	}

}

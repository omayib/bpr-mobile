package com.proposalbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Personal;

public class LocalPersonalLoaded implements BprEvent {
	public Personal personal;

	public LocalPersonalLoaded(Personal personal) {
		super();
		this.personal = personal;
	}

}

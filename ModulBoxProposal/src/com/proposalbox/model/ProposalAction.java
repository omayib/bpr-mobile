package com.proposalbox.model;

import java.util.List;

import com.customer.model.Assets;
import com.customer.model.Business;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralDeposito;
import com.customer.model.CollateralShm;
import com.customer.model.CollateralTabungan;
import com.customer.model.EnvironmentalCheck;
import com.customer.model.Job;
import com.customer.model.Personal;
import com.customer.model.PhotoCandidate;
import com.customer.model.PhotoFolder;
import com.customer.model.Purposes;
import com.customer.model.SubmissionCredit;
import com.customer.model.SummaryCandidate;

public interface ProposalAction {
	void uploadProposal(String data, int idproposal, String stringRecommendation);

	void uploadSummary(int idproposal, SummaryCandidate candidate);
	
	void getSummary(String token,int idProposal);

	void createFolderGallery(String token, String name, int idProposal,
			String galleryName);

	void uploadPhoto(String token, int folderId, PhotoCandidate photoCandidate);

	void getPersonalFromLocal(int idProposal);

	void getAssetsFromLocal(int idProposal);

	void getJobFromLocal(int idProposal);

	void getBusinessFromLocal(int idProposal);

	void getSubmissionFromLocal(int idProposal);

	void getPurposeFromLocal(int idProposal);

	void getCapacityFromLocal(int idProposal);

	void getBpkbFromLocal(int idProposal);

	void getShmFromLocal(int idProposal);

	void getEnvFromLocal(int idProposal);

	void getFoldersFromLocal(int idProposal, String galleryName);

	void getGalleryPending();

	void getTabunganFromLocal(int idProposal);

	void getDepositoFromLocal(int idProposal);
	
	void putProposal(int proposalId);

	void putPersonal(Personal personal, int proposalId);

	void putAssets(List<Assets> listAssets, int proposalId);

	void putJob(Job job, int proposalId);

	void putBusiness(Business business, int proposalId);

	void putBusinessCollection(List<Business> business, int proposalId);

	void putSubmission(SubmissionCredit subm, int proposalId);

	void putPurposes(List<Purposes> listPurpose, int proposalId);

	void putCapacity(List<CapacityIncome> income,
			List<CapacityExpenditure> expenditure, int idCapacity,
			int proposalId);

	void putCollateralShm(List<CollateralShm> shm, int proposalId);

	void putCollateralBpkb(List<CollateralBpkb> bpkb, int proposalId);

	void puteEnvCheckup(List<EnvironmentalCheck> listCheck, int proposalId);

	void putTabungan(List<CollateralTabungan> listTabungan, int proposalId);

	void putDeposito(List<CollateralDeposito> listDeposito, int proposalId);

	void putPhotoBusiness(List<PhotoFolder> listFolder, int proposalId);

	void putPhotoHome(List<PhotoFolder> listFolder, int proposalId);

	void putPhotoCollateral(List<PhotoFolder> listFolder, int proposalId);

	void putPhotoOther(List<PhotoFolder> listFolder, int proposalId);
}

package com.proposalbox.model;

public interface ProposalBoxAction {
	void proposalBoxRefresh(String token);

	void getSids(String token, int idCustomer);
}

package com.modul.customer.test;

import java.util.regex.Matcher;

import org.hamcrest.core.IsEqual;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import com.customer.model.Business;

public class BusinessTest {
	static Business business;

	@BeforeClass
	public static void setup_object() {
		business = new Business.Builder().companyAddress("address 1")
				.companyInfo("additional info")
				.companyLegallity(Business.Legallity.CV)
				.companyName("technomotion").companyProject("banyak")
				.companySiup("12332123321").companyTdp("23132132/12312")
				.companyTenure(Business.OfficeTenure.SENDIRI).build();
	}

	@Test
	public void business_should_have_legality() {
		assertEquals(Business.Legallity.CV, business.getCompanyLegallity());
	}

	@Test
	public void assign_to_legallity() {
		String leg = "kontrak";

		assertThat(Business.OfficeTenure.SENDIRI.toString(), is(leg));

	}
}

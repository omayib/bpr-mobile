package com.modul.customer.test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import com.customer.model.CapacityCollection;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class CapacityCollectionTest {
	CapacityCollection capacityCollection;

	@Before
	public void setup() {
		CapacityIncome income1 = new CapacityIncome.Builder().incomes("gaji1",
				"5000000").build();
		CapacityIncome income2 = new CapacityIncome.Builder().incomes("gaji2",
				"3000000").build();
		CapacityIncome income3 = new CapacityIncome.Builder().incomes("gaji3",
				"4000000").build();
		Collection<CapacityIncome> in = new ArrayList<CapacityIncome>();
		in.add(income1);
		in.add(income2);
		in.add(income3);

		CapacityExpenditure outcome1 = new CapacityExpenditure.Builder()
				.expenditures("pengeluaran1", "1300000").build();
		CapacityExpenditure outcome2 = new CapacityExpenditure.Builder()
				.expenditures("pengeluaran2", "400000").build();
		CapacityExpenditure outcome3 = new CapacityExpenditure.Builder()
				.expenditures("pengeluaran3", "700000").build();
		CapacityExpenditure outcome4 = new CapacityExpenditure.Builder()
				.expenditures("pengeluaran4", "500000").build();
		Collection<CapacityExpenditure> out = new ArrayList<CapacityExpenditure>();
		out.add(outcome1);
		out.add(outcome2);
		out.add(outcome3);
		out.add(outcome4);
		capacityCollection = new CapacityCollection.Builder(in, out)
				.submission(new BigDecimal(150000000)).build();
	}

	@Test
	public void big_decimal_test() {
		BigDecimal b1 = new BigDecimal(0);
		BigDecimal b2 = new BigDecimal(0);
		b1 = new BigDecimal(4);
		b2 = new BigDecimal(3);
		System.out.println("tes:" + b1.add(b2));
	}

	@Test
	public void income_should_more_then_one() {
		assertThat(capacityCollection.getIncome().size(), is(3));
	}

	@Test
	public void out_should_more_then_one() {
		assertThat(capacityCollection.getExpenditure().size(), is(4));
	}

	@Test
	public void total_income_should_same() {
		assertThat(capacityCollection.getIncomeTotal(), is(new BigDecimal(
				12000000)));
	}

	@Test
	public void total_out_should_same() {
		assertThat(capacityCollection.getExpenditureTotal(), is(new BigDecimal(
				2900000)));
	}

	@Test
	public void total_net_income_should_same() {
		assertThat(capacityCollection.getIncomeNet(), is(new BigDecimal(
				12000000).subtract(new BigDecimal(2900000))));

	}
}

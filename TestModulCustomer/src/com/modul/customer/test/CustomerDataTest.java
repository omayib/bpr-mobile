package com.modul.customer.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.BeforeClass;
import org.junit.Test;

import com.customer.model.Business;
import com.customer.model.Business.Legallity;
import com.customer.model.Business.OfficeTenure;
import com.customer.model.BusinessCollection;
import com.customer.model.CapacityCollection;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import com.customer.model.CustomerData;
import com.customer.model.EnvironmentalCheck;
import com.customer.model.EnvironmentalCheckCollection;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralBpkbCollection;
import com.customer.model.CollateralShm;
import com.customer.model.CollateralShmCollection;
import com.customer.model.Job;
import com.customer.model.Recommendation;
import com.customer.model.Job.statusOfWork;
import com.customer.model.Note;
import com.customer.model.NotesCollection;
import com.customer.model.Personal;
import com.customer.model.Proposal;
import com.customer.model.Purposes;
import com.customer.model.PurposesCollection;
import com.customer.model.SubmissionCredit;
import com.customer.model.SubmissionCredit.CreditProduct;
import com.user.modul.User;

public class CustomerDataTest {
	private static Personal personal() throws ParseException {
		return new Personal.Builder().addressByCurrent("current address")
				.addressByKtp("ktp adress").birthDate("23/02/1988")
				.birthPlace("wonogiri").childrens("anak1", 12)
				.childrens("anak2", 14).spouseName("nama istri")
				.spouseJob("peerjaan istri").name("Joko Priyanto").build();

	}

	private static Job job() {
		return new Job.Builder().achievment("penghargaan pertama")
				.officeAddress("alamat kantor").officeName("nama kantor")
				.positionByCurrent("programmer").positionByInitial("tester")
				.salary(new BigDecimal(45000000))
				.statusOfWorker(statusOfWork.TETAP).build();
	}

	private static ArrayList<CollateralBpkb> bpkb() {
		ArrayList<CollateralBpkb> bpkbs = new ArrayList<CollateralBpkb>();
		CollateralBpkb bpkb1 = new CollateralBpkb.Builder().carBrand("Honda Jazz")
				.nameOfBpkb("arif akbarul").carName("sedan")
				.numberBpkb("12332123").numberStnk("2123123")
				.priceOnAssessed("23000000").priceOnMarket("200000000")
				.carType("sedan").carYear("2010").build();
		CollateralBpkb bpkb2 = new CollateralBpkb.Builder().carBrand("Honda2 Jazz")
				.nameOfBpkb("arif2 akbarul").carName("sedan2")
				.numberBpkb("222232123").numberStnk("2122223")
				.priceOnAssessed("222200000").priceOnMarket("200000000")
				.carType("2sedan").carYear("2210").build();
		bpkbs.add(bpkb2);
		bpkbs.add(bpkb1);
		return bpkbs;
	}

	private static ArrayList<Business> business() {
		ArrayList<Business> b = new ArrayList<Business>();
		Business b1 = new Business.Builder().companyAddress("company1 address")
				.companyInfo("company info 1").companyLegallity(Legallity.CV)
				.companyName("company name 1")
				.companyProject("projects projects").companySiup("123siup321")
				.companyTdp("987tdp88909").companyTenure(OfficeTenure.SENDIRI)
				.fieldOfBusiness("IT").build();
		Business b2 = new Business.Builder().companyAddress("company2 address")
				.companyInfo("company info 2").companyLegallity(Legallity.PT)
				.companyName("company name 2")
				.companyProject("projects 2 projects")
				.companySiup("11113siup321").companyTdp("987tdp88909")
				.companyTenure(OfficeTenure.KONTRAK).fieldOfBusiness("Garmen")
				.build();

		b.add(b1);
		b.add(b2);
		return b;
	}

	private static ArrayList<EnvironmentalCheck> environment() {
		ArrayList<EnvironmentalCheck> checks = new ArrayList<EnvironmentalCheck>();
		EnvironmentalCheck check1 = new EnvironmentalCheck.Builder()
				.impression("POSITIF").name("Diah ayu").relation("tetangga")
				.build();
		EnvironmentalCheck check2 = new EnvironmentalCheck.Builder()
				.impression("POSITIF").name("Septi nurlita")
				.relation("tetangga").build();
		EnvironmentalCheck check3 = new EnvironmentalCheck.Builder()
				.impression("POSITIF").name("Ammar").relation("saudara")
				.build();
		checks.add(check3);
		checks.add(check2);
		checks.add(check1);
		return checks;
	}

	private static CapacityExpenditure expenditure() {
		return new CapacityExpenditure.Builder()
				.expenditures("rumah tangga", new BigDecimal(2300000))
				.expenditures("listrik", new BigDecimal(500000)).build();
	}

	private static Purposes purposes() {
		return new Purposes.Builder().purpose("tujuan 1", "detail tujuan 1")
				.purpose("tujuan 2", "detail tujuan 2").build();
	}

	private static CapacityIncome income() {
		return new CapacityIncome.Builder()
				.incomes("gaji perbulan", new BigDecimal(5000000))
				.incomes("perusahaan 1", new BigDecimal(4000000))
				.incomes("usaha 2", new BigDecimal(5000000)).build();
	}

	private static Collection<CollateralShm> shm() {
		Collection<CollateralShm> shm = new ArrayList<CollateralShm>();
		CollateralShm shm1 = new CollateralShm.Builder()
				.certificateName("arif akbarul").certificateNumber("12332123")
				.loadOfArea(new BigDecimal(230))
				.statusOfLand("TANAH PEKARANGAN").priceOnAssessed("231000000")
				.priceOnMarket("212000000").build();
		CollateralShm shm2 = new CollateralShm.Builder()
				.certificateName("huda akbarul").certificateNumber("1112123")
				.loadOfArea(new BigDecimal(130))
				.statusOfLand("TANAH PEKARANGAN 2")
				.priceOnAssessed("331000000").priceOnMarket("317000000")
				.build();
		shm.add(shm1);
		shm.add(shm2);
		return shm;
	}

	private static SubmissionCredit submission() {
		return new SubmissionCredit.Builder().periode(45)
				.plafon(new BigDecimal(150000000)).recommendation("bagus")
				.creditProduct(CreditProduct.KREDIT_TETAP).build();
	}

	private static CapacityCollection capacity() {
		return new CapacityCollection.Builder(income(), expenditure()).build();
	}

	private static NotesCollection notesCollection() {
		User head = new User.Builder().id(3).level("head of marketing")
				.name("musa").build();
		Note n1 = new Note.Builder()
				.messages(
						"Dear Students,.!!!! CEGONSOFT PVT LTD, Gandhipuram - Pioneers in software training programmes, which has quality training and infrastructure ")
				.creator(head).build();
		Note n2 = new Note.Builder()
				.messages(
						"java packages ppt and java inbuilt class ppt like math class vector class hashtable")
				.creator(head).build();
		NotesCollection n = new NotesCollection.Builder().notes(n2).notes(n1)
				.build();
		return n;
	}

	private static CustomerData customer;

	@BeforeClass
	public static void customer() throws ParseException {
		Proposal p1 = new Proposal.Builder()
				.bpkb(new CollateralBpkbCollection.Builder().bpkb(bpkb())
						.build())
				.business(
						new BusinessCollection.Builder().business(business())
								.build())
				.environmentalCheck(
						new EnvironmentalCheckCollection.Builder()
								.environmentalCheck(environment()).build())
				.capacity(capacity())
				.job(job())
				.personal(personal())
				.purpose(
						new PurposesCollection.Builder().purpose(purposes())
								.build())
				.shm(new CollateralShmCollection.Builder().shm(shm()).build())
				.submissionOfCredit(submission()).checkedDate("11-june-14")
				.build();
		Proposal p2 = new Proposal.Builder()
				.bpkb(new CollateralBpkbCollection.Builder().bpkb(bpkb())
						.build())
				.business(
						new BusinessCollection.Builder().business(business())
								.build())
				.environmentalCheck(
						new EnvironmentalCheckCollection.Builder()
								.environmentalCheck(environment()).build())
				.capacity(capacity())
				.job(job())
				.personal(personal())
				.purpose(
						new PurposesCollection.Builder().purpose(purposes())
								.build())
				.shm(new CollateralShmCollection.Builder().shm(shm()).build())
				.submissionOfCredit(submission()).build();
		Proposal p3 = new Proposal.Builder()
				.bpkb(new CollateralBpkbCollection.Builder().bpkb(bpkb())
						.build())
				.business(
						new BusinessCollection.Builder().business(business())
								.build())
				.environmentalCheck(
						new EnvironmentalCheckCollection.Builder()
								.environmentalCheck(environment()).build())
				.capacity(capacity())
				.job(job())
				.personal(personal())
				.purpose(
						new PurposesCollection.Builder().purpose(purposes())
								.build())
				.shm(new CollateralShmCollection.Builder().shm(shm()).build())
				.submissionOfCredit(submission()).build();
		Collection<Proposal> po = new ArrayList<Proposal>();
		po.add(p1);
		po.add(p2);
		po.add(p3);

		// Recommendation recommendation=new
		// Recommendation.Builder().notesCollection(notesCollection())
		// .setAccept().signature("asdasd").user(u)

		customer = new CustomerData.Builder().customer(po,
				personal().getName(), submission().getPlafon()).build();
	}

	@Test
	public void customer_should_have_3kinds_of_prposals() {
		assertThat(customer.getProposals().size(), is(3));
	}

	@Test
	public void customer_should_have_name() {
		assertThat(customer.getName(), is("Joko Priyanto"));
		// assertThat(customer.getBpkbCollection().getGuaranteeBpkb().size(),
		// is(2));
	}

	@Test
	public void customer_should_have_notes() {
		// assertThat(customer.getNotesCollection().getNotes().size(), is(2));
		// assertThat(customer.getBusinessCollection().getBusinessCollection()
		// .size(), is(0));
	}

	@Test
	public void test_about_date() {
		ArrayList<Proposal> p = (ArrayList<Proposal>) customer.getProposals();
		System.out.println(p.get(0).getCheckedDate().toString());
	}
}

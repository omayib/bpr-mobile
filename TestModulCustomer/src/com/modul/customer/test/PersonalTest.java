package com.modul.customer.test;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import org.junit.BeforeClass;
import org.junit.Test;

import com.customer.model.Personal;

public class PersonalTest {
	static Personal p;

	@BeforeClass
	public static void create_personal_Object() throws ParseException {
		p = new Personal.Builder().name("arif akbarul")
				.statusOfMarital("menikah").addressByCurrent("perum peen asri")
				.addressByKtp("wonogiri").birthDate("29/01/1988")
				.birthPlace("wonogiri").spouseName("fitri")
				.spouseJob("laboran UII").build();
	}

	@Test
	public void birth_of_place_should_not_null() {
		assertNotNull(p.getBirthPlace());
	}

	@Test
	public void update_birth_of_place() throws ParseException {
		p.setBirthPlace("yogyakarta");
		assertEquals("yogyakarta", p.getBirthPlace());
	}

	public void child_should_empty() {
		System.out.println("a" + p.getChildrens());
		assertTrue(!p.getChildrens().isEmpty());
	}

	@Test
	public void insert_childrens() {

		p.setChildrens("hammam", 3);
		p.setChildrens("ammar", 5);
		assertTrue(p.getChildrens().size() > 0);
		// System.out.println(p.getChildrens());
	}

	@Test
	public void update_childrens_age() {

		p.setChildrens("hammam", 3);
		p.setChildrens("ammar", 7);
		assertTrue(p.getChildrens().size() > 0);
		// System.out.println(p.getChildrens());
	}

	@Test
	public void age_should_auto_filled() {
		assertEquals(new Integer("26"), p.getAge());
	}
}

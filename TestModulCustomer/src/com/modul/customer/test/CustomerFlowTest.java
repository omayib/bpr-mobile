package com.modul.customer.test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;

import com.customer.model.CustomerData;
import com.customer.model.CustomerData.CustomerStatus;
import com.customer.model.Note;
import com.customer.model.NotesCollection;
import com.customer.model.Proposal;
import com.customer.model.Proposal.FLAG;
import com.customer.model.Recommendation;
import com.user.modul.User;
import com.user.modul.UserLevel;

public class CustomerFlowTest {
	public static CustomerData customerData;
	static CustomerDataProducer data;
	static User joko;
	static User budi;
	static User head;

	@BeforeClass
	public static void setup() throws ParseException {
		data = new CustomerDataProducer();
		customerData = new CustomerData.Builder().customer(data.proposal(),
				data.personal().getName(), data.submission().getPlafon())
				.build();
		joko = new User.Builder().id(12).level("analysist officer")
				.name("joko").build();

		budi = new User.Builder().id(24).level("analysist officer")
				.name("budi").build();
		head = new User.Builder().id(3).level("head of marketing").name("musa")
				.build();

	}

	/**
	 * user level HEAD should pass
	 * */
	@Test
	public void A1_user_MUSA_created_as_a_HEAD() {
		User user = new User.Builder().id(34).level("head of marketing")
				.name("Musa").build();
		assertThat(user.getLevel(), is(UserLevel.HEAD_OF_MARKETING));
	}

	/**
	 * user level AO should pass
	 * */
	@Test
	public void A2_user_JOKO_created_as_a_AO() {

		User user = new User.Builder().id(12).level("analysist officer")
				.name("joko").build();
		assertThat(user.getLevel(), is(UserLevel.ANALYSIST_OFFICER));
	}

	/**
	 * should pass proposal size equals 1
	 * */
	@Test
	public void A3_new_customer_comming_with_1_proposal() {
		assertThat(customerData.getProposals().size(), is(1));
	}

	/**
	 * should pass proposal size equals 1 and and customer has assigned_to value
	 * equals -1
	 * */
	@Test
	public void A4_new_customer_comming_with_1_proposal_not_assigned_to_anyone() {
		assertThat(customerData.getAssignToId(), is(-1));
	}

	/**
	 * should pass proposal size equals 1 and has value created_by ORIGINAL
	 * */
	@Test
	public void A5_new_customer_comming_with_1_proposal_createdby_original() {
		ArrayList<Proposal> proposals = (ArrayList<Proposal>) customerData
				.getProposals();
		assertThat(proposals.get(0).getCheckedByName(), is("ORIGINAL"));
	}

	/**
	 * should pass proposal size equals 1 and has value created_by ORIGINAL
	 * */
	@Test
	public void A5_1_new_customer_comming_with_1_proposal_original() {
		ArrayList<Proposal> proposals = (ArrayList<Proposal>) customerData
				.getProposals();
		assertThat(proposals.get(0).isOriginal(), is(true));
	}

	/**
	 * should pass proposal size equals 1 and has value created_by ORIGINAL
	 * */
	@Test
	public void A5_2_new_customer_comming_with_1_proposal_original_and_FLAG_should_COMPLETED() {
		ArrayList<Proposal> proposals = (ArrayList<Proposal>) customerData
				.getProposals();
		assertThat(proposals.get(0).getFlag(), is(FLAG.COMPLETED));
	}

	/**
	 * should pass proposal size equals 1 and notes collections size equals 0
	 * */
	@Test
	public void A6_new_customer_comming_with_1_proposal_dont_have_notes() {
		List<Recommendation> recos = (List<Recommendation>) customerData
				.getRecommendations();
		assertNull(recos.get(0).getNotes().size());
	}

	/**
	 * should pass proposal status equals NEW
	 * */
	@Test
	public void A7_new_customer_have_status_NEW() {
		assertThat(customerData.getStatus(), is(CustomerStatus.NEW));
	}

	/**
	 * customer should has assigned_to value equals JOKO'S id
	 * */
	@Test
	public void A8_new_customer_assigned_to_JOKO_without_notes() {
		User user = new User.Builder().id(12).level("analysist officer")
				.name("joko").build();
		// then a head of marketing assign to those user
		customerData.setAssignToId(user);
		assertThat(customerData.getAssignToId(), is(12));
	}

	/**
	 * customer should has collections
	 * */
	@Test
	public void A9_new_customer_assigned_to_JOKO_and_BUDI_with_notes() {

		// then a head of marketing assign to those user
		customerData.setAssignToId(joko);
		customerData.setStatus(CustomerStatus.PROPOSAL);

		// duplicate the proposal data for joko
		Proposal p2 = data.proposal();
		p2.setCheckedBy(joko);
		customerData.setProposals(p2);

		// duplicate the proposal data for Budi
		Proposal p3 = data.proposal();
		p3.setCheckedBy(budi);
		customerData.setProposals(p3);

		// also add a note for this customer
		Note n = new Note.Builder().creator(head)
				.messages("coba disurvei dulu ya.... orangnya bisa dipercaya")
				.build();
		// NotesCollection nc = new NotesCollection.Builder().notes(n).build();
		// create recommendations
		Recommendation recommendation = new Recommendation.Builder().notes(n)
				.setAccept().signature("123").user(head).build();
		customerData.setRecommendations(recommendation);
		List<Recommendation> recos = (List<Recommendation>) customerData
				.getRecommendations();
		assertThat(recos.get(0).getNotes().size(), is(1));
	}

	/**
	 * after assignment, now customer has 2 kinds of proposal, the original
	 * proposal and the proposal assigned to himself
	 * */
	@Test
	public void A10_customer_assigned_to_JOKO_with_notes_and_has_2_proposals() {

		assertThat(customerData.getProposalsBy(joko).size(), is(2));
	}

	@Test
	public void A10_1_customer_assigned_to_JOKO_with_notes_and_has_2_proposals_completed_and_incompleted() {
		Collection<Proposal> proposals = customerData.getProposalsBy(joko);
		for (Proposal proposal : proposals) {
			System.out.println(proposal.getFlag());
			if (proposal.getFlag().equals(FLAG.COMPLETED))
				assertThat(proposal.getFlag(), is(FLAG.COMPLETED));
			else
				assertThat(proposal.getFlag(), is(FLAG.INCOMPLETED));
		}
	}

	/**
	 * customer status should equals PROPOSAL.
	 * */
	@Test
	public void A11_customer_assigned_to_JOKO_with_notes_and_has_status_PROPOSAL() {
		assertThat(customerData.getStatus(), is(CustomerStatus.PROPOSAL));
	}

	/**
	 * customer have 1 user approve
	 */
	@Test
	public void A12_JOKO_mark_customer_as_complete() {
		Note n = new Note.Builder().creator(joko).messages("sip oke oke oke")
				.build();
		// create recommendations
		Recommendation recommendation = new Recommendation.Builder().notes(n)
				.setAccept().signature("123").user(head).build();
		customerData.setRecommendations(recommendation);

		List<Recommendation> recos = (List<Recommendation>) customerData
				.getRecommendations();
		assertThat(recos.size(), is(2));
	}

	@Test
	public void A12_1_all_proposals_fave_COMPLETE() {
		Collection<Proposal> proposals = customerData.getProposalsBy(joko);
		for (Proposal proposal : proposals) {
			System.out.println(proposal.getFlag());
			assertThat(proposal.getFlag(), is(FLAG.COMPLETED));
		}
	}

	@Test
	public void A13_now_customer_should_have_2_notes() {
		List<Recommendation> recos = (List<Recommendation>) customerData
				.getRecommendations();
		assertThat(recos.size(), is(2));
	}
}

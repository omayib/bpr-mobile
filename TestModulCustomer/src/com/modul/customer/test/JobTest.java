package com.modul.customer.test;

import static org.junit.Assert.*;
import java.math.BigDecimal;

import org.junit.BeforeClass;
import org.junit.Test;

import com.customer.model.Job;
import com.customer.model.Job.statusOfWork;

public class JobTest {
	static Job job;

	@BeforeClass
	public static void setup_object() {
		job = new Job.Builder().officeName("QISCUS")
				.officeAddress("perum petinggen")
				.statusOfWorker(statusOfWork.KONTRAK)
				.positionByCurrent("programmer").positionByInitial("tester")
				.yearOfJoined(2010).salary(new BigDecimal(5000000))
				.achievment("best guitars").build();
	}

	@Test
	public void he_should_have_office() {
		assertEquals("QISCUS", job.getOfficeName());
	}

}

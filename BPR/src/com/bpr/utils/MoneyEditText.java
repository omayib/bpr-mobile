package com.bpr.utils;

import java.lang.ref.WeakReference;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.EditText;

public class MoneyEditText extends EditText {
	public MoneyEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.addTextChangedListener(new MoneyTextWatcher(this));
	}

	public MoneyEditText(Context context) {
		super(context);
		this.addTextChangedListener(new MoneyTextWatcher(this));
	}

	public MoneyEditText(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		this.addTextChangedListener(new MoneyTextWatcher(this));
	}

	public BigDecimal getNominal() {
		String d = getText().toString();
		if (d.isEmpty()) {
			return new BigDecimal(0);
		}
		String replaceable = String.format("[%s,.\\s]", "Rp.");
		System.out.println("replacable " + replaceable);
		String cleanString = d.replaceAll(replaceable, "");
		return new BigDecimal(cleanString);
	}

	@Override
	public void setText(CharSequence text, BufferType type) {
		super.setText(text, type);
	}

	public class MoneyTextWatcher implements TextWatcher {
		private final WeakReference<EditText> editTextWeakReference;

		// private String current;

		public MoneyTextWatcher(EditText editText) {
			editTextWeakReference = new WeakReference<EditText>(editText);
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
		}

		@Override
		public void afterTextChanged(Editable editable) {
			EditText inputValue = editTextWeakReference.get();

			// String a = editable.toString().equalsIgnoreCase(current) ?
			// "equals"
			// : "not equals";

			inputValue.removeTextChangedListener(this);

			String replaceable = String.format("[%s,.\\s]", "Rp.");
			String cleanString = editable.toString()
					.replaceAll(replaceable, "");

			BigDecimal parsed;
			try {
				parsed = new BigDecimal(cleanString);
			} catch (NumberFormatException e) {
				parsed = new BigDecimal(0);
			}

			DecimalFormat rupiahIndo = (DecimalFormat) DecimalFormat
					.getCurrencyInstance();
			DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
			formatRp.setCurrencySymbol("Rp.");
			formatRp.setMonetaryDecimalSeparator(',');
			formatRp.setGroupingSeparator('.');
			rupiahIndo.setDecimalFormatSymbols(formatRp);
			rupiahIndo.setMaximumFractionDigits(0);

			String formatted = rupiahIndo.format((parsed));

			// current = formatted;
			inputValue.setText(formatted);
			inputValue.setSelection(formatted.length());
			inputValue.addTextChangedListener(this);

		}

	}
}

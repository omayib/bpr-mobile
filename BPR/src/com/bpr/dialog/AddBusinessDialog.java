package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.utils.RandomInteger;
import com.customer.model.Business;

public class AddBusinessDialog extends DialogFragment {
	private EditText editText_businessField, editText_companyName,
			editText_companyAddr, editText_companyTenure, editText_siup,
			editText_tdp, editText_numberOfEmployee,
			editText_companyFoundedYear, editText_copmanyProjects,
			editText_otherInfo, editText_yearOperation;
	private Spinner spnLegallity;
	private Button bSave;
	private OnBusinessAddedListener listener;

	public interface OnBusinessAddedListener {
		void onBusinessAdded(Business b);
	}

	public void setOnBusinessAddedListener(OnBusinessAddedListener l) {
		this.listener = l;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah bisnis");
		View v = inflater.inflate(R.layout.dialog_business, container, false);
		editText_yearOperation = (EditText) v
				.findViewById(R.id.biz_companyYearOfOperation);
		spnLegallity = (Spinner) v.findViewById(R.id.biz_companyLegallity);
		editText_businessField = (EditText) v
				.findViewById(R.id.biz_businessFields);
		editText_companyAddr = (EditText) v
				.findViewById(R.id.biz_companyAddress);
		editText_companyName = (EditText) v.findViewById(R.id.biz_companyName);
		editText_companyTenure = (EditText) v
				.findViewById(R.id.biz_companyTenure);
		editText_companyFoundedYear = (EditText) v
				.findViewById(R.id.biz_companyYear);
		editText_copmanyProjects = (EditText) v.findViewById(R.id.biz_Project);
		editText_numberOfEmployee = (EditText) v
				.findViewById(R.id.biz_numberOfEmployee);
		editText_otherInfo = (EditText) v.findViewById(R.id.biz_companyInfo);
		editText_siup = (EditText) v.findViewById(R.id.biz_companySiup);
		editText_tdp = (EditText) v.findViewById(R.id.biz_companyTdp);
		bSave = (Button) v.findViewById(R.id.dialog_business_button_simpan);
		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (editText_companyName.getText().toString().isEmpty()) {
					Toast.makeText(getActivity(), "nama perusahan wajib diisi",
							Toast.LENGTH_SHORT).show();
					return;
				}
				if (editText_businessField.getText().toString().isEmpty()) {
					Toast.makeText(getActivity(),
							"nama bidang usaha wajib diisi", Toast.LENGTH_SHORT)
							.show();
					return;
				}

				String additionalInfo = editText_otherInfo.getText().toString();
				String address = editText_companyAddr.getText().toString();
				String companyLegallity = spnLegallity.getSelectedItem()
						.toString();
				String field = editText_businessField.getText().toString();
				int foundedYear = Integer.parseInt(editText_companyFoundedYear
						.getText().toString().isEmpty() ? "1990"
						: editText_companyFoundedYear.getText().toString());
				String name = editText_companyName.getText().toString();
				int numberOfEmployees = Integer
						.parseInt(editText_numberOfEmployee.getText()
								.toString().isEmpty() ? "0"
								: editText_numberOfEmployee.getText()
										.toString());
				String officeOwnership = editText_companyTenure.getText()
						.toString();
				String projectExperiences = editText_copmanyProjects.getText()
						.toString();
				String siupNumber = editText_siup.getText().toString();
				String tdpNumber = editText_tdp.getText().toString();
				int yearsOfOperation = Integer.parseInt(editText_yearOperation
						.getText().toString().isEmpty() ? "1990"
						: editText_yearOperation.getText().toString());
				int randomId = RandomInteger.randInt(1, 1000);
				Business business = new Business.Builder().id(randomId)
						.companyAddress(address).companyInfo(additionalInfo)
						.companyLegallity(companyLegallity).companyName(name)
						.companyProject(projectExperiences)
						.companySiup(siupNumber).companyTdp(tdpNumber)
						.companyTenure(officeOwnership).fieldOfBusiness(field)
						.foundedYear(foundedYear)
						.numberOfEmployee(numberOfEmployees)
						.year(yearsOfOperation).build();

				listener.onBusinessAdded(business);
				getDialog().dismiss();
			}
		});
		return v;
	}

}

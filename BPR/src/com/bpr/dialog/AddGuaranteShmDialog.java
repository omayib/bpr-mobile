package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bpr.R;
import com.bpr.utils.MoneyEditText;
import com.bpr.utils.RandomInteger;
import com.customer.model.CollateralShm;

public class AddGuaranteShmDialog extends DialogFragment {

	private EditText etShmNumber, etShmName, etSize, etInfo;
	private MoneyEditText etPriceMarket, etPriceAssesed;
	private Spinner spnBinding, spnStatus, spnShmType;
	private Button bSave;
	private Boolean editMode;
	private CollateralShm shm;
	private OnShmAddedListener listener;

	public interface OnShmAddedListener {
		void onShmAdded(CollateralShm shm);

		void onShmEdited();
	}

	public AddGuaranteShmDialog(CollateralShm currentShm) {
		if (currentShm == null) {
			editMode = false;
		} else {
			editMode = true;
			shm = currentShm;
		}
	}

	public void setOnShmAddedListener(OnShmAddedListener list) {
		this.listener = list;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah jaminan");
		View v = inflater.inflate(R.layout.dialog_shm, container, false);

		etShmName = (EditText) v.findViewById(R.id.shm_name);
		etShmNumber = (EditText) v.findViewById(R.id.shm_number);
		etSize = (EditText) v.findViewById(R.id.shm_size);
		etInfo = (EditText) v.findViewById(R.id.shm_info);
		etPriceMarket = (MoneyEditText) v.findViewById(R.id.shm_price_market);
		etPriceAssesed = (MoneyEditText) v
				.findViewById(R.id.shm_price_assessed);

		spnShmType = (Spinner) v.findViewById(R.id.shm_type);
		spnBinding = (Spinner) v.findViewById(R.id.shm_binding);
		spnStatus = (Spinner) v.findViewById(R.id.type_land);

		bSave = (Button) v.findViewById(R.id.dialog_shm_save);
		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String shmName = etShmName.getText().toString();
				String shmNumber = etShmNumber.getText().toString();
				String size = etSize.getText().toString().isEmpty() ? "0"
						: etSize.getText().toString();
				String info = etInfo.getText().toString();
				// String priceMarket = etPriceMarket.getText().toString()
				// .isEmpty() ? "0" : etPriceMarket.getNominal()
				// .toString();
				// String priceAssesed = etPriceAssesed.getNominal().toString()
				// .isEmpty() ? "0" : etPriceAssesed.getNominal().toString();
				String priceMarket = etPriceMarket.getNominal().toString();
				String priceAssesed = etPriceAssesed.getNominal().toString();
				String binding = spnBinding.getSelectedItem().toString();
				String status = spnStatus.getSelectedItem().toString();
				String type = spnShmType.getSelectedItem().toString();

				if (editMode) {
					shm.setBinding(binding);
					shm.setCertificateName(shmName);
					shm.setCertificateNumber(shmNumber);
					shm.setInfo(info);
					shm.setLandOfArea(Double.parseDouble(size));
					shm.setPriceOnAssessed(priceAssesed);
					shm.setPriceOnMarket(priceMarket);
					shm.setStatusOfLand(status);
					shm.setType(type);
					listener.onShmEdited();

				} else {
					int randomId = RandomInteger.randInt(1, 1000);
					CollateralShm shm = new CollateralShm.Builder()
							.binding(binding).id(randomId)
							.certificateName(shmName)
							.certificateNumber(shmNumber).info(info)
							.loadOfArea(size).priceOnAssessed(priceAssesed)
							.priceOnMarket(priceMarket).statusOfLand(status)
							.type(type).build();
					listener.onShmAdded(shm);
				}
				getDialog().dismiss();

			}
		});
		spnShmType.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				String itemSelected = arg0.getAdapter().getItem(arg2)
						.toString();
				if (!itemSelected.equalsIgnoreCase("hgb")) {
					spnStatus.setEnabled(true);
				} else {
					spnStatus.setEnabled(false);
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});
		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (!editMode)
			return;

		etInfo.setText(shm.getInfo());
		etPriceAssesed.setText(shm.getPriceOnAssessed());
		etPriceMarket.setText(shm.getPriceOnMarket());
		etShmName.setText(shm.getCertificateName());
		etShmNumber.setText(shm.getCertificateNumber());
		etSize.setText(shm.getLandOfArea() + "");

		spnStatus.setSelection(getStatusShmIndex(shm.getStatusOfLand()));
		spnBinding.setSelection(getBindingShmIndex(shm.getBinding()));
		spnShmType.setSelection(getTypeIndex(shm.getType()));

	}

	private int getStatusShmIndex(String input) {
		String[] bindings = getResources().getStringArray(R.array.land_type);
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i].equalsIgnoreCase(input)) {
				return i;
			}
		}
		return 0;
	}

	private int getTypeIndex(String input) {
		String[] bindings = getResources().getStringArray(R.array.shm_type);
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i].equalsIgnoreCase(input)) {
				return i;
			}
		}
		return 0;
	}

	private int getBindingShmIndex(String input) {
		String[] bindings = getResources().getStringArray(R.array.binding_shm);
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i].equalsIgnoreCase(input)) {
				return i;
			}
		}
		return 0;
	}
}

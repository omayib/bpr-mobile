package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.utils.MoneyEditText;
import com.bpr.utils.RandomInteger;
import com.customer.model.CollateralDeposito;

public class DepositoDialog extends DialogFragment {
	private EditText etName, etAccountNumber, etBilyetNumber;
	private MoneyEditText etAmmount;
	private Button bSave;
	private OnDepositoAddedListener listener;
	private boolean MODE_EDIT;
	private CollateralDeposito deposito;

	public interface OnDepositoAddedListener {
		void onDepositoAdded(CollateralDeposito p);

		void onDepositoEdited();
	}

	public void setOnDepositoAddedListener(OnDepositoAddedListener p) {
		this.listener = p;
	}

	public void setDeposito(CollateralDeposito dp) {
		this.deposito = dp;
		this.MODE_EDIT = true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah deposito");
		View v = inflater.inflate(R.layout.dialog_deposito, container, false);
		etName = (EditText) v.findViewById(R.id.deposito_name_edittext);
		etAccountNumber = (EditText) v
				.findViewById(R.id.deposito_account_number_edittext);
		etAmmount = (MoneyEditText) v
				.findViewById(R.id.deposito_amount_edittext);
		etBilyetNumber = (EditText) v
				.findViewById(R.id.deposito_bilyet_number_edittext);
		bSave = (Button) v.findViewById(R.id.dialog_deposito_save);

		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String name = etName.getText().toString();
				String rekening = etAccountNumber.getText().toString();
				String ammount = etAmmount.getNominal().toString();
				String bilyetNumber = etBilyetNumber.getText().toString();
				validateForm();
				if (name.isEmpty()) {
					Toast.makeText(getActivity(), "tujuan umum wajib diisi",
							Toast.LENGTH_SHORT).show();
					return;
				}
				int idDepostio = -1;
				if (MODE_EDIT) {
					deposito.setAccountNumber(rekening);
					deposito.setAmount(ammount);
					deposito.setBilyetNumber(bilyetNumber);
					deposito.setName(name);
					listener.onDepositoEdited();
				} else {
					idDepostio = RandomInteger.randInt(1, 1000);
					CollateralDeposito deposito = new CollateralDeposito.Builder()
							.accountNumber(rekening).amount(ammount)
							.bilyetNumber(bilyetNumber).id(idDepostio)
							.name(name).build();
					listener.onDepositoAdded(deposito);
				}
				getDialog().dismiss();
			}
		});
		return v;
	}

	protected void validateForm() {
		if (etName.getText().toString().isEmpty())
			Toast.makeText(getActivity(), "nama wajib diisi",
					Toast.LENGTH_SHORT).show();
		if (etAccountNumber.getText().toString().isEmpty())
			Toast.makeText(getActivity(), "nomer rekening wajib diisi",
					Toast.LENGTH_SHORT).show();
		if (etAmmount.getText().toString().isEmpty())
			Toast.makeText(getActivity(), "jumlah wajib diisi",
					Toast.LENGTH_SHORT).show();
		if (etBilyetNumber.getText().toString().isEmpty())
			Toast.makeText(getActivity(), "nomer bilyet wajib diisi",
					Toast.LENGTH_SHORT).show();
		return;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (!MODE_EDIT)
			return;

		etName.setText(deposito.getName());
		etAccountNumber.setText(deposito.getAccountNumber());
		etAmmount.setText(deposito.getAmount().toString());
		etBilyetNumber.setText(deposito.getBilyetNumber());

	}
}

package com.bpr.dialog;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SeekBar;

import com.bpr.R;
import com.bpr.adapter.ListUserAdapter;
import com.bpr.event.manager.EventPublisher;
import com.bpr.integration.controller.BoxController;
import com.newbox.event.AssignRequested;
import com.newbox.event.ReassignRequested;
import com.user.modul.User;

public class AssigntoDialog extends DialogFragment {
	private ListView listview;
	private ListUserAdapter userAdapter;
	private List<User> listUser = new ArrayList<User>();
	private String token;
	private EventPublisher eventPublisher;
	private int idCustomer;
	private EditText etNote;
	private Button bOk;
	private User selectedUser;
	private boolean isAssignMode;

	public AssigntoDialog(List<User> user, EventPublisher eventPublisher,
			String token, int idCustomer, boolean isAssignMode) {
		System.out.println("assign created cunstructor");
		this.listUser.addAll(user);
		this.token = token;
		this.eventPublisher = eventPublisher;
		this.idCustomer = idCustomer;
		this.isAssignMode = isAssignMode;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		System.out.println("assign on CREATED");
		userAdapter = new ListUserAdapter(getActivity(),
				android.R.layout.simple_list_item_single_choice,
				android.R.id.text1, listUser);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("ASSIGN TO");

		View rootView = inflater.inflate(R.layout.dialog_assignto, container,
				false);
		bOk = (Button) rootView.findViewById(R.id.assignto_btn);
		etNote = (EditText) rootView.findViewById(R.id.assignto_notes);
		listview = (ListView) rootView.findViewById(R.id.assignto_list_ao);
		listview.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		listview.setAdapter(userAdapter);

		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapter, View arg1,
					int arg2, long arg3) {
				selectedUser = (User) adapter.getItemAtPosition(arg2);
			}
		});
		bOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (isAssignMode) {
					eventPublisher.publish(new AssignRequested(token,
							idCustomer, selectedUser.getId(), etNote.getText()
									.toString()));
				} else {
					eventPublisher.publish(new ReassignRequested(token,
							idCustomer, selectedUser.getId(), etNote.getText()
									.toString()));

				}
				getDialog().dismiss();
			}
		});

		return rootView;
	}
}

package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bpr.R;
import com.bpr.application.ConfigApplication;
import com.bpr.utils.MoneyEditText;
import com.customer.model.CollateralShm;

public class DetailCollateralShmDialog extends DialogFragment {
	private CollateralShm shm;
	private EditText etNumberCertificate, etName, etSize, etInfo;
	private MoneyEditText etPriceLowest, etPriceAssessed;
	private Spinner spnStatus, spnBinding;
	private ConfigApplication conf;
	private boolean isReadOnly;
	private Button bSave;
	private OnShmUpdatedListener updatedListener;

	public interface OnShmUpdatedListener {
		void onShmUpdated();
	}

	public void setOnShmUpdatedListener(OnShmUpdatedListener o) {
		this.updatedListener = o;
	}

	public DetailCollateralShmDialog(CollateralShm currentShm) {
		this.shm = currentShm;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		isReadOnly = conf.isReadOnly();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah jaminan");
		View v = inflater.inflate(R.layout.dialog_shm, container, false);

		etNumberCertificate = (EditText) v.findViewById(R.id.shm_number);
		etName = (EditText) v.findViewById(R.id.shm_name);
		etSize = (EditText) v.findViewById(R.id.shm_size);
		etPriceLowest = (MoneyEditText) v.findViewById(R.id.shm_price_market);
		etInfo = (EditText) v.findViewById(R.id.shm_info);
		etPriceAssessed = (MoneyEditText) v
				.findViewById(R.id.shm_price_assessed);
		spnBinding = (Spinner) v.findViewById(R.id.shm_binding);
		spnStatus = (Spinner) v.findViewById(R.id.type_land);
		bSave = (Button) v.findViewById(R.id.dialog_shm_save);
		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String shmName = etName.getText().toString();
				String shmNumber = etNumberCertificate.getText().toString();
				String size = etSize.getText().toString().isEmpty() ? "0"
						: etSize.getText().toString();
				String info = etInfo.getText().toString();
				String priceMarket = etPriceLowest.getNominal().toString();
				String priceAssesed = etPriceAssessed.getNominal().toString();
				String binding = spnBinding.getSelectedItem().toString();
				String status = spnStatus.getSelectedItem().toString();

				shm.setBinding(binding);
				shm.setCertificateName(shmName);
				shm.setCertificateNumber(shmNumber);
				shm.setInfo(info);
				shm.setLandOfArea(Double.parseDouble(size));
				shm.setPriceOnAssessed(priceAssesed);
				shm.setPriceOnMarket(priceMarket);
				shm.setStatusOfLand(status);

				updatedListener.onShmUpdated();
				getDialog().dismiss();

			}
		});
		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		etNumberCertificate.setText(shm.getCertificateNumber());
		etName.setText(shm.getCertificateName());
		etSize.setText(shm.getLandOfArea() + "");
		etPriceLowest.setText(shm.getPriceOnMarket());
		etInfo.setText(shm.getInfo());
		etPriceAssessed.setText(shm.getPriceOnAssessed());
		spnStatus.setSelection(getStatusShmIndex(shm.getStatusOfLand()));
		spnBinding.setSelection(getBindingShmIndex(shm.getBinding()));

		etNumberCertificate.setEnabled(!isReadOnly);
		etName.setEnabled(!isReadOnly);
		etSize.setEnabled(!isReadOnly);
		etPriceLowest.setEnabled(!isReadOnly);
		etInfo.setEnabled(!isReadOnly);
		etPriceAssessed.setEnabled(!isReadOnly);
		spnStatus.setEnabled(!isReadOnly);
		spnBinding.setEnabled(!isReadOnly);
	}

	private int getStatusShmIndex(String input) {
		String[] bindings = getResources().getStringArray(R.array.land_type);
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i].equalsIgnoreCase(input)) {
				return i;
			}
		}
		return 0;
	}

	private int getBindingShmIndex(String input) {
		String[] bindings = getResources().getStringArray(R.array.binding_shm);
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i].equalsIgnoreCase(input)) {
				return i;
			}
		}
		return 0;
	}

}

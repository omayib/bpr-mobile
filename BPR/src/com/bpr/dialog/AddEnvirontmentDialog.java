package com.bpr.dialog;

import java.util.Arrays;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bpr.R;
import com.bpr.utils.RandomInteger;
import com.customer.model.EnvironmentalCheck;

public class AddEnvirontmentDialog extends DialogFragment {

	private EditText etName, etRelation;
	private Spinner spnImpression;
	private Button bSave;
	private OnEnvCheckAddedListener listener;
	private boolean editMode = false;
	private EnvironmentalCheck selected;

	public interface OnEnvCheckAddedListener {
		void onEnvAdded(EnvironmentalCheck check);

		void onEnvEdited();
	}

	public AddEnvirontmentDialog(EnvironmentalCheck selected) {
		if (selected != null) {
			this.selected = selected;
			editMode = true;
		} else {
			editMode = false;
		}
	}

	public void setOnEnvCheckAddedListener(OnEnvCheckAddedListener l) {
		this.listener = l;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah cek lingkungan");
		View v = inflater
				.inflate(R.layout.dialog_environment, container, false);

		etName = (EditText) v.findViewById(R.id.env_form_name);
		spnImpression = (Spinner) v.findViewById(R.id.env_form_impression);
		etRelation = (EditText) v.findViewById(R.id.env_form_relation);
		bSave = (Button) v.findViewById(R.id.env_form_save);
		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String name = etName.getText().toString();
				String impression = spnImpression.getSelectedItem().toString();
				String relation = etRelation.getText().toString();
				if (editMode) {
					selected.setImpression(impression);
					selected.setName(name);
					selected.setRelation(relation);
					listener.onEnvEdited();
				} else {
					int randomId = RandomInteger.randInt(1, 1000);
					EnvironmentalCheck check = new EnvironmentalCheck.Builder()
							.id(randomId).name(name).relation(relation)
							.impression(impression).build();
					listener.onEnvAdded(check);
				}
				getDialog().dismiss();
			}
		});

		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		if (!editMode)
			return;

		etName.setText(selected.getName());
		spnImpression.setSelection(getImpressionIndex(selected.getImpression()));
		etRelation.setText(selected.getRelation());

	}
	private int getImpressionIndex(String input) {
		String localInput = input.toLowerCase();
		System.out.println("local input::" + localInput);
		String[] maritals = getResources().getStringArray(R.array.impression);
		return Arrays.asList(maritals).indexOf(localInput);
	}
}

package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.bpr.R;

public class AddPhotoDialog extends DialogFragment {
	private PhotoDialogListener listener;

	public interface PhotoDialogListener {
		void onSaved(String name);
	}

	public void setOnAddPhotoListener(PhotoDialogListener listener) {
		this.listener = listener;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah foto");
		View rootView = inflater.inflate(R.layout.dialog_photo, container,
				false);
		final EditText editTextCategory = (EditText) rootView
				.findViewById(R.id.dialog_photo_category);
		Button bSave = (Button) rootView
				.findViewById(R.id.dialog_photo_button_simpan);

		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String name = editTextCategory.getText().toString();
				if (name.isEmpty()) {
					getDialog().dismiss();
					return;
				}
				listener.onSaved(name);
				getDialog().dismiss();
			}
		});
		return rootView;
	}

}

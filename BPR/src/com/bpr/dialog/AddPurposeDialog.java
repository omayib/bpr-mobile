package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.utils.RandomInteger;
import com.customer.model.Purposes;

public class AddPurposeDialog extends DialogFragment {
	private EditText etGeneral, etDetail;
	private Button bSave;
	private OnPurposeAddedListener listener;

	public interface OnPurposeAddedListener {
		void onPurposeAdded(Purposes p);
	}

	public void setOnPurposeAddedListener(OnPurposeAddedListener p) {
		this.listener = p;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah tujuan");
		View v = inflater.inflate(R.layout.dialog_purpose, container, false);
		etGeneral = (EditText) v.findViewById(R.id.dialog_purpose_general);
		etDetail = (EditText) v.findViewById(R.id.dialog_purpose_detail);
		bSave = (Button) v.findViewById(R.id.dialog_purpose_save);

		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String general = etGeneral.getText().toString();
				String detail = etDetail.getText().toString();
				if (general.isEmpty()) {
					Toast.makeText(getActivity(), "tujuan umum wajib diisi",
							Toast.LENGTH_SHORT).show();
					return;
				}
				int random = RandomInteger.randInt(1, 1000);
				Purposes purpose = new Purposes.Builder().purpose(random,
						general, detail).build();
				listener.onPurposeAdded(purpose);
				getDialog().dismiss();
			}
		});
		return v;
	}

}

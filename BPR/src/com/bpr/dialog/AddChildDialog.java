package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.utils.RandomInteger;
import com.customer.model.Children;

public class AddChildDialog extends DialogFragment {
	private EditText etName, etAge;
	private Button bSave;
	private AddChildDialogListener dialogListener;

	public interface AddChildDialogListener {
		void onChildSaved(Children child);
	}

	public void setOnAddChildDialogListener(AddChildDialogListener d) {
		this.dialogListener = d;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah Anak");
		View rootView = inflater.inflate(R.layout.dialog_add_child, container,
				false);

		etName = (EditText) rootView.findViewById(R.id.et_child_name);
		etAge = (EditText) rootView.findViewById(R.id.et_child_age);
		bSave = (Button) rootView.findViewById(R.id.btn_child_save);
		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String name = etName.getText().toString();
				String value = etAge.getText().toString();
				if (!name.isEmpty() && !value.isEmpty()) {
					int age = Integer.parseInt(value);
					int radomId = RandomInteger.randInt(1, 100);
					Children c = new Children.Builder().age(age).name(name)
							.id(radomId).build();
					dialogListener.onChildSaved(c);
				} else {
					Toast.makeText(getActivity(), "wajib diisi",
							Toast.LENGTH_SHORT).show();
				}
				getDialog().dismiss();
			}
		});
		return rootView;
	}
}

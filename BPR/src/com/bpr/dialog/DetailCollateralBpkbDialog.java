package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bpr.R;
import com.bpr.application.ConfigApplication;
import com.bpr.utils.MoneyEditText;
import com.customer.model.CollateralBpkb;

public class DetailCollateralBpkbDialog extends DialogFragment {
	private CollateralBpkb bpkb;
	private EditText etBpkbNumber, etBpkbName, etCarName, etPoliceNumber,
			etCarBrand, etCarType, etCarYear, etInfo, etStnkNumber;
	private MoneyEditText etPriceMarket, etPriceAssesed;
	private Spinner spnBinding;
	private ConfigApplication conf;
	private Button bSave;
	private boolean isReadonly;
	private OnDetailBpkbUpdated listener;

	public interface OnDetailBpkbUpdated {
		void onDetailBpkbUpdated();
	}

	public void setOnDetailBpkbUpdated(OnDetailBpkbUpdated l) {
		this.listener = l;
	}

	public DetailCollateralBpkbDialog(CollateralBpkb currentBpkb) {
		this.bpkb = currentBpkb;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		isReadonly = conf.isReadOnly();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah jaminan");
		View v = inflater.inflate(R.layout.dialog_bpkb, container, false);

		etBpkbNumber = (EditText) v.findViewById(R.id.bpkb_number);
		etBpkbName = (EditText) v.findViewById(R.id.bpkb_name);
		etStnkNumber = (EditText) v.findViewById(R.id.bpkb_number_stnk);
		etCarName = (EditText) v.findViewById(R.id.bpkb_car_name);
		etPoliceNumber = (EditText) v.findViewById(R.id.bpkb_police_number);
		etCarBrand = (EditText) v.findViewById(R.id.bpkb_brand);
		etCarYear = (EditText) v.findViewById(R.id.bpkb_year);
		etCarType = (EditText) v.findViewById(R.id.bpkb_type);
		etPriceMarket = (MoneyEditText) v.findViewById(R.id.bpkb_price);
		etPriceAssesed = (MoneyEditText) v
				.findViewById(R.id.bpkb_price_taksasi);
		etInfo = (EditText) v.findViewById(R.id.bpkb_info);
		spnBinding = (Spinner) v.findViewById(R.id.bpkb_binding);
		bSave = (Button) v.findViewById(R.id.dialog_bpkb_save);
		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String bpkbNumber = etBpkbNumber.getText().toString();
				String bpkbName = etBpkbName.getText().toString();
				String stnkNumber = etStnkNumber.getText().toString();
				String carName = etCarName.getText().toString();
				String policeNumber = etPoliceNumber.getText().toString();
				String carBrand = etCarBrand.getText().toString();
				String carYear = etCarYear.getText().toString();
				String carType = etCarType.getText().toString();
				String priceMarket = etPriceMarket.getNominal().toString();
				String priceAssesed = etPriceAssesed.getNominal().toString();
				String info = etInfo.getText().toString();
				String binding = spnBinding.getSelectedItem().toString();

				bpkb.setBinding(binding);
				bpkb.setCarBrand(carBrand);
				bpkb.setNameOfBpkb(bpkbName);
				bpkb.setCarName(carName);
				bpkb.setNumberBpkb(bpkbNumber);
				bpkb.setNumberStnk(stnkNumber);
				bpkb.setPoliceNumber(policeNumber);
				bpkb.setPriceOnAssessed(priceAssesed);
				bpkb.setPriceOnMarket(priceMarket);
				bpkb.setCarType(carType);
				bpkb.setCarYear(carYear);
				bpkb.setInfo(info);

				listener.onDetailBpkbUpdated();
				getDialog().dismiss();
			}
		});
		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		etBpkbName.setText(bpkb.getNameOfBpkb());
		etBpkbNumber.setText(bpkb.getNumberBpkb());
		etCarName.setText(bpkb.getCarName());
		etPoliceNumber.setText(bpkb.getPoliceNumber());
		etCarBrand.setText(bpkb.getCarBrand());
		etCarType.setText(bpkb.getCarType());
		etCarYear.setText(bpkb.getCarYear());
		etPriceMarket.setText(bpkb.getPriceOnMarket());
		etPriceAssesed.setText(bpkb.getPriceOnAssessed());
		etInfo.setText(bpkb.getInfo());
		etStnkNumber.setText(bpkb.getNumberStnk());
		spnBinding.setSelection(getBindingIndex(bpkb.getBinding()));

		etBpkbName.setEnabled(!isReadonly);
		etBpkbNumber.setEnabled(!isReadonly);
		etCarName.setEnabled(!isReadonly);
		etPoliceNumber.setEnabled(!isReadonly);
		etCarBrand.setEnabled(!isReadonly);
		etCarType.setEnabled(!isReadonly);
		etCarYear.setEnabled(!isReadonly);
		etPriceMarket.setEnabled(!isReadonly);
		etPriceAssesed.setEnabled(!isReadonly);
		etInfo.setEnabled(!isReadonly);
		spnBinding.setEnabled(!isReadonly);
	}

	private int getBindingIndex(String input) {
		String[] bindings = getResources().getStringArray(R.array.binding_bpkb);
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i].equalsIgnoreCase(input)) {
				return i;
			}
		}
		return 0;
	}
}

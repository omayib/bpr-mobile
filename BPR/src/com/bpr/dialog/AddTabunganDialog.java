package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.utils.MoneyEditText;
import com.bpr.utils.RandomInteger;
import com.customer.model.CollateralTabungan;

public class AddTabunganDialog extends DialogFragment {
	private EditText etName, etAccountNumber, etBookNumber;
	private MoneyEditText etAmmount;
	private Button bSave;
	private OnTabunganAddedListener listener;
	private CollateralTabungan tabungan;
	private boolean MODE_EDIT;

	public interface OnTabunganAddedListener {
		void onTabunganAdded(CollateralTabungan p);

		void onTabunganEdited();
	}

	public void setOnTabunganAddedListener(OnTabunganAddedListener p) {
		this.listener = p;
	}

	public void setTabungan(CollateralTabungan tab) {
		this.tabungan = tab;
		MODE_EDIT = true;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah tabungan");
		View v = inflater.inflate(R.layout.dialog_tabungan, container, false);
		etName = (EditText) v.findViewById(R.id.tabungan_name_edittext);
		etAccountNumber = (EditText) v
				.findViewById(R.id.tabungan_account_number_edittext);
		etAmmount = (MoneyEditText) v
				.findViewById(R.id.tabungan_amount_edittext);
		etBookNumber = (EditText) v
				.findViewById(R.id.tabungan_book_number_edittext);
		bSave = (Button) v.findViewById(R.id.dialog_tabungan_save);

		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String name = etName.getText().toString();
				String rekening = etAccountNumber.getText().toString();
				String ammount = etAmmount.getNominal().toString();
				String bookNumber = etBookNumber.getText().toString();
				validateForm();
				if (MODE_EDIT) {
					tabungan.setAccountNumber(rekening);
					tabungan.setAmount(ammount);
					tabungan.setBookNumber(bookNumber);
					tabungan.setName(name);
					listener.onTabunganEdited();
				} else {
					int random = RandomInteger.randInt(1, 1000);
					CollateralTabungan tabungan = new CollateralTabungan.Builder()
							.name(name).accountNumber(rekening)
							.ammount(ammount).bookNumber(bookNumber).id(random)
							.build();
					listener.onTabunganAdded(tabungan);
				}
				getDialog().dismiss();
			}
		});
		return v;
	}

	protected void validateForm() {
		if (etName.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "tujuan umum wajib diisi",
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (etAccountNumber.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "nomor rekening wajib diisi",
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (etAmmount.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "jumlah wajib diisi",
					Toast.LENGTH_SHORT).show();
			return;
		}
		if (etBookNumber.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "nomor buku wajib diisi",
					Toast.LENGTH_SHORT).show();
			return;
		}
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (!MODE_EDIT) {
			return;
		}
		etName.setText(tabungan.getName());
		etAccountNumber.setText(tabungan.getAccountNumber());
		etAmmount.setText(tabungan.getAmount().toString());
		etBookNumber.setText(tabungan.getBookNumber());
	}

}

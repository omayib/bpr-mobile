package com.bpr.dialog;

import qisc.us.signature.Signature;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import com.bpr.R;

public class CopyOfApprovalDialog extends DialogFragment {
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.dialog_signature, container,
				false);
		LinearLayout v = (LinearLayout) rootView
				.findViewById(R.id.container_signature);
		Signature signature = new Signature(getActivity(), null);
		signature.setBackgroundColor(Color.WHITE);
		v.addView(signature);
		getDialog().setTitle("Pengajuan disetujui");

		Button b = (Button) rootView.findViewById(R.id.bApprove);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				getDialog().dismiss();
			}
		});
		return rootView;
	}
}
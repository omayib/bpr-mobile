package com.bpr.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bpr.R;
import com.bpr.utils.MoneyEditText;
import com.bpr.utils.RandomInteger;
import com.customer.model.CollateralBpkb;

public class AddGuaranteBpkbDialog extends DialogFragment {
	private EditText etBpkbNumber, etBpkbName, etStnkNumber, etCarName,
			etPoliceNumber, etCarBrand, etCarYear, etCarType, etInfo;
	private MoneyEditText etPriceMarket, etPriceAssesed;
	private Spinner spnBinding;
	private OnBpkbAddedListener listener;
	private Button bSave;

	public interface OnBpkbAddedListener {
		void onBpkbAdded(CollateralBpkb bpkb);
	}

	public void setOnBpkbAddedListener(OnBpkbAddedListener l) {
		this.listener = l;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah jaminan");
		View v = inflater.inflate(R.layout.dialog_bpkb, container, false);

		etBpkbNumber = (EditText) v.findViewById(R.id.bpkb_number);
		etBpkbName = (EditText) v.findViewById(R.id.bpkb_name);
		etStnkNumber = (EditText) v.findViewById(R.id.bpkb_number_stnk);
		etCarName = (EditText) v.findViewById(R.id.bpkb_car_name);
		etPoliceNumber = (EditText) v.findViewById(R.id.bpkb_police_number);
		etCarBrand = (EditText) v.findViewById(R.id.bpkb_brand);
		etCarYear = (EditText) v.findViewById(R.id.bpkb_year);
		etCarType = (EditText) v.findViewById(R.id.bpkb_type);
		etPriceMarket = (MoneyEditText) v.findViewById(R.id.bpkb_price);
		etPriceAssesed = (MoneyEditText) v
				.findViewById(R.id.bpkb_price_taksasi);
		etInfo = (EditText) v.findViewById(R.id.bpkb_info);
		spnBinding = (Spinner) v.findViewById(R.id.bpkb_binding);
		bSave = (Button) v.findViewById(R.id.dialog_bpkb_save);
		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String bpkbNumber = etBpkbNumber.getText().toString();
				String bpkbName = etBpkbName.getText().toString();
				String stnkNumber = etStnkNumber.getText().toString();
				String carName = etCarName.getText().toString();
				String policeNumber = etPoliceNumber.getText().toString();
				String carBrand = etCarBrand.getText().toString();
				String carYear = etCarYear.getText().toString();
				String carType = etCarType.getText().toString();
				String priceMarket = etPriceMarket.getNominal().toString();
				String priceAssesed = etPriceAssesed.getNominal().toString();
				String info = etInfo.getText().toString();
				String binding = spnBinding.getSelectedItem().toString();
				int randomId = RandomInteger.randInt(1, 1000);
				CollateralBpkb bpkb = new CollateralBpkb.Builder().id(randomId)
						.binding(binding).carBrand(carBrand).info(info)
						.nameOfBpkb(bpkbName).carName(carName)
						.numberBpkb(bpkbNumber).numberStnk(stnkNumber)
						.policeNumber(policeNumber)
						.priceOnAssessed(priceAssesed)
						.priceOnMarket(priceMarket).carType(carType)
						.carYear(carYear).build();

				listener.onBpkbAdded(bpkb);
				getDialog().dismiss();
			}
		});
		return v;
	}

}

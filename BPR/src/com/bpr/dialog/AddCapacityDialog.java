package com.bpr.dialog;

import java.math.BigDecimal;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.utils.MoneyEditText;
import com.bpr.utils.RandomInteger;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;

public class AddCapacityDialog extends DialogFragment {
	private EditText etName;
	private MoneyEditText etValue;
	private Button bSave;
	private OnAddCapacityListener listener;
	private CapacityIncome income;
	private CapacityExpenditure expenditure;

	public interface OnAddCapacityListener {
		void onIncomeAdded(CapacityIncome income);

		void onExpenditureAdded(CapacityExpenditure expn);
	}

	public void setIncome(CapacityIncome income) {
		this.income = income;
	}

	public void setExpenditure(CapacityExpenditure expenditure) {
		this.expenditure = expenditure;
	}

	public void setOnAddCapacityListener(OnAddCapacityListener l) {
		this.listener = l;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah " + getTag());
		View v = inflater.inflate(R.layout.dialog_capacity, container, false);
		etName = (EditText) v.findViewById(R.id.dialog_capacity_name);
		etValue = (MoneyEditText) v.findViewById(R.id.dialog_capacity_value);
		bSave = (Button) v.findViewById(R.id.dialog_capacity_save);

		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String name = etName.getText().toString();
				String value = etValue.getNominal().toString();
				int randomId = RandomInteger.randInt(0, 1000);
				if (name.isEmpty() || value.isEmpty()) {
					Toast.makeText(getActivity(), "wajib diisi",
							Toast.LENGTH_SHORT).show();
					return;
				}

				if (income != null) {
					income.setName(name);
					income.setValue(new BigDecimal(value));
					getDialog().dismiss();
					listener.onIncomeAdded(income);
					return;
				}
				if (expenditure != null) {
					expenditure.setName(name);
					expenditure.setValue(new BigDecimal(value));
					listener.onExpenditureAdded(expenditure);
					getDialog().dismiss();
					return;
				}

				if (getTag().equalsIgnoreCase("pemasukan")) {
					CapacityIncome inc = new CapacityIncome.Builder().incomes(
							randomId, name, value).build();
					listener.onIncomeAdded(inc);
				} else {
					CapacityExpenditure exp = new CapacityExpenditure.Builder()
							.expenditures(randomId, name, value).build();
					listener.onExpenditureAdded(exp);
				}
				getDialog().dismiss();
			}
		});

		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (income != null) {
			etName.setText(income.getName());
			etValue.setText(income.getValue().toString());
		}
		if (expenditure != null) {
			etName.setText(expenditure.getName());
			etValue.setText(expenditure.getValue().toPlainString());
		}
	}
}

package com.bpr.dialog;

import java.math.BigDecimal;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.utils.MoneyEditText;
import com.bpr.utils.RandomInteger;
import com.customer.model.Assets;

public class AddAssetDialog extends DialogFragment {
	private EditText etName;
	private MoneyEditText etValue;
	private Button bSave;
	private AddAssetDialogListener dialogListener;
	private Assets asset;

	public interface AddAssetDialogListener {
		void onAssetDialogSaved(Assets asset);
	}

	public AddAssetDialog(Assets asset) {
		this.asset = asset;
	}

	public void setOnAddAssetDialogListener(AddAssetDialogListener d) {
		this.dialogListener = d;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah aset");
		View rootView = inflater.inflate(R.layout.dialog_asset, container,
				false);

		etName = (EditText) rootView.findViewById(R.id.dialog_asset_name);
		etValue = (MoneyEditText) rootView
				.findViewById(R.id.dialog_asset_value);
		bSave = (Button) rootView.findViewById(R.id.dialog_asset_save);
		bSave.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String name = etName.getText().toString();
				String value = etValue.getNominal().toString();
				if (name.isEmpty() && value.isEmpty()) {
					Toast.makeText(getActivity(), "wajib diisi",
							Toast.LENGTH_SHORT).show();
					getDialog().dismiss();
					return;
				}
				if (asset == null) {
					int randomId = RandomInteger.randInt(1, 1000);
					Assets assetCreated = new Assets.Builder().asset(randomId, name,
							value).build();
					dialogListener.onAssetDialogSaved(assetCreated);
				} else {
					asset.setName(name);
					asset.setValue(new BigDecimal(value));
					System.out.println("asset edited "+asset.toString());
					dialogListener.onAssetDialogSaved(asset);
				}

				getDialog().dismiss();
			}
		});
		return rootView;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (asset == null)
			return;
		etName.setText(asset.getName());
		etValue.setText(asset.getValue().toString());
	}
}

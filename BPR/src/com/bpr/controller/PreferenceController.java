package com.bpr.controller;

import android.content.SharedPreferences;
import android.util.Log;

import com.user.modul.User;

public class PreferenceController {
	private SharedPreferences preference;
	private SharedPreferences.Editor editor;

	public PreferenceController(SharedPreferences preference) {
		super();
		this.preference = preference;
		editor = this.preference.edit();
	}

	public int getCurrentProposal() {
		return preference.getInt("proposalId", -1);
	}

	public void setCurrentProposal(int id) {
		editor.putInt("proposalId", id).commit();
	}

	public void resetCurrentProposal() {
		editor.remove("proposalId").commit();
	}

	public void clear() {
		Log.d("omayib", "cleaaarrr");
		editor.clear().commit();
	}

	public void setReadOnly(boolean b) {
		System.out.println("set readonly to " + b);
		editor.putBoolean("read_only", b).commit();
	}

	public boolean isReadOnly() {
		return preference.getBoolean("read_only", false);
	}

	public void setUser(User user, String token, String boxname) {
		editor.putString("user_name", user.getName()).commit();
		editor.putInt("user_id", user.getId()).commit();
		editor.putString("user_level", user.getLevel().name().replace("_", " "))
				.commit();
		editor.putString("user_token", token).commit();
		editor.putString("user_box", boxname).commit();
	}

	public String getToken() {
		return preference.getString("user_token", "");
	}

	public boolean hasToken() {
		return preference.contains("user_token");
	}

	public User getUser() {
		User u = new User.Builder().id(preference.getInt("user_id", -1))
				.level(preference.getString("user_level", ""))
				.name(preference.getString("user_name", "")).build();
		if (u.getId() == -1 && u.getLevel().toString().isEmpty()
				&& u.getName().isEmpty())
			return null;
		return u;
	}

	public String getBox() {
		return preference.getString("user_box", "");
	}

	public void setCurrentCustomerDataId(int customerId) {
		editor.putInt("customer_data_id", customerId).commit();
	}

	public void setCurrentCustomerId_(int customerId) {
		editor.putInt("customer_id", customerId).commit();
	}
	public int getCurrentCustomerId_() {
		return preference.getInt("customer_id", -1);
	}
	public int getCurrentCustomerDataId() {
		return preference.getInt("customer_data_id", -1);
	}

	public void resetCurrentCustomer() {
		editor.remove("customerId").commit();
	}

	public void setUnreadBox(String boxName, int unread) {
		editor.putInt(boxName, unread).commit();
	}

	public int getUnreadBox(String boxName) {
		return preference.getInt(boxName, 0);
	}
}

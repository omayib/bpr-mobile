package com.bpr.application;

import java.util.ArrayList;

import android.app.Application;
import android.content.Intent;

import com.activeandroid.ActiveAndroid;
import com.bpr.controller.PreferenceController;
import com.bpr.dashboard.BoxModel;
import com.bpr.dashboard.DashboardFactory;
import com.bpr.event.manager.DefaultEventPublisher;
import com.bpr.event.manager.EventPublisher;
import com.bpr.integration.controller.BoxController;
import com.bpr.integration.controller.PhotoController;
import com.bpr.integration.controller.StorageController;
import com.bpr.integration.controller.UserController;
import com.bpr.integration.parser.BprParser;
import com.bpr.integration.service.StorageSynchronizer;
import com.bpr.notification.manager.BprNotificationManager;
import com.bpr.repository.LocalRepository;
import com.bpr.repository.Repository;
import com.bpr.service.BprService;
import com.bpr.service.PhotoServices;
import com.bpr.service.PullRequestWorker;
import com.customer.model.Summary;
import com.splunk.mint.Mint;

public class ConfigApplication extends Application {
	public static enum BOX_SELECTED {
		NEW, PROPOSAL, FOLLOWUP, ALL, ACK
	}

	private UserController userController;
	private BoxController boxController;
	private PhotoController photoController;

	private BprNotificationManager notifManager;

	public static BOX_SELECTED currentBoxSelected;
	private PreferenceController pref;
	private EventPublisher eventPublisher;
	private BprParser bprParser;
	private ArrayList<BoxModel> boxList = new ArrayList<BoxModel>();
	private StorageSynchronizer storageSynchronizer;
	private Summary summary;
	private PullRequestWorker pullRequestWorker;
	public static boolean INITIALIZED;
	private PhotoServices photoServices;
	private StorageController storageController;
	private Repository bprRepository;

	@Override
	public void onCreate() {
		super.onCreate();

		initWorker();

		prepareBox();

	}

	public void initWorker() {
		System.out.println("init worker called...");

		if (INITIALIZED) {
			System.out.println("already initialized");
			return;
		}
		ActiveAndroid.initialize(this);

		pref = new PreferenceController(getSharedPreferences("bpr_preference",
				0));

		eventPublisher = new DefaultEventPublisher();
		bprParser = new BprParser();

		bprRepository = new LocalRepository();

		userController = new UserController(this, bprParser, eventPublisher,
				bprRepository);
		boxController = new BoxController(bprParser, eventPublisher);
		photoController = new PhotoController(bprParser, eventPublisher);
		notifManager = new BprNotificationManager(this);
		storageController = new StorageController();

		storageSynchronizer = new StorageSynchronizer(eventPublisher,
				userController, bprRepository);
		storageSynchronizer.init();

		pullRequestWorker = new PullRequestWorker(eventPublisher,
				boxController, pref, notifManager);
		pullRequestWorker.init();

		photoServices = new PhotoServices(this, eventPublisher);
		photoServices.init();

		startForegroundService();

		/**
		 * start init bug sense
		 * */
		Mint.initAndStartSession(this, "98a36bf4");

		INITIALIZED = true;
	}

	public void tearDownApp() {
		System.out.println("teardown worker...");
		INITIALIZED = false;
		pullRequestWorker.tearDown();
		storageSynchronizer.tearDown();
		notifManager.clearNotification();
	}

	public void stopForegroundService() {
		Intent intentService = new Intent(this, BprService.class);
		stopService(intentService);
	}

	public void startForegroundService() {
		System.out.println("status BprService.isRunning::"
				+ BprService.isRunning);
		System.out.println("status has token::" + pref.hasToken());
		if (pref.hasToken() && !BprService.isRunning) {
			Intent intentService = new Intent(this, BprService.class);
			startService(intentService);
		}
	}

	public StorageController getStorageController() {
		return storageController;
	}

	public void cloneSummary(Summary s) {
		summary = s;
	}

	public Summary getSummary() {
		return summary;
	}

	public BprNotificationManager getNotifManager() {
		return notifManager;
	}

	public PhotoController getPhotoController() {
		return photoController;
	}

	public BoxController getBoxController() {
		return boxController;
	}

	public void prepareBox() {
		String a = pref.getBox();
		DashboardFactory dashboardFactory = DashboardFactory.newInstance(a);
		boxList = dashboardFactory.getBoxs();
	}

	public ArrayList<BoxModel> getBox() {
		for (BoxModel box : boxList) {
			if (box.getName().equalsIgnoreCase("new")) {
				box.setUnread(getUnreadBox("new_unread"));
			}
			if (box.getName().equalsIgnoreCase("followup")) {
				box.setUnread(getUnreadBox("followup_unread"));
			}
			if (box.getName().equalsIgnoreCase("proposal")) {
				box.setUnread(getUnreadBox("proposal_unread"));
			}
			if (box.getName().equalsIgnoreCase("ack")) {
				box.setUnread(getUnreadBox("ack_unread"));
			}
		}
		return boxList;
	}

	private int getUnreadBox(String boxName) {
		return pref.getUnreadBox(boxName);
	}

	public UserController getUserController() {
		return userController;
	}

	public EventPublisher getEventPublisher() {
		return eventPublisher;
	}

	public PreferenceController getPref() {
		return pref;
	}

	public void setCurrentBox(BOX_SELECTED newSelection) {
		System.out.println("current box : " + newSelection);
		currentBoxSelected = newSelection;
		ruleFormUpdate(newSelection);
	}

	public boolean isReadOnly() {
		return pref.isReadOnly();
	}

	private void ruleFormUpdate(BOX_SELECTED newSelection) {
		switch (newSelection) {
		case PROPOSAL:
			pref.setReadOnly(false);
			break;
		case NEW:
			pref.setReadOnly(true);
			break;
		case FOLLOWUP:
			pref.setReadOnly(true);
			break;
		case ALL:
			pref.setReadOnly(true);
			break;
		case ACK:
			pref.setReadOnly(true);
			break;
		}
	}
	
	public boolean isSummaryEditable(){
		return !(currentBoxSelected==BOX_SELECTED.ACK);
	}

}

package com.bpr.dashboard;

public class BoxModel {
	private int drawable;
	private String name;
	private int unread;

	public BoxModel(int drawable, String name, int unread) {
		super();
		this.drawable = drawable;
		this.name = name;
		this.unread = unread;
	}

	public void setUnread(int unread) {
		System.out.println("set unread " + name + " :: " + unread);
		this.unread = unread;
	}

	public int getDrawable() {
		return drawable;
	}

	public String getName() {
		return name;
	}

	public int getUnread() {
		return unread;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoxModel other = (BoxModel) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return drawable + ":" + name + ":" + unread;
	}
}

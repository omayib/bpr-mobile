package com.bpr.dashboard;

import java.util.ArrayList;
import java.util.List;

import com.bpr.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class DashboardAdapter extends ArrayAdapter<BoxModel> {
	private int inflatedLayout;
	private ArrayList<BoxModel> box;
	private Context context;
	private BoxModel currentBox;
	private Holder holder;
	private LayoutInflater inflater;

	public DashboardAdapter(Context context, int layout, List<BoxModel> objects) {
		super(context, layout, objects);
		this.inflatedLayout = layout;
		this.context = context;
		this.box = (ArrayList<BoxModel>) objects;

	}

	public View getView(int position, android.view.View convertView,
			android.view.ViewGroup parent) {

		currentBox = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(inflatedLayout, parent, false);

			holder.img = (ImageView) convertView.findViewById(R.id.box_pic);
			holder.name = (TextView) convertView.findViewById(R.id.box_name);
			holder.unread = (TextView) convertView
					.findViewById(R.id.box_unread);

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		System.out.println(currentBox);
		holder.img.setImageResource(currentBox.getDrawable());
		holder.name.setText(currentBox.getName());
		holder.unread.setText(currentBox.getUnread() + "");

		return convertView;
	};

	static class Holder {
		ImageView img;
		TextView name;
		TextView unread;
	}
}

package com.bpr.dashboard;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.bpr.R;

public class DashboardFactory {
	private String boxcollection;
	private ArrayList<BoxModel> boxList = new ArrayList<BoxModel>();

	public static DashboardFactory newInstance(String a) {
		return new DashboardFactory(a);
	}

	private DashboardFactory(String a) {
		super();
		this.boxcollection = a;
	}

	public ArrayList<BoxModel> getBoxs() {
		List<String> boxs = new ArrayList<String>();
		try {
			JSONArray ja = new JSONArray(boxcollection);
			for (int i = 0; i < ja.length(); i++) {
				boxs.add(ja.getString(i));
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
		for (String boxname : boxs) {
			if (boxname.equalsIgnoreCase("new")) {
				BoxModel box1 = new BoxModel(R.drawable.ic_new, "NEW", 0);
				boxList.add(box1);
			} else if (boxname.equalsIgnoreCase("followup")) {
				BoxModel box2 = new BoxModel(R.drawable.ic_followup,
						"FOLLOWUP", 0);
				boxList.add(box2);

			} else if (boxname.equalsIgnoreCase("proposal")) {
				BoxModel box3 = new BoxModel(R.drawable.ic_proposal,
						"PROPOSAL", 0);
				boxList.add(box3);

			} else if (boxname.equalsIgnoreCase("all")) {
				BoxModel box4 = new BoxModel(R.drawable.ic_all, "ALL", 0);
				boxList.add(box4);

			} else if (boxname.equalsIgnoreCase("ack")) {
				BoxModel box5 = new BoxModel(R.drawable.ic_all, "ACK", 0);
				boxList.add(box5);
			}
		}
		return boxList;
	}

}

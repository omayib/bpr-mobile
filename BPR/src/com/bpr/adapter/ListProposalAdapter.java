package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.Proposal;
import com.customer.model.ProposalLite;

public class ListProposalAdapter extends ArrayAdapter<ProposalLite> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private ProposalLite currentProposal;

	public ListProposalAdapter(Context context, int layout,
			List<ProposalLite> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentProposal = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.name = (TextView) convertView
					.findViewById(R.id.item_checkedby_name);
			holder.date = (TextView) convertView.findViewById(R.id.item_date);
			holder.viewstub = convertView.findViewById(R.id.viewstub);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.name.setText(currentProposal.getUser().getName());
		holder.date.setText(currentProposal.getCheckedDate().toString());
		if (position % 2 == 0) {
			holder.viewstub.setBackgroundResource(R.color.orange_dark);
			Log.d("view", "darker");
		} else {
			holder.viewstub.setBackgroundResource(R.color.orange);
			Log.d("view", "light");
		}
		return convertView;
	}

	private class Holder {
		TextView name, date;
		View viewstub;
	}
}

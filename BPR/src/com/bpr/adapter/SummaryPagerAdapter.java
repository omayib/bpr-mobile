package com.bpr.adapter;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.bpr.view.fragment.FragmentApprovalStep0a;
import com.bpr.view.fragment.FragmentApprovalStep1;
import com.bpr.view.fragment.FragmentApprovalStep2;
import com.bpr.view.fragment.FragmentApprovalStep3;
import com.bpr.view.fragment.FragmentApprovalStep4;

public class SummaryPagerAdapter extends ArrayPagerAdapter<Fragment> {

	public SummaryPagerAdapter(FragmentManager fragmentManager,
			ArrayList<PageDescriptor> descriptors) {
		super(fragmentManager, descriptors);
	}

	@Override
	protected Fragment createFragment(PageDescriptor desc) {
		if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_SUMMARY_SPLITER0a)) {
			Fragment f = new FragmentApprovalStep0a();
			Bundle bundle = new Bundle();
			bundle.putSerializable("summary", desc.getModel());
			f.setArguments(bundle);
			return f;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_SUMMARY_SPLITER1)) {
			Fragment f = new FragmentApprovalStep1();
			Bundle bundle = new Bundle();
			bundle.putSerializable("summary", desc.getModel());
			f.setArguments(bundle);
			return f;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_SUMMARY_SPLITER2)) {
			Fragment f = new FragmentApprovalStep2();
			Bundle bundle = new Bundle();
			bundle.putSerializable("summary", desc.getModel());
			f.setArguments(bundle);
			return f;

		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_SUMMARY_SPLITER3)) {
			Fragment f = new FragmentApprovalStep3();
			Bundle bundle = new Bundle();
			bundle.putSerializable("summary", desc.getModel());
			f.setArguments(bundle);
			return f;

		} else {
			Fragment f = new FragmentApprovalStep4();
			Bundle bundle = new Bundle();
			bundle.putSerializable("summary", desc.getModel());
			f.setArguments(bundle);
			return f;

		}

	}
}

package com.bpr.adapter;

import java.util.List;

import qisc.us.signature.Signature;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.Recommendation;

public class RecommendationAdapter extends ArrayAdapter<Recommendation> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private Recommendation currentrRecommendation;
	String stringNote = "";
	StringBuilder notesBuilder;

	public RecommendationAdapter(Context context, int layout,
			List<Recommendation> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentrRecommendation = getItem(position);
		System.out.println("recomm:" + currentrRecommendation.toString());
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.note = (TextView) convertView
					.findViewById(R.id.summary_item_step3_note);
			holder.date = (TextView) convertView
					.findViewById(R.id.summary_item_step3_date);
			holder.decision = (CheckBox) convertView
					.findViewById(R.id.summary_item_step3_approval_checkbox);
			holder.signatureCotainer = (LinearLayout) convertView
					.findViewById(R.id.summary_item_step3_container_signature);
			holder.signatureCotainer.setEnabled(false);
			holder.userLevel = (TextView) convertView
					.findViewById(R.id.summary_item_step3_level);

			holder.decision.setEnabled(false);

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		if (!currentrRecommendation.getNotes().isEmpty()) {
			notesBuilder = new StringBuilder();
			for (String notes : currentrRecommendation.getNotes()) {
				notesBuilder.append(notes + "\n");
			}
			stringNote = notesBuilder.toString();
		}
		holder.note.setText(stringNote);
		if (currentrRecommendation.isAccepted()) {
			holder.decision.setText("ya");
			holder.decision.setChecked(true);
		} else {
			holder.decision.setText("tidak");
			holder.decision.setChecked(false);
		}
		holder.date.setText(currentrRecommendation.getDate()==null?"":currentrRecommendation.getDate());
		holder.userLevel.setText(currentrRecommendation.getUser().getName()
				.toString());// before based on level
		if (currentrRecommendation.getSignature() != null) {
			System.out.println("not null");
			Signature signature = new Signature(context, null);
			signature.setBackgroundColor(Color.WHITE);
			signature.injectView(holder.signatureCotainer);
			holder.signatureCotainer.removeView(signature);
			holder.signatureCotainer.addView(signature);
			signature.clear();
			signature.createSignature(currentrRecommendation.getSignature());
			signature.setEnabled(false);
		} else {
			System.out.println(" null");
			Signature signature = new Signature(context, null);
			signature.setBackgroundColor(Color.WHITE);
			signature.injectView(holder.signatureCotainer);
			holder.signatureCotainer.removeView(signature);
			holder.signatureCotainer.addView(signature);
			signature.clear();
			signature.setEnabled(false);
		}
		return convertView;
	}

	private class Holder {
		TextView note, date, userLevel;
		CheckBox decision;
		LinearLayout signatureCotainer;
	}
}

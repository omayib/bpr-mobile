package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.CollateralBpkb;

public class CollateralBpkbAdapter extends ArrayAdapter<CollateralBpkb> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private CollateralBpkb currentBpkb;
	private ArrayAdapter<String> adapterSpinner;
	private boolean isReadOnly;

	public CollateralBpkbAdapter(Context context, int layout,
			List<CollateralBpkb> objects, boolean b) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
		this.isReadOnly = b;

		this.adapterSpinner = new ArrayAdapter<String>(context,
				android.R.layout.simple_list_item_1, context.getResources()
						.getStringArray(R.array.binding_bpkb));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentBpkb = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.priceAssesed = (TextView) convertView
					.findViewById(R.id.guarantee_price_assessed);
			holder.priceOnMarket = (TextView) convertView
					.findViewById(R.id.guarantee_price_onmarket);
			holder.owner = (TextView) convertView
					.findViewById(R.id.guarantee_owner);
			holder.name = (TextView) convertView
					.findViewById(R.id.guarantee_name);
			holder.spinner = (Spinner) convertView
					.findViewById(R.id.guarantee_binding);
			holder.spinner
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> arg0,
								View arg1, int arg2, long arg3) {
							String selected = (String) arg0.getAdapter()
									.getItem(arg2);
							// update model
							currentBpkb.setBinding(selected);
						}

						@Override
						public void onNothingSelected(AdapterView<?> arg0) {
						}
					});

			holder.spinner.setAdapter(adapterSpinner);
			holder.spinner.setEnabled(!isReadOnly);

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.priceAssesed.setText(currentBpkb.getPriceOnAssessedAsRupiah());
		holder.priceOnMarket.setText(currentBpkb.getPriceOnMarketAsRupiah());
		holder.owner.setText(currentBpkb.getNameOfBpkb());
		holder.name.setText(currentBpkb.getCarName());
		holder.spinner.setSelection(getBindingIndex(currentBpkb.getBinding()));
		holder.spinner.setEnabled(!isReadOnly);

		return convertView;
	}

	private int getBindingIndex(String input) {
		String[] bindings = context.getResources().getStringArray(
				R.array.binding_bpkb);
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i].equalsIgnoreCase(input)) {
				return i;
			}
		}
		return 0;
	}

	private class Holder {
		TextView priceAssesed, priceOnMarket, owner, name;
		Spinner spinner;
	}
}

package com.bpr.adapter;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.ViewGroup;

import com.bpr.view.fragment.FragmentBusiness;

public class BusinessPagerAdapter extends ArrayPagerAdapter<FragmentBusiness> {

	public BusinessPagerAdapter(
			FragmentManager fragmentManager,
			ArrayList<PageDescriptor> descriptors,
			com.bpr.adapter.ArrayPagerAdapter.RetentionStrategy retentionStrategy) {
		super(fragmentManager, descriptors, retentionStrategy);
	}

	@Override
	public void destroyItem(ViewGroup container, int position, Object object) {
		System.out.println("DESTROY ITEM");
		super.destroyItem(container, position, object);
		try {
			FragmentManager manager = ((Fragment) object).getFragmentManager();
			FragmentTransaction trans = manager.beginTransaction();
			trans.remove((Fragment) object);
			trans.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected FragmentBusiness createFragment(PageDescriptor desc) {
		Log.d("fragment- adapter", "create fragment" + desc.getFragmentTag());
		FragmentBusiness f = new FragmentBusiness();
		Bundle bundle = new Bundle();
		bundle.putSerializable(FragmentPage.TAG_BUSINESS, desc.getModel());
		f.setArguments(bundle);
		return f;
	}

}
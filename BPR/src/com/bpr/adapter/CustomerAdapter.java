package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.event.BoxData;
import com.customer.model.CustomerData;

public class CustomerAdapter extends ArrayAdapter<BoxData> {
	private BoxData currentCustomer;
	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;

	public CustomerAdapter(Context context, int layout, List<BoxData> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentCustomer = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);

			holder.name = (TextView) convertView.findViewById(R.id.item_name);
			holder.plafon = (TextView) convertView
					.findViewById(R.id.item_plafon);
			holder.status = (TextView) convertView
					.findViewById(R.id.item_status);
			holder.viewstub = convertView.findViewById(R.id.viewstub);

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.name.setText(currentCustomer.getCustomerName());
		holder.plafon.setText(currentCustomer.getCustomerPlafon());
		holder.status.setText(currentCustomer.getBoxStatus());
		if (position % 2 == 0) {
			holder.viewstub.setBackgroundResource(R.color.orange_dark);
		} else {
			holder.viewstub.setBackgroundResource(R.color.orange);
		}
		return convertView;
	}

	static class Holder {
		TextView name, plafon, status;
		View viewstub;
	}

}

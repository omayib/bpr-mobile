package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.Note;

public class NotesAdapter extends ArrayAdapter<Note> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private Note currentNote;

	public NotesAdapter(Context context, int layout, List<Note> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentNote = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.creator = (TextView) convertView
					.findViewById(R.id.item_note_creator);
			holder.message = (TextView) convertView
					.findViewById(R.id.item_note_message);
			holder.viewstub = convertView.findViewById(R.id.viewstub);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.creator.setText(currentNote.getCreator());
		holder.message.setText(currentNote.getMessages());
		if (position % 2 == 0) {
			holder.viewstub.setBackgroundResource(R.color.orange_dark);
		} else {
			holder.viewstub.setBackgroundResource(R.color.orange);
		}
		return convertView;
	}

	private class Holder {
		TextView creator, message;
		View viewstub;
	}
}

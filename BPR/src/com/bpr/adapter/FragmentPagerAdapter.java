package com.bpr.adapter;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.bpr.view.fragment.FragmentAssets;
import com.bpr.view.fragment.FragmentBusinessParent;
import com.bpr.view.fragment.FragmentCapacity;
import com.bpr.view.fragment.FragmentCollateralBpkb;
import com.bpr.view.fragment.FragmentCollateralDeposito;
import com.bpr.view.fragment.FragmentCollateralShm;
import com.bpr.view.fragment.FragmentCollateralTabungan;
import com.bpr.view.fragment.FragmentEnvironmentalCheckup;
import com.bpr.view.fragment.FragmentJob;
import com.bpr.view.fragment.FragmentPersonal;
import com.bpr.view.fragment.FragmentPhotoBusiness;
import com.bpr.view.fragment.FragmentPhotoCollateral;
import com.bpr.view.fragment.FragmentPhotoHome;
import com.bpr.view.fragment.FragmentPhotoOther;
import com.bpr.view.fragment.FragmentPhotoSid;
import com.bpr.view.fragment.FragmentPurpose;
import com.bpr.view.fragment.FragmentSubmissionCredit;

public class FragmentPagerAdapter extends ArrayPagerAdapter<Fragment> {

	public FragmentPagerAdapter(FragmentManager fragmentManager,
			ArrayList<PageDescriptor> descriptors) {
		super(fragmentManager, descriptors);
	}

	@Override
	protected Fragment createFragment(PageDescriptor desc) {

		if (desc.getFragmentTag().equalsIgnoreCase(FragmentPage.TAG_PERSONAL)) {
			Fragment f = new FragmentPersonal();
			Bundle bundle = new Bundle();
			bundle.putSerializable(FragmentPage.TAG_PERSONAL, desc.getModel());
			f.setArguments(bundle);
			return f;
		} else if (desc.getFragmentTag().equalsIgnoreCase(FragmentPage.TAG_JOB)) {
			Fragment job = new FragmentJob();
			Bundle bJob = new Bundle();
			bJob.putSerializable(FragmentPage.TAG_JOB, desc.getModel());
			job.setArguments(bJob);
			return job;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_BUSINESS)) {
			Fragment fBusiness = new FragmentBusinessParent();
			Bundle bBusiness = new Bundle();
			bBusiness.putSerializable(FragmentPage.TAG_BUSINESS,
					desc.getModel());
			fBusiness.setArguments(bBusiness);
			return fBusiness;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_ASSETS)) {
			Fragment fAssets = new FragmentAssets();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_ASSETS, desc.getModel());
			fAssets.setArguments(aBundle);
			return fAssets;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_COLLATERAL_BPKB)) {
			Fragment fBpkb = new FragmentCollateralBpkb();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_COLLATERAL_BPKB,
					desc.getModel());
			fBpkb.setArguments(aBundle);
			return fBpkb;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_COLLATERAL_SHM)) {
			Fragment fShm = new FragmentCollateralShm();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_COLLATERAL_SHM,
					desc.getModel());
			fShm.setArguments(aBundle);
			return fShm;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_COLLATERAL_TABUNGAN)) {
			Fragment fShm = new FragmentCollateralTabungan();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_COLLATERAL_TABUNGAN,
					desc.getModel());
			fShm.setArguments(aBundle);
			return fShm;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_COLLATERAL_DEPOSITO)) {
			Fragment fShm = new FragmentCollateralDeposito();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_COLLATERAL_DEPOSITO,
					desc.getModel());
			fShm.setArguments(aBundle);
			return fShm;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_ENVIRONMENT_CHECK)) {
			Fragment fCheck = new FragmentEnvironmentalCheckup();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_ENVIRONMENT_CHECK,
					desc.getModel());
			fCheck.setArguments(aBundle);
			return fCheck;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_SUMBISSION)) {
			Fragment fSub = new FragmentSubmissionCredit();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_SUMBISSION,
					desc.getModel());
			fSub.setArguments(aBundle);
			return fSub;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_PHOTO_GUARANTEE)) {
			Fragment fPhoto = new FragmentPhotoCollateral();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_PHOTO_GUARANTEE,
					desc.getModel());
			fPhoto.setArguments(aBundle);
			return fPhoto;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_PHOTO_BUSINESS)) {
			Fragment fPhoto = new FragmentPhotoBusiness();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_PHOTO_BUSINESS,
					desc.getModel());
			fPhoto.setArguments(aBundle);
			return fPhoto;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_PHOTO_HOME)) {
			Fragment fPhoto = new FragmentPhotoHome();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_PHOTO_HOME,
					desc.getModel());
			fPhoto.setArguments(aBundle);
			return fPhoto;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_PHOTO_OTHER)) {
			Fragment fPhoto = new FragmentPhotoOther();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_PHOTO_OTHER,
					desc.getModel());
			fPhoto.setArguments(aBundle);
			return fPhoto;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_PURPOSE)) {
			Fragment fPurpose = new FragmentPurpose();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_PURPOSE, desc.getModel());
			fPurpose.setArguments(aBundle);
			return fPurpose;
		} else if (desc.getFragmentTag().equalsIgnoreCase(
				FragmentPage.TAG_PHOTO_SID)) {
			Fragment fSid = new FragmentPhotoSid();
			return fSid;
		} else {

			Fragment fCap = new FragmentCapacity();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_CAPACITY, desc.getModel());
			fCap.setArguments(aBundle);
			return fCap;
		}
	}
}

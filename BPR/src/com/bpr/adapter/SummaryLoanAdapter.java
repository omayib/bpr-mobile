package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.SummaryLoan;

public class SummaryLoanAdapter extends ArrayAdapter<SummaryLoan> {

	private int layout;
	private LayoutInflater inflater;
	private Context context;
	private SummaryLoan currentLoan;

	public SummaryLoanAdapter(Context context, int layout,
			List<SummaryLoan> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentLoan = getItem(position);
		Holder holder;
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.tvDebitBalance = (TextView) convertView
					.findViewById(R.id.item_loan_debit_balance);
			holder.tvLoansTo = (TextView) convertView
					.findViewById(R.id.item_loan_to);
			holder.tvPlafon = (TextView) convertView
					.findViewById(R.id.item_loan_plafon);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.tvDebitBalance.setText(currentLoan.getDebitBalance());
		holder.tvLoansTo.setText(currentLoan.getLoansTo() + "");
		holder.tvPlafon.setText(currentLoan.getPlafon());
		return convertView;
	}

	private class Holder {
		TextView tvLoansTo, tvPlafon, tvDebitBalance;
	}
}

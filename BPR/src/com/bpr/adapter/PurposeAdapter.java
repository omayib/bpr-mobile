package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.Purposes;

public class PurposeAdapter extends ArrayAdapter<Purposes> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private Purposes currentPurposes;

	public PurposeAdapter(Context context, int layout, List<Purposes> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentPurposes = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.general = (TextView) convertView
					.findViewById(R.id.item_purpose_general);
			holder.detail = (TextView) convertView
					.findViewById(R.id.item_purpose_detail);
			holder.viewstub = convertView.findViewById(R.id.viewstub);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.general.setText(currentPurposes.getGeneral());
		holder.detail.setText(currentPurposes.getDetail());
		if (position % 2 == 0) {
			holder.viewstub.setBackgroundResource(R.color.orange_dark);
		} else {
			holder.viewstub.setBackgroundResource(R.color.orange);
		}
		return convertView;
	}

	private class Holder {
		TextView general, detail;
		View viewstub;
	}
}

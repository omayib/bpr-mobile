package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.Children;

public class ChildrenAdapter extends ArrayAdapter<Children> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private Children currentChild;

	public ChildrenAdapter(Context context, int layout, List<Children> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentChild = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.name = (TextView) convertView.findViewById(R.id.child_name);
			holder.age = (TextView) convertView.findViewById(R.id.child_age);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.name.setText(currentChild.getName());
		holder.age.setText(currentChild.getAge() + " th");
		return convertView;
	}

	private class Holder {
		TextView name, age;
	}
}

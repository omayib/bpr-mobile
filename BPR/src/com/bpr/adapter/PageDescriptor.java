package com.bpr.adapter;

import java.io.Serializable;

public interface PageDescriptor {
	String getFragmentTag();

	String getTitle();

	Serializable getModel();

}
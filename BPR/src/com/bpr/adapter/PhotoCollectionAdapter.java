package com.bpr.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.PhotoFolder;

public class PhotoCollectionAdapter extends ArrayAdapter<PhotoFolder> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private PhotoFolder currentPhotoCollection;
	private ArrayList<PhotoFolder> photos;

	public PhotoCollectionAdapter(Context context, int layout,
			List<PhotoFolder> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
		this.photos = (ArrayList<PhotoFolder>) objects;
		for (PhotoFolder p : photos) {
			Log.d("omayib adapter", p.getName());
			Log.d("omayib adapter", p.getPhotoCount() + "");
		}
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentPhotoCollection = photos.get(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);

			holder.name = (TextView) convertView.findViewById(R.id.photo_name);
			holder.count = (TextView) convertView
					.findViewById(R.id.photo_count);
			holder.viewstub = convertView.findViewById(R.id.viewstub);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.name.setText(currentPhotoCollection.getName());
		holder.count.setText(currentPhotoCollection.getPhotoCount() + " Photos");
		if (position % 2 == 0) {
			holder.viewstub.setBackgroundResource(R.color.orange_dark);
		} else {
			holder.viewstub.setBackgroundResource(R.color.orange);
		}
		return convertView;
	}

	private class Holder {
		TextView name, count;
		View viewstub;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return photos.size();
	}

}

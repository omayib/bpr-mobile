package com.bpr.adapter;

import java.io.Serializable;

public class FragmentPage<T extends Serializable> implements PageDescriptor {
	public static String TAG_PERSONAL = "personal";
	public static String TAG_JOB = "job";
	public static String TAG_BUSINESS = "business";
	public static String TAG_BUSINESS_COLLECTION = "business_collection";
	public static String TAG_CAPACITY = "capacity";
	public static String TAG_ASSETS = "assets";
	public static String TAG_SUMBISSION = "submission";
	public static String TAG_ENVIRONMENT_CHECK = "environment_check";
	public static String TAG_COLLATERAL_BPKB = "bpkb";
	public static String TAG_COLLATERAL_SHM = "shm";
	public static String TAG_COLLATERAL_DEPOSITO = "deposito";
	public static String TAG_COLLATERAL_TABUNGAN = "tabungan";
	public static String TAG_PHOTO_GUARANTEE = "photo_guarantee";
	public static String TAG_PHOTO_HOME = "photo_home";
	public static String TAG_PHOTO_BUSINESS = "photo_business";
	public static String TAG_PHOTO_OTHER = "photo";
	public static String TAG_PHOTO_SID = "sid";
	public static String TAG_PURPOSE = "purpose";
	public static String TAG_SUMMARY_SPLITER0a = "split0a";
	public static String TAG_SUMMARY_SPLITER0b = "split0b";
	public static String TAG_SUMMARY_SPLITER1 = "split1";
	public static String TAG_SUMMARY_SPLITER2 = "split2";
	public static String TAG_SUMMARY_SPLITER3 = "split3";
	public static String TAG_SUMMARY_SPLITER4 = "split4";
	private String title;
	private String tag;
	private T model;

	public FragmentPage(String title, String tag, T model) {
		super();
		this.title = title;
		this.tag = tag;
		this.model = model;
	}

	@Override
	public String getFragmentTag() {
		return tag;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public Serializable getModel() {
		return model;
	}

}

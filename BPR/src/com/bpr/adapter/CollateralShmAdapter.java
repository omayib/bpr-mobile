package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.CollateralShm;

public class CollateralShmAdapter extends ArrayAdapter<CollateralShm> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private CollateralShm currentShm;
	private ArrayAdapter<String> adapterSpinner;
	private boolean isReadOnly;

	public CollateralShmAdapter(Context context, int layout,
			List<CollateralShm> objects, boolean b) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
		this.isReadOnly = b;
		this.adapterSpinner = new ArrayAdapter<String>(context,
				android.R.layout.simple_list_item_1, context.getResources()
						.getStringArray(R.array.binding_shm));
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentShm = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.priceAssesed = (TextView) convertView
					.findViewById(R.id.guarantee_price_assessed);
			holder.priceOnMarket = (TextView) convertView
					.findViewById(R.id.guarantee_price_onmarket);
			holder.owner = (TextView) convertView
					.findViewById(R.id.guarantee_owner);
			holder.name = (TextView) convertView
					.findViewById(R.id.guarantee_name);
			holder.spinner = (Spinner) convertView
					.findViewById(R.id.guarantee_binding);
			holder.spinner.setAdapter(adapterSpinner);

			holder.spinner
					.setOnItemSelectedListener(new OnItemSelectedListener() {

						@Override
						public void onItemSelected(AdapterView<?> adapter,
								View arg1, int arg2, long arg3) {
							holder.binding = adapter.getAdapter().getItem(arg2)
									.toString();
							currentShm.setBinding(holder.binding);
							System.out.println("binding changed:"
									+ holder.binding);
						}

						@Override
						public void onNothingSelected(AdapterView<?> arg0) {

						}
					});
			holder.spinner.setEnabled(!isReadOnly);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.priceAssesed.setText(currentShm.getPriceOnAssessedAsRupiah());
		holder.priceOnMarket.setText(currentShm.getPriceOnMarketAsRupiah());
		holder.owner.setText(currentShm.getCertificateName());
		holder.name.setText(currentShm.getStatusOfLand());
		holder.spinner
				.setSelection(getBindingShmIndex(currentShm.getBinding()));
		holder.spinner.setEnabled(!isReadOnly);

		// update current shm model
		System.out.println("current shm "+currentShm.toString());
		System.out.println("binding:" + currentShm.getBinding());

		return convertView;
	}

	private int getBindingShmIndex(String input) {
		System.out.println("binding input:" + input);
		String[] bindings = context.getResources().getStringArray(
				R.array.binding_shm);
		for (int i = 0; i < bindings.length; i++) {
			if (bindings[i].equalsIgnoreCase(input)) {
				return i;
			}
		}
		return 0;
	}

	private class Holder {
		TextView priceAssesed, priceOnMarket, owner, name;
		Spinner spinner;
		String binding;
	}
}

package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.bpr.R;
import com.customer.model.Sid;
import com.loopj.android.image.SmartImageView;

public class SidAdapter extends ArrayAdapter<Sid> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private Sid currentSid;

	public SidAdapter(Context context, int layout, List<Sid> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentSid = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.imageViewSid = (SmartImageView) convertView
					.findViewById(R.id.imageView_sid);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.imageViewSid.setImageUrl(currentSid.getPhotoThumbnailUrl());
		return convertView;
	}

	private class Holder {
		SmartImageView imageViewSid;
	}
}

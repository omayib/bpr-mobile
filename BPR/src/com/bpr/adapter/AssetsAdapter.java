package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.Assets;

public class AssetsAdapter extends ArrayAdapter<Assets> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private Assets currentAssets;

	public AssetsAdapter(Context context, int layout, List<Assets> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentAssets = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.name = (TextView) convertView.findViewById(R.id.aaaaa);
			holder.vallue = (TextView) convertView
					.findViewById(R.id.item_value_assets);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.name.setText(currentAssets.getName());
		holder.vallue.setText(currentAssets.getValueAsRupiah());

		return convertView;
	}

	private class Holder {
		TextView name, vallue;
	}
}

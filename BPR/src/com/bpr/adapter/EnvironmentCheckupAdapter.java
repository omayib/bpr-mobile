package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.EnvironmentalCheck;

public class EnvironmentCheckupAdapter extends ArrayAdapter<EnvironmentalCheck> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private EnvironmentalCheck currentBpkb;

	public EnvironmentCheckupAdapter(Context context, int layout,
			List<EnvironmentalCheck> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentBpkb = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);

			holder.envImpression = (TextView) convertView
					.findViewById(R.id.env_impression);
			holder.envName = (TextView) convertView.findViewById(R.id.env_name);
			holder.envRelation = (TextView) convertView
					.findViewById(R.id.env_relation);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.envName.setText(currentBpkb.getName());
		holder.envImpression.setText(currentBpkb.getImpression());
		holder.envRelation.setText(currentBpkb.getRelation());

		return convertView;
	}

	private class Holder {

		TextView envName, envImpression, envRelation;
	}
}

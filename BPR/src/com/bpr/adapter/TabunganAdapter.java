package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.CollateralTabungan;

public class TabunganAdapter extends ArrayAdapter<CollateralTabungan> {

	private int layout;
	private LayoutInflater inflater;
	private Context context;
	private CollateralTabungan currentTabungan;

	public TabunganAdapter(Context context, int layout,
			List<CollateralTabungan> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentTabungan = getItem(position);
		Holder holder = new Holder();
		if (convertView == null) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.tvName = (TextView) convertView
					.findViewById(R.id.tabungan_name);
			holder.tvAccountNumber = (TextView) convertView
					.findViewById(R.id.tabungan_account_number);
			holder.tvAmmount = (TextView) convertView
					.findViewById(R.id.tabungan_amount);
			holder.tvBookNumber = (TextView) convertView
					.findViewById(R.id.tabungan_book_number);
			holder.viewstub = convertView.findViewById(R.id.viewstub);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.tvAccountNumber.setText(currentTabungan.getAccountNumber());
		holder.tvAmmount.setText(currentTabungan.getAmountAsRupiah());
		holder.tvBookNumber.setText(currentTabungan.getBookNumber());
		holder.tvName.setText(currentTabungan.getName());

		if (position % 2 == 0) {
			holder.viewstub.setBackgroundResource(R.color.orange_dark);
		} else {
			holder.viewstub.setBackgroundResource(R.color.orange);
		}
		return convertView;
	}

	private class Holder {
		TextView tvName, tvAccountNumber, tvBookNumber, tvAmmount;
		View viewstub;
	}
}

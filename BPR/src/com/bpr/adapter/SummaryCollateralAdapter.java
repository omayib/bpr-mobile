package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.SummaryCollateral;

public class SummaryCollateralAdapter extends ArrayAdapter<SummaryCollateral> {

	private int layout;
	private LayoutInflater inflater;
	private Context context;
	private SummaryCollateral currentCollateral;

	public SummaryCollateralAdapter(Context context, int layout,
			List<SummaryCollateral> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentCollateral = getItem(position);
		Holder holder;
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.tvCollateralName = (TextView) convertView
					.findViewById(R.id.item_collateral_name);
			holder.tvCollateralPriceAssesed = (TextView) convertView
					.findViewById(R.id.item_collateral_price_assessed);
			holder.tvCollateralPriceMarket = (TextView) convertView
					.findViewById(R.id.item_collateral_price_market);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.tvCollateralName.setText(currentCollateral.getColletralType());
		holder.tvCollateralPriceAssesed.setText(currentCollateral
				.getCollateralPriceAssesedAsRupiah());
		holder.tvCollateralPriceMarket.setText(currentCollateral
				.getCollateralPriceMarketAsRupiah());
		return convertView;
	}

	private class Holder {
		TextView tvCollateralName, tvCollateralPriceAssesed,
				tvCollateralPriceMarket;
	}
}

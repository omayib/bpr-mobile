package com.bpr.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bpr.R;
import com.customer.model.CollateralDeposito;

public class DepositoAdapter extends ArrayAdapter<CollateralDeposito> {

	private int layout;
	private LayoutInflater inflater;
	private Context context;
	private CollateralDeposito currentDeposito;

	public DepositoAdapter(Context context, int layout,
			List<CollateralDeposito> objects) {
		super(context, layout, objects);
		this.layout = layout;
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentDeposito = getItem(position);
		Holder holder = new Holder();
		if (convertView == null) {
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.tvName = (TextView) convertView
					.findViewById(R.id.deposito_name);
			holder.tvAccountNumber = (TextView) convertView
					.findViewById(R.id.deposito_account_number);
			holder.tvAmmount = (TextView) convertView
					.findViewById(R.id.deposito_amount);
			holder.tvBilyetNumber = (TextView) convertView
					.findViewById(R.id.deposito_bilyet_number);
			holder.viewstub = convertView.findViewById(R.id.viewstub);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.tvAccountNumber.setText(currentDeposito.getAccountNumber());
		holder.tvAmmount.setText(currentDeposito.getAmountAsRupiah());
		holder.tvBilyetNumber.setText(currentDeposito.getBilyetNumber());
		holder.tvName.setText(currentDeposito.getName());

		if (position % 2 == 0) {
			holder.viewstub.setBackgroundResource(R.color.orange_dark);
		} else {
			holder.viewstub.setBackgroundResource(R.color.orange);
		}
		return convertView;
	}

	private class Holder {
		TextView tvName, tvAccountNumber, tvBilyetNumber, tvAmmount;
		View viewstub;
	}
}

package com.bpr.adapter;

import java.util.List;

import android.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.user.modul.User;

public class ListUserAdapter extends ArrayAdapter<User> {

	private int layout;
	private LayoutInflater inflater;
	private Holder holder;
	private Context context;
	private User currentUser;
	private int tv;

	public ListUserAdapter(Context context, int resource,
			int textViewResourceId, List<User> objects) {
		super(context, resource, textViewResourceId, objects);
		this.layout = resource;
		this.context = context;
		this.tv = textViewResourceId;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		currentUser = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(layout, parent, false);
			holder.name = (TextView) convertView.findViewById(tv);

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		holder.name.setText(currentUser.getName());

		return convertView;
	}

	private class Holder {
		TextView name;
	}
}

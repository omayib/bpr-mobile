package com.bpr.notification.manager;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import com.bpr.photo.viewer.R;
import com.bpr.view.activity.DashboardActivity;

public class BprNotificationManager {
	private Context context;
	private NotificationManager notifManager;
	private NotificationCompat.Builder uploadNotifBuilder;
	private NotificationCompat.Builder pullNotification;
	private int uploadNotifId = 212;
	private int pullNotifId = 223;
	private int numUpload = 0;

	public BprNotificationManager(Context context) {
		super();
		this.context = context;
		notifManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		uploadNotifBuilder = new NotificationCompat.Builder(context);
		pullNotification = new NotificationCompat.Builder(context);
		Intent intent = new Intent(this.context, DashboardActivity.class);
		PendingIntent pi = PendingIntent.getActivity(this.context, 0, intent,
				Intent.FLAG_ACTIVITY_NEW_TASK);
		pullNotification.setContentIntent(pi);

	}

	public void updatePullNotification(String message) {
		pullNotification.setDefaults(Notification.DEFAULT_SOUND);
		pullNotification.setContentTitle("Synchronize");
		pullNotification.setSmallIcon(R.drawable.ic_launcher);
		pullNotification.setContentInfo(message);
		pullNotification.setAutoCancel(false);
		Notification pullNoti = pullNotification.build();
		notifManager.notify(pullNotifId, pullNoti);
	}

	public Notification getUploadNotification() {
		System.out.println("creat notification");
		uploadNotifBuilder.setContentTitle("uploading...");
		uploadNotifBuilder.setSmallIcon(R.drawable.ic_launcher);
		uploadNotifBuilder.setContentInfo("name");
		return uploadNotifBuilder.build();
	}

	public void updateUploadNotification(long j) {
		uploadNotifBuilder.setContentText("name_" + j).setNumber(++numUpload);
		notifManager.notify(uploadNotifId, uploadNotifBuilder.build());
	}

	public void clearNotification() {
		notifManager.cancelAll();
	}

}

package com.bpr.notification.manager;

public class NotificationUploadConfig {

	private final int iconResourceID;
	private final String title;
	private final String message;
	private final String completed;
	private final String error;
	private final boolean autoClearOnSuccess;

	public NotificationUploadConfig() {
		super();
		iconResourceID = android.R.drawable.ic_menu_upload;
		title = "File Upload";
		message = "uploading in progress";
		completed = "upload completed successfully!";
		error = "error during upload";
		autoClearOnSuccess = false;
	}

	public NotificationUploadConfig(final int iconResourceID,
			final String title, final String message, final String completed,
			final String error, final boolean autoClearOnSuccess)
			throws IllegalArgumentException {

		if (title == null || message == null || completed == null
				|| error == null) {
			throw new IllegalArgumentException(
					"You can't provide null parameters");
		}

		this.iconResourceID = iconResourceID;
		this.title = title;
		this.message = message;
		this.completed = completed;
		this.error = error;
		this.autoClearOnSuccess = autoClearOnSuccess;
	}

	public int getIconResourceID() {
		return iconResourceID;
	}

	public String getTitle() {
		return title;
	}

	public String getMessage() {
		return message;
	}

	public String getCompleted() {
		return completed;
	}

	public String getError() {
		return error;
	}

	public boolean isAutoClearOnSuccess() {
		return autoClearOnSuccess;
	}

}

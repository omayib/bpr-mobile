package com.bpr.view.activity;

import java.io.IOException;
import java.io.InputStream;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.bpr.R;
import com.bpr.notification.manager.NotificationUploadConfig;
import com.bpr.service.UploadRequest;
import com.bpr.service.UploaderService;
import com.customer.model.Photo;
import com.customer.model.PhotoCandidate;
import com.splunk.mint.Mint;

public class PhotoTest extends Activity {
	Intent intent;
	// UploadRequest uploadRequest;
	int counter = 1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_test);
		Button b = (Button) findViewById(R.id.btest);
		b.setText("start service");
		intent = new Intent(PhotoTest.this, UploaderService.class);

		// uploadRequest = new UploadRequest(this);

		// FakeUploadRequest();
		// uploadRequest = new UploadRequest(this);

		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String a = null;
				TextView c = (TextView) findViewById(R.id.btest);
				String b = a.toString();

				// FakeUploadRequest();
				// if (!UploaderService.isRunning)
				// startService(intent);
				// else {
				// UploaderService.go();
				// }
				// String data = generator("data_proposal.txt");

				// ProposalActiveRecord p = new ProposalActiveRecord();
				// p.action = "a";
				// p.id = -123123;
				// p.sync = false;
				// p.save();
			}
		});
		findViewById(R.id.button1).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// ProposalActiveRecord.truncate();
				counter = 1;
			}
		});
	}

	protected void FakeUploadRequest() {
		Photo photo = new Photo.Builder().id(-1).info("info")
				.name("photo-" + counter).pathLocal("pathlocal")
				.uniqueId("12321").url("url").build();
		PhotoCandidate photoCandidate = new PhotoCandidate.Builder()
				.folderId(-12).folderName("foldername").galleryName("bisnis")
				.photo(photo).proposalId(-23).build();

		NotificationUploadConfig notif = new NotificationUploadConfig(
				R.drawable.ic_launcher, photo.getName(), photo.getName()
						+ " message", photo.getName() + " completed",
				photo.getName() + " on error", true);
		// uploadRequest.setPhotoCandidate(photoCandidate);
		// uploadRequest.setNotifConfig(notif);
		// UploaderService.startToUpload(uploadRequest,this);
		// UploaderService.startToUpload(uploadRequest);
		counter++;
	}

	private String generator(String filename) {
		InputStream input;
		String respon = "";
		try {
			input = getAssets().open(filename);
			int size = input.available();
			byte[] buffer = new byte[size];
			input.read(buffer);
			input.close();
			respon = new String(buffer);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return respon;
	}

	private String generateData() {

		JSONObject obAttribute = new JSONObject();
		try {
			obAttribute.put("id", 41);
			obAttribute.put("name", "aaaagJohn Doe");
			obAttribute.put("ktp_number", "myktpnumber");
			obAttribute.put("birth_date", "1987/07/01");
			obAttribute.put("birth_place", "Jogja");
			obAttribute.put("age", 30);
			obAttribute.put("gender", "Laki-laki");
			obAttribute.put("marital_status", "Menikah");
			obAttribute.put("ktp_address", "Jogja");
			obAttribute.put("current_address", "Jogja");
			obAttribute.put("spouse_name", "Jane Doe");
			obAttribute.put("spouse_job", "Housewive");
			obAttribute.put("alias_name", "John");
			obAttribute.put("home_status", "SENDIRI");
			obAttribute.put("mother_maiden_name", "Dolores");
			obAttribute.put("number_of_dependents", 4);
			obAttribute.put("phone_number", "123123123");
			obAttribute.put("handphone_number", "123123123");
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		JSONObject obProposal = new JSONObject();
		try {
			obProposal.put("survey_personal_attributes", obAttribute);
		} catch (JSONException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		JSONObject ob = new JSONObject();
		try {
			ob.put("token", "227e60e60a598335eb352594857e8e49");
			ob.put("proposal_survey", obProposal);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return ob.toString();
	}
}

package com.bpr.view.activity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.ackbox.event.RefreshAckBoxSucceded;
import com.bpr.R;
import com.bpr.application.ConfigApplication;
import com.bpr.application.ConfigApplication.BOX_SELECTED;
import com.bpr.dashboard.BoxModel;
import com.bpr.dashboard.DashboardAdapter;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.bpr.photo.viewer.activity.PhotoViewerActivity;
import com.customer.model.Photo;
import com.followupbox.event.RefreshFollowupBoxSucceeded;
import com.newbox.event.RefreshNewboxSucceeded;
import com.proposalbox.event.LocalGalleryLoaded;
import com.proposalbox.event.RefreshProposalBoxSucceeded;
import com.user.event.SignoutFailed;
import com.user.event.SignoutSucceeded;

public class DashboardActivity extends Activity implements OnItemClickListener {
	private ConfigApplication conf;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private DashboardAdapter boxAdapter;
	private ArrayList<BoxModel> listBox = new ArrayList<BoxModel>();
	private List<Photo> pendingPhotos = new ArrayList<Photo>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_dashboard);

		conf = (ConfigApplication) getApplication();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();

		if (!ConfigApplication.INITIALIZED) {
			conf.initWorker();
		}

		if (!conf.getPref().hasToken()) {
			startActivity(new Intent(this, LoginActivity.class));
			this.finish();
		}

		ActionBar ab = getActionBar();
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		ab.setCustomView(R.layout.action_bar_custom);
		ab.setLogo(R.drawable.ic_launcher);

		listBox = conf.getBox();
		boxAdapter = new DashboardAdapter(this, R.layout.item_box_dashboard,
				listBox);

		GridView gridBox = (GridView) findViewById(R.id.gridBox);
		gridBox.setAdapter(boxAdapter);
		gridBox.setOnItemClickListener(this);

		registerSubscribers();

		conf.getNotifManager().clearNotification();

	}

	@Override
	protected void onResume() {
		super.onResume();
		userController.getGalleryPending();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		removeEventSubscribers();
	}

	private void registerSubscribers() {
		addEventSubscriber(new EventSubscriber<SignoutSucceeded>() {

			@Override
			public void handleEvent(SignoutSucceeded event) {
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						conf.getPref().clear();
						finish();
					}
				});
			}
		});
		addEventSubscriber(new EventSubscriber<SignoutFailed>() {

			@Override
			public void handleEvent(SignoutFailed event) {
				Log.d("omayib", "failed");
			}
		});
		addEventSubscriber(new EventSubscriber<RefreshNewboxSucceeded>() {

			@Override
			public void handleEvent(RefreshNewboxSucceeded event) {
				System.out.println("dashboard new::"
						+ event.response.toString());
				for (BoxModel box : listBox) {
					if (box.getName().equalsIgnoreCase("new")) {
						System.out.println("contoint");
						box.setUnread(event.response.size());
						boxAdapter.notifyDataSetChanged();
					}
				}
			}
		});
		addEventSubscriber(new EventSubscriber<RefreshProposalBoxSucceeded>() {

			@Override
			public void handleEvent(RefreshProposalBoxSucceeded event) {
				System.out.println("dashboard proposal::"
						+ event.response.toString());
				for (BoxModel box : listBox) {
					if (box.getName().equalsIgnoreCase("proposal")) {
						System.out.println("contoint");
						box.setUnread(event.response.size());
						boxAdapter.notifyDataSetChanged();
					}
				}
			}
		});
		addEventSubscriber(new EventSubscriber<RefreshFollowupBoxSucceeded>() {

			@Override
			public void handleEvent(RefreshFollowupBoxSucceeded event) {
				System.out.println("dashboard followup::"
						+ event.response.toString());
				for (BoxModel box : listBox) {
					if (box.getName().equalsIgnoreCase("followup")) {
						System.out.println("contoint");
						box.setUnread(event.response.size());
						boxAdapter.notifyDataSetChanged();
					}
				}
			}
		});

		addEventSubscriber(new EventSubscriber<RefreshAckBoxSucceded>() {

			@Override
			public void handleEvent(RefreshAckBoxSucceded event) {
				System.out.println("dashboard ack::"
						+ event.response.toString());
				for (BoxModel box : listBox) {
					if (box.getName().equalsIgnoreCase("ack")) {
						System.out.println("contoint");
						box.setUnread(event.response.size());
						boxAdapter.notifyDataSetChanged();
					}
				}
			}
		});
		addEventSubscriber(new EventSubscriber<LocalGalleryLoaded>() {

			@Override
			public void handleEvent(LocalGalleryLoaded event) {
				System.out.println("ada photo yang pending");
				pendingPhotos.clear();
				pendingPhotos.addAll(event.listPhoto);
				updateHotCount(event.listPhoto.size());

			}
		});
	}

	private int hot_number = 0;
	private TextView ui_hot;

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.dashboard, menu);
		final View menu_hotlist = menu.findItem(R.id.menu_hotlist)
				.getActionView();
		ui_hot = (TextView) menu_hotlist.findViewById(R.id.hotlist_hot);
		updateHotCount(hot_number);
		new MyMenuItemStuffListener(menu_hotlist, "shortcut to gallery") {
			@Override
			public void onClick(View v) {
				System.out.println("on hot selected");
				if (pendingPhotos.isEmpty()) {
					Toast.makeText(getApplicationContext(),
							"tidak ada photo tertinggal", Toast.LENGTH_SHORT)
							.show();
					return;
				}
				Bundle b = new Bundle();
				b.putSerializable("pendingPhotos", (Serializable) pendingPhotos);
				startActivityForResult(new Intent(DashboardActivity.this,
						PhotoViewerActivity.class).putExtras(b), 101);
			}
		};
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.signout:
			userController.signout("123");
			conf.stopForegroundService();
			conf.tearDownApp();
			break;

		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		BoxModel selectedBox = (BoxModel) arg0.getItemAtPosition(arg2);
		if (selectedBox.getName().equalsIgnoreCase("new")) {
			conf.setCurrentBox(BOX_SELECTED.NEW);
			startActivity(new Intent(getApplicationContext(),
					CustomerListActivity.class).putExtra("title", "NEW"));
		} else if (selectedBox.getName().equalsIgnoreCase("FOLLOWUP")) {
			conf.setCurrentBox(BOX_SELECTED.FOLLOWUP);
			startActivity(new Intent(getApplicationContext(),
					CustomerListActivity.class).putExtra("title", "FOLLOWUP"));
		} else if (selectedBox.getName().equalsIgnoreCase("PROPOSAL")) {
			conf.setCurrentBox(BOX_SELECTED.PROPOSAL);
			startActivity(new Intent(getApplicationContext(),
					CustomerListActivity.class).putExtra("title", "PROPOSAL"));
		} else if (selectedBox.getName().equalsIgnoreCase("ALL")) {
			conf.setCurrentBox(BOX_SELECTED.ALL);
			startActivity(new Intent(getApplicationContext(),
					CustomerListActivity.class).putExtra("title", "ALL"));
		} else if (selectedBox.getName().equalsIgnoreCase("ACK")) {
			conf.setCurrentBox(BOX_SELECTED.ACK);
			startActivity(new Intent(getApplicationContext(),
					CustomerListActivity.class).putExtra("title",
					"ACKNOWLADGEMENT"));

		}
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	public void updateHotCount(final int new_hot_number) {
		hot_number = new_hot_number;
		if (ui_hot == null)
			return;
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (new_hot_number == 0)
					ui_hot.setVisibility(View.INVISIBLE);
				else {
					ui_hot.setVisibility(View.VISIBLE);
					ui_hot.setText(Integer.toString(new_hot_number));
				}
			}
		});
	}

	static abstract class MyMenuItemStuffListener implements
			View.OnClickListener, View.OnLongClickListener {
		private String hint;
		private View view;

		MyMenuItemStuffListener(View view, String hint) {
			this.view = view;
			this.hint = hint;
			view.setOnClickListener(this);
			view.setOnLongClickListener(this);
		}

		@Override
		abstract public void onClick(View v);

		@Override
		public boolean onLongClick(View v) {
			final int[] screenPos = new int[2];
			final Rect displayFrame = new Rect();
			view.getLocationOnScreen(screenPos);
			view.getWindowVisibleDisplayFrame(displayFrame);
			final Context context = view.getContext();
			final int width = view.getWidth();
			final int height = view.getHeight();
			final int midy = screenPos[1] + height / 2;
			final int screenWidth = context.getResources().getDisplayMetrics().widthPixels;
			Toast cheatSheet = Toast
					.makeText(context, hint, Toast.LENGTH_SHORT);
			if (midy < displayFrame.height()) {
				cheatSheet.setGravity(Gravity.TOP | Gravity.RIGHT, screenWidth
						- screenPos[0] - width / 2, height);
			} else {
				cheatSheet.setGravity(Gravity.BOTTOM
						| Gravity.CENTER_HORIZONTAL, 0, height);
			}
			cheatSheet.show();
			return true;
		}
	}
}

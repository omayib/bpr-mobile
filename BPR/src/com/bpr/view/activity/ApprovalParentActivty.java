package com.bpr.view.activity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.bpr.R;
import com.bpr.adapter.FragmentPage;
import com.bpr.adapter.PageDescriptor;
import com.bpr.adapter.SummaryPagerAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.utils.MyViewPager;
import com.customer.model.Summary;

public class ApprovalParentActivty extends FragmentActivity {
	public static MyViewPager pager = null;
	private ArrayList<PageDescriptor> pages = new ArrayList<PageDescriptor>();
	private ConfigApplication conf;
	private SummaryPagerAdapter adapter;
	private Summary summary;
	private TextView titleFragment;
	private Button bBack, bNext;
	private List<ApprovalSlidedListener> sliderListeners = new ArrayList<ApprovalSlidedListener>();

	public interface ApprovalSlidedListener {
		void onApprovalPageSlided(int currentPage, int totalPages);
	}

	public void setSliderListener(ApprovalSlidedListener l) {
		sliderListeners.add(l);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_approval);
		conf = (ConfigApplication) getApplication();

		Bundle b = getIntent().getExtras();
		if (b != null) {
			if (b.containsKey("summary")) {
				summary = (Summary) b.getSerializable("summary");
				conf.cloneSummary(summary);
			}
			FragmentPage<Serializable> page0a = new FragmentPage<Serializable>(
					"Step 1/5", FragmentPage.TAG_SUMMARY_SPLITER0a, summary);
			pages.add(page0a);
			// FragmentPage<Serializable> page0b = new
			// FragmentPage<Serializable>(
			// "Step 2/6", FragmentPage.TAG_SUMMARY_SPLITER0b, summary);
			// pages.add(page0b);

			FragmentPage<Serializable> page1 = new FragmentPage<Serializable>(
					"Step 2/5", FragmentPage.TAG_SUMMARY_SPLITER1, summary);
			pages.add(page1);

			FragmentPage<Serializable> page2 = new FragmentPage<Serializable>(
					"Step 3/5", FragmentPage.TAG_SUMMARY_SPLITER2, summary);
			pages.add(page2);

			if (!summary.getRecommendations().isEmpty()) {
				FragmentPage<Serializable> page3 = new FragmentPage<Serializable>(
						"Step 4/5", FragmentPage.TAG_SUMMARY_SPLITER3, summary);
				pages.add(page3);
			}

			FragmentPage<Serializable> page4 = new FragmentPage<Serializable>(
					"Step 5/5", FragmentPage.TAG_SUMMARY_SPLITER4, summary);
			pages.add(page4);

			titleFragment = (TextView) findViewById(R.id.fragment_title);
			pager = (MyViewPager) findViewById(R.id.pager);
			bBack = (Button) findViewById(R.id.bBack);
			bNext = (Button) findViewById(R.id.bNext);

			adapter = new SummaryPagerAdapter(getSupportFragmentManager(),
					pages);
			pager.setAdapter(adapter);
			pager.setPagingEnabled(false);
			pager.setOnPageChangeListener(new OnPageChangeListener() {

				@Override
				public void onPageSelected(int arg0) {
					titleFragment
							.setText(pager.getAdapter().getPageTitle(arg0));
					if(arg0==adapter.getCount()-1){
						bNext.setVisibility(View.GONE);
					}else{
						bNext.setVisibility(View.VISIBLE);
					}
				}

				@Override
				public void onPageScrolled(int arg0, float arg1, int arg2) {

				}

				@Override
				public void onPageScrollStateChanged(int arg0) {

				}
			});
			bBack.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					changeSlide(false);
				}
			});
			bNext.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					changeSlide(true);
				}
			});
			titleFragment.setText(adapter.getPageTitle(pager.getCurrentItem()));
		}
	}

	protected void changeSlide(boolean isNext) {
		int totalPage = adapter.getCount();
		int currentPage = pager.getCurrentItem();// start from 0
		System.out.println("total " + totalPage + "; current " + currentPage);
		if (isNext) {
			if (currentPage == totalPage) {
				return;
			}
			currentPage += 1;
			System.out.println("next ::" + currentPage);
			pager.setCurrentItem(currentPage, true);
			broadcastToSliderListener(currentPage, totalPage);
		} else {
			if (currentPage == 0) {
				return;
			}
			currentPage -= 1;
			System.out.println("next ::" + currentPage);
			pager.setCurrentItem(currentPage, true);
			broadcastToSliderListener(currentPage, totalPage);
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		sliderListeners.clear();
	}

	private void broadcastToSliderListener(int currentPage, int totalPage) {
		for (ApprovalSlidedListener slider : sliderListeners) {
			slider.onApprovalPageSlided(currentPage, totalPage);
		}
	}
}

package com.bpr.view.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ackbox.event.RefreshAckBoxFailed;
import com.ackbox.event.RefreshAckBoxSucceded;
import com.allbox.event.RefreshAllBoxFailed;
import com.allbox.event.RefreshAllBoxSucceeded;
import com.bpr.R;
import com.bpr.adapter.CustomerAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.application.ConfigApplication.BOX_SELECTED;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.BoxController;
import com.customer.event.BoxData;
import com.followupbox.event.RefreshFollowupBoxFailed;
import com.followupbox.event.RefreshFollowupBoxSucceeded;
import com.newbox.event.RefreshNewboxFailed;
import com.newbox.event.RefreshNewboxSucceeded;
import com.proposalbox.event.RefreshProposalBoxFailed;
import com.proposalbox.event.RefreshProposalBoxSucceeded;

public class CustomerListActivity extends Activity implements
		OnItemClickListener {
	private ListView customerListview;
	private CustomerAdapter adapter;
	private ArrayList<BoxData> customers = new ArrayList<BoxData>();
	private TextView title;
	private ConfigApplication conf;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private BoxController controller;
	private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
		setProgressBarIndeterminateVisibility(true);

		setContentView(R.layout.activity_list_customer);

		conf = (ConfigApplication) getApplication();
		controller = conf.getBoxController();
		eventPublisher = conf.getEventPublisher();
		pd = new ProgressDialog(this);
		pd.setMessage("please wait...");

		// setting up the actionbar
		ActionBar ab = getActionBar();
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		ab.setCustomView(R.layout.action_bar_custom);
		ab.setLogo(R.drawable.ic_launcher);
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(true);
		// initialize all view

		title = (TextView) findViewById(R.id.env_name);
		customerListview = (ListView) findViewById(R.id.list_customer);

		adapter = new CustomerAdapter(this, R.layout.item_list_customer,
				customers);

		customerListview.setAdapter(adapter);
		customerListview.setOnItemClickListener(this);

		if (getIntent().hasExtra("title")) {
			String t = getIntent().getStringExtra("title");
			title.setText(t);
		}
		title.setText(ConfigApplication.currentBoxSelected.name().toUpperCase());

		registerSubscribers();

		if (ConfigApplication.currentBoxSelected.equals(BOX_SELECTED.NEW)) {

			addEventSubscriber(new EventSubscriber<RefreshNewboxSucceeded>() {

				@Override
				public void handleEvent(RefreshNewboxSucceeded event) {
					System.out.println("newbox::" + event.response.toString());
					if (event.response.size() > 0) {
						customers.clear();
					}
					customers.addAll(event.response);
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							if (pd.isShowing())
								pd.dismiss();
							adapter.notifyDataSetChanged();
						}
					});
				}
			});
			addEventSubscriber(new EventSubscriber<RefreshNewboxFailed>() {

				@Override
				public void handleEvent(RefreshNewboxFailed event) {
					if (pd.isShowing())
						pd.dismiss();
					Toast.makeText(getApplicationContext(), event.exception,
							Toast.LENGTH_SHORT).show();
				}
			});
			pd.show();
			controller.newBoxrefresh(conf.getPref().getToken());
			if (!customers.isEmpty())
				customers.clear();

		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.PROPOSAL)) {

			addEventSubscriber(new EventSubscriber<RefreshProposalBoxSucceeded>() {

				@Override
				public void handleEvent(RefreshProposalBoxSucceeded event) {
					if (event.response.size() > 0) {
						customers.clear();
					}
					customers.addAll(event.response);
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							if (pd.isShowing())
								pd.dismiss();
							adapter.notifyDataSetChanged();
						}
					});
				}
			});
			addEventSubscriber(new EventSubscriber<RefreshProposalBoxFailed>() {

				@Override
				public void handleEvent(RefreshProposalBoxFailed event) {
					if (pd.isShowing())
						pd.dismiss();
					Toast.makeText(getApplicationContext(), event.exception,
							Toast.LENGTH_SHORT).show();
				}
			});

			pd.show();
			controller.proposalBoxRefresh(conf.getPref().getToken());
			if (!customers.isEmpty())
				customers.clear();

		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.FOLLOWUP)) {
			addEventSubscriber(new EventSubscriber<RefreshFollowupBoxSucceeded>() {

				@Override
				public void handleEvent(RefreshFollowupBoxSucceeded event) {
					if (event.response.size() > 0) {
						customers.clear();
					}
					customers.addAll(event.response);
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							if (pd.isShowing())
								pd.dismiss();
							adapter.notifyDataSetChanged();
						}
					});
				}
			});
			addEventSubscriber(new EventSubscriber<RefreshFollowupBoxFailed>() {

				@Override
				public void handleEvent(RefreshFollowupBoxFailed event) {
					if (pd.isShowing())
						pd.dismiss();
					Toast.makeText(getApplicationContext(), event.exception,
							Toast.LENGTH_SHORT).show();
				}
			});

			pd.show();
			controller.refreshFollowupBox(conf.getPref().getToken());
			if (!customers.isEmpty())
				customers.clear();
		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.ALL)) {

			addEventSubscriber(new EventSubscriber<RefreshAllBoxSucceeded>() {

				@Override
				public void handleEvent(RefreshAllBoxSucceeded event) {
					if (event.response.size() > 0) {
						customers.clear();
					}
					customers.addAll(event.response);
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							if (pd.isShowing())
								pd.dismiss();
							adapter.notifyDataSetChanged();
						}
					});
				}
			});
			addEventSubscriber(new EventSubscriber<RefreshAllBoxFailed>() {

				@Override
				public void handleEvent(RefreshAllBoxFailed event) {
					if (pd.isShowing())
						pd.dismiss();
					Toast.makeText(getApplicationContext(), event.exception,
							Toast.LENGTH_SHORT).show();
				}
			});

			pd.show();
			controller.refreshAllBox(conf.getPref().getToken());
			if (!customers.isEmpty())
				customers.clear();

		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.ACK)) {

			addEventSubscriber(new EventSubscriber<RefreshAckBoxSucceded>() {

				@Override
				public void handleEvent(RefreshAckBoxSucceded event) {
					if (event.response.size() > 0) {
						customers.clear();
					}
					customers.addAll(event.response);
					runOnUiThread(new Runnable() {

						@Override
						public void run() {
							if (pd.isShowing())
								pd.dismiss();
							adapter.notifyDataSetChanged();
						}
					});
				}
			});

			addEventSubscriber(new EventSubscriber<RefreshAckBoxFailed>() {

				@Override
				public void handleEvent(RefreshAckBoxFailed event) {

					if (pd.isShowing())
						pd.dismiss();
					Toast.makeText(getApplicationContext(), event.exception,
							Toast.LENGTH_SHORT).show();
				}
			});

			pd.show();
			controller.ackBoxRefresh(conf.getPref().getToken());
			if (!customers.isEmpty())
				customers.clear();
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		removeEventSubscribers();
	}

	private void registerSubscribers() {

	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		BoxData selected = (BoxData) arg0.getItemAtPosition(arg2);
		Bundle b = new Bundle();
		b.putSerializable("customerSelected", selected);
		if (ConfigApplication.currentBoxSelected.equals(BOX_SELECTED.NEW)) {
			startActivity(new Intent(getApplicationContext(),
					ListProposalActivity.class).putExtras(b));
		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.PROPOSAL)) {
			startActivity(new Intent(getApplicationContext(),
					ListProposalActivity.class).putExtras(b));
		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.FOLLOWUP)) {
			startActivity(new Intent(getApplicationContext(),
					ListProposalActivity.class).putExtras(b));
		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.ALL)) {
			startActivity(new Intent(getApplicationContext(),
					ListProposalActivity.class).putExtras(b));
		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.ACK)) {
			startActivity(new Intent(getApplicationContext(),
					ListProposalActivity.class).putExtras(b));
		}
		conf.getPref().setCurrentCustomerId_(selected.getCustomerId());
		conf.getPref().setCurrentCustomerDataId(selected.getId());
		finish();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}

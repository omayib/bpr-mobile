package com.bpr.view.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ackbox.event.ApproveAckFailed;
import com.ackbox.event.ApproveAckRequested;
import com.ackbox.event.ApproveAckSucceded;
import com.ackbox.event.GetSummaryFailed;
import com.ackbox.event.GetSummarySucceeded;
import com.ackbox.event.RejectAckFailed;
import com.ackbox.event.RejectAckRequested;
import com.ackbox.event.RejectAckSucceeded;
import com.bpr.R;
import com.bpr.adapter.ListProposalAdapter;
import com.bpr.adapter.NotesAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.application.ConfigApplication.BOX_SELECTED;
import com.bpr.dialog.AssigntoDialog;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.BoxController;
import com.bpr.integration.controller.UserController;
import com.bpr.utils.ListviewUtils;
import com.customer.event.BoxData;
import com.customer.event.ProposalCollectionLoadFailed;
import com.customer.event.ProposalCollectionLoadSucceeded;
import com.customer.event.ProposalDetailLoadFailed;
import com.customer.event.ProposalDetailLoaded;
import com.customer.model.Note;
import com.customer.model.ProposalLite;
import com.customer.model.Recommendation;
import com.followupbox.event.ApproveFollowupFailed;
import com.followupbox.event.ApproveFollowupRequested;
import com.followupbox.event.ApproveFollowupSucceded;
import com.followupbox.event.RejectFollowupFailed;
import com.followupbox.event.RejectFollowupRequested;
import com.followupbox.event.RejectFollowupSucceeded;
import com.newbox.event.AssignFailed;
import com.newbox.event.AssignRequested;
import com.newbox.event.AssignSucceeded;
import com.newbox.event.LoadListAoFailed;
import com.newbox.event.LoadListAoSucceded;
import com.newbox.event.ReassignRequested;

public class ListProposalActivity extends FragmentActivity {
	private ListProposalAdapter proposalAdapter;
	private NotesAdapter noteAdapter;
	private ArrayList<ProposalLite> proposals = new ArrayList<ProposalLite>();
	private ArrayList<Note> listNotes = new ArrayList<Note>();
	private ListView listviewProposals, listviewNotes;
	private TextView name, plafon;

	private ConfigApplication conf;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private UserController controller;
	private BoxController boxController;
	private ProgressDialog pd;
	private boolean isAssignMode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_box_selected_customer);

		conf = (ConfigApplication) getApplication();
		controller = conf.getUserController();
		boxController = conf.getBoxController();
		eventPublisher = conf.getEventPublisher();
		pd = new ProgressDialog(this);
		pd.setMessage("please wait...");

		// setting up the actionbar
		ActionBar ab = getActionBar();
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		ab.setCustomView(R.layout.action_bar_custom);
		ab.setLogo(R.drawable.ic_launcher);
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(true);

		proposalAdapter = new ListProposalAdapter(this,
				R.layout.item_list_checkedby, proposals);
		noteAdapter = new NotesAdapter(this, R.layout.item_note, listNotes);

		listviewNotes = (ListView) findViewById(R.id.listView_notes);
		listviewProposals = (ListView) findViewById(R.id.listView_proposal);
		name = (TextView) findViewById(R.id.item_listproposal_name);
		plafon = (TextView) findViewById(R.id.item_listproposal_plafon);

		listviewProposals.setAdapter(proposalAdapter);
		listviewProposals.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {

				ProposalLite selectedProposal = (ProposalLite) arg0
						.getAdapter().getItem(arg2);

				String surveyor = selectedProposal.getUser().getName();
				String me = conf.getPref().getUser().getName();
				System.out
						.println("proposal clicked:" + surveyor + " vs " + me);
				if (me.equalsIgnoreCase(surveyor)) {
					conf.getPref().setReadOnly(false);
				} else {
					conf.getPref().setReadOnly(true);
				}

				controller.requestProposalDetail(conf.getPref().getToken(),
						selectedProposal.getIdProposal(), conf.getPref()
								.isReadOnly());
				pd.show();

			}
		});
		listviewNotes.setEmptyView(findViewById(R.id.empty_notes));
		listviewNotes.setAdapter(noteAdapter);

		registeringSubscribers();

		pd.show();
		// controller.requestListOfProposal(conf.getPref().getToken(), conf
		// .getPref().getCurrentCustomer());
		
		Bundle b = getIntent().getExtras();
		BoxData bd = (BoxData) b.getSerializable("customerSelected");

		controller.requestListOfProposal(conf.getPref().getToken(), bd.getId());
	}

	private void registeringSubscribers() {
		addEventSubscriber(new EventSubscriber<ProposalCollectionLoadFailed>() {

			@Override
			public void handleEvent(ProposalCollectionLoadFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(ListProposalActivity.this,
						"sorry, something went wrong", Toast.LENGTH_SHORT)
						.show();

			}
		});
		addEventSubscriber(new EventSubscriber<ProposalCollectionLoadSucceeded>() {

			@Override
			public void handleEvent(ProposalCollectionLoadSucceeded event) {
				System.out.println("CALLED");
				if (pd.isShowing())
					pd.dismiss();
				proposals.addAll(event.proposals);
				name.setText(event.name);
				plafon.setText(event.plafon);

				ArrayList<Recommendation> r = (ArrayList<Recommendation>) event.recomends;
				for (Recommendation recommendation : r) {
					recommendation.getNotes();
					Note n = new Note.Builder()
							.creator(recommendation.getUser())
							.messages(recommendation.getNotesAsString())
							.build();
					listNotes.add(n);
				}

				noteAdapter.notifyDataSetChanged();
				proposalAdapter.notifyDataSetChanged();
				ListviewUtils
						.setListViewHeightBasedOnChildren(listviewProposals);

			}
		});
		addEventSubscriber(new EventSubscriber<ProposalDetailLoaded>() {

			@Override
			public void handleEvent(ProposalDetailLoaded event) {
				if (pd.isShowing())
					pd.dismiss();
				System.out.println("detail proposal::"
						+ event.proposal.toString());

				Bundle b = new Bundle();
				b.putSerializable("currentProposal", event.proposal);
				if (ConfigApplication.currentBoxSelected
						.equals(BOX_SELECTED.PROPOSAL)) {
					startActivity(new Intent(getApplicationContext(),
							ProposalActivity.class).putExtras(b));
				} else {
					startActivity(new Intent(getApplicationContext(),
							CustomerDetailActivity.class).putExtras(b));
				}
			}
		});
		addEventSubscriber(new EventSubscriber<ProposalDetailLoadFailed>() {

			@Override
			public void handleEvent(ProposalDetailLoadFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(ListProposalActivity.this, event.exception,
						Toast.LENGTH_SHORT).show();
			}
		});
		addEventSubscriber(new EventSubscriber<LoadListAoSucceded>() {

			@Override
			public void handleEvent(LoadListAoSucceded event) {
				System.out.println("ao loadded");
				if (pd.isShowing())
					pd.dismiss();
				AssigntoDialog dialog = new AssigntoDialog(event.user,
						eventPublisher, conf.getPref().getToken(), conf
								.getPref().getCurrentCustomerDataId(), isAssignMode);
				dialog.show(getSupportFragmentManager(), "dialog");
			}
		});

		addEventSubscriber(new EventSubscriber<LoadListAoFailed>() {

			@Override
			public void handleEvent(LoadListAoFailed event) {
				System.out.println("ao failed load");
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(getApplicationContext(), "sorry, try again",
						Toast.LENGTH_SHORT).show();
			}
		});

		addEventSubscriber(new EventSubscriber<AssignFailed>() {

			@Override
			public void handleEvent(AssignFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(getApplicationContext(),
						"sorry, failed to assign", Toast.LENGTH_SHORT).show();
			}
		});

		addEventSubscriber(new EventSubscriber<AssignSucceeded>() {

			@Override
			public void handleEvent(AssignSucceeded event) {
				if (pd.isShowing())
					pd.dismiss();
				startActivity(new Intent(ListProposalActivity.this,
						CustomerListActivity.class));
				finish();

			}
		});
		addEventSubscriber(new EventSubscriber<GetSummarySucceeded>() {

			@Override
			public void handleEvent(GetSummarySucceeded event) {
				if (pd.isShowing())
					pd.dismiss();
				Bundle b = new Bundle();
				b.putSerializable("summary", event.summary);
				startActivity(new Intent(ListProposalActivity.this,
						ApprovalParentActivty.class).putExtras(b));

			}
		});
		addEventSubscriber(new EventSubscriber<GetSummaryFailed>() {

			@Override
			public void handleEvent(GetSummaryFailed event) {
				// TODO Auto-generated method stub
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(getApplicationContext(), event.exception,
						Toast.LENGTH_SHORT).show();

			}
		});
		addEventSubscriber(new EventSubscriber<ApproveFollowupRequested>() {

			@Override
			public void handleEvent(ApproveFollowupRequested event) {
				pd.show();
				boxController.approveFollowupBox(conf.getPref().getToken(),
						conf.getPref().getCurrentCustomerDataId(),
						event.summaryCandidate);
			}
		});
		addEventSubscriber(new EventSubscriber<ApproveAckRequested>() {

			@Override
			public void handleEvent(ApproveAckRequested event) {
				pd.show();
				boxController.ackBoxAcknowladgement(conf.getPref().getToken(),
						conf.getPref().getCurrentCustomerDataId(),
						event.summaryCandidate);
			}
		});
		addEventSubscriber(new EventSubscriber<RejectAckRequested>() {

			@Override
			public void handleEvent(RejectAckRequested event) {
				pd.show();
				boxController
						.ackBoxReject(conf.getPref().getToken(), conf.getPref()
								.getCurrentCustomerDataId(), event.summaryCandidate);
			}
		});
		addEventSubscriber(new EventSubscriber<RejectFollowupRequested>() {

			@Override
			public void handleEvent(RejectFollowupRequested event) {
				pd.show();
				boxController
						.rejectFollowupBox(conf.getPref().getToken(), conf
								.getPref().getCurrentCustomerDataId(),
								event.summaryCandidate);
			}
		});
		addEventSubscriber(new EventSubscriber<ApproveFollowupSucceded>() {

			@Override
			public void handleEvent(ApproveFollowupSucceded event) {
				if (pd.isShowing())
					pd.dismiss();
				startActivity(new Intent(ListProposalActivity.this,
						CustomerListActivity.class));
				finish();
			}
		});
		addEventSubscriber(new EventSubscriber<ApproveFollowupFailed>() {

			@Override
			public void handleEvent(ApproveFollowupFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(ListProposalActivity.this,
						"sorry something wrong", Toast.LENGTH_SHORT).show();
			}
		});
		addEventSubscriber(new EventSubscriber<ApproveAckSucceded>() {

			@Override
			public void handleEvent(ApproveAckSucceded event) {
				if (pd.isShowing())
					pd.dismiss();
				startActivity(new Intent(ListProposalActivity.this,
						CustomerListActivity.class));
				finish();
			}
		});
		addEventSubscriber(new EventSubscriber<ApproveAckFailed>() {

			@Override
			public void handleEvent(ApproveAckFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(ListProposalActivity.this,
						"sorry something wrong", Toast.LENGTH_SHORT).show();
			}
		});
		addEventSubscriber(new EventSubscriber<RejectAckSucceeded>() {

			@Override
			public void handleEvent(RejectAckSucceeded event) {
				if (pd.isShowing())
					pd.dismiss();
				startActivity(new Intent(ListProposalActivity.this,
						CustomerListActivity.class));
				finish();
			}
		});
		addEventSubscriber(new EventSubscriber<RejectAckFailed>() {

			@Override
			public void handleEvent(RejectAckFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(ListProposalActivity.this, event.exception,
						Toast.LENGTH_SHORT).show();

			}
		});
		addEventSubscriber(new EventSubscriber<RejectFollowupFailed>() {

			@Override
			public void handleEvent(RejectFollowupFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(ListProposalActivity.this, event.exeption,
						Toast.LENGTH_SHORT).show();

			}
		});
		addEventSubscriber(new EventSubscriber<RejectFollowupSucceeded>() {

			@Override
			public void handleEvent(RejectFollowupSucceeded event) {
				if (pd.isShowing())
					pd.dismiss();
				startActivity(new Intent(ListProposalActivity.this,
						CustomerListActivity.class));
				finish();
			}
		});
		addEventSubscriber(new EventSubscriber<AssignRequested>() {

			@Override
			public void handleEvent(AssignRequested event) {
				pd.show();
				boxController.assign(event.token, event.idcustomer, event.idAo,
						event.note);
			}
		});
		addEventSubscriber(new EventSubscriber<ReassignRequested>() {

			@Override
			public void handleEvent(ReassignRequested event) {
				pd.show();
				boxController.reassign(event.token, event.idcustomer,
						event.idAo, event.note);
			}
		});
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		removeEventSubscribers();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		if (ConfigApplication.currentBoxSelected == null)
			return false;

		MenuInflater inflater = getMenuInflater();

		if (ConfigApplication.currentBoxSelected.equals(BOX_SELECTED.NEW)) {
			inflater.inflate(R.menu.page_new, menu);
		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.PROPOSAL)) {
		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.FOLLOWUP)) {

			inflater.inflate(R.menu.page_followup_head, menu);
			/*
			 * if (conf.getPref().getUser().getLevel()
			 * .equals(UserLevel.HEAD_OF_MARKETING)) {
			 * inflater.inflate(R.menu.page_followup_head, menu); } else {
			 * inflater.inflate(R.menu.page_followup, menu); }
			 */
		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.ALL)) {
		} else if (ConfigApplication.currentBoxSelected
				.equals(BOX_SELECTED.ACK)) {
			inflater.inflate(R.menu.page_followup, menu);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.followup_reassign:
			pd.show();
			boxController.getListAo(conf.getPref().getToken(), conf.getPref()
					.getCurrentCustomerDataId());
			isAssignMode = false;
			break;
		case R.id.new_assignto:
			pd.show();
			boxController.getListAo(conf.getPref().getToken(), conf.getPref()
					.getCurrentCustomerDataId());
			isAssignMode = true;
			break;

		case android.R.id.home:
			finish();
			break;
		case R.id.followup_approve:
			pd.show();
			boxController.ackBoxGetSummary(conf.getPref().getToken(), conf
					.getPref().getCurrentCustomerDataId());
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}

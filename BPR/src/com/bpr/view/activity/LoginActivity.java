package com.bpr.view.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.application.ConfigApplication;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.user.event.SigninFailed;
import com.user.event.SigninSucceeded;

public class LoginActivity extends Activity {
	private EditText inUsername, inPassword;
	private Button bLogin;
	private ConfigApplication config;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private ProgressDialog pd;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);

		config = (ConfigApplication) getApplication();
		userController = config.getUserController();
		eventPublisher = config.getEventPublisher();
		pd = new ProgressDialog(this);

		inUsername = (EditText) findViewById(R.id.login_email_activity);
		inPassword = (EditText) findViewById(R.id.login_password_activity);
		bLogin = (Button) findViewById(R.id.login_btn_activity);
		bLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				processLogin();
			}
		});
		registerSubscribers();
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		removeEventSubscribers();
	}

	private void registerSubscribers() {
		addEventSubscriber(new EventSubscriber<SigninSucceeded>() {

			@Override
			public void handleEvent(final SigninSucceeded event) {
				if (pd.isShowing())
					pd.dismiss();
				config.getPref()
						.setUser(event.user, event.token, event.listBox);
				config.prepareBox();
				startActivity(new Intent(LoginActivity.this,
						DashboardActivity.class));
				config.startForegroundService();
				finish();
			}
		});
		addEventSubscriber(new EventSubscriber<SigninFailed>() {

			@Override
			public void handleEvent(SigninFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(LoginActivity.this, event.exeption,
						Toast.LENGTH_SHORT).show();
			}
		});
	}

	protected void processLogin() {
		if (!inPassword.getText().toString().isEmpty()
				&& !inUsername.getText().toString().isEmpty()) {
			pd.show();
			userController.signin(inUsername.getText().toString(), inPassword
					.getText().toString());
		}
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}

package com.bpr.view.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.bpr.view.fragment.FragmentAssets;
import com.bpr.view.fragment.FragmentBusinessParent;
import com.bpr.view.fragment.FragmentCapacity;
import com.bpr.view.fragment.FragmentCollateralBpkb;
import com.bpr.view.fragment.FragmentCollateralDeposito;
import com.bpr.view.fragment.FragmentCollateralShm;
import com.bpr.view.fragment.FragmentCollateralTabungan;
import com.bpr.view.fragment.FragmentEnvironmentalCheckup;
import com.bpr.view.fragment.FragmentJob;
import com.bpr.view.fragment.FragmentPersonal;
import com.bpr.view.fragment.FragmentPhotoBusiness;
import com.bpr.view.fragment.FragmentPhotoCollateral;
import com.bpr.view.fragment.FragmentPhotoHome;
import com.bpr.view.fragment.FragmentPhotoOther;
import com.bpr.view.fragment.FragmentPhotoSid;
import com.bpr.view.fragment.FragmentProposalDefault;
import com.bpr.view.fragment.FragmentPurpose;
import com.bpr.view.fragment.FragmentSubmissionCredit;
import com.customer.model.Proposal;
import com.drawer.nav.AbstractNavDrawerActivity;
import com.drawer.nav.NavDrawerActivityConfiguration;
import com.drawer.nav.NavDrawerExpandableAdapter;
import com.drawer.nav.NavDrawerItem;
import com.drawer.nav.NavMenuItem;
import com.drawer.nav.NavMenuSection;
import com.proposalbox.event.MarkProposalAsCompleted;
import com.proposalbox.event.MarkSummaryAsCompleted;
import com.proposalbox.event.ProposalCandidateIsNotValid;
import com.proposalbox.event.ProposalCompleteFailed;
import com.proposalbox.event.ProposalUploadFailed;
import com.proposalbox.event.ProposalUploadSucceeded;
import com.proposalbox.event.SidRequested;
import com.proposalbox.event.SummaryUploadFailed;
import com.proposalbox.event.SummaryUploadSucceded;

public class ProposalActivity extends AbstractNavDrawerActivity {
	private Proposal proposal;
	private ConfigApplication conf;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private ProgressDialog pd;
	private UserController userController;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		conf = (ConfigApplication) getApplication();
		eventPublisher = conf.getEventPublisher();
		userController = conf.getUserController();

		pd = new ProgressDialog(this);
		pd.setMessage("please wait...");

		ActionBar ab = getActionBar();
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		ab.setCustomView(R.layout.action_bar_custom);
		ab.setLogo(R.drawable.ic_launcher);

		Bundle b = getIntent().getExtras();
		if (b != null) {
			if (b.containsKey("currentProposal")) {
				proposal = (Proposal) b.getSerializable("currentProposal");
				conf.getStorageController().saveProposal(proposal.getId());
				// set the proposals id accessable from framgents
				conf.getPref().setCurrentProposal(proposal.getId());
			}
		}

		getSupportFragmentManager().beginTransaction()
				.replace(R.id.content_frame, new FragmentProposalDefault())
				.commit();

		registerSubscribers();
	}

	private void registerSubscribers() {
		addEventSubscriber(new EventSubscriber<ProposalUploadSucceeded>() {

			@Override
			public void handleEvent(ProposalUploadSucceeded event) {
				if (pd.isShowing())
					pd.dismiss();
				System.out.println("summary obj::" + event.summary.toString());
				Bundle b = new Bundle();
				b.putSerializable("summary", event.summary);
				startActivity(new Intent(ProposalActivity.this,
						ApprovalParentActivty.class).putExtras(b));
			}
		});
		addEventSubscriber(new EventSubscriber<ProposalUploadFailed>() {

			@Override
			public void handleEvent(ProposalUploadFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				System.out.println(event.exception);
				Toast.makeText(ProposalActivity.this,
						"sorry,  something went wrong", Toast.LENGTH_SHORT)
						.show();
			}
		});
		addEventSubscriber(new EventSubscriber<ProposalCompleteFailed>() {

			@Override
			public void handleEvent(ProposalCompleteFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(ProposalActivity.this,
						"sorry,  something went wrong", Toast.LENGTH_SHORT)
						.show();
			}
		});

		addEventSubscriber(new EventSubscriber<SummaryUploadSucceded>() {

			@Override
			public void handleEvent(SummaryUploadSucceded event) {
				if (pd.isShowing())
					pd.dismiss();
				conf.getPref().resetCurrentProposal();
				conf.getPref().resetCurrentCustomer();
				startActivity(new Intent(ProposalActivity.this,
						CustomerListActivity.class));
				finish();
			}
		});
		addEventSubscriber(new EventSubscriber<SummaryUploadFailed>() {

			@Override
			public void handleEvent(SummaryUploadFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(ProposalActivity.this, event.exception,
						Toast.LENGTH_SHORT).show();

			}
		});
		addEventSubscriber(new EventSubscriber<SidRequested>() {

			@Override
			public void handleEvent(SidRequested event) {
				System.out.println("sid requested from proposal activity");
				conf.getBoxController().getSids(conf.getPref().getToken(),
						conf.getPref().getCurrentCustomerId_());
			}
		});
		addEventSubscriber(new EventSubscriber<MarkSummaryAsCompleted>() {

			public void handleEvent(MarkSummaryAsCompleted event) {
				if (!pd.isShowing())
					pd.show();
				userController.uploadSummary(conf.getPref()
						.getCurrentProposal(), event.summaryCandidate);
			}
		});

		addEventSubscriber(new EventSubscriber<ProposalCandidateIsNotValid>() {

			@Override
			public void handleEvent(ProposalCandidateIsNotValid event) {
				if (pd.isShowing())
					pd.dismiss();

				Toast.makeText(ProposalActivity.this, event.exception,
						Toast.LENGTH_SHORT).show();
			}
		});
	}

	@Override
	protected NavDrawerActivityConfiguration getNavDrawerConfiguration() {

		NavDrawerItem[] menu = new NavDrawerItem[] {
				NavMenuSection.create(100, "Profil Nasabah"),
				NavMenuItem.create(101, "Data Personal", "navdrawer_friends",
						true, this),
				NavMenuItem
						.create(103, "Aset", "navdrawer_airport", true, this),
				NavMenuItem.create(104, "Pekerjaan (karyawan)",
						"navdrawer_airport", true, this),
				NavMenuItem.create(105, "Bisnis (wirausaha)",
						"navdrawer_airport", true, this),
				NavMenuSection.create(200, "Tujuan & Kapasitas"),
				NavMenuItem.create(201, "pengajuan baru", "navdrawer_rating",
						true, this),
				NavMenuItem.create(202, "tujuan kredit", "navdrawer_eula",
						true, this),
				NavMenuItem.create(203, "kapasitas", "navdrawer_quit", true,
						this),

				NavMenuSection.create(400, "Detil Jaminan"),
				NavMenuItem.create(402, "jaminan tanah", "navdrawer_rating",
						true, this),
				NavMenuItem.create(403, "jaminan BPKB", "navdrawer_rating",
						true, this),
				NavMenuItem.create(404, "jaminan deposito", "navdrawer_eula",
						true, this),
				NavMenuItem.create(405, "jaminan tabungan", "navdrawer_eula",
						true, this),
				NavMenuItem.create(406, "cek lingkungan", "navdrawer_eula",
						true, this),
				NavMenuSection.create(500, "Foto"),
				NavMenuItem.create(505, "sid", "navdrawer_rating", true, this),
				NavMenuItem.create(501, "jaminan", "navdrawer_rating", true,
						this),
				NavMenuItem.create(502, "usaha", "navdrawer_eula", true, this),
				NavMenuItem.create(503, "Tempat tinggal", "navdrawer_quit",
						true, this),
				NavMenuItem.create(504, "Lain-lain", "navdrawer_quit", true,
						this),
				NavMenuSection.create(600, "Sinkronisasi"),
				NavMenuItem.create(601, "Kumpulkan", "navdrawer_quit", true,
						this) };

		NavDrawerActivityConfiguration navDrawerActivityConfiguration = new NavDrawerActivityConfiguration();
		navDrawerActivityConfiguration
				.setMainLayout(R.layout.activity_proposal);
		navDrawerActivityConfiguration.setDrawerLayoutId(R.id.drawer_layout);
		navDrawerActivityConfiguration.setLeftDrawerId(R.id.left_drawer);
		navDrawerActivityConfiguration.setNavItems(menu);
		navDrawerActivityConfiguration
				.setDrawerShadow(R.drawable.drawer_shadow);
		navDrawerActivityConfiguration.setDrawerOpenDesc(R.string.drawer_open);
		navDrawerActivityConfiguration
				.setDrawerCloseDesc(R.string.drawer_close);
		navDrawerActivityConfiguration
				.setExpendableAdapter(new NavDrawerExpandableAdapter(this,
						R.layout.navdrawer_item, menu));

		// navDrawerActivityConfiguration.setBaseAdapter(new NavDrawerAdapter(
		// this, R.layout.navdrawer_item, menu));
		return navDrawerActivityConfiguration;
	}

	@Override
	protected void onNavItemSelected(int id) {
		switch ((int) id) {
		case 101:
			Fragment f = new FragmentPersonal();
			Bundle bundle = new Bundle();
			bundle.putSerializable(FragmentPage.TAG_PERSONAL,
					proposal.getPersonal());
			f.setArguments(bundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, f).commit();
			break;

		case 103:
			Fragment asset = new FragmentAssets();
			Bundle bAssets = new Bundle();
			bAssets.putSerializable(FragmentPage.TAG_ASSETS,
					proposal.getAssets());
			asset.setArguments(bAssets);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, asset).commit();
			break;
		case 104:
			Fragment job = new FragmentJob();
			Bundle bJob = new Bundle();
			bJob.putSerializable(FragmentPage.TAG_JOB, proposal.getJob());
			job.setArguments(bJob);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, job).commit();
			break;
		case 105:
			Fragment fBusiness = new FragmentBusinessParent();
			Bundle bBusiness = new Bundle();
			bBusiness.putSerializable(FragmentPage.TAG_BUSINESS,
					proposal.getBusinessCollection());
			fBusiness.setArguments(bBusiness);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fBusiness).commit();
			break;
		case 201:
			Fragment fSub = new FragmentSubmissionCredit();
			Bundle aBundle = new Bundle();
			aBundle.putSerializable(FragmentPage.TAG_SUMBISSION,
					proposal.getSubmission());
			fSub.setArguments(aBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fSub).commit();
			break;
		case 202:
			Fragment fPurpose = new FragmentPurpose();
			Bundle pBundle = new Bundle();
			pBundle.putSerializable(FragmentPage.TAG_PURPOSE,
					proposal.getPurposeCollection());
			fPurpose.setArguments(pBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fPurpose).commit();
			break;
		case 203:
			Fragment fCap = new FragmentCapacity();
			Bundle capBundle = new Bundle();
			capBundle.putSerializable(FragmentPage.TAG_CAPACITY,
					proposal.getCapacity());
			fCap.setArguments(capBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fCap).commit();
			break;
		case 402:
			Fragment fShm = new FragmentCollateralShm();
			Bundle shmBundle = new Bundle();
			shmBundle.putSerializable(FragmentPage.TAG_COLLATERAL_SHM,
					proposal.getShmCollection());
			fShm.setArguments(shmBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fShm).commit();
			break;
		case 403:
			Fragment fBpkb = new FragmentCollateralBpkb();
			Bundle bpkbBundle = new Bundle();
			bpkbBundle.putSerializable(FragmentPage.TAG_COLLATERAL_BPKB,
					proposal.getBpkbCollection());
			fBpkb.setArguments(bpkbBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fBpkb).commit();
			break;
		case 404:
			Fragment fDp = new FragmentCollateralDeposito();
			Bundle dpBundle = new Bundle();
			dpBundle.putSerializable(FragmentPage.TAG_COLLATERAL_DEPOSITO,
					proposal.getDeposito());
			fDp.setArguments(dpBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fDp).commit();
			break;
		case 405:
			Fragment fTabungan = new FragmentCollateralTabungan();
			Bundle bTabungan = new Bundle();
			bTabungan.putSerializable(FragmentPage.TAG_COLLATERAL_TABUNGAN,
					proposal.getTabungan());
			fTabungan.setArguments(bTabungan);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fTabungan).commit();
			break;
		case 406:
			Fragment fCheck = new FragmentEnvironmentalCheckup();
			Bundle checkBundle = new Bundle();
			checkBundle.putSerializable(FragmentPage.TAG_ENVIRONMENT_CHECK,
					proposal.getEnviromental());
			fCheck.setArguments(checkBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fCheck).commit();
			break;
		case 501:
			Fragment fPhoto = new FragmentPhotoCollateral();
			Bundle photoBundle = new Bundle();
			photoBundle.putSerializable(FragmentPage.TAG_PHOTO_GUARANTEE,
					proposal.getPhotoGuarantee());
			fPhoto.setArguments(photoBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fPhoto).commit();
			break;
		case 502:
			Fragment fPhotoBusiness = new FragmentPhotoBusiness();
			Bundle pBusinessBundle = new Bundle();
			pBusinessBundle.putSerializable(FragmentPage.TAG_PHOTO_BUSINESS,
					proposal.getPhotoBusiness());
			fPhotoBusiness.setArguments(pBusinessBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fPhotoBusiness).commit();
			break;
		case 503:
			Fragment fPhotoHome = new FragmentPhotoHome();
			Bundle photoHomeBundle = new Bundle();
			photoHomeBundle.putSerializable(FragmentPage.TAG_PHOTO_HOME,
					proposal.getPhotoHome());
			fPhotoHome.setArguments(photoHomeBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fPhotoHome).commit();
			break;
		case 504:
			Fragment fPhotoOther = new FragmentPhotoOther();
			Bundle photoOtherBundle = new Bundle();
			photoOtherBundle.putSerializable(FragmentPage.TAG_PHOTO_OTHER,
					proposal.getPhotoOther());
			fPhotoOther.setArguments(photoOtherBundle);
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, fPhotoOther).commit();
			break;
		case 601:
			if (conf.getPref().isReadOnly()) {
				Toast.makeText(ProposalActivity.this,
						"sorry,  you are not allowed to do it",
						Toast.LENGTH_SHORT).show();
				return;
			}
			try {
				if (!pd.isShowing())
					pd.show();
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("kumpulkan");
			conf.getEventPublisher().publish(
					new MarkProposalAsCompleted(proposal.getId(), conf
							.getPref().getToken()));
			break;
		case 505:
			Fragment photoSod = new FragmentPhotoSid();
			getSupportFragmentManager().beginTransaction()
					.replace(R.id.content_frame, photoSod).commit();
			break;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		conf.getPref().resetCurrentProposal();
		removeEventSubscribers();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		System.out.println("result activity requestCode:" + requestCode);
		System.out.println("result activity resultCode:" + resultCode);
		System.out.println("result activity RESULT_OK:"
				+ FragmentActivity.RESULT_OK);
		System.out.println("result activity RESULT_CANCELED:"
				+ FragmentActivity.RESULT_CANCELED);
		if (data == null) {
			System.out.println("result null");
			return;
		}
		Bundle b = data.getExtras();
		System.out.println("result activity " + b.getString("cobaa"));
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}

package com.bpr.view.activity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.bpr.R;
import com.bpr.adapter.FragmentPage;
import com.bpr.adapter.FragmentPagerAdapter;
import com.bpr.adapter.PageDescriptor;
import com.bpr.application.ConfigApplication;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.customer.event.ProposalDetailLoaded;
import com.customer.model.Proposal;
import com.proposalbox.event.SidRequested;

public class CustomerDetailActivity extends FragmentActivity {
	private FragmentPagerAdapter adapter;
	private ArrayList<PageDescriptor> listPages = new ArrayList<PageDescriptor>();
	private ViewPager pager;
	private TextView titleFragment;
	private Button bBack, bNext;
	private Proposal currentProposal;

	private ConfigApplication conf;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private UserController controller;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_form_parent);

		// setting up the actionbar
		ActionBar ab = getActionBar();
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
		ab.setCustomView(R.layout.action_bar_custom);
		ab.setLogo(R.drawable.ic_launcher);
		ab.setDisplayHomeAsUpEnabled(true);
		ab.setDisplayShowHomeEnabled(true);

		conf = (ConfigApplication) getApplication();
		controller = conf.getUserController();
		eventPublisher = conf.getEventPublisher();

		Bundle b = getIntent().getExtras();
		if (b != null) {
			if (b.containsKey("currentProposal")) {
				currentProposal = (Proposal) b
						.getSerializable("currentProposal");
			}
		}

		titleFragment = (TextView) findViewById(R.id.fragment_title);
		pager = (ViewPager) findViewById(R.id.pager);
		bBack = (Button) findViewById(R.id.bBack);
		bNext = (Button) findViewById(R.id.bNext);

		adapter = new FragmentPagerAdapter(getSupportFragmentManager(),
				listPages);
		pager.setAdapter(adapter);
		pager.setOnPageChangeListener(new OnPageChangeListener() {

			@Override
			public void onPageSelected(int arg0) {
				titleFragment.setText(pager.getAdapter().getPageTitle(arg0));
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {

			}

			@Override
			public void onPageScrollStateChanged(int arg0) {

			}
		});
		bBack.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				changeSlide(false);
			}
		});
		bNext.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				changeSlide(true);
			}
		});
		// titleFragment.setText(adapter.getPageTitle(pager.getCurrentItem()));

		registeringSubscribers();
		updateView();
	}

	private void updateView() {
		Proposal proposal = currentProposal;
		FragmentPage<Serializable> page1 = new FragmentPage<Serializable>(
				"Personal", FragmentPage.TAG_PERSONAL, proposal.getPersonal());
		FragmentPage<Serializable> page2 = new FragmentPage<Serializable>(
				"Pekerjaan", FragmentPage.TAG_JOB, proposal.getJob());
		FragmentPage<Serializable> page3 = new FragmentPage<Serializable>(
				"Bisnis", FragmentPage.TAG_BUSINESS,
				proposal.getBusinessCollection());
		FragmentPage<Serializable> page4 = new FragmentPage<Serializable>(
				"Aset", FragmentPage.TAG_ASSETS, proposal.getAssets());
		FragmentPage<Serializable> page5 = new FragmentPage<Serializable>(
				"Kapasitas", FragmentPage.TAG_CAPACITY, proposal.getCapacity());
		FragmentPage<Serializable> page6 = new FragmentPage<Serializable>(
				"Pengajuan kredit", FragmentPage.TAG_SUMBISSION,
				proposal.getSubmission());
		FragmentPage<Serializable> page7 = new FragmentPage<Serializable>(
				"Tujuan kredit", FragmentPage.TAG_PURPOSE,
				proposal.getPurposeCollection());
		FragmentPage<Serializable> page8 = new FragmentPage<Serializable>(
				"Jaminan Bpkb", FragmentPage.TAG_COLLATERAL_BPKB,
				proposal.getBpkbCollection());
		FragmentPage<Serializable> page9 = new FragmentPage<Serializable>(
				"Jaminan SHM", FragmentPage.TAG_COLLATERAL_SHM,
				proposal.getShmCollection());
		FragmentPage<Serializable> page10 = new FragmentPage<Serializable>(
				"Jaminan Deposito", FragmentPage.TAG_COLLATERAL_DEPOSITO,
				proposal.getDeposito());
		FragmentPage<Serializable> page11 = new FragmentPage<Serializable>(
				"Jaminan Tabungan", FragmentPage.TAG_COLLATERAL_TABUNGAN,
				proposal.getTabungan());
		FragmentPage<Serializable> page12 = new FragmentPage<Serializable>(
				"Cek lingkungan", FragmentPage.TAG_ENVIRONMENT_CHECK,
				proposal.getEnviromental());
		// =========set photo
		FragmentPage<Serializable> page17 = new FragmentPage<Serializable>(
				"Photo SID", FragmentPage.TAG_PHOTO_SID, null);
		FragmentPage<Serializable> page13 = new FragmentPage<Serializable>(
				"Photo Jaminan", FragmentPage.TAG_PHOTO_GUARANTEE,
				proposal.getPhotoGuarantee());
		FragmentPage<Serializable> page14 = new FragmentPage<Serializable>(
				"Photo Bisnis", FragmentPage.TAG_PHOTO_BUSINESS,
				proposal.getPhotoBusiness());
		FragmentPage<Serializable> page15 = new FragmentPage<Serializable>(
				"Photo Rumah", FragmentPage.TAG_PHOTO_HOME,
				proposal.getPhotoHome());
		FragmentPage<Serializable> page16 = new FragmentPage<Serializable>(
				"Photo Lainnya", FragmentPage.TAG_PHOTO_OTHER,
				proposal.getPhotoOther());

		adapter.insert(page1, 0);
		adapter.insert(page2, 1);
		adapter.insert(page3, 2);
		adapter.insert(page4, 3);
		adapter.insert(page5, 4);
		adapter.insert(page6, 5);
		adapter.insert(page7, 6);
		adapter.insert(page8, 7);
		adapter.insert(page9, 8);
		adapter.insert(page10, 9);
		adapter.insert(page11, 10);
		adapter.insert(page12, 11);
		adapter.insert(page17, 12);
		adapter.insert(page13, 12);
		adapter.insert(page14, 13);
		adapter.insert(page15, 14);
		adapter.insert(page16, 15);

		titleFragment.setText(pager.getAdapter().getPageTitle(0));
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		removeEventSubscribers();
	}

	private void registeringSubscribers() {

		addEventSubscriber(new EventSubscriber<SidRequested>() {

			@Override
			public void handleEvent(SidRequested event) {
				System.out.println("sid requested from proposal activity");
				conf.getBoxController().getSids(conf.getPref().getToken(),
						conf.getPref().getCurrentCustomerId_());
			}
		});
	}

	protected void changeSlide(boolean isNext) {
		int totalPage = adapter.getCount();
		int currentPage = pager.getCurrentItem();// start from 0
		System.out.println("total " + totalPage + "; current " + currentPage);
		if (isNext) {
			if (currentPage == totalPage) {
				return;
			}
			currentPage += 1;
			System.out.println("next ::" + currentPage);
			pager.setCurrentItem(currentPage, true);
		} else {
			if (currentPage == 0) {
				return;
			}
			currentPage -= 1;
			System.out.println("next ::" + currentPage);
			pager.setCurrentItem(currentPage, true);

		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}

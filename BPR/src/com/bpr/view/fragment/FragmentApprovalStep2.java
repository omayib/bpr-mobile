package com.bpr.view.fragment;

import java.util.ArrayList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ListView;

import com.bpr.R;
import com.bpr.adapter.CollateralBpkbAdapter;
import com.bpr.adapter.CollateralShmAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.utils.ListviewUtils;
import com.bpr.view.activity.ApprovalParentActivty;
import com.bpr.view.activity.ApprovalParentActivty.ApprovalSlidedListener;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralShm;
import com.customer.model.Summary;

public class FragmentApprovalStep2 extends Fragment implements
		ApprovalSlidedListener {
	private ListView summ_listview_shm, summ_listview_bpkb;
	private CheckBox summ_reference;
	private CollateralBpkbAdapter adapterBpkb;
	private CollateralShmAdapter adapterShm;
	private Summary summary;
	private boolean isReadonly;

	private ArrayList<CollateralShm> shmCollections = new ArrayList<CollateralShm>();
	private ArrayList<CollateralBpkb> bpkbCollections = new ArrayList<CollateralBpkb>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ConfigApplication conf = (ConfigApplication) getActivity()
				.getApplication();
		summary = conf.getSummary();
		isReadonly = !conf.isSummaryEditable();
		System.out.println("sumary editable "+isReadonly);

		Log.d("omayib", "step2");
		bpkbCollections = (ArrayList<CollateralBpkb>) summary.getBpkb()
				.getGuaranteeBpkb();
		shmCollections = (ArrayList<CollateralShm>) summary.getShm()
				.getGuaranteeShm();
		ApprovalParentActivty parentActivity = (ApprovalParentActivty) getActivity();
		parentActivity.setSliderListener(this);
		adapterBpkb = new CollateralBpkbAdapter(getActivity(),
				R.layout.item_list_guarantee, bpkbCollections,
				isReadonly);
		adapterShm = new CollateralShmAdapter(getActivity(),
				R.layout.item_list_guarantee, shmCollections, isReadonly);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.approval_step2,
				container, false);

		summ_reference = (CheckBox) v.findViewById(R.id.summary_reference);
		summ_listview_bpkb = (ListView) v
				.findViewById(R.id.summary_list_guarantee_bpkb);
		summ_listview_shm = (ListView) v
				.findViewById(R.id.summary_list_guarantee_shm);
		summ_reference = (CheckBox) v.findViewById(R.id.summary_reference);

		summ_listview_bpkb.setAdapter(adapterBpkb);
		summ_listview_shm.setAdapter(adapterShm);

		ListviewUtils.setListViewHeightBasedOnChildren(summ_listview_bpkb);
		ListviewUtils.setListViewHeightBasedOnChildren(summ_listview_shm);

		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (summary.getReference() != null) {
			summ_reference.setEnabled(true);
			summ_reference.setText(summary.getReference().getName());
			System.out.println("step2::" + summary.getReference().getName());
		} else {
			summ_reference.setText("Tidak ada referensi");
			summ_reference.setEnabled(false);
		}
	}

	@Override
	public void onApprovalPageSlided(int currentPage, int totalPages) {
		System.out.println("page " + currentPage + " saved");
		System.out.println(summary.toString());
	}
}

package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.bpr.R;
import com.bpr.activerecord.model.PhotoFolderActiveRecord;
import com.bpr.adapter.FragmentPage;
import com.bpr.adapter.PhotoCollectionAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddPhotoDialog;
import com.bpr.dialog.AddPhotoDialog.PhotoDialogListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.bpr.photo.viewer.activity.PhotoViewerActivity;
import com.customer.model.Photo;
import com.customer.model.PhotoCollateral;
import com.customer.model.PhotoFolder;
import com.proposalbox.event.FolderGalleryCreateFailed;
import com.proposalbox.event.FolderGalleryCreated;
import com.proposalbox.event.LocalFolderLoaded;
import com.proposalbox.event.PhotoUpdateSucceded;

public class FragmentPhotoCollateral extends Fragment implements
		PhotoDialogListener {
	private PhotoCollectionAdapter adapter;
	private ListView lv;
	private ArrayList<PhotoFolder> listFolder = new ArrayList<PhotoFolder>();
	private ConfigApplication conf;
	private int proposalId;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private UserController user;
	private ProgressDialog pd;
	private PhotoCollateral galleryCollateral;
	private Fragment thisFragment;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		eventPublisher = conf.getEventPublisher();
		user = conf.getUserController();
		pd = new ProgressDialog(getActivity());
		pd.setMessage("please wait...");
		thisFragment = this;
		proposalId = conf.getPref().getCurrentProposal();
		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_PHOTO_GUARANTEE)) {
			galleryCollateral = (PhotoCollateral) b
					.getSerializable(FragmentPage.TAG_PHOTO_GUARANTEE);
			listFolder = (ArrayList<PhotoFolder>) galleryCollateral
					.getFolders();
		}
		adapter = new PhotoCollectionAdapter(getActivity(),
				R.layout.item_photo, listFolder);
		registeringSubcribers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_photo,
				container, false);
		lv = (ListView) v.findViewById(R.id.listview_photo);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				PhotoFolder currentFolder = (PhotoFolder) arg0
						.getItemAtPosition(arg2);

				Intent i = new Intent(getActivity(), PhotoViewerActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("currentFolder", currentFolder);
				b.putString("galleryName", "collateral");
				b.putInt("proposalId", proposalId);

				i.putExtras(b);
				getActivity().startActivityForResult(i, 191);
			}
		});
		lv.setAdapter(adapter);
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		user.getFoldersFromLocal(proposalId, "collateral");
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			AddPhotoDialog photoDialog = new AddPhotoDialog();
			photoDialog.setOnAddPhotoListener(this);
			photoDialog.show(getFragmentManager(), "guaranteePhoto");
			break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		System.out.println("result requestCode:" + requestCode);
		System.out.println("result resultCode:" + resultCode);
		System.out.println("result RESULT_OK:" + FragmentActivity.RESULT_OK);
		System.out.println("result RESULT_CANCELED:"
				+ FragmentActivity.RESULT_CANCELED);
		if (data == null) {
			System.out.println("result null");
			return;
		}
		System.out.println(data.getDataString());
		Bundle b = data.getBundleExtra("bundle");
		if (b != null) {
			System.out.println("result  bundle not null");
			List<Photo> lp = ((List<Photo>) b.getSerializable("listPhoto"));
			System.out.println("result size :" + lp.size());
		} else {
			System.out.println("result size null");
		}

	}

	@Override
	public void onSaved(String name) {
		System.out.println("added:" + name);
		pd.show();
		user.createFolderGallery(conf.getPref().getToken(), name, conf
				.getPref().getCurrentProposal(), "collateral");
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("collateral pause");
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		user.putPhotoCollateral(listFolder, proposalId);
		Toast.makeText(getActivity(), "foto berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		removeEventSubscribers();
	}

	private void registeringSubcribers() {
		addEventSubscriber(new EventSubscriber<FolderGalleryCreateFailed>() {

			@Override
			public void handleEvent(FolderGalleryCreateFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(getActivity(), event.exception,
						Toast.LENGTH_SHORT).show();
			}
		});
		addEventSubscriber(new EventSubscriber<FolderGalleryCreated>() {

			@Override
			public void handleEvent(FolderGalleryCreated event) {
				if (pd.isShowing())
					pd.dismiss();
				listFolder.add(event.photoGallery);
				adapter.notifyDataSetChanged();
				Toast.makeText(getActivity(), "pembuatan folder berhasil",
						Toast.LENGTH_SHORT).show();
			}
		});
		addEventSubscriber(new EventSubscriber<PhotoUpdateSucceded>() {

			@Override
			public void handleEvent(PhotoUpdateSucceded event) {
				Photo photoUploaded = new Photo.Builder()
						.id(event.latestCandidate.getPhoto().getId())
						.info(event.latestCandidate.getPhoto().getInfo())
						.name(event.latestCandidate.getPhoto().getName())
						.url(event.latestCandidate.getPhoto().getUrl()).build();

				List<PhotoFolder> listCategory = (List<PhotoFolder>) galleryCollateral
						.getFolders();
				for (PhotoFolder photoCategory : listCategory) {
					// if (photoCategory.getName().equalsIgnoreCase(
					// event.photo.getGalleryName()))
					// photoCategory.getPhotoGalleries().add(photoUploaded);
				}
			}
		});
		addEventSubscriber(new EventSubscriber<LocalFolderLoaded>() {

			@Override
			public void handleEvent(LocalFolderLoaded event) {
				listFolder.clear();
				listFolder.addAll(event.listFolder);
				adapter.notifyDataSetChanged();
			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}
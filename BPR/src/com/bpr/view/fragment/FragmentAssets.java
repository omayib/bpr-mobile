package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.AssetsAdapter;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddAssetDialog;
import com.bpr.dialog.AddAssetDialog.AddAssetDialogListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.bpr.utils.ListviewUtils;
import com.customer.model.Assets;
import com.customer.model.AssetsCollection;
import com.proposalbox.event.LocalAssetsLoaded;

public class FragmentAssets extends Fragment implements AddAssetDialogListener {
	private AssetsAdapter adapter;
	private List<Assets> listAsset = new ArrayList<Assets>();
	private ListView listviewAssets;
	private ConfigApplication conf;
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageControler;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		// storageControler = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_ASSETS)) {
			try {
				AssetsCollection a = (AssetsCollection) b
						.getSerializable(FragmentPage.TAG_ASSETS);
				listAsset = (List<Assets>) a.getAsset();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		adapter = new AssetsAdapter(getActivity(), R.layout.item_list_assets,
				listAsset);
		registerSubcribers();
	}

	private void registerSubcribers() {
		addEventSubscriber(new EventSubscriber<LocalAssetsLoaded>() {

			@Override
			public void handleEvent(LocalAssetsLoaded event) {
				listAsset.clear();
				listAsset.addAll(event.assets);
				adapter.notifyDataSetChanged();
			}
		});
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.form_assets,
				container, false);
		listviewAssets = (ListView) rootView.findViewById(R.id.listview_assets);
		listviewAssets.setAdapter(adapter);
		listviewAssets.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Assets asset = (Assets) arg0.getAdapter().getItem(arg2);
				AddAssetDialog addDialog = new AddAssetDialog(asset);
				addDialog.setOnAddAssetDialogListener(FragmentAssets.this);
				addDialog.show(getFragmentManager(), "add");
			}
		});

		ListviewUtils.setListViewHeightBasedOnChildren(listviewAssets);
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}

		return rootView;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		userController.getAssetsFromLocal(proposalId);
	}

	@Override
	public void onPause() {
		super.onPause();
		save();
		// storageControler.saveAssets(listAsset, proposalId);
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		userController.putAssets(listAsset, proposalId);
		Toast.makeText(getActivity(), "data aset berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			AddAssetDialog addDialog = new AddAssetDialog(null);
			addDialog.setOnAddAssetDialogListener(this);
			addDialog.show(getFragmentManager(), "add");
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onAssetDialogSaved(Assets asset) {
		if (!listAsset.contains(asset)) {
			listAsset.add(asset);
		}
		adapter.notifyDataSetChanged();
		ListviewUtils.setListViewHeightBasedOnChildren(listviewAssets);
		save();
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

}
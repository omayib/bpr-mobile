package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.bpr.R;
import com.bpr.adapter.SummaryCollateralAdapter;
import com.bpr.adapter.SummaryLoanAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.utils.ListviewUtils;
import com.bpr.view.activity.ApprovalParentActivty;
import com.bpr.view.activity.ApprovalParentActivty.ApprovalSlidedListener;
import com.customer.model.Summary;
import com.customer.model.SummaryCollateral;
import com.customer.model.SummaryLoan;

public class FragmentApprovalStep0a extends Fragment implements
		ApprovalSlidedListener {
	private Summary summary;
	private ListView lvLoan, lvCollateral;
	private List<SummaryCollateral> listCollateral = new ArrayList<SummaryCollateral>();
	private List<SummaryLoan> listLoan = new ArrayList<SummaryLoan>();
	private SummaryLoanAdapter loanAdapter;
	private SummaryCollateralAdapter collateralAdapter;
	private TextView tvTotalLoan, tvTotalCollateral, tvPercentage;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ConfigApplication conf = (ConfigApplication) getActivity()
				.getApplication();
		summary = conf.getSummary();

		loanAdapter = new SummaryLoanAdapter(getActivity(),
				R.layout.item_approval_loan, listLoan);
		collateralAdapter = new SummaryCollateralAdapter(getActivity(),
				R.layout.item_approval_collateral, listCollateral);

		ApprovalParentActivty parentActivity = (ApprovalParentActivty) getActivity();
		parentActivity.setSliderListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.approval_step0a,
				container, false);
		tvPercentage = (TextView) v.findViewById(R.id.step0_percentage_current);
		tvTotalCollateral = (TextView) v
				.findViewById(R.id.step0_total_jaminan_current);
		tvTotalLoan = (TextView) v
				.findViewById(R.id.step0_total_pinjaman_current);
		lvLoan = (ListView) v.findViewById(R.id.lv_loan);
		lvCollateral = (ListView) v.findViewById(R.id.lv_collateral);
		lvLoan.setAdapter(loanAdapter);
		lvCollateral.setAdapter(collateralAdapter);

		ListviewUtils.setListViewHeightBasedOnChildren(lvCollateral);
		ListviewUtils.setListViewHeightBasedOnChildren(lvLoan);

		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		listLoan.clear();
		listCollateral.clear();
		listLoan.addAll(summary.getSummaryPlafon());
		listCollateral.addAll(summary.getSummaryCollaterals());
		collateralAdapter.notifyDataSetChanged();
		loanAdapter.notifyDataSetChanged();

		tvPercentage.setText(summary.getRatio());
		tvTotalCollateral.setText(summary.getTotalValuesAsRupiah());
		tvTotalLoan.setText(summary.getSubmission().getPlafonAsRupiah());
		

		ListviewUtils.setListViewHeightBasedOnChildren(lvCollateral);
		ListviewUtils.setListViewHeightBasedOnChildren(lvLoan);
		System.out.println("size "+lvCollateral.getAdapter().getCount());
		System.out.println("size "+lvLoan.getAdapter().getCount());
	}

	@Override
	public void onApprovalPageSlided(int currentPage, int totalPages) {

		System.out.println("page " + currentPage + " saved");

	}
}

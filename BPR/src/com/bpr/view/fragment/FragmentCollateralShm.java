package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.CollateralShmAdapter;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddGuaranteShmDialog;
import com.bpr.dialog.AddGuaranteShmDialog.OnShmAddedListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.customer.model.CollateralShm;
import com.customer.model.CollateralShmCollection;
import com.proposalbox.event.LocalShmLoaded;

public class FragmentCollateralShm extends Fragment implements
		OnShmAddedListener {
	private ListView lv;
	private CollateralShmAdapter adapter;
	private CollateralShmCollection shmCollection;
	private ConfigApplication conf;
	private List<CollateralShm> listShm = new ArrayList<CollateralShm>();
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_COLLATERAL_SHM)) {
			shmCollection = (CollateralShmCollection) b
					.getSerializable(FragmentPage.TAG_COLLATERAL_SHM);
			listShm.addAll(shmCollection.getGuaranteeShm());
		}
		adapter = new CollateralShmAdapter(getActivity(),
				R.layout.item_list_guarantee, listShm, conf.isReadOnly());

		registerSubcriber();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_guarantee,
				container, false);
		lv = (ListView) v.findViewById(R.id.listview_guarantee);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				CollateralShm currentShm = (CollateralShm) arg0.getAdapter()
						.getItem(arg2);

				AddGuaranteShmDialog shmDialog = new AddGuaranteShmDialog(
						currentShm);
				shmDialog.setOnShmAddedListener(FragmentCollateralShm.this);
				shmDialog.show(getFragmentManager(), "shmDialog");

				// DetailCollateralShmDialog detail = new
				// DetailCollateralShmDialog(
				// currentShm);
				// detail.setOnShmUpdatedListener(FragmentCollateralShm.this);
				// detail.show(getFragmentManager(), "shm");
			}
		});
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		userController.getShmFromLocal(proposalId);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add, menu);
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("shm pause");
		save();
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		userController.putCollateralShm(listShm, proposalId);
		// storageController.saveCollateralShm(listShm, proposalId);
		Toast.makeText(getActivity(), "shm berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			AddGuaranteShmDialog shmDialog = new AddGuaranteShmDialog(null);
			shmDialog.setOnShmAddedListener(this);
			shmDialog.show(getFragmentManager(), "shmDialog");
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onShmAdded(CollateralShm shm) {
		listShm.add(shm);
		adapter.notifyDataSetChanged();
		save();
	}

	private void registerSubcriber() {
		addEventSubscriber(new EventSubscriber<LocalShmLoaded>() {

			@Override
			public void handleEvent(LocalShmLoaded event) {
				listShm.clear();
				listShm.addAll(event.listShm);
				adapter.notifyDataSetChanged();
			}

		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	@Override
	public void onShmEdited() {
		adapter.notifyDataSetChanged();
		save();
	}
}
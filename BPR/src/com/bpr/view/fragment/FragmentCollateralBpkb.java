package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.CollateralBpkbAdapter;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddGuaranteBpkbDialog;
import com.bpr.dialog.AddGuaranteBpkbDialog.OnBpkbAddedListener;
import com.bpr.dialog.DetailCollateralBpkbDialog;
import com.bpr.dialog.DetailCollateralBpkbDialog.OnDetailBpkbUpdated;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralBpkbCollection;
import com.proposalbox.event.LocalBpkbLoaded;

public class FragmentCollateralBpkb extends Fragment implements
		OnBpkbAddedListener, OnDetailBpkbUpdated {
	private CollateralBpkbAdapter adapter;
	private CollateralBpkbCollection bpkbCollection;
	private ListView lv;
	private ConfigApplication conf;
	private List<CollateralBpkb> listBpkb = new ArrayList<CollateralBpkb>();
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_COLLATERAL_BPKB)) {
			bpkbCollection = (CollateralBpkbCollection) b
					.getSerializable(FragmentPage.TAG_COLLATERAL_BPKB);
			listBpkb = (List<CollateralBpkb>) bpkbCollection.getGuaranteeBpkb();
		}
		adapter = new CollateralBpkbAdapter(getActivity(),
				R.layout.item_list_guarantee, listBpkb, conf.isReadOnly());
		registerSubcriber();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_guarantee,
				container, false);
		lv = (ListView) v.findViewById(R.id.listview_guarantee);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				CollateralBpkb currentBpkb = (CollateralBpkb) arg0.getAdapter()
						.getItem(arg2);

				DetailCollateralBpkbDialog detail = new DetailCollateralBpkbDialog(
						currentBpkb);
				detail.setOnDetailBpkbUpdated(FragmentCollateralBpkb.this);
				detail.show(getFragmentManager(), "bpkb");
			}
		});
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		userController.getBpkbFromLocal(proposalId);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add, menu);
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("bpkb pause");
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		userController.putCollateralBpkb(listBpkb, proposalId);
		// storageController.saveCollateralBpkb(listBpkb, proposalId);
		Toast.makeText(getActivity(), "jaminan berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		removeEventSubscribers();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			AddGuaranteBpkbDialog bpkbDialog = new AddGuaranteBpkbDialog();
			bpkbDialog.setOnBpkbAddedListener(this);
			bpkbDialog.show(getFragmentManager(), "bpkbDialog");
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBpkbAdded(CollateralBpkb bpkb) {
		listBpkb.add(bpkb);
		adapter.notifyDataSetChanged();
		save();
	}

	private void registerSubcriber() {
		addEventSubscriber(new EventSubscriber<LocalBpkbLoaded>() {

			@Override
			public void handleEvent(LocalBpkbLoaded event) {
				System.out.println("local bpkb loadeds");
				listBpkb.clear();
				listBpkb.addAll(event.listBpkb);
				adapter.notifyDataSetChanged();
			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	@Override
	public void onDetailBpkbUpdated() {
		adapter.notifyDataSetChanged();
		save();
	}
}
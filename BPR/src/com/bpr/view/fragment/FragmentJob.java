package com.bpr.view.fragment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.bpr.utils.MoneyEditText;
import com.bpr.utils.RandomInteger;
import com.customer.model.Job;
import com.proposalbox.event.LocalJobLoaded;

public class FragmentJob extends Fragment {
	private EditText officeName, officeField, officeAddr,
			officePositionByCurrent, officeYearJoined, officePoitionByFirsr,
			officeAchievement, officeJobname;
	private MoneyEditText officeSalary;
	private Job job;
	private Spinner spnWorkerStatus;
	private ConfigApplication conf;
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_JOB)) {
			job = (Job) b.getSerializable(FragmentPage.TAG_JOB);
		}
		registerSubcribers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_job,
				container, false);
		officeJobname = (EditText) v.findViewById(R.id.job_name);
		officeField = (EditText) v.findViewById(R.id.job_field);
		officeAchievement = (EditText) v.findViewById(R.id.job_achievement);
		officeAddr = (EditText) v.findViewById(R.id.job_officeAddress);
		officeName = (EditText) v.findViewById(R.id.job_nameOffice);
		officePoitionByFirsr = (EditText) v
				.findViewById(R.id.job_firstPosition);
		officePositionByCurrent = (EditText) v
				.findViewById(R.id.job_currentPosition);
		officeSalary = (MoneyEditText) v.findViewById(R.id.job_salary);
		spnWorkerStatus = (Spinner) v.findViewById(R.id.job_statusOfEmployee);
		officeYearJoined = (EditText) v.findViewById(R.id.job_yearJoined);

		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		if (job != null)
			updateView(job);
		// set the form read only or editable
		setHasOptionsMenu(!conf.isReadOnly());
		officeJobname.setEnabled(!conf.isReadOnly());
		officeField.setEnabled(!conf.isReadOnly());
		officeAchievement.setEnabled(!conf.isReadOnly());
		officeAddr.setEnabled(!conf.isReadOnly());
		officeName.setEnabled(!conf.isReadOnly());
		officePoitionByFirsr.setEnabled(!conf.isReadOnly());
		officePositionByCurrent.setEnabled(!conf.isReadOnly());
		officeSalary.setEnabled(!conf.isReadOnly());
		spnWorkerStatus.setEnabled(!conf.isReadOnly());
		officeYearJoined.setEnabled(!conf.isReadOnly());

	}

	private void updateView(Job job) {
		officeJobname.setText(job.getName());
		officeField.setText(job.getJobField());
		officeAchievement.setText(job.getAchievement());
		officeAddr.setText(job.getOfficeAddr());
		officeName.setText(job.getOfficeName());
		officePoitionByFirsr.setText(job.getPositionByInitial());
		officePositionByCurrent.setText(job.getPositionByCurrent());
		officeSalary.setText(job.getSalary() + "");
		officeYearJoined.setText(job.getYearOfJoined() + "");
		spnWorkerStatus.setSelection(getWorkerIndex(job.getStatusOfWorker()));
	}

	private int getWorkerIndex(String input) {
		String[] status = getResources()
				.getStringArray(R.array.employee_status);
		return Arrays.asList(status).indexOf(input.toLowerCase());
	}

	@Override
	public void onResume() {
		super.onResume();
		userController.getJobFromLocal(proposalId);
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("job pause");
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		String name = officeJobname.getText().toString();
		String achievement = officeAchievement.getText().toString();
		int id = (job == null) ? RandomInteger.randInt(0, 1000) : job.getId();
		String jobField = officeField.getText().toString();
		String addr = officeAddr.getText().toString();
		String nameOffice = officeName.getText().toString();
		String positionByCurrent = officePositionByCurrent.getText().toString();
		String positionByInitial = officePoitionByFirsr.getText().toString();
		BigDecimal salary = officeSalary.getNominal();
		String statusOfWorker = spnWorkerStatus.getSelectedItem().toString()
				.replace(" ", "_").toUpperCase();
		int yearOfJoined = Integer.parseInt(officeYearJoined.getText()
				.toString());

		Job newJob = new Job.Builder().name(name).achievment(achievement)
				.field(jobField).id(id).officeAddress(addr)
				.officeName(nameOffice).positionByCurrent(positionByCurrent)
				.positionByInitial(positionByInitial).salary(salary.toString())
				.statusOfWorker(statusOfWorker).yearOfJoined(yearOfJoined)
				.build();
		userController.putJob(newJob, proposalId);
		// storageController.saveJob(newJob, proposalId);
		Toast.makeText(getActivity(), "Pekerjaan berhasil disimpan",
				Toast.LENGTH_SHORT).show();

	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.save, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.saved:
			save();
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void registerSubcribers() {
		addEventSubscriber(new EventSubscriber<LocalJobLoaded>() {

			@Override
			public void handleEvent(LocalJobLoaded event) {
				updateView(event.job);
			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}
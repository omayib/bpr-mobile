package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.SidAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.customer.model.Sid;
import com.proposalbox.event.SidLoadFailed;
import com.proposalbox.event.SidLoadSucceded;
import com.proposalbox.event.SidRequested;

public class FragmentPhotoSid extends Fragment {
	private ConfigApplication conf;
	private GridView gridViewSid;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private SidAdapter sidAdapter;
	private List<Sid> listSid = new ArrayList<Sid>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		eventPublisher = conf.getEventPublisher();
		sidAdapter = new SidAdapter(getActivity(), R.layout.item_sid, listSid);
		registerSubscribers();
		System.out.println("fragment sid created");
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_sid,
				container, false);
		gridViewSid = (GridView) v.findViewById(R.id.GridView1);
		gridViewSid.setAdapter(sidAdapter);

		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		conf.getEventPublisher().publish(new SidRequested());
		System.out.println("fragment sid SidRequested");
	}

	private void registerSubscribers() {
		addEventSubscriber(new EventSubscriber<SidLoadFailed>() {

			@Override
			public void handleEvent(SidLoadFailed event) {
				Toast.makeText(getActivity(), "something wrong",
						Toast.LENGTH_SHORT).show();

			}
		});
		addEventSubscriber(new EventSubscriber<SidLoadSucceded>() {

			@Override
			public void handleEvent(SidLoadSucceded event) {
				listSid.addAll(event.sids);
				sidAdapter.notifyDataSetChanged();

				if (event.sids.isEmpty())
					Toast.makeText(getActivity(), "no data", Toast.LENGTH_SHORT)
							.show();

			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		removeEventSubscribers();
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}
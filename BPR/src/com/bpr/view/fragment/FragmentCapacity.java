package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.CapacityExpenditureAdapter;
import com.bpr.adapter.CapacityIncomeAdapter;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddCapacityDialog;
import com.bpr.dialog.AddCapacityDialog.OnAddCapacityListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.customer.model.CapacityCollection;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import com.proposalbox.event.LocalCapacityLoaded;
import com.proposalbox.event.LocalExpenditureLoaded;
import com.proposalbox.event.LocalIncomeLoaded;

public class FragmentCapacity extends Fragment implements OnAddCapacityListener {
	private ListView listviewIncome;
	private ListView listviewExpenditure;
	private TextView totalIncome, totalExpenditure, totalNetIncome,
			totalInSubsEx, totalSubms, totalSubmsVal;
	private CapacityExpenditureAdapter expenditureAdapter;
	private CapacityIncomeAdapter incomeAdapter;
	private List<CapacityIncome> incomeCollection = new ArrayList<CapacityIncome>();
	private List<CapacityExpenditure> expenditureCollection = new ArrayList<CapacityExpenditure>();
	private CapacityCollection capacityCollection;
	private ConfigApplication conf;
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_CAPACITY)) {
			capacityCollection = (CapacityCollection) b
					.getSerializable(FragmentPage.TAG_CAPACITY);
			incomeCollection = (List<CapacityIncome>) capacityCollection
					.getIncome();
			expenditureCollection = (List<CapacityExpenditure>) capacityCollection
					.getExpenditure();
			System.out.println("capacity out " + capacityCollection.toString());
			System.out.println("incomeCollection out "
					+ incomeCollection.toString());
			System.out.println("expenditureCollection out "
					+ capacityCollection.toString());
		}
		incomeAdapter = new CapacityIncomeAdapter(getActivity(),
				R.layout.item_capacity, incomeCollection);
		expenditureAdapter = new CapacityExpenditureAdapter(getActivity(),
				R.layout.item_capacity, expenditureCollection);

		registerSubcriber();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup rootView = (ViewGroup) inflater.inflate(
				R.layout.form_capacity, container, false);
		listviewExpenditure = (ListView) rootView
				.findViewById(R.id.listview_expenditure);
		listviewIncome = (ListView) rootView.findViewById(R.id.listview_income);
		totalIncome = (TextView) rootView.findViewById(R.id.total_income);
		totalExpenditure = (TextView) rootView
				.findViewById(R.id.total_expenditure);
		totalNetIncome = (TextView) rootView
				.findViewById(R.id.total_net_income);
		totalInSubsEx = (TextView) rootView
				.findViewById(R.id.capacity_in_subs_expenditure);
		totalSubms = (TextView) rootView
				.findViewById(R.id.capacity_submsission);
		totalSubmsVal = (TextView) rootView
				.findViewById(R.id.capacity_submsission_value);

		listviewExpenditure.setAdapter(expenditureAdapter);
		listviewIncome.setAdapter(incomeAdapter);

		listviewIncome.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				CapacityIncome inc = (CapacityIncome) arg0.getAdapter()
						.getItem(arg2);
				AddCapacityDialog addCapacity = new AddCapacityDialog();
				addCapacity.setIncome(inc);
				addCapacity.setOnAddCapacityListener(FragmentCapacity.this);
				addCapacity.show(getFragmentManager(), "Pemasukan");
			}
		});
		listviewExpenditure.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				CapacityExpenditure exp = (CapacityExpenditure) arg0
						.getAdapter().getItem(arg2);
				AddCapacityDialog addCapacity = new AddCapacityDialog();
				addCapacity.setExpenditure(exp);
				addCapacity.setOnAddCapacityListener(FragmentCapacity.this);
				addCapacity.show(getFragmentManager(), "Pengeluaran");
			}
		});

		return rootView;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		updateView();
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
	}

	private void updateView() {
		if (capacityCollection == null)
			return;
		totalIncome.setText(capacityCollection.getIncomeTotalAsRupiah());
		totalExpenditure.setText(capacityCollection
				.getExpenditureTotalAsRupiah());
		totalNetIncome.setText(capacityCollection.getIncomeNetAsRupiah());
		totalInSubsEx.setText(capacityCollection
				.getIncomeSubstractExpenditureAsRupiah());
		totalSubmsVal.setText(capacityCollection
				.getSubmissionPercentageValueAsRupiah());
		totalSubms.setText(capacityCollection.getSubmissionPercentage());
	}

	@Override
	public void onResume() {
		super.onResume();
		userController.getCapacityFromLocal(proposalId);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.capacity, menu);
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("capacity pause");
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		userController.putCapacity(incomeCollection, expenditureCollection,
				capacityCollection.getId(), proposalId);
		Toast.makeText(getActivity(), "data kapaistas berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		AddCapacityDialog addCapacity = new AddCapacityDialog();
		addCapacity.setOnAddCapacityListener(this);
		switch (item.getItemId()) {
		case R.id.capacity_add_income:
			addCapacity.show(getFragmentManager(), "Pemasukan");
			break;

		case R.id.capacity_add_expenditure:
			addCapacity.show(getFragmentManager(), "Pengeluaran");
			break;
		}
		return true;
	}

	@Override
	public void onIncomeAdded(CapacityIncome income) {
		if (!incomeCollection.contains(income))
			incomeCollection.add(income);
		incomeAdapter.notifyDataSetChanged();
		updateView();
		save();
	}

	@Override
	public void onExpenditureAdded(CapacityExpenditure expn) {
		if (!expenditureCollection.contains(expn))
			expenditureCollection.add(expn);
		expenditureAdapter.notifyDataSetChanged();
		updateView();
		save();
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	private void registerSubcriber() {
		addEventSubscriber(new EventSubscriber<LocalExpenditureLoaded>() {

			@Override
			public void handleEvent(LocalExpenditureLoaded event) {
				expenditureCollection.clear();
				expenditureCollection.addAll(event.expenditures);
				expenditureAdapter.notifyDataSetChanged();

				updateView();
			}
		});
		addEventSubscriber(new EventSubscriber<LocalIncomeLoaded>() {

			@Override
			public void handleEvent(LocalIncomeLoaded event) {
				incomeCollection.clear();
				incomeCollection.addAll(event.incomes);
				incomeAdapter.notifyDataSetChanged();

				updateView();

			}
		});
	}
}
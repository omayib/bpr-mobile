package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.FragmentPage;
import com.bpr.adapter.PurposeAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddPurposeDialog;
import com.bpr.dialog.AddPurposeDialog.OnPurposeAddedListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.customer.model.Purposes;
import com.customer.model.PurposesCollection;
import com.proposalbox.event.LocalPurposesLoaded;

public class FragmentPurpose extends Fragment implements OnPurposeAddedListener {
	private PurposesCollection pc;
	private ListView lv;
	private PurposeAdapter adapter;
	private ConfigApplication conf;
	private List<Purposes> listPurpose = new ArrayList<Purposes>();
	private int proposalId;
	private UserController user;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		user = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_PURPOSE)) {
			pc = (PurposesCollection) b
					.getSerializable(FragmentPage.TAG_PURPOSE);
			listPurpose.addAll(pc.getPurposesCollection());
		}
		adapter = new PurposeAdapter(getActivity(), R.layout.item_list_purpose,
				listPurpose);
		registerSubcribers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_purpose,
				container, false);
		lv = (ListView) v.findViewById(R.id.list_purpose);
		lv.setAdapter(adapter);
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		user.getPurposeFromLocal(proposalId);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add, menu);
	}

	@Override
	public void onPause() {
		super.onPause();
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		user.putPurposes(listPurpose, proposalId);
		// storageController.savePurposes(listPurpose, proposalId);
		Toast.makeText(getActivity(), "tujuan berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			AddPurposeDialog addDialog = new AddPurposeDialog();
			addDialog.setOnPurposeAddedListener(this);
			addDialog.show(getFragmentManager(), "addPurpose");
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onPurposeAdded(Purposes p) {
		listPurpose.add(p);
		adapter.notifyDataSetChanged();
		save();
	}

	private void registerSubcribers() {
		addEventSubscriber(new EventSubscriber<LocalPurposesLoaded>() {

			@Override
			public void handleEvent(LocalPurposesLoaded event) {
				System.out.println("purpose loaded");
				listPurpose.clear();
				listPurpose.addAll(event.purposes);
				adapter.notifyDataSetChanged();
			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}
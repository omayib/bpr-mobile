package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.FragmentPage;
import com.bpr.adapter.PhotoCollectionAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddPhotoDialog;
import com.bpr.dialog.AddPhotoDialog.PhotoDialogListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.bpr.photo.viewer.activity.PhotoViewerActivity;
import com.customer.model.PhotoBusiness;
import com.customer.model.PhotoFolder;
import com.proposalbox.event.FolderGalleryCreateFailed;
import com.proposalbox.event.FolderGalleryCreated;
import com.proposalbox.event.LocalFolderLoaded;

public class FragmentPhotoBusiness extends Fragment implements
		PhotoDialogListener {
	private PhotoCollectionAdapter adapter;
	private PhotoBusiness pb;
	private ListView lv;
	private ArrayList<PhotoFolder> listFolder = new ArrayList<PhotoFolder>();
	private ConfigApplication conf;
	private int proposalId;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private UserController user;
	private ProgressDialog pd;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		eventPublisher = conf.getEventPublisher();
		user = conf.getUserController();

		pd = new ProgressDialog(getActivity());
		pd.setMessage("please wait...");

		proposalId = conf.getPref().getCurrentProposal();
		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_PHOTO_BUSINESS)) {
			pb = (PhotoBusiness) b
					.getSerializable(FragmentPage.TAG_PHOTO_BUSINESS);
			listFolder = (ArrayList<PhotoFolder>) pb.getFolders();
		}

		adapter = new PhotoCollectionAdapter(getActivity(),
				R.layout.item_photo, listFolder);

		registeringSubscribers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_photo,
				container, false);
		lv = (ListView) v.findViewById(R.id.listview_photo);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				PhotoFolder currentFolder = (PhotoFolder) arg0
						.getItemAtPosition(arg2);
				Intent i = new Intent(getActivity(), PhotoViewerActivity.class);
				Bundle b = new Bundle();
				b.putSerializable("currentFolder", currentFolder);
				b.putString("galleryName", "business");
				b.putInt("proposalId", proposalId);
				i.putExtras(b);
				startActivityForResult(i, 191);
			}
		});
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
		return v;
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add, menu);
	}

	@Override
	public void onResume() {
		super.onResume();
		user.getFoldersFromLocal(proposalId, "business");
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			AddPhotoDialog photoDialog = new AddPhotoDialog();
			photoDialog.setOnAddPhotoListener(this);
			photoDialog.show(getFragmentManager(), "businessPhoto");
			break;

		default:
			break;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (requestCode == 1) {
			getActivity();
			if (resultCode == FragmentActivity.RESULT_OK) {
				String result = data.getStringExtra("result");
				adapter.notifyDataSetChanged();
			}
			if (resultCode == FragmentActivity.RESULT_CANCELED) {
				// Write your code if there's no result
				Log.d("omayib", "cnaceled");
			}
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("business pause");
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		user.putPhotoBusiness(listFolder, proposalId);

		Toast.makeText(getActivity(), "foto berhasil disimpan",
				Toast.LENGTH_SHORT).show();		
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		removeEventSubscribers();
	}

	@Override
	public void onSaved(String name) {
		System.out.println("added:" + name);
		pd.show();
		user.createFolderGallery(conf.getPref().getToken(), name, conf
				.getPref().getCurrentProposal(), "business");
	}

	private void registeringSubscribers() {
		addEventSubscriber(new EventSubscriber<FolderGalleryCreateFailed>() {

			@Override
			public void handleEvent(FolderGalleryCreateFailed event) {
				if (pd.isShowing())
					pd.dismiss();
				Toast.makeText(getActivity(), event.exception,
						Toast.LENGTH_SHORT).show();
			}
		});
		addEventSubscriber(new EventSubscriber<FolderGalleryCreated>() {

			@Override
			public void handleEvent(FolderGalleryCreated event) {
				if (pd.isShowing())
					pd.dismiss();
				listFolder.add(event.photoGallery);
				adapter.notifyDataSetChanged();
				Toast.makeText(getActivity(), "pembuatan folder berhasil",
						Toast.LENGTH_SHORT).show();
			}
		});
		addEventSubscriber(new EventSubscriber<LocalFolderLoaded>() {

			@Override
			public void handleEvent(LocalFolderLoaded event) {
				listFolder.clear();
				listFolder.addAll(event.listFolder);
				adapter.notifyDataSetChanged();
			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}
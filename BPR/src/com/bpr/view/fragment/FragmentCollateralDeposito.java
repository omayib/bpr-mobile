package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.DepositoAdapter;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.DepositoDialog;
import com.bpr.dialog.DepositoDialog.OnDepositoAddedListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.customer.model.CollateralDeposito;
import com.customer.model.CollateralDepositoCollection;
import com.proposalbox.event.LocalDepositoLoaded;

public class FragmentCollateralDeposito extends Fragment implements
		OnDepositoAddedListener {
	private CollateralDepositoCollection deposito;
	private ListView lv;
	private DepositoAdapter adapter;
	private ConfigApplication conf;
	private List<CollateralDeposito> listDeposito = new ArrayList<CollateralDeposito>();
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_COLLATERAL_DEPOSITO)) {
			deposito = (CollateralDepositoCollection) b
					.getSerializable(FragmentPage.TAG_COLLATERAL_DEPOSITO);
			listDeposito.addAll(deposito.listDeposito);
		}
		adapter = new DepositoAdapter(getActivity(), R.layout.item_deposito,
				listDeposito);
		registerSubcribers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_purpose,
				container, false);
		lv = (ListView) v.findViewById(R.id.list_purpose);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				CollateralDeposito dep = (CollateralDeposito) arg0.getAdapter()
						.getItem(arg2);

				DepositoDialog addDialog = new DepositoDialog();
				addDialog.setDeposito(dep);
				addDialog
						.setOnDepositoAddedListener(FragmentCollateralDeposito.this);
				addDialog.show(getFragmentManager(), "addPurpose");

			}
		});
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		userController.getDepositoFromLocal(proposalId);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add, menu);
	}

	@Override
	public void onPause() {
		super.onPause();
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		userController.putDeposito(listDeposito, proposalId);
		// storageController.saveDeposito(listDeposito, proposalId);
		Toast.makeText(getActivity(), "tabungan berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:

			DepositoDialog addDialog = new DepositoDialog();
			addDialog.setOnDepositoAddedListener(this);
			addDialog.show(getFragmentManager(), "addPurpose");
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void registerSubcribers() {
		addEventSubscriber(new EventSubscriber<LocalDepositoLoaded>() {

			@Override
			public void handleEvent(LocalDepositoLoaded event) {
				System.out.println("deposito loaded");
				listDeposito.clear();
				listDeposito.addAll(event.listDeposito);
				adapter.notifyDataSetChanged();
			}
		});

	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	@Override
	public void onDepositoAdded(CollateralDeposito p) {
		listDeposito.add(p);
		adapter.notifyDataSetChanged();
		save();
	}

	@Override
	public void onDepositoEdited() {
		adapter.notifyDataSetChanged();
		save();
	}
}
package com.bpr.view.fragment;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.bpr.R;
import com.bpr.adapter.ArrayPagerAdapter.RetentionStrategy;
import com.bpr.adapter.BusinessPagerAdapter;
import com.bpr.adapter.FragmentPage;
import com.bpr.adapter.PageDescriptor;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddBusinessDialog;
import com.bpr.dialog.AddBusinessDialog.OnBusinessAddedListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.customer.model.Business;
import com.customer.model.BusinessCollection;
import com.proposalbox.event.LocalBusinessLoadded;

public class FragmentBusinessParent extends Fragment implements
		OnBusinessAddedListener, RetentionStrategy {
	private ViewPager pager = null;
	private BusinessPagerAdapter adapter;
	private ArrayList<Business> business = new ArrayList<Business>();
	private ArrayList<PageDescriptor> pages = new ArrayList<PageDescriptor>();
	private ConfigApplication conf;
	private static List<OnAllBusinessSavedListener> onBusinessSavedListenerList = new ArrayList<OnAllBusinessSavedListener>();
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	public interface OnAllBusinessSavedListener {
		void onAllBusinessSaved();
	}

	public static void registerOnAllBusinessSavedListener(
			OnAllBusinessSavedListener p) {
		onBusinessSavedListenerList.add(p);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		eventPublisher = conf.getEventPublisher();
		userController = conf.getUserController();
		proposalId = conf.getPref().getCurrentProposal();

		adapter = new BusinessPagerAdapter(getFragmentManager(), pages, this);

		Bundle b = getArguments();
		if (b == null)
			return;

		if (b.containsKey(FragmentPage.TAG_BUSINESS)) {
			BusinessCollection bu = (BusinessCollection) b
					.getSerializable(FragmentPage.TAG_BUSINESS);
			business = (ArrayList<Business>) bu.getBusinessCollection();
		}
		registerSubcribers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		container = (ViewGroup) inflater.inflate(R.layout.form_business_parent,
				container, false);
		pager = (ViewPager) container.findViewById(R.id.pager_business_parent);
		pager.setAdapter(adapter);

		return container;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		Log.d("fragment", "onViewCreated");

		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
			System.out.println("business parent" + business.toString());
			updateView(business);
		} else {
			setHasOptionsMenu(true);
			userController.getBusinessFromLocal(proposalId);
		}

	}

	@Override
	public void onResume() {
		super.onResume();
	}

	private void updateView(List<Business> business) {
		// removeAllViews();
		for (Business bi : business) {
			FragmentPage<Serializable> page = new FragmentPage<Serializable>(
					bi.getName(), UUID.randomUUID().toString(), bi);
			adapter.add(page);
			// pages.add(page);
		}
		adapter.notifyDataSetChanged();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.business, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.business_add:
			AddBusinessDialog addDialog = new AddBusinessDialog();
			addDialog.setOnBusinessAddedListener(this);
			addDialog.show(getFragmentManager(), "addBisnis");
			break;
		case R.id.business_save:
			save();
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("business parent onpause");
		save();
	}

	private void save() {
		for (OnAllBusinessSavedListener pr : onBusinessSavedListenerList) {
			pr.onAllBusinessSaved();
		}
	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
		System.out.println("ondestroyview");
		removeAllViews();
	}

	private void removeAllViews() {
		if (adapter.getCount() == 0)
			return;
		for (int i = 0; i < adapter.getCount(); i++) {
			adapter.destroyItem(pager, i, this);
		}
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	@Override
	public void onBusinessAdded(Business b) {
		System.out.println("business added");
		FragmentPage<Serializable> page = new FragmentPage<Serializable>(
				b.getName(), UUID.randomUUID().toString(), b);
		adapter.add(page);
		adapter.notifyDataSetChanged();
	}

	private void registerSubcribers() {
		addEventSubscriber(new EventSubscriber<LocalBusinessLoadded>() {
			@Override
			public void handleEvent(LocalBusinessLoadded event) {
				updateView((List<Business>) event.businessCollection
						.getBusinessCollection());
			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	@Override
	public void attach(Fragment fragment, FragmentTransaction currTransaction) {
		System.out.println("retention strategy on attach");
	}

	@Override
	public void detach(Fragment fragment, FragmentTransaction currTransaction) {
		System.out.println("retention strategy on detach");

	}
}
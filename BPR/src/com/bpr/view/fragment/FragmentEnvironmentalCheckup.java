package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.bpr.R;
import com.bpr.activerecord.model.EnvironmentActiveRecord;
import com.bpr.activerecord.model.QueryAction;
import com.bpr.adapter.EnvironmentCheckupAdapter;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddEnvirontmentDialog;
import com.bpr.dialog.AddEnvirontmentDialog.OnEnvCheckAddedListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.StorageController;
import com.bpr.integration.controller.UserController;
import com.customer.model.EnvironmentalCheck;
import com.customer.model.EnvironmentalCheckCollection;
import com.proposalbox.event.LocalEnvLoaded;

public class FragmentEnvironmentalCheckup extends Fragment implements
		OnEnvCheckAddedListener {
	private EnvironmentalCheckCollection checks;
	private EnvironmentCheckupAdapter adapter;
	private ListView lv;
	private ConfigApplication conf;
	private ArrayList<EnvironmentalCheck> listCheck = new ArrayList<EnvironmentalCheck>();
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_ENVIRONMENT_CHECK)) {
			checks = (EnvironmentalCheckCollection) b
					.getSerializable(FragmentPage.TAG_ENVIRONMENT_CHECK);
			listCheck = (ArrayList<EnvironmentalCheck>) checks
					.getCheckCollection();
		}
		adapter = new EnvironmentCheckupAdapter(getActivity(),
				R.layout.item_list_enviromental_check, listCheck);
		registerSubcriber();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(
				R.layout.form_environtment_check, container, false);
		lv = (ListView) v.findViewById(R.id.listview_env_checkup);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				EnvironmentalCheck selected = (EnvironmentalCheck) arg0
						.getAdapter().getItem(arg2);
				AddEnvirontmentDialog envDialog = new AddEnvirontmentDialog(
						selected);
				envDialog
						.setOnEnvCheckAddedListener(FragmentEnvironmentalCheckup.this);
				envDialog.show(getFragmentManager(), "envDialog");
			}
		});
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
		return v;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:
			AddEnvirontmentDialog envDialog = new AddEnvirontmentDialog(null);
			envDialog.setOnEnvCheckAddedListener(this);
			envDialog.show(getFragmentManager(), "envDialog");
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();
		userController.getEnvFromLocal(proposalId);
	}

	@Override
	public void onPause() {
		super.onPause();
		System.out.println("env pause");
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		userController.puteEnvCheckup(listCheck, proposalId);
		// storageController.saveEnvCheckup(listCheck, proposalId);
		Toast.makeText(getActivity(), "cek lingkungan berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add, menu);
	}

	@Override
	public void onEnvAdded(EnvironmentalCheck check) {
		listCheck.add(check);
		adapter.notifyDataSetChanged();
		save();
	}

	private void registerSubcriber() {
		addEventSubscriber(new EventSubscriber<LocalEnvLoaded>() {

			@Override
			public void handleEvent(LocalEnvLoaded event) {
				System.out.println("local bpkb loadeds");
				listCheck.clear();
				listCheck.addAll(event.listEnv);
				adapter.notifyDataSetChanged();
			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	@Override
	public void onEnvEdited() {
		adapter.notifyDataSetChanged();
		save();
	}
}
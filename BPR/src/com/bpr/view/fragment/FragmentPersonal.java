package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.activeandroid.ActiveAndroid;
import com.bpr.R;
import com.bpr.activerecord.model.ChildActiveRecord;
import com.bpr.activerecord.model.PersonalActiveRecord;
import com.bpr.activerecord.model.QueryAction;
import com.bpr.adapter.ChildrenAdapter;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddChildDialog;
import com.bpr.dialog.AddChildDialog.AddChildDialogListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.StorageController;
import com.bpr.integration.controller.UserController;
import com.bpr.utils.ListviewUtils;
import com.customer.model.Children;
import com.customer.model.Personal;
import com.proposalbox.event.LocalPersonalLoaded;

public class FragmentPersonal extends Fragment implements
		AddChildDialogListener {
	private Personal personal;
	private TextView tvKtpNumber, tvNameFull, tvNameAlias, tvDatePlace,
			tvDateBirth, tvAge, tvAddrKtp, tvAddrCurrent, tvPhoneNumber,
			tvHandphoneNumber, tvMotherName, tvNumberDependents, tvSpouceName,
			tvSpouceJob;
	private ListView lvChild;
	private Spinner spnMaritalStatus, spnHomeStatus, spnGender;
	private ConfigApplication conf;
	private ArrayList<Children> children = new ArrayList<Children>();
	private ChildrenAdapter childrenAdapter;
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		System.out.println("ONCREATE");
		conf = (ConfigApplication) getActivity().getApplication();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		proposalId = conf.getPref().getCurrentProposal();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_PERSONAL)) {
			try {
				personal = (Personal) b
						.getSerializable(FragmentPage.TAG_PERSONAL);
				System.out.println("cek personal:" + personal.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		childrenAdapter = new ChildrenAdapter(getActivity(),
				R.layout.item_child, children);

		registerSubcribers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		System.out.println("ONCREATEVIEW");
		ViewGroup rootView = (ViewGroup) inflater.inflate(
				R.layout.form_personal, container, false);
		tvKtpNumber = (TextView) rootView
				.findViewById(R.id.personal_ktp_number);
		tvNameFull = (TextView) rootView.findViewById(R.id.personal_fullName);
		tvNameAlias = (TextView) rootView
				.findViewById(R.id.personal_alias_name);
		tvDatePlace = (TextView) rootView
				.findViewById(R.id.personal_placeOfBirth);
		tvDateBirth = (TextView) rootView
				.findViewById(R.id.personal_dateOfBirth);
		tvAge = (TextView) rootView.findViewById(R.id.personal_age);
		tvAddrKtp = (TextView) rootView
				.findViewById(R.id.personal_addressByKtp);
		tvAddrCurrent = (TextView) rootView
				.findViewById(R.id.personal_addressByCurrent);
		tvPhoneNumber = (TextView) rootView
				.findViewById(R.id.personal_phone_number);
		tvHandphoneNumber = (TextView) rootView
				.findViewById(R.id.personal_handphone_number);
		tvSpouceName = (TextView) rootView
				.findViewById(R.id.personal_nameOfCouple);
		tvSpouceJob = (TextView) rootView
				.findViewById(R.id.personal_jobOfCouple);
		tvMotherName = (TextView) rootView
				.findViewById(R.id.personal_mother_maiden_name);
		tvNumberDependents = (TextView) rootView
				.findViewById(R.id.personal_number_dependents);
		spnMaritalStatus = (Spinner) rootView
				.findViewById(R.id.personal_statusOfMarital);
		spnHomeStatus = (Spinner) rootView
				.findViewById(R.id.personal_status_home);
		spnGender = (Spinner) rootView.findViewById(R.id.personal_gender);

		lvChild = (ListView) rootView.findViewById(R.id.personal_childrens);
		ListviewUtils.setListViewHeightBasedOnChildren(lvChild);
		lvChild.setAdapter(childrenAdapter);
		return rootView;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		System.out.println("ONVIEWCREATED");
		if (personal == null)
			return;
		updateView(personal);

		// set read only or editable
		/**
		 * if conf.isReadonly equals true, then all edit_view will set enabled
		 * to false
		 * */

		setHasOptionsMenu(!conf.isReadOnly());
		tvKtpNumber.setEnabled(!conf.isReadOnly());
		tvNameFull.setEnabled(!conf.isReadOnly());
		tvNameAlias.setEnabled(!conf.isReadOnly());
		tvDatePlace.setEnabled(!conf.isReadOnly());
		tvDateBirth.setEnabled(!conf.isReadOnly());
		tvAge.setEnabled(!conf.isReadOnly());
		tvAddrKtp.setEnabled(!conf.isReadOnly());
		tvAddrCurrent.setEnabled(!conf.isReadOnly());
		tvPhoneNumber.setEnabled(!conf.isReadOnly());
		tvHandphoneNumber.setEnabled(!conf.isReadOnly());
		tvSpouceName.setEnabled(!conf.isReadOnly());
		tvSpouceJob.setEnabled(!conf.isReadOnly());
		tvNumberDependents.setEnabled(!conf.isReadOnly());
		spnGender.setEnabled(!conf.isReadOnly());
		spnHomeStatus.setEnabled(!conf.isReadOnly());
		spnMaritalStatus.setEnabled(!conf.isReadOnly());
		tvMotherName.setEnabled(!conf.isReadOnly());
		lvChild.setEnabled(!conf.isReadOnly());

		ListviewUtils.setListViewHeightBasedOnChildren(lvChild);
	}

	private void updateView(Personal p) {
		System.out.println("updateing..." + p.toString());
		tvKtpNumber.setText(p.getKtpNumber());
		tvNameFull.setText(p.getName());
		tvNameAlias.setText(p.getAliasName());
		tvDatePlace.setText(p.getBirthPlace());
		tvDateBirth.setText(p.getBirthDate());
		tvAge.setText(p.getAge() + "");
		tvAddrKtp.setText(p.getAddressByKtp());
		tvAddrCurrent.setText(p.getAddressByCurrent());
		tvPhoneNumber.setText(p.getPhoneNumber());
		tvHandphoneNumber.setText(p.getHandphoneNumber());
		tvSpouceJob.setText(p.getSpouseWork());
		tvSpouceName.setText(p.getSpouseName());
		tvMotherName.setText(p.getMotherMaidenName());
		tvNumberDependents.setText(p.getChildren().size() + "");
		spnGender.setSelection(getGenderIndex(p.getGender()));
		spnHomeStatus.setSelection(getHomeIndex(p.getHomeStatusForSpinner()));
		spnMaritalStatus.setSelection(getMaritalIndex(p.getMaritalStatusForSpinner()));
		children.clear();
		children.addAll(p.getChildren());
		childrenAdapter.notifyDataSetChanged();
	}

	private int getGenderIndex(String input) {
		String localInput = input.equalsIgnoreCase("male")
				|| input.equalsIgnoreCase("laki-laki") ? "laki-laki"
				: "perempuan";
		String[] genders = getResources().getStringArray(R.array.gender);
		return Arrays.asList(genders).indexOf(localInput);
	}

	private int getMaritalIndex(String input) {
		String localInput = input.toLowerCase();
		System.out.println("local input::" + localInput);
		String[] maritals = getResources().getStringArray(R.array.marital);
		return Arrays.asList(maritals).indexOf(localInput);
	}

	private int getHomeIndex(String input) {
		System.out.println("local input::" + input);
		String[] homes = getResources().getStringArray(R.array.home_status);
		return Arrays.asList(homes).indexOf(input);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_personal_fragment, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.personal_save:
			saving();
			break;

		case R.id.personal_add_child:
			AddChildDialog addChildDialog = new AddChildDialog();
			addChildDialog.setOnAddChildDialogListener(this);
			addChildDialog.show(getFragmentManager(), "child");
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void saving() {
		if (conf.getPref().isReadOnly())
			return;

		String addrCurrent = tvAddrCurrent.getText().toString();
		String addrKtp = tvAddrKtp.getText().toString();
		int age = tvAge.getText().toString().isEmpty() ? 0 : Integer
				.parseInt(tvAge.getText().toString());
		String nameAlias = tvNameAlias.getText().toString();
		String birthDate = tvDateBirth.getText().toString();
		String birthPlace = tvDatePlace.getText().toString();
		String gender = spnGender.getSelectedItem().toString()
				.replace(" ", "_").toUpperCase();
		String hpNumber = tvHandphoneNumber.getText().toString();
		String homeStatus = spnHomeStatus.getSelectedItem().toString()
				.replace(" ", "_").toUpperCase();
		int idPersonal = personal.getId();
		String ktpNumber = tvKtpNumber.getText().toString();
		String maritalStatus = spnMaritalStatus.getSelectedItem().toString()
				.replace(" ", "_").toUpperCase();
		String motherName = tvMotherName.getText().toString();
		String name = tvNameFull.getText().toString();
		int numberDependents = tvNumberDependents.getText().toString()
				.isEmpty() ? 0 : Integer.parseInt(tvNumberDependents.getText()
				.toString());
		String phoneNumber = tvPhoneNumber.getText().toString();
		String spouseJob = tvSpouceJob.getText().toString();
		String spouseName = tvSpouceName.getText().toString();

		Personal data = new Personal.Builder().addressByCurrent(addrCurrent)
				.addressByKtp(addrKtp).age(age).birthDate(birthDate)
				.birthPlace(birthPlace).children(children).gender(gender)
				.handphoneNumber(hpNumber).homeStatus(homeStatus)
				.id(idPersonal).ktpNumber(ktpNumber)
				.motherMaidenName(motherName).name(name).nameAlias(nameAlias)
				.numberOfDependents(numberDependents).phoneNumber(phoneNumber)
				.spouseJob(spouseJob).spouseName(spouseName)
				.statusOfMarital(maritalStatus).build();

		userController.putPersonal(data, proposalId);
		// storageController.savePersonal(data, proposalId);

		Toast.makeText(getActivity(), "data personal berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onResume() {
		super.onResume();
		userController.getPersonalFromLocal(proposalId);
		System.out.println("ONRESUME");
	}

	@Override
	public void onPause() {
		super.onPause();
		saving();
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	@Override
	public void onChildSaved(Children child) {
		children.add(child);
		childrenAdapter.notifyDataSetChanged();
		ListviewUtils.setListViewHeightBasedOnChildren(lvChild);
		saving();
	}

	private void registerSubcribers() {
		addEventSubscriber(new EventSubscriber<LocalPersonalLoaded>() {

			@Override
			public void handleEvent(LocalPersonalLoaded event) {
				updateView(event.personal);
			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}
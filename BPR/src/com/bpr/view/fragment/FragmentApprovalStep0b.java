package com.bpr.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;

import com.bpr.R;
import com.bpr.application.ConfigApplication;
import com.bpr.view.activity.ApprovalParentActivty;
import com.bpr.view.activity.ApprovalParentActivty.ApprovalSlidedListener;
import com.customer.model.Summary;

public class FragmentApprovalStep0b extends Fragment implements
		ApprovalSlidedListener {
	private EditText plafon;
	private TextView bakiDebet, totalPinjaman, totalJaminan;
	private TableRow tableRowPinjaman, tableRowJaminan;
	private Summary summary;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ConfigApplication conf=(ConfigApplication) getActivity().getApplication();
		summary =conf.getSummary();
		
		ApprovalParentActivty parentActivity = (ApprovalParentActivty) getActivity();
		parentActivity.setSliderListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.approval_step0b,
				container, false);
		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onApprovalPageSlided(int currentPage, int totalPages) {

		System.out.println("page " + currentPage + " saved");
	}
}

package com.bpr.view.fragment;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.bpr.utils.MoneyEditText;
import com.customer.model.SubmissionCredit;
import com.proposalbox.event.LocalSubmissionLoaded;

public class FragmentSubmissionCredit extends Fragment {
	private EditText periode;
	private MoneyEditText plafon;
	private Spinner product;
	private SubmissionCredit submission;
	private ConfigApplication conf;
	private int proposalId;
	private UserController user;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		// storageController = conf.getStorageController();
		user = conf.getUserController();
		eventPublisher = conf.getEventPublisher();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_SUMBISSION)) {
			submission = (SubmissionCredit) b
					.getSerializable(FragmentPage.TAG_SUMBISSION);
		}
		registerSubcribers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_submission,
				container, false);
		plafon = (MoneyEditText) v.findViewById(R.id.submission_plafon);
		product = (Spinner) v.findViewById(R.id.submission_credit_product);
		periode = (EditText) v.findViewById(R.id.submission_periode);
		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		updateView(submission);

		// / setup the form readable or not
		setHasOptionsMenu(!conf.isReadOnly());
		plafon.setEnabled(!conf.isReadOnly());
		product.setEnabled(!conf.isReadOnly());
		periode.setEnabled(!conf.isReadOnly());

	}

	@Override
	public void onResume() {
		super.onResume();
		user.getSubmissionFromLocal(proposalId);
	}

	private void updateView(SubmissionCredit submission) {
		plafon.setText(submission.getPlafon() + "");
		periode.setText(submission.getPeriode() + "");
		product.setSelection(getCreditProductIndex(submission
				.getCreditProduct()));
	}

	private int getCreditProductIndex(String input) {
		String inputConverted = input.replace("_", " ").toLowerCase();
		String[] products = getResources().getStringArray(
				R.array.credit_product);
		for (int i = 0; i < products.length; i++) {
			if (products[i].equalsIgnoreCase(inputConverted)
					|| products[i].contains(inputConverted)) {
				return i;
			}
		}
		return 0;
	}

	@Override
	public void onPause() {
		super.onPause();
		save();
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		String productLabel = product.getSelectedItem().toString()
				.replace(" ", "_").toUpperCase();
		int id = submission.getId();
		// BigDecimal plafonBigdecimal = plafon.getText().toString().isEmpty() ?
		// new BigDecimal(
		// 0) : new BigDecimal(plafon.getNominal().toString());
		BigDecimal plafonBigdecimal = plafon.getNominal();
		double interest = submission.getInterest();
		int period = periode.getText().toString().isEmpty() ? 0 : Integer
				.parseInt(periode.getText().toString());

		SubmissionCredit submission = new SubmissionCredit.Builder()
				.creditProduct(productLabel).id(id).interest(interest)
				.periode(period).plafon(plafonBigdecimal.toString()).build();

		user.putSubmission(submission, proposalId);
		// storageController.saveSubmission(submission, proposalId);

		Toast.makeText(getActivity(), "pengajuan kredit berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.saved:
			save();
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.save, menu);
	}

	private void registerSubcribers() {
		addEventSubscriber(new EventSubscriber<LocalSubmissionLoaded>() {

			@Override
			public void handleEvent(LocalSubmissionLoaded event) {
				updateView(event.submissionCredit);
			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}
package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.FragmentPage;
import com.bpr.adapter.TabunganAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.dialog.AddTabunganDialog;
import com.bpr.dialog.AddTabunganDialog.OnTabunganAddedListener;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.StorageController;
import com.bpr.integration.controller.UserController;
import com.customer.model.CollateralTabungan;
import com.customer.model.CollateralTabunganCollection;
import com.proposalbox.event.LocalTabunganLoaded;

public class FragmentCollateralTabungan extends Fragment implements
		OnTabunganAddedListener {
	private CollateralTabunganCollection tabungan;
	private ListView lv;
	private TabunganAdapter adapter;
	private ConfigApplication conf;
	private List<CollateralTabungan> listTabungan = new ArrayList<CollateralTabungan>();
	private int proposalId;
	private UserController userController;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b.containsKey(FragmentPage.TAG_COLLATERAL_TABUNGAN)) {
			tabungan = (CollateralTabunganCollection) b
					.getSerializable(FragmentPage.TAG_COLLATERAL_TABUNGAN);
			listTabungan.addAll(tabungan.listTabungan);
		}
		adapter = new TabunganAdapter(getActivity(), R.layout.item_tabungan,
				listTabungan);
		registerSubcribers();
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_purpose,
				container, false);
		lv = (ListView) v.findViewById(R.id.list_purpose);
		lv.setAdapter(adapter);
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				CollateralTabungan tabungan = (CollateralTabungan) arg0
						.getAdapter().getItem(arg2);

				AddTabunganDialog addDialog = new AddTabunganDialog();
				addDialog
						.setOnTabunganAddedListener(FragmentCollateralTabungan.this);
				addDialog.setTabungan(tabungan);
				addDialog.show(getFragmentManager(), "addTabungan");
			}
		});
		if (conf.isReadOnly()) {
			setHasOptionsMenu(false);
		} else {
			setHasOptionsMenu(true);
		}
		return v;
	}

	@Override
	public void onResume() {
		super.onResume();
		userController.getTabunganFromLocal(proposalId);
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.add, menu);
	}

	@Override
	public void onPause() {
		super.onPause();
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		userController.putTabungan(listTabungan, proposalId);
		// storageController.saveTabungan(listTabungan, proposalId);
		Toast.makeText(getActivity(), "tabungan berhasil disimpan",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onStop() {
		super.onStop();
		removeEventSubscribers();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.add:

			AddTabunganDialog addDialog = new AddTabunganDialog();
			addDialog.setOnTabunganAddedListener(this);
			addDialog.show(getFragmentManager(), "addTabungan");
			break;

		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private void registerSubcribers() {
		addEventSubscriber(new EventSubscriber<LocalTabunganLoaded>() {

			@Override
			public void handleEvent(LocalTabunganLoaded event) {
				System.out.println("tabungan loaded");
				listTabungan.clear();
				listTabungan.addAll(event.listTabungan);
				adapter.notifyDataSetChanged();
			}
		});

	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	@Override
	public void onTabunganAdded(CollateralTabungan p) {
		listTabungan.add(p);
		adapter.notifyDataSetChanged();
		save();
	}

	@Override
	public void onTabunganEdited() {
		adapter.notifyDataSetChanged();
		save();
	}
}
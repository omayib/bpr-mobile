package com.bpr.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bpr.R;
import com.bpr.adapter.FragmentPage;
import com.bpr.application.ConfigApplication;
import com.bpr.integration.controller.StorageController;
import com.bpr.integration.controller.UserController;
import com.bpr.view.fragment.FragmentBusinessParent.OnAllBusinessSavedListener;
import com.customer.model.Business;

public class FragmentBusiness extends Fragment implements OnAllBusinessSavedListener {
	private Business bu;
	private EditText editText_businessField, editText_companyName,
			editText_companyAddr, editText_companyTenure, editText_siup,
			editText_tdp, editText_numberOfEmployee,
			editText_companyFoundedYear, editText_copmanyProjects,
			editText_otherInfo, editText_yearOperation;
	private ConfigApplication conf;
	private Spinner spnLegallity;
	private int proposalId;

	// private StorageController storageController;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		conf = (ConfigApplication) getActivity().getApplication();
		proposalId = conf.getPref().getCurrentProposal();
		// storageController = conf.getStorageController();

		Bundle b = getArguments();
		if (b == null) {
			return;
		}
		if (b.containsKey(FragmentPage.TAG_BUSINESS)) {
			bu = (Business) b.getSerializable(FragmentPage.TAG_BUSINESS);
		}

		FragmentBusinessParent.registerOnAllBusinessSavedListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.form_business,
				container, false);
		editText_yearOperation = (EditText) v
				.findViewById(R.id.biz_companyYearOfOperation);
		spnLegallity = (Spinner) v.findViewById(R.id.biz_companyLegallity);
		editText_businessField = (EditText) v
				.findViewById(R.id.biz_businessFields);
		editText_companyAddr = (EditText) v
				.findViewById(R.id.biz_companyAddress);
		editText_companyName = (EditText) v.findViewById(R.id.biz_companyName);
		editText_companyTenure = (EditText) v
				.findViewById(R.id.biz_companyTenure);
		editText_companyFoundedYear = (EditText) v
				.findViewById(R.id.biz_companyYear);
		editText_copmanyProjects = (EditText) v.findViewById(R.id.biz_Project);
		editText_numberOfEmployee = (EditText) v
				.findViewById(R.id.biz_numberOfEmployee);
		editText_otherInfo = (EditText) v.findViewById(R.id.biz_companyInfo);
		editText_siup = (EditText) v.findViewById(R.id.biz_companySiup);
		editText_tdp = (EditText) v.findViewById(R.id.biz_companyTdp);
		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		updateView(bu);
		/*
		 * setup view to editable or not
		 */
		editText_yearOperation.setEnabled(!conf.isReadOnly());
		editText_businessField.setEnabled(!conf.isReadOnly());
		editText_companyAddr.setEnabled(!conf.isReadOnly());
		spnLegallity.setEnabled(!conf.isReadOnly());
		editText_companyName.setEnabled(!conf.isReadOnly());
		editText_companyTenure.setEnabled(!conf.isReadOnly());
		editText_companyFoundedYear.setEnabled(!conf.isReadOnly());
		editText_copmanyProjects.setEnabled(!conf.isReadOnly());
		editText_numberOfEmployee.setEnabled(!conf.isReadOnly());
		editText_otherInfo.setEnabled(!conf.isReadOnly());
		editText_siup.setEnabled(!conf.isReadOnly());
		editText_tdp.setEnabled(!conf.isReadOnly());

	}

	private void updateView(Business bu) {
		editText_yearOperation.setText(bu.getYearsOfOperation() + "");
		editText_businessField.setText(bu.getField());
		editText_companyAddr.setText(bu.getAddress());
		editText_companyName.setText(bu.getName());
		editText_companyTenure.setText(bu.getOfficeOwnership());
		editText_companyFoundedYear.setText(bu.getFoundedYear() + "");
		editText_copmanyProjects.setText(bu.getProjectExperiences());
		editText_numberOfEmployee.setText(bu.getNumberOfEmployees() + "");
		editText_otherInfo.setText(bu.getAdditionalInfo());
		editText_siup.setText(bu.getSiupNumber());
		editText_tdp.setText(bu.getTdpNumber());
		spnLegallity.setSelection(getLegallityIndex(bu.getCompanyLegallity()));
	}

	private int getLegallityIndex(String input) {
		String[] legalities = getResources().getStringArray(
				R.array.company_legallity);
		for (int i = 0; i < legalities.length; i++) {
			if (legalities[i].equalsIgnoreCase(input)) {
				System.out.println(legalities[i] + " vs " + input);
				return i;
			}
		}
		return 0;
	}

	@Override
	public void onAllBusinessSaved() {
		System.out.println("on parent paused");
		save();
	}

	private void save() {
		if (conf.getPref().isReadOnly())
			return;
		String additionalInfo = editText_otherInfo.getText().toString();
		String address = editText_companyAddr.getText().toString();
		String companyLegallity = spnLegallity.getSelectedItem().toString();
		String field = editText_businessField.getText().toString();
		int foundedYear = editText_companyFoundedYear.getText().toString()
				.isEmpty() ? 0 : Integer.parseInt(editText_companyFoundedYear
				.getText().toString());

		String name = editText_companyName.getText().toString();
		int numberOfEmployees = editText_numberOfEmployee.getText().toString()
				.isEmpty() ? 0 : Integer.parseInt(editText_numberOfEmployee
				.getText().toString());
		String officeOwnership = editText_companyTenure.getText().toString();
		String projectExperiences = editText_copmanyProjects.getText()
				.toString();
		String siupNumber = editText_siup.getText().toString();
		String tdpNumber = editText_tdp.getText().toString();
		int yearsOfOperation = editText_yearOperation.getText().toString()
				.isEmpty() ? 0 : Integer.parseInt(editText_yearOperation
				.getText().toString());

		Business businees = new Business.Builder().companyAddress(address)
				.companyInfo(additionalInfo).companyLegallity(companyLegallity)
				.companyName(name).companyProject(projectExperiences)
				.companySiup(siupNumber).companyTdp(tdpNumber)
				.companyTenure(officeOwnership).fieldOfBusiness(field)
				.foundedYear(foundedYear).id(bu.getId())
				.numberOfEmployee(numberOfEmployees).year(yearsOfOperation)
				.build();
		conf.getUserController().putBusiness(businees, proposalId);
		// storageController.saveBusiness(businees, proposalId);
		// ===================================================

		try {
			Toast.makeText(
					getActivity(),
					"usaha " + editText_companyName.getText().toString()
							+ " berhasil disimpan", Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
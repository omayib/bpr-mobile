package com.bpr.view.fragment;

import qisc.us.signature.Signature;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.ackbox.event.ApproveAckRequested;
import com.ackbox.event.RejectAckRequested;
import com.bpr.R;
import com.bpr.application.ConfigApplication;
import com.bpr.application.ConfigApplication.BOX_SELECTED;
import com.bpr.view.activity.ApprovalParentActivty;
import com.bpr.view.activity.ApprovalParentActivty.ApprovalSlidedListener;
import com.customer.model.Summary;
import com.customer.model.SummaryCandidate;
import com.followupbox.event.ApproveFollowupRequested;
import com.followupbox.event.RejectFollowupRequested;
import com.proposalbox.event.MarkSummaryAsCompleted;

public class FragmentApprovalStep4 extends Fragment implements
		ApprovalSlidedListener {
	private LinearLayout layoutSignture;
	private ImageButton btnClearSignature;
	private EditText editTextNote;
	private Summary summary;
	private Button buttonApprove, buttonReject;
	private Signature signature;
	private ConfigApplication conf;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		conf = (ConfigApplication) getActivity().getApplication();
		summary = conf.getSummary();

		ApprovalParentActivty parentActivity = (ApprovalParentActivty) getActivity();
		parentActivity.setSliderListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(
				R.layout.approval_step4_signature, container, false);

		btnClearSignature = (ImageButton) v
				.findViewById(R.id.btn_signature_clear);
		layoutSignture = (LinearLayout) v
				.findViewById(R.id.summary_step4_container_signature);
		editTextNote = (EditText) v.findViewById(R.id.summary_step4_note);
		buttonReject = (Button) v.findViewById(R.id.summary_step4_reject);
		buttonApprove = (Button) v.findViewById(R.id.summary_step4_approve);
		btnClearSignature.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (signature != null) {
					signature.clear();
				}
			}
		});
		buttonApprove.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				
				boolean disabled = false;
				if (disabled)
					return;
				
				if(signature.isEmpty()){
					Toast.makeText(getActivity(), "wajib mengisi tanda tangan", Toast.LENGTH_SHORT).show();
					return;
				}
				
				SummaryCandidate candidate = new SummaryCandidate.Builder(conf
						.getPref().getToken(), summary.getProposalId(), true,
						signature.getJson(), editTextNote.getText().toString(),
						summary)
						.idCustomer(conf.getPref().getCurrentCustomerDataId())
						.build();
				System.out.println("ready to upload " + candidate.toString());
				System.out.println("ID PROPOSAL ==>"
						+ conf.getPref().getCurrentProposal());
				
				if (ConfigApplication.currentBoxSelected
						.equals(BOX_SELECTED.PROPOSAL)) {
					conf.getEventPublisher().publish(
							new MarkSummaryAsCompleted(candidate));
				} else if (ConfigApplication.currentBoxSelected
						.equals(BOX_SELECTED.FOLLOWUP)) {
					conf.getEventPublisher().publish(
							new ApproveFollowupRequested(candidate));
				} else if (ConfigApplication.currentBoxSelected
						.equals(BOX_SELECTED.ACK)) {
					conf.getEventPublisher().publish(
							new ApproveAckRequested(candidate));
				}
				getActivity().finish();
			}

		});
		buttonReject.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				SummaryCandidate candidate = new SummaryCandidate.Builder(conf
						.getPref().getToken(), summary.getProposalId(), true,
						signature.getJson(), editTextNote.getText().toString(),
						summary)
						.idCustomer(conf.getPref().getCurrentCustomerDataId())
						.build();
				if (ConfigApplication.currentBoxSelected
						.equals(BOX_SELECTED.FOLLOWUP)) {
					conf.getEventPublisher().publish(
							new RejectFollowupRequested(candidate));
				} else if (ConfigApplication.currentBoxSelected
						.equals(BOX_SELECTED.ACK)) {
					conf.getEventPublisher().publish(
							new RejectAckRequested(candidate));
				}
				getActivity().finish();
			}
		});
		if (ConfigApplication.currentBoxSelected.equals(BOX_SELECTED.PROPOSAL)) {
			buttonApprove.setText("COMPLETE");
			buttonReject.setVisibility(View.GONE);
		} else {
			buttonApprove.setText("APPROVE");
		}
		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		signature = new Signature(getActivity(), null);
		signature.setBackgroundColor(Color.WHITE);
		// signature.onTouchEvent();
		layoutSignture.removeAllViews();
		layoutSignture.addView(signature);

	}

	@Override
	public void onDestroyView() {
		super.onDestroyView();
	}

	@Override
	public void onApprovalPageSlided(int currentPage, int totalPages) {
		System.out.println("page " + currentPage + " saved");
	}
}

package com.bpr.view.fragment;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.bpr.R;
import com.bpr.adapter.RecommendationAdapter;
import com.bpr.application.ConfigApplication;
import com.bpr.view.activity.ApprovalParentActivty;
import com.bpr.view.activity.ApprovalParentActivty.ApprovalSlidedListener;
import com.customer.model.Recommendation;
import com.customer.model.Summary;

public class FragmentApprovalStep3 extends Fragment implements
		ApprovalSlidedListener {
	private ListView summ_listview_approval;
	private Summary summary;
	private RecommendationAdapter adapter;
	private List<Recommendation> recommendations = new ArrayList<Recommendation>();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		ConfigApplication conf = (ConfigApplication) getActivity()
				.getApplication();
		summary = conf.getSummary();
		recommendations = (List<Recommendation>) summary.getRecommendations();

		ApprovalParentActivty parentActivity = (ApprovalParentActivty) getActivity();
		parentActivity.setSliderListener(this);
		adapter = new RecommendationAdapter(getActivity(),
				R.layout.item_user_approval, recommendations);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.approval_step3,
				container, false);
		summ_listview_approval = (ListView) v
				.findViewById(R.id.summary_listview_approval);
		summ_listview_approval.setAdapter(adapter);
		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	@Override
	public void onApprovalPageSlided(int currentPage, int totalPages) {

		System.out.println("page " + currentPage + " saved");
		System.out.println(summary.toString());
	}
}

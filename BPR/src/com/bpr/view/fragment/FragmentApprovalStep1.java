package com.bpr.view.fragment;

import com.bpr.R;
import com.bpr.application.ConfigApplication;
import com.bpr.utils.MoneyEditText;
import com.bpr.view.activity.ApprovalParentActivty;
import com.bpr.view.activity.ApprovalParentActivty.ApprovalSlidedListener;
import com.customer.model.Summary;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

public class FragmentApprovalStep1 extends Fragment implements
		ApprovalSlidedListener {
	private EditText summ_name, summ_periode, summ_interest,
			summ_creditProduct, summ_p, summ_a;
	private MoneyEditText summ_plafon;
	private Summary summary;
	private ConfigApplication conf;
	private boolean isReadonly;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		conf = (ConfigApplication) getActivity().getApplication();
		isReadonly = !conf.isSummaryEditable();
		System.out.println("sumary editable " + isReadonly);
		summary = conf.getSummary();

		ApprovalParentActivty parentActivity = (ApprovalParentActivty) getActivity();
		parentActivity.setSliderListener(this);
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		ViewGroup v = (ViewGroup) inflater.inflate(R.layout.approval_step1,
				container, false);
		summ_creditProduct = (EditText) v
				.findViewById(R.id.summary_step1_credit_product);
		summ_interest = (EditText) v.findViewById(R.id.summary_step1_interest);
		summ_name = (EditText) v.findViewById(R.id.summary_step1_name);
		summ_periode = (EditText) v.findViewById(R.id.summary_step1_periode);
		summ_plafon = (MoneyEditText) v.findViewById(R.id.summary_step1_plafon);
		summ_a = (EditText) v.findViewById(R.id.summary_step1_a);
		summ_p = (EditText) v.findViewById(R.id.summary_step1_p);

		summ_name.setEnabled(false);
		summ_periode.setEnabled(false);
		summ_creditProduct.setEnabled(false);

		summ_plafon.setEnabled(!isReadonly);
		summ_a.setEnabled(!isReadonly);
		summ_p.setEnabled(!isReadonly);
		summ_interest.setEnabled(!isReadonly);
		return v;
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		System.out.println("summary "+summary.toString());
		summ_creditProduct.setText(summary.getSubmission().getCreditProduct()
				.toString());
		summ_interest.setText(summary.getSubmission().getInterest() + "");
		summ_name.setText(summary.getName());
		summ_periode.setText(summary.getSubmission().getPeriode() + " bulan");
		summ_plafon.setText(summary.getSubmission().getPlafonAsRupiah());
		summ_a.setText(summary.getA() + "");
		summ_p.setText(summary.getP() + "");
	}

	@Override
	public void onApprovalPageSlided(int currentPage, int totalPages) {

		if (!inputValid()) {
			return;
		}

		summary.setA(Float.parseFloat(summ_a.getText().toString()));
		summary.setP(Float.parseFloat(summ_p.getText().toString()));
		summary.getSubmission().setPlafon(summary.getSubmission().getPlafon().toString());
		summary.getSubmission().setInterest(
				Float.parseFloat(summ_interest.getText().toString()));

		System.out.println(summary.toString());

	}

	private boolean inputValid() {
		if (summ_plafon.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "plafon wajib diisi",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		if (summ_a.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "a wajib diisi", Toast.LENGTH_SHORT)
					.show();
			return false;
		}
		if (summ_p.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "p wajib diisi", Toast.LENGTH_SHORT)
					.show();
			return false;
		}
		if (summ_interest.getText().toString().isEmpty()) {
			Toast.makeText(getActivity(), "bunga wajib diisi",
					Toast.LENGTH_SHORT).show();
			return false;
		}
		return true;
	}
}

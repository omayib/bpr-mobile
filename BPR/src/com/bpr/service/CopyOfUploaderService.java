package com.bpr.service;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;

import com.bpr.application.ConfigApplication;
import com.bpr.integration.controller.UserController;
import com.bpr.notification.manager.BprNotificationManager;
import com.customer.model.Photo;
import com.customer.model.PhotoCandidate;
import com.proposalbox.event.PhotoDeleteRequest;
import com.proposalbox.event.PhotoUploadRequested;

public class CopyOfUploaderService extends IntentService {
	private ConfigApplication conf;
	private Photo photo;
	private int folderId, proposalId;
	private String folderName, galleryName;
	private String token;
	private BprNotificationManager notifManager;
	private UserController userController;

	public CopyOfUploaderService() {
		super("Downloader");
		System.out.println("UPLOADER CREATED");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		conf = (ConfigApplication) getApplication();
		userController = conf.getUserController();
		token = conf.getPref().getToken();
		notifManager = conf.getNotifManager();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
	}

	@Override
	public void onHandleIntent(Intent i) {
		Bundle b = i.getExtras();
		if (b == null)
			return;

		if (b.containsKey("photoCreated")) {
			photo = (Photo) b.getSerializable("photoCreated");
			folderId = b.getInt("folderId");
			folderName = b.getString("folderName");
			galleryName = b.getString("galleryName");
			proposalId = b.getInt("proposalId");

			PhotoCandidate photoCandidate = new PhotoCandidate.Builder()
					.folderId(folderId).folderName(folderName)
					.galleryName(galleryName).photo(photo)
					.proposalId(proposalId).build();

			System.out.println("RECEIVE-- PHOTO CANDIDATE"
					+ photoCandidate.toString());
			conf.getEventPublisher().publish(
					new PhotoUploadRequested(photoCandidate));
		} else if (b.containsKey("photoDeleted")) {
			photo = (Photo) b.getSerializable("photoDeleted");
			conf.getEventPublisher().publish(
					new PhotoDeleteRequest(photo, token));
		}
	}
}
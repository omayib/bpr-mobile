package com.bpr.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bpr.service.CopyOfUploaderService;
import com.bpr.service.PhotoReuploadService;
import com.customer.model.Photo;

/**
 * this class receiver used for receive broadcast message from Plugin Photo
 * Viewer
 * */
public class PluginPhotoReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent arg1) {
		// System.out.println("RECEIVE---");
		Bundle b = arg1.getExtras();
		if (b == null)
			return;
		if (b.containsKey("status")) {
			System.out.println(b.getString("status"));
			if (b.getString("status").equalsIgnoreCase("photo_created")) {
				System.out.println("phto created executed");
				Photo photo = (Photo) b.getSerializable("photoCreated");
				System.out.println("RECEIVE--" + photo.getName());
				System.out.println("RECEIVE--" + photo.getInfo());
				System.out.println("RECEIVE--" + photo.getPath());
				System.out.println("RECEIVE--" + photo.getUrl());
				Intent uploaderService = new Intent(context,
						CopyOfUploaderService.class);
				uploaderService.putExtras(b);
				context.startService(uploaderService);
			} else if (b.getString("status").equalsIgnoreCase("upload")) {
				System.out.println("upload executed");
				Intent uploaderService = new Intent(context,
						PhotoReuploadService.class);
				context.startService(uploaderService);
			}else if(b.getString("status").equalsIgnoreCase("photo_deleted")){
				Photo photo = (Photo) b.getSerializable("photoDeleted");
				Intent uploaderService = new Intent(context,
						CopyOfUploaderService.class);
				uploaderService.putExtras(b);
				context.startService(uploaderService);
			}
		}

	}

}

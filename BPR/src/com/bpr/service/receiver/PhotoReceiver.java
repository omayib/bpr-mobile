package com.bpr.service.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;

import com.customer.model.Photo;
import com.customer.model.PhotoCandidate;

public abstract class PhotoReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context arg0, Intent arg1) {
		Bundle b = arg1.getExtras();
		if (b == null)
			return;
		Photo photo = (Photo) b.getSerializable("photoCreated");
		int folderId = b.getInt("folderId");
		int proposalId = b.getInt("proposalId");
		String folderName = b.getString("folderName");
		String galleryName = b.getString("galleryName");

		PhotoCandidate p = new PhotoCandidate.Builder().folderId(folderId)
				.folderName(folderName).galleryName(galleryName).photo(photo)
				.proposalId(proposalId).build();
		onPhotoCreated(p);
	}

	public abstract void onPhotoCreated(PhotoCandidate photoCandidate);

	public void register(final Context activity) {
		System.out.println("registering");
		final IntentFilter intentFilter = new IntentFilter();
		intentFilter.addAction("com.photo.viewer.broadcast");
		activity.registerReceiver(this, intentFilter);
	}

	public void unregister(final Context activity) {
		activity.unregisterReceiver(this);
	}
}

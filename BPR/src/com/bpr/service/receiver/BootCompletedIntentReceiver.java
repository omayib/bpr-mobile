package com.bpr.service.receiver;

import com.bpr.service.BprService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootCompletedIntentReceiver extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		System.out.println("BPR BOOT COMPLETED");
		if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
			Intent pushIntent = new Intent(context, BprService.class);
			context.startService(pushIntent);
		}
	}
}
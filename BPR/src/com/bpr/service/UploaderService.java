package com.bpr.service;

import java.util.ArrayList;
import java.util.List;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;

import com.bpr.application.ConfigApplication;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.bpr.notification.manager.NotificationUploadConfig;
import com.customer.model.Photo;
import com.customer.model.PhotoCandidate;
import com.proposalbox.event.PhotoUpdateSucceded;
import com.proposalbox.event.PhotoUploadFailed;
import com.proposalbox.event.PhotoUploadProgress;

public class UploaderService extends IntentService {

	private static final String BROADCAST_ACTION_SUFFIX = ".broadcast.status";
	private static final String PACKAGE_NAME = UploaderService.class
			.getPackage().getName();
	public static final String UPLOAD_ID = "id";
	public static final String STATUS = "status";
	public static final int STATUS_IN_PROGRESS = 1;
	public static final int STATUS_COMPLETED = 2;
	public static final int STATUS_ERROR = 3;
	private static final int UPLOAD_NOTIFICATION_ID = 1234; // Something unique
	private static final int UPLOAD_NOTIFICATION_ID_DONE = 1235; // Something
																	// unique

	public static boolean isRunning = false;
	private static List<UploadRequest> listUploadRequests;
	private static final String TAG = "AndroidUploadService";

	private NotificationManager notificationManager;
	private Builder notification;
	private PowerManager.WakeLock wakeLock;
	private NotificationUploadConfig notificationConfig;
	private UserController userController;
	private ConfigApplication conf;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;

	private Photo photo;
	private int folderId, proposalId;
	private String folderName, galleryName;
	private String token;

	public static String getActionBroadcast() {
		return PACKAGE_NAME + BROADCAST_ACTION_SUFFIX;
	}

	public UploaderService() {
		super("UploaderService");
	}

	public static void startToUpload(List<UploadRequest> task, Context context) {
		System.out.println("is service running " + isRunning);
		if (!isRunning) {
			// start service
			final Intent intent = new Intent(context, UploaderService.class);
			context.startService(intent);
			listUploadRequests = new ArrayList<UploadRequest>();
		}
		listUploadRequests.addAll(task);
		System.out.println("size task: " + listUploadRequests.size());
		System.out.println(" task: " + listUploadRequests.toString());
	}

	@Override
	public void onCreate() {
		super.onCreate();
		System.out.println("ONCREATE");
		conf = (ConfigApplication) getApplication();
		userController = conf.getUserController();
		eventPublisher = conf.getEventPublisher();
		notificationConfig = new NotificationUploadConfig();
		notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		notification = new NotificationCompat.Builder(this);
		PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
		wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);

		registerSubcribers();
	}

	private void registerSubcribers() {
		addEventSubscriber(new EventSubscriber<PhotoUploadProgress>() {

			@Override
			public void handleEvent(PhotoUploadProgress event) {
				broadcastProgress((int) event.progress, event.name);
			}
		});

		addEventSubscriber(new EventSubscriber<PhotoUpdateSucceded>() {

			@Override
			public void handleEvent(PhotoUpdateSucceded event) {

				broadcastProgress(100, "completed");
			}
		});
		addEventSubscriber(new EventSubscriber<PhotoUploadFailed>() {

			@Override
			public void handleEvent(PhotoUploadFailed event) {
			}
		});
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		isRunning = false;
		System.out.println("destroyed");
		removeEventSubscribers();
	}

	public void handleUploadTask() {
		System.out.println("GO!!!");

		for (UploadRequest currentReq : listUploadRequests) {

			System.out.println("while ke-"
					+ currentReq.getPhotoCandidate().getPhoto().getUniqueId());
			System.out.println("size task: " + listUploadRequests.size());
			PhotoCandidate currentCandidate = currentReq.getPhotoCandidate();
			notificationConfig = currentReq.getNotifConfig();
			userController.uploadPhoto(conf.getPref().getToken(),
					currentCandidate.getFolderId(), currentCandidate);
		}

	}

	@Override
	public void onHandleIntent(Intent i) {
		System.out.println("UPLOADER onHandleIntent!!");
		System.out.println("size task: " + listUploadRequests.size());
		isRunning = true;
		createNotification();
		try {
			wakeLock.acquire();
			handleUploadTask();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			wakeLock.release();
		}
	}

	private void broadcastProgress(int progress, String name) {
		System.out.println("progres " + name + " : " + progress);
		updateNotificationProgress(progress);

		final Intent intent = new Intent(getActionBroadcast());
		intent.putExtra(UPLOAD_ID, name);
		intent.putExtra(STATUS, STATUS_IN_PROGRESS);
		sendBroadcast(intent);
	}

	private void broadcastCompleted(int progress, String name) {
		System.out.println("complete progres " + name + " : " + progress);
		updateNotificationCompleted();
		final Intent intent = new Intent(getActionBroadcast());
		intent.putExtra(UPLOAD_ID, name);
		intent.putExtra(STATUS, STATUS_COMPLETED);
		sendBroadcast(intent);
	}

	private void createNotification() {
		notification
				.setContentTitle(notificationConfig.getTitle())
				.setContentText(notificationConfig.getMessage())
				.setContentIntent(
						PendingIntent.getBroadcast(this, 0, new Intent(),
								PendingIntent.FLAG_UPDATE_CURRENT))
				.setSmallIcon(notificationConfig.getIconResourceID())
				.setProgress(10, 0, true).setOngoing(true);

		startForeground(UPLOAD_NOTIFICATION_ID, notification.build());
	}

	private void updateNotificationProgress(final int progress) {
		notification.setContentTitle(notificationConfig.getTitle())
				.setContentText(notificationConfig.getMessage())
				.setSmallIcon(notificationConfig.getIconResourceID())
				.setProgress(10, progress, false).setOngoing(true);

		startForeground(UPLOAD_NOTIFICATION_ID, notification.build());
	}

	private void updateNotificationCompleted() {
		stopForeground(notificationConfig.isAutoClearOnSuccess());
		if (!notificationConfig.isAutoClearOnSuccess()) {
			notification.setContentTitle(notificationConfig.getTitle())
					.setContentText(notificationConfig.getCompleted())
					.setSmallIcon(notificationConfig.getIconResourceID())
					.setProgress(0, 0, false).setOngoing(false);

			notificationManager.notify(UPLOAD_NOTIFICATION_ID_DONE,
					notification.build());
		}
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	private class UploadRunnable implements Runnable {
		private final String token;
		private final int idFolder;
		private final PhotoCandidate candidate;

		public UploadRunnable(String token, int idFolder,
				PhotoCandidate candidate) {
			super();
			this.token = token;
			this.idFolder = idFolder;
			this.candidate = candidate;
		}

		@Override
		public void run() {
			System.out.println("running thread photo:"
					+ candidate.getPhoto().getName());
			userController.uploadPhoto(token, idFolder, candidate);
		}
	}
}
package com.bpr.service;

import android.app.IntentService;
import android.content.Intent;

import com.bpr.application.ConfigApplication;
import com.proposalbox.event.PhotoReupload;

public class PhotoReuploadService extends IntentService {
	private ConfigApplication conf;

	public PhotoReuploadService() {
		super("Downloader");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		conf = (ConfigApplication) getApplication();
	}

	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);
	}

	@Override
	public void onHandleIntent(Intent i) {
		System.out.println("reupload");
		conf.getEventPublisher().publish(new PhotoReupload());
	}
}
package com.bpr.service;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;

import com.bpr.R;
import com.bpr.application.ConfigApplication;
import com.bpr.event.PullRequestRequested;
import com.bpr.event.manager.EventPublisher;
import com.bpr.view.activity.DashboardActivity;

public class BprService extends IntentService {
	private static int NOTIFICATION_ID = 343;
	private AlarmManager alarmMgr;
	private PendingIntent alarmIntent;
	private static EventPublisher eventPublisher;
	private static ConfigApplication conf;
	private Intent pintent;
	public static boolean isRunning;

	public BprService() {
		super("BprService");
	}

	@Override
	public void onCreate() {
		super.onCreate();
		conf = (ConfigApplication) getApplication();
		System.out.println("oncreate servic::" + ConfigApplication.INITIALIZED);
		if (!ConfigApplication.INITIALIZED) {
			System.out.println("oncreate not inititalized");
			conf.initWorker();
		}
		eventPublisher = conf.getEventPublisher();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		isRunning = true;
		startForeground(NOTIFICATION_ID, getNotification());

		alarmMgr = (AlarmManager) getSystemService(ALARM_SERVICE);
		pintent = new Intent(this, AlarmReciever.class);
		pintent.setAction("alarm.receiver.on.bpr");
		alarmIntent = PendingIntent.getBroadcast(this, 1, pintent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		alarmMgr.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,
				SystemClock.elapsedRealtime() + 4 * 1000,
				AlarmManager.INTERVAL_FIFTEEN_MINUTES, alarmIntent);
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		System.out.println("service destroyed");
		isRunning = false;
		alarmMgr.cancel(alarmIntent);
		stopForeground(true);
	}

	@Override
	protected void onHandleIntent(Intent arg0) {

	}

	private Notification getNotification() {
		Notification notification = new Notification(R.drawable.ic_launcher,
				"BPR", System.currentTimeMillis());
		Intent notificationIntent = new Intent(this, DashboardActivity.class);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(this, "BPR", "connected", pendingIntent);
		return notification;
	}

	public static class AlarmReciever extends BroadcastReceiver {
		@Override
		public void onReceive(Context context, Intent intent) {
			System.out.println("called");
			System.out.println("AlarmReciever servic::"
					+ ConfigApplication.INITIALIZED);

			if (!ConfigApplication.INITIALIZED) {
				System.out.println("not inititalized");
				conf.initWorker();
			}

			if (eventPublisher == null) {
				return;
			}
			if (intent == null) {
				return;
			}
			eventPublisher.publish(new PullRequestRequested());
		}
	}

}

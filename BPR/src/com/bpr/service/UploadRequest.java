package com.bpr.service;

import android.content.Context;

import com.bpr.notification.manager.NotificationUploadConfig;
import com.customer.model.PhotoCandidate;

public class UploadRequest {
	private PhotoCandidate photoCandidate;
	private NotificationUploadConfig notifConfig;

	public UploadRequest(PhotoCandidate photoCandidate,
			NotificationUploadConfig notifConfig) {
		super();
		this.photoCandidate = photoCandidate;
		this.notifConfig = notifConfig;
	}

	private Context context;

	public UploadRequest(Context context) {
		super();
		this.context = context;
	}

	public Context getContext() {
		return context;
	}

	public PhotoCandidate getPhotoCandidate() {
		return photoCandidate;
	}

	public void setPhotoCandidate(PhotoCandidate photoCandidate) {
		this.photoCandidate = photoCandidate;
	}

	public NotificationUploadConfig getNotifConfig() {
		return notifConfig;
	}

	public void setNotifConfig(NotificationUploadConfig notifConfig) {
		this.notifConfig = notifConfig;
	}

}

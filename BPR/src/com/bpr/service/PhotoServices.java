package com.bpr.service;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.bpr.R;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.notification.manager.NotificationUploadConfig;
import com.customer.model.Photo;
import com.customer.model.PhotoCandidate;
import com.proposalbox.event.PhotoCandidateToUpload;
import com.proposalbox.event.PhotoDeleteSucceeded;
import com.proposalbox.event.PhotoUpdateSucceded;

public class PhotoServices {

	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private EventPublisher eventPublisher;
	private Context context;

	public PhotoServices(Context context, EventPublisher eventPublisher) {
		super();
		this.context = context;
		this.eventPublisher = eventPublisher;
	}

	public void init() {
		registeringSubscriber();
	}

	public void tearDown() {
		removeEventSubscribers();
	}

	private void registeringSubscriber() {

		addEventSubscriber(new EventSubscriber<PhotoCandidateToUpload>() {

			@Override
			public void handleEvent(PhotoCandidateToUpload event) {
				System.out.println("prepare to upload");
				List<PhotoCandidate> listPhotoCandidate = event.listPhotoCandidates;
				List<UploadRequest> listUploadRequests = new ArrayList<UploadRequest>();
				for (PhotoCandidate p : listPhotoCandidate) {
					NotificationUploadConfig notif = new NotificationUploadConfig(
							R.drawable.ic_launcher, p.getPhoto().getName(), p
									.getPhoto().getName() + " uploading...", p
									.getPhoto().getName() + " completed", p
									.getPhoto().getName() + " on error", true);
					UploadRequest uploadRequest = new UploadRequest(p, notif);
					listUploadRequests.add(uploadRequest);
				}
				UploaderService.startToUpload(listUploadRequests, context);
			}
		});
		addEventSubscriber(new EventSubscriber<PhotoUpdateSucceded>() {

			@Override
			public void handleEvent(PhotoUpdateSucceded event) {
				System.out.println("UPLOAD SUCCEDED");
				Photo photo = new Photo.Builder()
						.id(event.latestCandidate.getPhoto().getId())
						.info(event.latestCandidate.getPhoto().getInfo())
						.name(event.latestCandidate.getPhoto().getName())
						.url(event.latestCandidate.getPhoto().getUrl()).build();
				Bundle b = new Bundle();
				b.putSerializable("photoCreated", photo);
				Intent returnIntent = new Intent();
				returnIntent.setAction("com.photo.uploaded");
				returnIntent.putExtras(b);

				context.sendBroadcast(returnIntent);
			}
		});
		addEventSubscriber(new EventSubscriber<PhotoDeleteSucceeded>() {

			@Override
			public void handleEvent(PhotoDeleteSucceeded event) {
				System.out.println("DELETE SUCCEDED");
				Photo photo = new Photo.Builder()
						.id(event.photo.getId())
						.info(event.photo.getInfo())
						.name(event.photo.getName())
						.url(event.photo.getUrl()).build();
				Bundle b = new Bundle();
				b.putSerializable("photoDeleted", photo);
				Intent returnIntent = new Intent();
				returnIntent.setAction("com.photo.uploaded");
				returnIntent.putExtras(b);

				context.sendBroadcast(returnIntent);
			}
		});

	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}

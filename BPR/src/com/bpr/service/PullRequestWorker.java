package com.bpr.service;

import java.util.ArrayList;
import java.util.List;

import com.ackbox.event.RefreshAckBoxSucceded;
import com.bpr.controller.PreferenceController;
import com.bpr.event.PullRequestRequested;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.BoxController;
import com.bpr.notification.manager.BprNotificationManager;
import com.followupbox.event.RefreshFollowupBoxSucceeded;
import com.newbox.event.RefreshNewboxSucceeded;
import com.proposalbox.event.RefreshProposalBoxSucceeded;
import com.user.modul.User;
import com.user.modul.UserLevel;

public class PullRequestWorker {
	private EventPublisher eventPublisher;
	private BoxController boxController;
	private BprNotificationManager notifManager;
	private PreferenceController pref;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();

	public PullRequestWorker(EventPublisher eventPublisher,
			BoxController boxController, PreferenceController pref,
			BprNotificationManager notifManager) {
		super();
		this.eventPublisher = eventPublisher;
		this.boxController = boxController;
		this.pref = pref;
		this.notifManager = notifManager;
	}

	public void tearDown() {
		removeEventSubscribers();
	}

	public void init() {
		addEventSubscriber(new EventSubscriber<PullRequestRequested>() {

			public void handleEvent(PullRequestRequested event) {
				User us = pref.getUser();
				System.out.println("pull:" + us.toString());
				System.out.println("pull level:" + us.getLevel());
				if (us.getLevel() == null) {
					System.out.println("============ NULL +++++++");
					return;
				}
				if (us.getLevel().equals(UserLevel.ANALYSIS_OFFICER)) {
					System.out.println("pull ao");
					boxController.proposalBoxRefresh(pref.getToken());
				}
				if (us.getLevel().equals(UserLevel.COMMISSIONER)) {
					System.out.println("pull COMMISSIONERS");

					boxController.ackBoxRefresh(pref.getToken());
				}
				if (us.getLevel().equals(UserLevel.DIRECTOR)) {
					System.out.println("pull DIRECTOR");
					boxController.newBoxrefresh(pref.getToken());
					boxController.proposalBoxRefresh(pref.getToken());
					boxController.refreshFollowupBox(pref.getToken());
					boxController.ackBoxRefresh(pref.getToken());

				}
				if (us.getLevel().equals(UserLevel.HEAD_OF_MARKETING)) {
					System.out.println("pull HEAD_OF_MARKETING");
					boxController.newBoxrefresh(pref.getToken());
					boxController.proposalBoxRefresh(pref.getToken());
					boxController.refreshFollowupBox(pref.getToken());
				}

			}
		});

		addEventSubscriber(new EventSubscriber<RefreshFollowupBoxSucceeded>() {

			public void handleEvent(RefreshFollowupBoxSucceeded event) {
				notifManager.updatePullNotification(event.response.size()
						+ " data followup");
				pref.setUnreadBox("followup_unread", event.response.size());
			}
		});
		addEventSubscriber(new EventSubscriber<RefreshNewboxSucceeded>() {

			public void handleEvent(RefreshNewboxSucceeded event) {
				notifManager.updatePullNotification(event.response.size()
						+ " data baru");
				pref.setUnreadBox("new_unread", event.response.size());

			}
		});

		addEventSubscriber(new EventSubscriber<RefreshProposalBoxSucceeded>() {

			public void handleEvent(RefreshProposalBoxSucceeded event) {
				notifManager.updatePullNotification(event.response.size()
						+ " data proposal");
				pref.setUnreadBox("proposal_unread", event.response.size());

			}
		});
		addEventSubscriber(new EventSubscriber<RefreshAckBoxSucceded>() {

			public void handleEvent(RefreshAckBoxSucceded event) {
				notifManager.updatePullNotification(event.response.size()
						+ " data ack");
				pref.setUnreadBox("ack_unread", event.response.size());

			}
		});
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}
}

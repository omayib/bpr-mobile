package com.drawer.nav;

import android.content.Context;

public class NavMenuItem implements NavDrawerItem {

	public static final int ITEM_TYPE = 1;
	private int id;
	private String label;
	private int icon;
	private boolean updateActionBarTittle;

	@Override
	public String toString() {
		return "NavMenuItem [id=" + id + ", label=" + label + ", icon=" + icon
				+ ", updateActionBarTittle=" + updateActionBarTittle + "]";
	}

	public NavMenuItem() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static NavMenuItem create(int _id, String _label, String _icon,
			boolean _updateActionBarTittle, Context context) {

		NavMenuItem item = new NavMenuItem();
		item.setID(_id);
		item.setLabel(_label);
		item.setIcon(context.getResources().getIdentifier(_icon, "drawable",
				context.getPackageName()));
		item.setUpdateActionBaraTitle(_updateActionBarTittle);

		return item;

	}

	public void setID(int _id) {
		this.id = _id;
	}

	@Override
	public int getID() {
		// TODO Auto-generated method stub
		return id;
	}

	public void setLabel(String _label) {
		this.label = _label;
	}

	public void setIcon(int _icon) {
		this.icon = _icon;
	}

	public int getIcon() {
		return icon;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return label;
	}

	@Override
	public int getType() {
		// TODO Auto-generated method stub
		return ITEM_TYPE;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean updateActionBarTittle() {
		// TODO Auto-generated method stub
		return this.updateActionBarTittle;
	}

	public void setUpdateActionBaraTitle(boolean _updateTitle) {
		this.updateActionBarTittle = _updateTitle;
	}

}

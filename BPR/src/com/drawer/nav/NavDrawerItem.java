package com.drawer.nav;

public interface NavDrawerItem {
	int getID();

	String getLabel();

	int getType();

	boolean isEnabled();

	boolean updateActionBarTittle();

	// int getViewTypeCount();
	//
	// int getItemViewType(int _position);
}

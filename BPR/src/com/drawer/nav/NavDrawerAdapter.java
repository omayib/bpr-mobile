package com.drawer.nav;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bpr.R;

public class NavDrawerAdapter extends ArrayAdapter<NavDrawerItem> {

	private LayoutInflater inflater;

	public NavDrawerAdapter(Context context, int textViewresourceID,
			NavDrawerItem[] objects) {
		super(context, textViewresourceID, objects);
		// TODO Auto-generated constructor stub
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		View view = null;
		NavDrawerItem menuItem = this.getItem(position);
		if (menuItem.getType() == NavMenuItem.ITEM_TYPE) {
			view = getItemView(convertView, parent, menuItem);

		} else {
			view = getSectionView(convertView, parent, menuItem);
		}
		return view;
	}

	private View getSectionView(View convertView, ViewGroup parent,
			NavDrawerItem menuItem) {
		// TODO Auto-generated method stub
		NavMenuSection menuSection = (NavMenuSection) menuItem;
		NavMenuSectionHolder navMenuSectionHolder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.navdrawer_section, parent,
					false);
			TextView labelView = (TextView) convertView
					.findViewById(R.id.navmenusection_label);

			navMenuSectionHolder = new NavMenuSectionHolder();
			navMenuSectionHolder.labelView = labelView;
			convertView.setTag(navMenuSectionHolder);

		}
		if (navMenuSectionHolder == null) {
			navMenuSectionHolder = (NavMenuSectionHolder) convertView.getTag();
		}

		navMenuSectionHolder.labelView.setText(menuSection.getLabel());

		return convertView;
	}

	private View getItemView(View convertView, ViewGroup parent,
			NavDrawerItem navDrawerItem) {
		// TODO Auto-generated method stub
		NavMenuItem menuItem = (NavMenuItem) navDrawerItem;
		NavMenuItemHolder navMenuItemHolder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.navdrawer_item, parent,
					false);
			TextView labelView = (TextView) convertView
					.findViewById(R.id.navmenuitem_label);
			ImageView iconView = (ImageView) convertView
					.findViewById(R.id.navmenuitem_icon);
			iconView.setVisibility(View.INVISIBLE);
			navMenuItemHolder = new NavMenuItemHolder();
			navMenuItemHolder.labelView = labelView;
			navMenuItemHolder.iconView = iconView;

			convertView.setTag(navMenuItemHolder);
		}

		if (navMenuItemHolder == null) {
			navMenuItemHolder = (NavMenuItemHolder) convertView.getTag();
		}

		navMenuItemHolder.labelView.setText(menuItem.getLabel());
		navMenuItemHolder.iconView.setImageResource(menuItem.getIcon());

		return convertView;
	}

	@Override
	public int getItemViewType(int position) {
		// TODO Auto-generated method stub
		return this.getItem(position).getType();
	}

	@Override
	public int getViewTypeCount() {
		// TODO Auto-generated method stub
		return 2;
	}

	@Override
	public boolean isEnabled(int position) {
		// TODO Auto-generated method stub
		return getItem(position).isEnabled();
	}

	private static class NavMenuItemHolder {
		private TextView labelView;
		private ImageView iconView;
	}

	private static class NavMenuSectionHolder {
		private TextView labelView;
	}

}

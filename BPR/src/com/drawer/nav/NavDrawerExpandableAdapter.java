package com.drawer.nav;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bpr.R;

public class NavDrawerExpandableAdapter extends BaseExpandableListAdapter {

	private LayoutInflater inflater;
	private List<NavMenuSection> listMenuSection = new ArrayList<NavMenuSection>();
	private HashMap<NavMenuSection, List<NavMenuItem>> listMenuChild = new HashMap<NavMenuSection, List<NavMenuItem>>();
	private NavMenuSection currentSection;

	public NavDrawerExpandableAdapter(Context context, int textViewresourceID,
			NavDrawerItem[] objects) {
		this.inflater = LayoutInflater.from(context);
		NavMenuItem menuItem = null;
		List<NavMenuItem> listChild = null;
		for (NavDrawerItem navDrawerItem : objects) {

			if (navDrawerItem.getType() != NavMenuItem.ITEM_TYPE) {
				NavMenuSection menuSection = (NavMenuSection) navDrawerItem;
				listChild = new ArrayList<NavMenuItem>();

				listMenuSection.add(menuSection);
				currentSection = menuSection;
			}
			if (navDrawerItem.getType() == NavMenuItem.ITEM_TYPE) {
				menuItem = (NavMenuItem) navDrawerItem;
				listChild.add(menuItem);
				listMenuChild.put(currentSection, listChild);
			}
		}
		System.out.println(listMenuChild.toString());
	}

	private static class NavMenuItemHolder {
		private TextView labelView;
		private ImageView iconView;
	}

	private static class NavMenuSectionHolder {
		private TextView labelView;
	}

	@Override
	public Object getChild(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return listMenuChild.get(listMenuSection.get(groupPosition)).get(
				childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		// TODO Auto-generated method stub
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		NavMenuItem currentItem = (NavMenuItem) getChild(groupPosition,
				childPosition);
		NavMenuItemHolder navMenuItemHolder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.navdrawer_item, parent,
					false);
			TextView labelView = (TextView) convertView
					.findViewById(R.id.navmenuitem_label);
			ImageView iconView = (ImageView) convertView
					.findViewById(R.id.navmenuitem_icon);
			iconView.setVisibility(View.INVISIBLE);
			navMenuItemHolder = new NavMenuItemHolder();
			navMenuItemHolder.labelView = labelView;
			navMenuItemHolder.iconView = iconView;

			convertView.setTag(navMenuItemHolder);
		}

		if (navMenuItemHolder == null) {
			navMenuItemHolder = (NavMenuItemHolder) convertView.getTag();
		}

		navMenuItemHolder.labelView.setText(currentItem.getLabel());
		navMenuItemHolder.iconView.setImageResource(currentItem.getIcon());

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		// TODO Auto-generated method stub
		return listMenuChild.get(listMenuSection.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int arg0) {
		// TODO Auto-generated method stub
		return listMenuSection.get(arg0);
	}

	@Override
	public int getGroupCount() {
		// TODO Auto-generated method stub
		return listMenuSection.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		// TODO Auto-generated method stub
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isLastChild,
			View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		NavMenuSectionHolder navMenuSectionHolder = null;

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.navdrawer_section, parent,
					false);
			TextView labelView = (TextView) convertView
					.findViewById(R.id.navmenusection_label);

			navMenuSectionHolder = new NavMenuSectionHolder();
			navMenuSectionHolder.labelView = labelView;
			convertView.setTag(navMenuSectionHolder);

		}
		if (navMenuSectionHolder == null) {
			navMenuSectionHolder = (NavMenuSectionHolder) convertView.getTag();
		}

		navMenuSectionHolder.labelView.setText(listMenuSection.get(
				groupPosition).getLabel());

		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean isChildSelectable(int arg0, int arg1) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public void onGroupCollapsed(int groupPosition) {
		// TODO Auto-generated method stub
		super.onGroupCollapsed(groupPosition);
	}

	@Override
	public void onGroupExpanded(int groupPosition) {
		// TODO Auto-generated method stub
		super.onGroupExpanded(groupPosition);
	}

}

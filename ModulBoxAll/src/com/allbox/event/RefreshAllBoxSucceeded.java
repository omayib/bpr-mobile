package com.allbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.event.BoxData;

public class RefreshAllBoxSucceeded implements BprEvent {
	public List<BoxData> response;

	public RefreshAllBoxSucceeded(List<BoxData> response) {
		super();
		this.response = response;
	}
}

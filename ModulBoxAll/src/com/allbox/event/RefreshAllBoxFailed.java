package com.allbox.event;

import com.bpr.event.manager.BprEvent;

public class RefreshAllBoxFailed implements BprEvent {

	public String exception;

	public RefreshAllBoxFailed(String exception) {
		super();
		this.exception = exception;
	}

}

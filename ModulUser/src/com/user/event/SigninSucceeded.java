package com.user.event;

import com.bpr.event.manager.BprEvent;
import com.user.modul.User;

public class SigninSucceeded implements BprEvent {
	public User user;
	public String token;
	public String listBox;

	public SigninSucceeded(User user, String token, String listBox) {
		super();
		this.user = user;
		this.token = token;
		this.listBox = listBox;
	}

	@Override
	public String toString() {
		return "SigninSucceeded [user=" + user + ", token=" + token
				+ ", listBox=" + listBox + "]";
	}

}

//package com.user.modul;
//
//import java.io.Serializable;
//
//public class UserApproval implements Serializable {
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//	private User user;
//	private boolean isAccepted;
//	private String signature;
//
//	private UserApproval(Builder b) {
//		this.user = b.user;
//		this.isAccepted = b.isAccepted;
//		this.signature = b.signature;
//	}
//
//	public User getUser() {
//		return user;
//	}
//
//	public void setUser(User user) {
//		this.user = user;
//	}
//
//	public boolean isAccepted() {
//		return isAccepted;
//	}
//
//	public void setAccepted(boolean isAccepted) {
//		this.isAccepted = isAccepted;
//	}
//
//	public String getSignature() {
//		return signature;
//	}
//
//	public void setSignature(String signature) {
//		this.signature = signature;
//	}
//
//	public static class Builder {
//		private User user;
//		private boolean isAccepted = false;
//		private String signature;
//
//		public Builder signature(String s) {
//			this.signature = s;
//			return this;
//		}
//
//		public Builder user(User u) {
//			this.user = u;
//			return this;
//		}
//
//		public Builder accept() {
//			this.isAccepted = true;
//			return this;
//		}
//
//		public Builder reject() {
//			this.isAccepted = false;
//			return this;
//		}
//
//		public UserApproval build() {
//			return new UserApproval(this);
//		}
//
//	}
// }

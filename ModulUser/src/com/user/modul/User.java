package com.user.modul;

import java.io.Serializable;

public class User implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private int id;
	private String level;
	private UserLevel userLevel = null;

	private User(Builder b) {
		this.name = b.name;
		this.id = b.id;
		this.level = b.level;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "name :" + name + "/n id :" + id + "/n level :" + level;
	}

	public UserLevel getLevel() {
		System.out.println("getlevel:" + level);
		if (level.equalsIgnoreCase("analysis officer")) {
			userLevel = UserLevel.ANALYSIS_OFFICER;
		} else if (level.equalsIgnoreCase("head of marketing")) {
			userLevel = UserLevel.HEAD_OF_MARKETING;
		} else if (level.equalsIgnoreCase("director")) {
			userLevel = UserLevel.DIRECTOR;
		} else if (level.equalsIgnoreCase("commissioner")) {
			userLevel = UserLevel.COMMISSIONER;
		} else {
			userLevel = UserLevel.CREDIT_ADMIN;
		}
		System.out.println("userLevel:" + userLevel.toString());
		return userLevel;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public static class Builder {
		private String name;
		private int id;
		private String level;

		public Builder name(String n) {
			this.name = n;
			return this;
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder level(String lev) {
			this.level = lev;
			return this;
		}

		public User build() {
			return new User(this);
		}

	}
}

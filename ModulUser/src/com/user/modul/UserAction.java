package com.user.modul;

public interface UserAction {
	void signin(String username, String password);

	void signout(String token);

	void requestListOfProposal(String token, int idcustomer);

	void requestProposalDetail(String token, int idproposal, boolean isReadonly);

}

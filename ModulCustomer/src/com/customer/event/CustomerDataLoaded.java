package com.customer.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.CustomerData;

public class CustomerDataLoaded implements BprEvent {
	public CustomerData customer;

	public CustomerDataLoaded(CustomerData customer) {
		super();
		this.customer = customer;
	}

}

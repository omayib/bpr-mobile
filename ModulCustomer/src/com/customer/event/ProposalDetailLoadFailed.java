package com.customer.event;

import com.bpr.event.manager.BprEvent;

public class ProposalDetailLoadFailed implements BprEvent {
	public String exception;

	public ProposalDetailLoadFailed(String exception) {
		super();
		this.exception = exception;
	}

}

package com.customer.event;

import java.io.Serializable;
import java.math.BigDecimal;

import com.bpr.event.manager.BprEvent;
import com.customer.model.RupiahConverter;

public class BoxData implements BprEvent, Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String customerName;
	private int customerId;
	private int id;
	private String customerPlafon;
	private String boxStatus;

	private BoxData(Builder b) {
		super();
		this.id=b.id;
		this.customerName = b.customerName;
		this.customerId = b.customerId;
		this.customerPlafon = b.customerPlafon;
		this.boxStatus = b.boxStatus;
	}

	public int getId() {
		return id;
	}

	public String getCustomerName() {
		return customerName;
	}

	public int getCustomerId() {
		return customerId;
	}

	public String getCustomerPlafon() {
		return RupiahConverter.convert(new BigDecimal(customerPlafon));
	}

	public String getBoxStatus() {
		return boxStatus;
	}

	@Override
	public String toString() {
		return "NewboxData [customerName=" + customerName + ", customerId="
				+ customerId + ", customerPlafon=" + customerPlafon
				+ ", boxStatus=" + boxStatus + "]";
	}

	public static class Builder {
		private String customerName;
		private int customerId;
		private String customerPlafon;
		private String boxStatus;
		private int id;

		public Builder id(int id){
			this.id=id;
			return this;
		}
		public Builder name(String n) {
			this.customerName = n;
			return this;
		}

		public Builder idCustomer(int id) {
			this.customerId = id;
			return this;
		}

		public Builder plafon(String plafon) {
			this.customerPlafon = plafon;
			return this;
		}

		public Builder box(String b) {
			this.boxStatus = b;
			return this;
		}

		public BoxData build() {
			return new BoxData(this);
		}
	}

}

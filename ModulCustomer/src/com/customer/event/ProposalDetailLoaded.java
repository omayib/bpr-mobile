package com.customer.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Proposal;

public class ProposalDetailLoaded implements BprEvent {
	/**
	 * 
	 */
	public Proposal proposal;

	public ProposalDetailLoaded(Proposal proposal) {
		super();
		this.proposal = proposal;
	}

	@Override
	public String toString() {
		return "ProposalDetailLoaded [proposal=" + proposal + "]";
	}

}

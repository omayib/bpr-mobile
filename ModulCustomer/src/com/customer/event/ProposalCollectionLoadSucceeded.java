package com.customer.event;

import java.math.BigDecimal;
import java.util.Collection;

import com.bpr.event.manager.BprEvent;
import com.customer.model.ProposalLite;
import com.customer.model.Recommendation;
import com.customer.model.RupiahConverter;

public class ProposalCollectionLoadSucceeded implements BprEvent {
	public Collection<ProposalLite> proposals;
	public Collection<Recommendation> recomends;
	public String name, plafon, recommendationAsStringObject;

	public ProposalCollectionLoadSucceeded(Collection<ProposalLite> proposals,
			Collection<Recommendation> recomends, String name, String plafon) {
		super();
		this.proposals = proposals;
		this.recomends = recomends;
		this.name = name;
		this.plafon = RupiahConverter.convert(new BigDecimal(plafon));
	}

	public void setRecommendationAsStringObject(
			String recommendationAsStringObject) {
		this.recommendationAsStringObject = recommendationAsStringObject;
	}

	@Override
	public String toString() {
		return "ProposalCollectionLoadSucceeded [proposals=" + proposals
				+ ", recomends=" + recomends + ", name=" + name + ", plafon="
				+ plafon + "]";
	}

}

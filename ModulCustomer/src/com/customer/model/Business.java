package com.customer.model;

import java.io.Serializable;
import java.util.Calendar;

public class Business extends Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static enum Legallity {
		PERORANGAN, CV, PT
	};

	public static enum OfficeTenure {
		SENDIRI, KONTRAK
	};

	private int id = -1;
	private String field = "";
	private String name = "";
	private String address = "";
	private String officeOwnership = "";
	private String siupNumber = "";
	private String tdpNumber = "";
	private int numberOfEmployees = -1;
	private int yearsOfOperation = -1;
	private int foundedYear = -1;
	private String projectExperiences = "";
	private String additionalInfo = "";
	private String companyLegallity = "";

	private Business(Builder b) {
		super();
		this.id = b.id;
		this.field = b.field;
		this.name = b.name;
		this.address = b.address;
		this.officeOwnership = b.officeOwnership;
		this.companyLegallity = b.companyLegallity;
		this.siupNumber = b.siupNumber;
		this.tdpNumber = b.tdpNumber;
		this.numberOfEmployees = b.numberOfEmployees;
		this.yearsOfOperation = b.yearsOfOperation;
		this.projectExperiences = b.projectExperiences;
		this.additionalInfo = b.additionalInfo;
		this.foundedYear = b.foundedYear;
	}

	public int getId() {
		return id;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getOfficeOwnership() {
		return officeOwnership;
	}

	public void setOfficeOwnership(String officeOwnership) {
		this.officeOwnership = officeOwnership;
	}

	public String getSiupNumber() {
		return siupNumber;
	}

	public void setSiupNumber(String siupNumber) {
		this.siupNumber = siupNumber;
	}

	public String getTdpNumber() {
		return tdpNumber;
	}

	public void setTdpNumber(String tdpNumber) {
		this.tdpNumber = tdpNumber;
	}

	public int getNumberOfEmployees() {
		return numberOfEmployees;
	}

	public void setNumberOfEmployees(int numberOfEmployees) {
		this.numberOfEmployees = numberOfEmployees;
	}

	public int getYearsOfOperation() {
		return (yearsOfOperation == -1) ? (Calendar.getInstance().get(
				Calendar.YEAR) - foundedYear) : yearsOfOperation;
	}

	public void setYearsOfOperation(int yearsOfOperation) {
		this.yearsOfOperation = yearsOfOperation;
	}

	public int getFoundedYear() {
		return foundedYear;
	}

	public void setFoundedYear(int foundedYear) {
		this.foundedYear = foundedYear;
	}

	public String getProjectExperiences() {
		return projectExperiences;
	}

	public void setProjectExperiences(String projectExperiences) {
		this.projectExperiences = projectExperiences;
	}

	public String getAdditionalInfo() {
		return additionalInfo;
	}

	public void setAdditionalInfo(String additionalInfo) {
		this.additionalInfo = additionalInfo;
	}

	public String getCompanyLegallity() {
		return companyLegallity;
	}

	public void setCompanyLegallity(String companyLegallity) {
		this.companyLegallity = companyLegallity;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Business other = (Business) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Business [id=" + id + ", field=" + field + ", name=" + name
				+ ", address=" + address + ", officeOwnership="
				+ officeOwnership + ", siupNumber=" + siupNumber
				+ ", tdpNumber=" + tdpNumber + ", numberOfEmployees="
				+ numberOfEmployees + ", yearsOfOperation=" + yearsOfOperation
				+ ", foundedYear=" + foundedYear + ", projectExperiences="
				+ projectExperiences + ", additionalInfo=" + additionalInfo
				+ ", companyLegallity=" + companyLegallity + "]";
	}

	public static class Builder {
		private int id = -1;
		private String field = "";
		private String name = "";
		private String address = "";
		private String officeOwnership = "";
		private String siupNumber = "";
		private String tdpNumber = "";
		private int numberOfEmployees = -1;
		private int yearsOfOperation = -1;
		private int foundedYear = -1;
		private String projectExperiences = "";
		private String additionalInfo = "";
		private String companyLegallity = "";

		public Builder foundedYear(int fy) {
			this.foundedYear = fy;
			return this;
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder fieldOfBusiness(String s) {
			this.field = s;
			return this;
		}

		public Builder companyName(String s) {
			this.name = s;
			return this;
		}

		public Builder companyAddress(String adress) {
			this.address = adress;
			return this;
		}

		public Builder companyTenure(String tenure) {
			this.officeOwnership = tenure;
			return this;
		}

		public Builder companyLegallity(String legal) {
			this.companyLegallity = legal;
			return this;
		}

		public Builder companySiup(String siup) {
			this.siupNumber = siup;
			return this;
		}

		public Builder companyTdp(String tdp) {
			this.tdpNumber = tdp;
			return this;
		}

		public Builder numberOfEmployee(int number) {
			this.numberOfEmployees = number;
			return this;
		}

		public Builder year(int year) {
			this.yearsOfOperation = year;
			return this;
		}

		public Builder companyProject(String project) {
			this.projectExperiences = project;
			return this;
		}

		public Builder companyInfo(String info) {
			this.additionalInfo = info;
			return this;
		}

		public Business build() {
			return new Business(this);
		}

	}

}

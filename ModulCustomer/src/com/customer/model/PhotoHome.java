package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class PhotoHome implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<PhotoFolder> folders = new ArrayList<>();

	private PhotoHome(Builder b) {
		super();
		this.folders = b.folders;
	}

	public Collection<PhotoFolder> getFolders() {
		return folders;
	}

	public void setFolder(PhotoFolder category) {
		this.folders.add(category);
	}

	public void setFolders(Collection<PhotoFolder> photoCollection) {
		this.folders.addAll(photoCollection);
	}

	public static class Builder {
		private Collection<PhotoFolder> folders = new ArrayList<>();

		public Builder folders(Collection<PhotoFolder> p) {
			this.folders.addAll(p);
			return this;
		}

		public Builder folder(PhotoFolder p) {
			this.folders.add(p);
			return this;
		}

		public PhotoHome build() {
			return new PhotoHome(this);
		}

	}

}

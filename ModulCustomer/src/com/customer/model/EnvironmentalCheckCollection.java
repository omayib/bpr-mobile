package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class EnvironmentalCheckCollection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<EnvironmentalCheck> checkCollection = new ArrayList<EnvironmentalCheck>();

	private EnvironmentalCheckCollection(Builder b) {
		this.checkCollection = b.checkCollection;
	}

	public Collection<EnvironmentalCheck> getCheckCollection() {
		return checkCollection;
	}

	public void setCheckCollection(EnvironmentalCheck checkCollection) {
		this.checkCollection.add(checkCollection);
	}

	public void setCheckCollection(
			Collection<EnvironmentalCheck> checkCollection) {
		this.checkCollection.addAll(checkCollection);
	}

	public static class Builder {
		private Collection<EnvironmentalCheck> checkCollection = new ArrayList<EnvironmentalCheck>();

		public Builder environmentalCheck(EnvironmentalCheck e) {
			checkCollection.add(e);
			return this;
		}

		public Builder environmentalCheck(Collection<EnvironmentalCheck> e) {
			checkCollection.addAll(e);
			return this;
		}

		public EnvironmentalCheckCollection build() {
			return new EnvironmentalCheckCollection(this);
		}

	}

}

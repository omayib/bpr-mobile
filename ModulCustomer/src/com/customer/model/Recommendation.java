package com.customer.model;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.user.modul.User;

public class Recommendation implements Serializable {

	private static final long serialVersionUID = 1L;

	private User user;
	private boolean isAccepted;
	private String signature;
	private List<String> notes = new ArrayList<>();
	private String date;
	private String recommendAs;

	private Recommendation(Builder b) {
		super();
		this.user = b.user;
		this.isAccepted = b.isAccepted;
		this.signature = b.signature;
		this.notes = b.notes;
		this.date = b.date;
		this.recommendAs=b.recommendAs;
	}

	
	public String getRecommendAs() {
		return recommendAs;
	}


	public List<String> getNotes() {
		return notes;
	}

	public String getNotesAsString() {
		StringBuilder sb = new StringBuilder();
		for (String n : notes) {
			sb.append(n + "\n");
		}
		return sb.toString();
	}

	public String getDate() {
		return date;
	}

	/*
	 * public List<Note> getNotes() { for (String n : notes) { Note notes = new
	 * Note.Builder().creator(user).messages(n).build();
	 * notesCollections.add(notes); } return notesCollections; }
	 */

	public void setNotes(String notes) {
		this.notes.add(notes);
	}

	public void setNotes(List<String> notes) {
		this.notes.addAll(notes);
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}

	@Override
	public String toString() {
		return "Recommendation [user=" + user + ", isAccepted=" + isAccepted
				+ ", signature=" + signature + ", notes=" + notes + ", date="
				+ date + "]";
	}

	public static class Builder {
		private User user;
		private boolean isAccepted;
		private String signature;
		private List<String> notes = new ArrayList<>();
		private String date;
		private int id;
		private String recommendAs;
		
		public Builder setAccepted(String accepted){
			this.isAccepted=accepted.equalsIgnoreCase("ACCEPTED");
			return this;
		}
		public Builder recommendAs(String recommendAs){
			this.recommendAs=recommendAs;
			return this;
		}
		public Builder date(String d) {
			this.date = d;
			return this;
		}

		public Builder user(User u) {
			this.user = u;
			return this;
		}

		public Builder setAccept() {
			this.isAccepted = true;
			return this;
		}

		public Builder signature(String s) {
			this.signature = s;
			return this;
		}

		public Builder notes(String n) {
			this.notes.add(n);
			return this;
		}

		public Recommendation build() {
			return new Recommendation(this);
		}

	}
}

package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class AssetsCollection implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<Assets> asset = new ArrayList<>();

	private AssetsCollection(Builder b) {
		super();
		this.asset = b.asset;
	}

	public Collection<Assets> getAsset() {
		return asset;
	}

	public void setAsset(Collection<Assets> asset) {
		this.asset.addAll(asset);
	}

	public void setAsset(Assets asset) {
		this.asset.add(asset);
	}

	public static class Builder {
		private Collection<Assets> asset = new ArrayList<>();

		public Builder asset(Collection<Assets> a) {
			this.asset.addAll(a);
			return this;
		}

		public Builder asset(Assets a) {
			this.asset.add(a);
			return this;
		}

		public AssetsCollection build() {
			return new AssetsCollection(this);
		}
	}

}

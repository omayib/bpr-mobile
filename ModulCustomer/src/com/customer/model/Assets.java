package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Assets extends Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;
	private BigDecimal value;

	private Assets(Builder b) {
		super();
		this.name = b.name;
		this.value = b.value;
		this.id = b.id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getValue() {
		return value;
	}

	public String getValueAsRupiah() {
		return RupiahConverter.convert(value);
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Assets other = (Assets) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Assets [id=" + id + ", name=" + name + ", value=" + value + "]";
	}

	public static class Builder {
		private int id;
		private String name;
		private BigDecimal value;

		public Builder asset(int id, String name, String value) {
			String cleaned = "0";
			if (value.contains(".")) {
				cleaned = value.substring(0, value.indexOf("."));
			} else {
				cleaned = value;
			}
			this.name = name;
			this.value = new BigDecimal(cleaned);
			this.id = id;
			return this;
		}

		public Assets build() {
			return new Assets(this);
		}

	}

}

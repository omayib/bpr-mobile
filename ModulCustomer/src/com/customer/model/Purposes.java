package com.customer.model;

import java.io.Serializable;

public class Purposes extends Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String general;
	private String detail;
	private int id;

	private Purposes(Builder b) {
		this.general = b.general;
		this.detail = b.detail;
		this.id = b.id;
	}

	public int getId() {
		return id;
	}

	public String getGeneral() {
		return general;
	}

	public void setGeneral(String general) {
		this.general = general;
	}

	public String getDetail() {
		return detail;
	}

	public void setDetail(String detail) {
		this.detail = detail;
	}

	public static class Builder {
		private int id;
		private String general;
		private String detail;

		public Builder purpose(int id, String general, String detail) {
			this.general = general;
			this.detail = detail;
			this.id = id;
			return this;
		}

		public Purposes build() {
			return new Purposes(this);
		}
	}
}

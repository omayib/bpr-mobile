package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class NotesCollection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Collection<Note> notes = new ArrayList<Note>();

	private NotesCollection(Builder b) {
		this.notes = b.notes;
	}

	public Collection<Note> getNotes() {
		return notes;
	}

	public void setNotes(Collection<Note> notes) {
		this.notes.addAll(notes);
	}

	public static class Builder {
		private Collection<Note> notes = new ArrayList<Note>();

		public Builder notes(Note n) {
			notes.add(n);
			return this;
		}

		public NotesCollection build() {
			return new NotesCollection(this);
		}

	}

}

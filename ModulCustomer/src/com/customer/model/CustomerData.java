package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.user.modul.User;

public class CustomerData extends Model implements Serializable {
	public enum CustomerStatus {
		NEW, PROPOSAL, COMPLETED, FOLLOWUP, ACKNOWLADGE
	}

	private static final long serialVersionUID = 1L;
	private Collection<Proposal> proposals = new ArrayList<Proposal>();
	private String name;
	private BigDecimal plafonSubmited;
	private CustomerStatus status;
	private Summary summary;
	private int assignToId = -1;
	private User reference;
	private Collection<Recommendation> recommendations = new ArrayList<>();

	private CustomerData(Builder b) {
		this.plafonSubmited = b.plafonSubmited;
		this.proposals = b.proposals;
		this.name = b.name;
		this.status = b.status;
		this.reference = b.reference;
		createSummary((List<Proposal>) proposals,reference);
	}

	public Collection<Recommendation> getRecommendations() {
		return recommendations;
	}

	public void setRecommendations(Collection<Recommendation> recommendations) {
		this.recommendations.addAll(recommendations);
		if (summary != null) {
			for (Recommendation recommendation2 : recommendations) {
				summary.setRecommendations(recommendation2);
			}
		}
	}

	public void setRecommendations(Recommendation recommendations) {
		this.recommendations.add(recommendations);
		if (summary != null)
			summary.setRecommendations(recommendations);
	}

	public User getReference() {
		return reference;
	}

	public void setReference(User reference) {
		this.reference = reference;
	}

	public int getAssignToId() {
		return assignToId;
	}

	public void setAssignToId(User user) {
		this.assignToId = user.getId();
	}

	private void createSummary(List<Proposal> p, User reference2) {
		if (p.isEmpty()) {
			return;
		}
		if (p.size() > 1) {
			summary = new Summary.Builder().proposal(p.get(1)).refrence(reference2).build();
			return;
		}
		summary = new Summary.Builder().proposal(p.get(0)).build();

	}

	public Summary getSummary() {
		return summary;
	}

	public void setSummary(Summary summary) {
		this.summary = summary;
	}

	public Collection<Proposal> getProposalsAll() {
		return proposals;
	}

	public Proposal getProposal(User user) {

		for (Proposal pr : proposals) {
			if (pr.getCheckedByName().equalsIgnoreCase(user.getName())) {
				return pr;
			}
		}
		return null;

	}

	public Collection<Proposal> getProposalsBy(User user) {

		Collection<Proposal> props = new ArrayList<>();
		for (Proposal pr : proposals) {
			if (pr.getCheckedByName().equalsIgnoreCase(user.getName())) {
				props.add(pr);
			}
			if (pr.getCheckedByName().equalsIgnoreCase("original")) {
				props.add(pr);
			}
		}

		return props;
	}

	public void setProposals(Proposal p) {
		this.proposals.add(p);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getPlafonSubmited() {
		return plafonSubmited;
	}

	public void setPlafonSubmited(String plafonSubmited) {
		this.plafonSubmited = new BigDecimal(plafonSubmited);
	}

	public CustomerStatus getStatus() {
		if (assignToId == -1) {
			return CustomerStatus.NEW;
		}
		return status;
	}

	public void setStatus(CustomerStatus status) {
		this.status = status;
	}

	public static class Builder {
		private Collection<Proposal> proposals = new ArrayList<Proposal>();
		private String name;
		private BigDecimal plafonSubmited;
		private CustomerStatus status;
		private User reference;

		public Builder reference(User u) {
			this.reference = u;
			return this;
		}

		public Builder customer(Collection<Proposal> proposal) {
			this.proposals.addAll(proposal);
			List<Proposal> proposals = (List<Proposal>) proposal;
			if (proposals.size() > 1) {
				this.name = proposals.get(1).getPersonal().getName();
				this.plafonSubmited = proposals.get(1).getSubmission()
						.getPlafon();
				return this;
			}
			this.name = proposals.get(0).getPersonal().getName();
			this.plafonSubmited = proposals.get(0).getSubmission().getPlafon();
			return this;
		}

		public Builder customer(Proposal proposal) {
			this.proposals.add(proposal);
			this.name = proposal.getPersonal().getName();
			this.plafonSubmited = proposal.getSubmission().getPlafon();
			return this;
		}

		public Builder status(CustomerStatus status) {
			this.status = status;
			return this;
		}

		public CustomerData build() {
			return new CustomerData(this);
		}

	}

}

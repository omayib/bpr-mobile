package com.customer.model;

import java.io.Serializable;

public class Photo extends Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String url = "";
	private String name;
	private String info;
	private String path;
	private int id;
	private String uniqueId = "";

	private Photo(Builder b) {
		this.url = b.url;
		this.name = b.name;
		this.info = b.info;
		this.path = b.path;
		this.id = b.id;
		this.uniqueId = b.uniqueId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((uniqueId == null) ? 0 : uniqueId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Photo other = (Photo) obj;
		if (uniqueId == null) {
			if (other.uniqueId != null)
				return false;
		} else if (!uniqueId.equals(other.uniqueId))
			return false;
		return true;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public int getId() {
		return id;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getPath() {
		return path;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	

	@Override
	public String toString() {
		return "Photo [url=" + url + ", name=" + name + ", info=" + info
				+ ", path=" + path + ", id=" + id + ", uniqueId=" + uniqueId
				+ "]";
	}



	public static class Builder {
		private String url;
		private String name;
		private String info;
		private String path;
		private int id;
		private String uniqueId = "";

		public Builder uniqueId(String u) {
			this.uniqueId = u;
			return this;
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder pathLocal(String p) {
			this.path = p;
			return this;
		}

		public Builder url(String url) {
			this.url = url;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder info(String info) {
			this.info = info;
			return this;
		}

		public Photo build() {
			return new Photo(this);
		}
	}
}

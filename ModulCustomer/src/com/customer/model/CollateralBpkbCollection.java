package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class CollateralBpkbCollection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<CollateralBpkb> guaranteeBpkb = new ArrayList<CollateralBpkb>();

	private CollateralBpkbCollection(Builder b) {
		this.guaranteeBpkb = b.guaranteeBpkb;
	}

	public Collection<CollateralBpkb> getGuaranteeBpkb() {
		return guaranteeBpkb;
	}

	public void setGuaranteeBpkb(Collection<CollateralBpkb> guaranteeBpkb) {
		this.guaranteeBpkb.addAll(guaranteeBpkb);
	}

	public void setGuaranteeBpkb(CollateralBpkb guaranteeBpkb) {
		this.guaranteeBpkb.add(guaranteeBpkb);
	}

	@Override
	public String toString() {
		return "CollateralBpkbCollection [guaranteeBpkb=" + guaranteeBpkb + "]";
	}

	public static class Builder {

		private Collection<CollateralBpkb> guaranteeBpkb = new ArrayList<CollateralBpkb>();

		public Builder bpkb(CollateralBpkb b) {
			this.guaranteeBpkb.add(b);
			return this;
		}

		public Builder bpkb(Collection<CollateralBpkb> b) {
			this.guaranteeBpkb.addAll(b);
			return this;
		}

		public CollateralBpkbCollection build() {
			return new CollateralBpkbCollection(this);
		}
	}
}

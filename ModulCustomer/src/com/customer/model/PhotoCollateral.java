package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class PhotoCollateral implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<PhotoFolder> folders = new ArrayList<>();

	private PhotoCollateral(Builder b) {
		super();
		this.folders = b.folder;
	}

	public Collection<PhotoFolder> getFolders() {
		return folders;
	}

	public void setFolder(PhotoFolder photoCollection) {
		this.folders.add(photoCollection);
	}

	public void setFolders(Collection<PhotoFolder> photoCollection) {
		this.folders.addAll(photoCollection);
	}

	public static class Builder {
		private Collection<PhotoFolder> folder = new ArrayList<>();

		public Builder foldes(Collection<PhotoFolder> p) {
			this.folder.addAll(p);
			return this;
		}

		public Builder folder(PhotoFolder p) {
			this.folder.add(p);
			return this;
		}

		public PhotoCollateral build() {
			return new PhotoCollateral(this);
		}

	}

}

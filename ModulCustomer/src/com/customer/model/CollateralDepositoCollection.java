package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CollateralDepositoCollection implements Serializable {

	private static final long serialVersionUID = 1L;
	public List<CollateralDeposito> listDeposito = new ArrayList<>();

	public CollateralDepositoCollection(List<CollateralDeposito> listDeposito) {
		super();
		this.listDeposito.addAll(listDeposito);
	}

	@Override
	public String toString() {
		return "CollateralDepositoCollection [listDeposito=" + listDeposito
				+ "]";
	}

}

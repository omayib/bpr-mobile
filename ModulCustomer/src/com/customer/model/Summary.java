package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import org.json.JSONArray;
import org.json.JSONObject;

import com.bpr.event.manager.BprEvent;
import com.user.modul.User;

public class Summary extends Model implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	// data splitted to step 1
	private int proposalId;
	private String name;
	private float P;
	private float A;
	private SubmissionCredit submission;
	private boolean avReferral;

	// data splitted to step 2
	private CollateralBpkbCollection bpkb;
	private CollateralShmCollection shm;
	// total values
	private BigDecimal totalValues=new BigDecimal(0);
	private float ratio;
	private User reference;

	// data splitted to step 3
	private Collection<Recommendation> recommendations;

	public Summary() {
		super();
	}

	private Summary(Builder b) {
		this.id=b.id;
		this.proposalId = b.proposalId;
		this.name = b.name;
		this.submission = b.submission;
		this.bpkb = b.bpkb;
		this.shm = b.shm;
		this.reference = b.reference;
		this.P = b.p;
		this.A = b.a;
		this.avReferral = b.avReferral;
		this.totalValues = b.totalValues;
		this.ratio = b.ratio;
		this.recommendations = b.recommendations;
	}

	public int getProposalId() {
		return proposalId;
	}

	public List<SummaryLoan> getSummaryPlafon() {
		List<SummaryLoan> listSummaryPlafon = new ArrayList<>();

		SummaryLoan sp = new SummaryLoan.Builder().loansTo(1)
				.plafon(submission.getPlafonAsRupiah())
				.debitBalance(submission.getPlafonAsRupiah()).build();
		listSummaryPlafon.add(sp);

		return listSummaryPlafon;
	}

	public List<SummaryCollateral> getSummaryCollaterals() {
		List<SummaryCollateral> listCollateral = new ArrayList<>();
		List<CollateralBpkb> listBpkb = (List<CollateralBpkb>) bpkb
				.getGuaranteeBpkb();
		List<CollateralShm> listShm = (List<CollateralShm>) shm
				.getGuaranteeShm();

		for (CollateralShm shm : listShm) {
			SummaryCollateral sc = new SummaryCollateral.Builder().type("shm")
					.name(shm.getStatusOfLand())
					.priceAssesed(shm.getPriceOnAssessed())
					.priceMarket(shm.getPriceOnMarket()).remark(shm.getInfo())
					.build();
			listCollateral.add(sc);
		}
		for (CollateralBpkb bpkb : listBpkb) {
			SummaryCollateral sc = new SummaryCollateral.Builder().type("bpkb")
					.name(bpkb.getCarName())
					.priceAssesed(bpkb.getPriceOnAssessed())
					.priceMarket(bpkb.getPriceOnMarket())
					.remark(bpkb.getInfo()).build();
			listCollateral.add(sc);
		}
		System.out.println("dafta collateral :" + listCollateral.toString());
		return listCollateral;
	}

	public int getId() {
		return id;
	}

	public String getRatio() {
		return String.valueOf(ratio);
	}

	public String getTotalValuesAsRupiah() {
		return RupiahConverter.convert(totalValues);
	}

	public boolean isAvReferral() {
		return avReferral;
	}

	public void setAvReferral(boolean avReferral) {
		this.avReferral = avReferral;
	}

	public SubmissionCredit getSubmission() {
		return submission;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public CollateralBpkbCollection getBpkb() {
		return bpkb;
	}

	public void setBpkb(CollateralBpkbCollection bpkb) {
		this.bpkb = bpkb;
	}

	public CollateralShmCollection getShm() {
		return shm;
	}

	public void setShm(CollateralShmCollection shm) {
		this.shm = shm;
	}

	public float getP() {
		return P;
	}

	public void setP(float p) {
		P = p;
	}

	public float getA() {
		return A;
	}

	public void setA(float a) {
		A = a;
	}

	public User getReference() {
		return reference;
	}

	public void setReference(User reference) {
		this.reference = reference;
	}

	public Collection<Recommendation> getRecommendations() {
		return recommendations;
	}

	public void setRecommendations(Recommendation recommendations) {
		this.recommendations.add(recommendations);
	}

	@Override
	public String toString() {
		return "Summary [name=" + name + ", P=" + P + ", A=" + A
				+ ", submission=" + submission + ", avReferral=" + avReferral
				+ ", bpkb=" + bpkb + ", shm=" + shm + ", totalValues="
				+ totalValues + ", ratio=" + ratio + ", reference=" + reference
				+ ", recommendations=" + recommendations + "]";
	}

	public static class Builder {
		private int proposalId;
		private String name;
		private SubmissionCredit submission;
		private boolean avReferral;
		private CollateralBpkbCollection bpkb;
		private CollateralShmCollection shm;
		private User reference;
		private float p;
		private float a;
		private BigDecimal totalValues;
		private float ratio;
		private Collection<Recommendation> recommendations = new ArrayList<>();
		private int id;
		public Builder summaryId(int id){
			this.id=id;
			return this;
		}
		public Builder proposalId(int id) {
			this.proposalId = id;
			return this;
		}

		public Builder recommendations(List<Recommendation> r) {
			this.recommendations.addAll(r);
			return this;
		}

		public Builder totalValues(String t) {
			this.totalValues = new BigDecimal(t);
			return this;
		}

		public Builder ratio(String r) {
			this.ratio = Float.parseFloat(r);
			return this;
		}

		public Builder proposal(Proposal p) {
			this.name = p.getPersonal().getName();
			this.submission = p.getSubmission();
			this.bpkb = p.getBpkbCollection();
			this.shm = p.getShmCollection();
			return this;
		}

		public Builder p(float p) {
			this.p = p;
			return this;
		}

		public Builder a(float a) {
			this.a = a;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder submission(SubmissionCredit s) {
			this.submission = s;
			return this;
		}

		public Builder avReferral(boolean r) {
			this.avReferral = r;
			return this;
		}

		public Builder bpkbCollection(CollateralBpkbCollection b) {
			this.bpkb = b;
			return this;
		}

		public Builder shmCollection(CollateralShmCollection s) {
			this.shm = s;
			return this;
		}

		public Builder refrence(User reference2) {
			this.reference = reference2;
			System.out.println("create reference");
			return this;
		}

		public Summary build() {
			return new Summary(this);
		}

	}

}

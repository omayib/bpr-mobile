package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;

public class CapacityCollection extends Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<CapacityIncome> income = new ArrayList<CapacityIncome>();;
	private Collection<CapacityExpenditure> expenditure = new ArrayList<CapacityExpenditure>();
	private int id = -1;
	BigDecimal submission = new BigDecimal(0);

	private CapacityCollection(Builder b) {
		super();
		this.income = b.income;
		this.expenditure = b.expenditure;
		this.submission = b.submission;
		this.id = b.id;
	}

	public int getId() {
		return id;
	}

	public Collection<CapacityIncome> getIncome() {
		return income;
	}

	public void setIncome(Collection<CapacityIncome> income) {
		this.income = income;
	}

	public Collection<CapacityExpenditure> getExpenditure() {
		return expenditure;
	}

	public void setExpenditure(Collection<CapacityExpenditure> expenditure) {
		this.expenditure = expenditure;
	}

	@Override
	public String toString() {
		return "CapacityCollection [income=" + income + ", expenditure="
				+ expenditure + ", id=" + id + ", submission=" + submission
				+ "]";
	}

	public BigDecimal getIncomeTotal() {
		BigDecimal totalIncome = new BigDecimal(0);
		for (CapacityIncome in : income) {
			totalIncome = totalIncome.add(in.getValue());
		}
		return totalIncome.setScale(2, RoundingMode.UP);
	}

	public String getIncomeTotalAsRupiah() {
		return RupiahConverter.convert(getIncomeTotal());
	}

	public BigDecimal getExpenditureTotal() {
		BigDecimal totalExpenditure = new BigDecimal(0);
		for (CapacityExpenditure out : expenditure) {
			totalExpenditure = totalExpenditure.add(out.getValue());
		}
		return totalExpenditure.setScale(2, RoundingMode.UP);
	}

	public String getExpenditureTotalAsRupiah() {
		return RupiahConverter.convert(getExpenditureTotal());
	}

	public BigDecimal getIncomeNet() {
		BigDecimal net = new BigDecimal(0, new MathContext(2));
		net = getIncomeTotal().subtract(getExpenditureTotal());
		/*
		 * System.out.println("net:" + getIncomeTotal());
		 * System.out.println("net:" + getExpenditureTotal());
		 * System.out.println("net:" + net); System.out.println("net:" + new
		 * BigDecimal(2.4 / 100)); System.out.println("net:" +
		 * submission.multiply(new BigDecimal(2.4 / 100)));
		 */
		net = net.subtract(submission.multiply(new BigDecimal(0.024)));
		System.out.println("net:" + net);
		return net.setScale(2, RoundingMode.UP);
	}

	public String getIncomeNetAsRupiah() {
		return RupiahConverter.convert(getIncomeNet());
	}

	public BigDecimal getIncomeSubstractExpenditure() {
		BigDecimal net = new BigDecimal(0, new MathContext(2));
		net = getIncomeTotal().subtract(getExpenditureTotal());
		return net.setScale(2, RoundingMode.UP);
	}

	public String getIncomeSubstractExpenditureAsRupiah() {
		return RupiahConverter.convert(getIncomeSubstractExpenditure());
	}

	public String getSubmissionPercentageValueAsRupiah() {
		BigDecimal v = submission.multiply(new BigDecimal(0.024)).setScale(2,
				RoundingMode.UP);
		return RupiahConverter.convert(v);
	}

	public String getSubmissionPercentage() {
		return "2.4% x " + RupiahConverter.convert(submission);
	}

	public void setSubmission(BigDecimal s) {
		this.submission = s;
	}

	public static class Builder {
		private int id;
		private Collection<CapacityIncome> income = new ArrayList<>();
		private Collection<CapacityExpenditure> expenditure = new ArrayList<>();
		BigDecimal submission = new BigDecimal(0);

		public Builder(int id, CapacityIncome income,
				CapacityExpenditure expenditure) {
			super();
			this.id = id;
			this.income.add(income);
			this.expenditure.add(expenditure);
		}

		public Builder submission(BigDecimal s) {
			this.submission = s;
			return this;
		}

		public Builder(int id, Collection<CapacityIncome> income,
				Collection<CapacityExpenditure> expenditure) {
			super();
			this.id = id;
			this.income.addAll(income);
			this.expenditure.addAll(expenditure);
		}

		public CapacityCollection build() {
			return new CapacityCollection(this);
		}
	}

}

package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CollateralTabunganCollection implements Serializable {

	private static final long serialVersionUID = 1L;
	public List<CollateralTabungan> listTabungan = new ArrayList<CollateralTabungan>();

	public CollateralTabunganCollection(List<CollateralTabungan> listTabungan) {
		super();
		this.listTabungan.addAll(listTabungan);
	}

	@Override
	public String toString() {
		return "CollateralTabunganCollection [listTabungan=" + listTabungan
				+ "]";
	}
	
}

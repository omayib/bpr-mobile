package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class PurposesCollection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<Purposes> purposesCollection = new ArrayList<Purposes>();

	private PurposesCollection(Builder b) {
		this.purposesCollection = b.purposesCollection;
	}

	public Collection<Purposes> getPurposesCollection() {
		return purposesCollection;
	}

	public void setPurposesCollection(Collection<Purposes> purposesCollection) {
		this.purposesCollection.addAll(purposesCollection);
	}

	public void setPurposesCollection(Purposes purposesCollection) {
		this.purposesCollection.add(purposesCollection);
	}

	public static class Builder {
		private Collection<Purposes> purposesCollection = new ArrayList<Purposes>();

		public Builder purpose(Purposes p) {
			purposesCollection.add(p);
			return this;
		}

		public Builder purpose(Collection<Purposes> p) {
			purposesCollection.addAll(p);
			return this;
		}

		public PurposesCollection build() {
			return new PurposesCollection(this);
		}
	}

}

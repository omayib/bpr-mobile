package com.customer.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PhotoCandidate {
	private Photo photo;
	private int folderId;
	private String folderName;

	/**
	 * collateral/business/home/other
	 */
	private String galleryName;
	private int proposalId;
	private int idGallery = 0;

	private PhotoCandidate(Builder b) {
		this.photo = b.photo;
		this.folderId = b.folderId;
		this.folderName = b.folderName;
		this.galleryName = b.galleryName;
		this.proposalId = b.proposalId;
	}

	public Photo getPhoto() {
		return photo;
	}

	public int getFolderId() {
		return folderId;
	}

	public String getFolderName() {
		return folderName;
	}

	public String getGalleryName() {
		return galleryName;
	}

	private int getGalleryId() {
		if (getGalleryName().equalsIgnoreCase("home")) {
			idGallery = 1;
		} else if (getGalleryName().equalsIgnoreCase("business")) {
			idGallery = 2;
		} else if (getGalleryName().equalsIgnoreCase("collateral")) {
			idGallery = 3;
		} else {
			idGallery = 4;
		}
		return idGallery;
	}

	public int getProposalId() {
		return proposalId;
	}

	public JSONObject getCanditateAsJson() {
		try {
			JSONObject objPhotoAtr = new JSONObject();
			objPhotoAtr.put("name", photo.getName());

			objPhotoAtr.put("url", photo.getUrl());
			objPhotoAtr.put("info", photo.getInfo());
			JSONArray arrPhotoAtr = new JSONArray();
			arrPhotoAtr.put(objPhotoAtr);

			JSONObject objFolder = new JSONObject();
			objFolder.put("id", getFolderId());
			objFolder.put("name", getFolderName());

			objFolder.put("gallery_category_id", getGalleryId());// folder id
			objFolder.put("gallery_photos_attributes", arrPhotoAtr);

			JSONArray arrAttr = new JSONArray();
			arrAttr.put(objFolder);

			JSONObject objProposal = new JSONObject();
			objProposal.put("survey_galleries_attributes", arrAttr);

			// JSONObject objPhotoToUpload = new JSONObject();
			// objPhotoToUpload.put("token", token);
			// objPhotoToUpload.put("proposal_survey", objProposal);
			//
			// String dataToUpload = objPhotoToUpload.toString();

			return objProposal;
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String toString() {
		return "PhotoCandidate [photo=" + photo + ", folderId=" + folderId
				+ ", folderName=" + folderName + ", galleryName=" + galleryName
				+ ", proposalId=" + proposalId + "]";
	}

	public static class Builder {
		// == photo attribute========
		private Photo photo;
		// == folders photo==========
		private int folderId;
		private String folderName;

		// == gallery name==========
		// collateral/business/home/other
		private String galleryName;
		// other attribute
		private int proposalId;

		public Builder photo(Photo p) {
			this.photo = p;
			return this;
		}

		public Builder folderId(int id) {
			this.folderId = id;
			return this;
		}

		public Builder folderName(String n) {
			this.folderName = n;
			return this;
		}

		public Builder galleryName(String s) {
			this.galleryName = s;
			return this;
		}

		public Builder proposalId(int id) {
			this.proposalId = id;
			return this;
		}

		public PhotoCandidate build() {
			return new PhotoCandidate(this);
		}
	}

}

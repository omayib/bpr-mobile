package com.customer.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.customer.model.Proposal.FLAG;
import com.user.modul.User;

public class Proposal extends Model implements Serializable {
	/**
	 * 
	 */
	public static enum FLAG {
		INCOMPLETED, COMPLETED
	}

	private static final long serialVersionUID = 1L;
	private int id;
	private Personal personal;
	private CapacityCollection capacity;
	private SubmissionCredit submission;
	private User checkedBy;
	private Boolean isOriginal;
	private Job job;// optional
	private AssetsCollection assets;
	private BusinessCollection business;// optional
	private PurposesCollection purpose;
	private EnvironmentalCheckCollection enviromental;
	private CollateralShmCollection shm;
	private CollateralBpkbCollection bpkb;
	private CollateralDepositoCollection deposito;
	private CollateralTabunganCollection tabungan;
	private PhotoBusiness photoBusiness;
	private PhotoCollateral photoGuarantee;
	private PhotoHome photoHome;
	private PhotoOther photoOther;
	private SidCollection sidCollction;
	// private NotesCollection notes;
	private String checkedDate;
	private FLAG flag;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((checkedBy == null) ? 0 : checkedBy.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proposal other = (Proposal) obj;
		if (checkedBy == null) {
			if (other.checkedBy != null)
				return false;
		} else if (!checkedBy.equals(other.checkedBy))
			return false;
		return true;
	}

	private Proposal(Builder b) {
		this.deposito = b.deposito;
		this.tabungan = b.tabungan;
		this.sidCollction = b.sidCollection;
		this.id = b.id;
		this.assets = b.assets;
		this.personal = b.personal;
		this.job = b.job;
		this.business = b.business;
		this.capacity = b.capacity;
		this.submission = b.submission;
		this.purpose = b.purpose;
		this.bpkb = b.bpkb;
		this.shm = b.shm;
		this.enviromental = b.enviromental;
		this.checkedBy = b.checkedBy;
		this.isOriginal = b.isOriginal;
		this.photoBusiness = b.photoBusiness;
		this.photoGuarantee = b.photoGuarantee;
		this.photoHome = b.photoHome;
		this.photoOther = b.photoOther;
		// this.notes = b.notes;
		this.checkedDate = b.checkedDate;
		this.flag = b.flag;
	}

	public FLAG getFlag() {
		if (isOriginal) {
			return FLAG.COMPLETED;
		}
		return flag;
	}

	public void setFlagComleted() {
		this.flag = FLAG.COMPLETED;
	}

	// public NotesCollection getNotes() {
	// return notes;
	// }
	//
	// public void setNotes(NotesCollection notes) {
	// this.notes = notes;
	// }

	public SidCollection getSidCollction() {
		return sidCollction;
	}

	public AssetsCollection getAssets() {
		return assets;
	}

	public void setAssets(AssetsCollection assets) {
		this.assets = assets;
	}

	public Personal getPersonal() {
		return personal;
	}

	public void setPersonal(Personal personal) {
		this.personal = personal;
	}

	public Job getJob() {
		return job;
	}

	public void setJob(Job job) {
		this.job = job;
	}

	public BusinessCollection getBusinessCollection() {
		return business;
	}

	public void setBusinessCollection(BusinessCollection business) {
		this.business = business;
	}

	public void setCapacity(CapacityCollection c) {
		this.capacity = c;
	}

	public CapacityCollection getCapacity() {
		return capacity;
	}

	public SubmissionCredit getSubmission() {
		return submission;
	}

	public void setSubmission(SubmissionCredit submission) {
		this.submission = submission;
	}

	public PurposesCollection getPurposeCollection() {
		return purpose;
	}

	public void setPurposeCollection(PurposesCollection purpose) {
		this.purpose = purpose;
	}

	public CollateralBpkbCollection getBpkbCollection() {
		return bpkb;
	}

	public void setGuaranteeBpkb(CollateralBpkbCollection bpkb) {
		this.bpkb = bpkb;
	}

	public CollateralShmCollection getShmCollection() {
		return shm;
	}

	public void setGuaranteeShm(CollateralShmCollection shm) {
		this.shm = shm;
	}

	public EnvironmentalCheckCollection getEnviromental() {
		return enviromental;
	}

	public void setEnviromental(EnvironmentalCheckCollection enviromental) {
		this.enviromental = enviromental;
	}

	public String getCheckedByName() {
		if (checkedBy == null) {
			return "ORIGINAL";
		}
		return checkedBy.getName();
	}

	public void setCheckedBy(User checkedBy) {
		this.isOriginal = false;
		this.checkedBy = checkedBy;
	}

	public Boolean isOriginal() {
		return isOriginal;
	}

	// public void setIsOriginal(Boolean isOriginal) {
	// this.isOriginal = isOriginal;
	// }

	public PhotoBusiness getPhotoBusiness() {
		return photoBusiness;
	}

	public void setPhotoBusiness(PhotoBusiness photoBusiness) {
		this.photoBusiness = photoBusiness;
	}

	public PhotoCollateral getPhotoGuarantee() {
		return photoGuarantee;
	}

	public void setPhotoGuarantee(PhotoCollateral photoGuarantee) {
		this.photoGuarantee = photoGuarantee;
	}

	public PhotoHome getPhotoHome() {
		return photoHome;
	}

	public void setPhotoHome(PhotoHome photoHome) {
		this.photoHome = photoHome;
	}

	public PhotoOther getPhotoOther() {
		return photoOther;
	}

	public void setPhotoOther(PhotoOther photoOther) {
		this.photoOther = photoOther;
	}

	public String getCheckedDate() {
		return checkedDate;
	}

	public void setCheckedDate(String checkedDate) {
		this.checkedDate = checkedDate;
	}

	public int getId() {
		return id;
	}

	public CollateralDepositoCollection getDeposito() {
		return deposito;
	}

	public CollateralTabunganCollection getTabungan() {
		return tabungan;
	}

	@Override
	public String toString() {
		return "Proposal [id=" + id + ", personal=" + personal + ", capacity="
				+ capacity + ", submission=" + submission + ", checkedBy="
				+ checkedBy + ", isOriginal=" + isOriginal + ", job=" + job
				+ ", assets=" + assets + ", business=" + business
				+ ", purpose=" + purpose + ", enviromental=" + enviromental
				+ ", shm=" + shm + ", bpkb=" + bpkb + ", deposito=" + deposito
				+ ", tabungan=" + tabungan + ", photoBusiness=" + photoBusiness
				+ ", photoGuarantee=" + photoGuarantee + ", photoHome="
				+ photoHome + ", photoOther=" + photoOther + ", sidCollction="
				+ sidCollction + ", checkedDate=" + checkedDate + ", flag="
				+ flag + "]";
	}

	public static class Builder {
		private SidCollection sidCollection;
		private int id;
		private Personal personal;
		private AssetsCollection assets;
		private Job job;
		private CapacityCollection capacity;
		private SubmissionCredit submission;
		private User checkedBy;
		private Boolean isOriginal = true;
		private BusinessCollection business;
		private PurposesCollection purpose;
		private EnvironmentalCheckCollection enviromental;
		private CollateralShmCollection shm;
		private CollateralBpkbCollection bpkb;
		private CollateralDepositoCollection deposito;
		private CollateralTabunganCollection tabungan;
		private PhotoBusiness photoBusiness;
		private PhotoCollateral photoGuarantee;
		private PhotoHome photoHome;
		private PhotoOther photoOther;
		private String checkedDate;
		private FLAG flag = FLAG.INCOMPLETED;

		public Builder tabungan(CollateralTabunganCollection tabungan) {
			this.tabungan = tabungan;
			return this;
		}

		public Builder deposito(CollateralDepositoCollection deposito) {
			this.deposito = deposito;
			return this;
		}

		// private NotesCollection notes;

		public Builder sidCollection(SidCollection sid) {
			this.sidCollection = sid;
			return this;
		}

		public Builder flag(FLAG flag) {
			this.flag = flag;
			return this;
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		/**
		 * 
		 * @param date
		 *            :input example 11-June-07
		 * @return Builder
		 */
		public Builder checkedDate(String date) {
			//
			SimpleDateFormat dt = new SimpleDateFormat("dd-MMM-yy");
			SimpleDateFormat dt1 = new SimpleDateFormat("dd-mm-yyyy");
			try {
				Date d = dt.parse(date);
				checkedDate = dt1.format(d);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return this;
		}

		public Builder photoOther(PhotoOther photo) {
			this.photoOther = photo;
			return this;
		}

		public Builder photoBusiness(PhotoBusiness photo) {
			this.photoBusiness = photo;
			return this;
		}

		public Builder photoHome(PhotoHome photo) {
			this.photoHome = photo;
			return this;
		}

		public Builder photoGuarantee(PhotoCollateral photo) {
			this.photoGuarantee = photo;
			return this;
		}

		public Builder bpkb(CollateralBpkbCollection bpkb) {
			this.bpkb = bpkb;
			return this;
		}

		public Builder shm(CollateralShmCollection s) {
			this.shm = s;
			return this;
		}

		public Builder environmentalCheck(EnvironmentalCheckCollection e) {
			this.enviromental = e;
			return this;
		}

		public Builder assets(AssetsCollection a) {
			this.assets = a;
			return this;
		}

		public Builder checkedBy(User c) {
			this.checkedBy = c;
			return this;
		}

		public Builder isOriginal(Boolean bo) {
			this.isOriginal = bo;
			return this;
		}

		public Builder personal(Personal p) {
			this.personal = p;
			return this;
		}

		public Builder job(Job j) {
			this.job = j;
			return this;
		}

		public Builder business(BusinessCollection b) {
			this.business = b;
			return this;
		}

		public Builder capacity(CapacityCollection c) {
			this.capacity = c;
			return this;
		}

		public Builder submissionOfCredit(SubmissionCredit s) {
			this.submission = s;
			return this;
		}

		public Builder purpose(PurposesCollection p) {
			this.purpose = p;
			return this;
		}

		// public Builder note(NotesCollection n) {
		// this.notes = n;
		// return this;
		// }

		public Proposal build() {
			return new Proposal(this);
		}
	}

}

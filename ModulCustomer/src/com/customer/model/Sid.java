package com.customer.model;

import java.io.Serializable;

public class Sid implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id = -1;
	private String photoUrl = "";
	private String photoThumbnailUrl = "";

	private Sid(Builder b) {
		this.id = b.id;
		this.photoThumbnailUrl = b.photoThumbnailUrl;
		this.photoUrl = b.photoUrl;
	}

	public int getId() {
		return id;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public String getPhotoThumbnailUrl() {
		return photoThumbnailUrl;
	}

	public static class Builder {
		private int id = -1;
		private String photoUrl = "";
		private String photoThumbnailUrl = "";

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder photo(String p) {
			this.photoUrl = p;
			return this;
		}

		public Builder thumbnail(String t) {
			this.photoThumbnailUrl = t;
			return this;
		}

		public Sid build() {
			return new Sid(this);
		}
	}
}

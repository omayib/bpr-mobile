package com.customer.model;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Personal extends Model implements Serializable {

	private static final long serialVersionUID = 1L;
	private String name = "";
	private String birthDate = "";
	private String birthPlace = "";
	private String maritalStatus = "";
	private String addressByKtp = "";
	private String addressByCurrent = "";
	private String spouseName = "";
	private String spouseJob = "";

	private int id = -1;
	private String ktpNumber = "";
	private String gender = "";
	private int age = -1;
	private String homeStatus = "";
	private String aliasName = "";
	private String motherMaidenName = "";
	private int numberOfDependents = -1;
	private String phoneNumber = "";
	private String handphoneNumber = "";
	private List<Children> children = new ArrayList<>();

	private Personal(Builder b) {
		super();
		this.id = b.id;
		this.ktpNumber = b.ktpNumber;
		this.gender = b.gender;
		this.age = b.age;
		this.homeStatus = b.homeStatus;
		this.aliasName = b.aliasName;
		this.motherMaidenName = b.motherMaidenName;
		this.numberOfDependents = b.numberOfDependents;
		this.phoneNumber = b.phoneNumber;
		this.handphoneNumber = b.handphoneNumber;
		this.name = b.name;
		this.birthDate = b.birthDate;
		this.birthPlace = b.birthPlace;
		this.maritalStatus = b.maritalStatus;
		this.addressByKtp = b.addressByKtp;
		this.addressByCurrent = b.addressByCurrent;
		this.spouseName = b.spouseName;
		this.spouseJob = b.spouseJob;
		this.children = b.children;

	}

	public String getName() {
		return name;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public String getBirthPlace() {
		return birthPlace;
	}

	public Integer getAge() {
		SimpleDateFormat sdfSource = new SimpleDateFormat("dd-MM-yyyy");
		Date dateTime;
		String yearBirth = "0";
		try {
			dateTime = sdfSource.parse(birthDate);
			SimpleDateFormat df = new SimpleDateFormat("yyyy");
			yearBirth = df.format(dateTime);
			System.out.println("age def " + age);
			System.out.println("tahun lahir " + yearBirth);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return (age == -1 || age == 0) ? Calendar.getInstance().get(
				Calendar.YEAR)
				- Integer.parseInt(yearBirth) : age;
	}

	public String getMaritalStatus() {
		return maritalStatus;
	}

	public String getMaritalStatusForSpinner() {
		return  maritalStatus.toLowerCase().replace("_", " ");
	}

	public String getAddressByKtp() {
		return addressByKtp;
	}

	public String getAddressByCurrent() {
		return addressByCurrent;
	}

	public String getSpouseName() {
		return spouseName;
	}

	public String getSpouseWork() {
		return spouseJob;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setBirthDate(String birthOfDate) {
		this.birthDate = birthOfDate;
	}

	public void setBirthPlace(String birthOfPlace) {
		this.birthPlace = birthOfPlace;
	}

	public void setMaritalStatus(String statusOfMarital) {
		this.maritalStatus = statusOfMarital;
	}

	public void setAddressByKtp(String addressByKtp) {
		this.addressByKtp = addressByKtp;
	}

	public void setAddressByCurrent(String addrussByCurrent) {
		this.addressByCurrent = addrussByCurrent;
	}

	public void setSpouseName(String couplesName) {
		this.spouseName = couplesName;
	}

	public void setSpouseJob(String couplesWork) {
		this.spouseJob = couplesWork;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getKtpNumber() {
		return ktpNumber;
	}

	public void setKtpNumber(String ktpNumber) {
		this.ktpNumber = ktpNumber;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getHomeStatus() {
		return homeStatus;
	}

	public String getHomeStatusForSpinner() {
		String localInput = "";
		String a = homeStatus.toLowerCase().replace("_", " ");
		if (a.contains("pribadi")) {
			localInput = "milik pribadi";
		} else if (a.contains("orang tua")) {
			localInput = "orang tua";
		} else if (a.contains("keluarga")) {
			localInput = "keluarga";
		} else {
			localInput = "kontrak";
		}
		return localInput;
	}

	public void setHomeStatus(String homeStatus) {
		this.homeStatus = homeStatus;
	}

	public String getAliasName() {
		return aliasName;
	}

	public void setAliasName(String aliasName) {
		this.aliasName = aliasName;
	}

	public String getMotherMaidenName() {
		return motherMaidenName;
	}

	public void setMotherMaidenName(String motherMaidenName) {
		this.motherMaidenName = motherMaidenName;
	}

	public int getNumberOfDependents() {
		return numberOfDependents;
	}

	public void setNumberOfDependents(int numberOfDependents) {
		this.numberOfDependents = numberOfDependents;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getHandphoneNumber() {
		return handphoneNumber;
	}

	public void setHandphoneNumber(String handphoneNumber) {
		this.handphoneNumber = handphoneNumber;
	}

	public List<Children> getChildren() {
		return children;
	}

	public void setChildren(List<Children> children) {
		this.children.addAll(children);
	}

	public String getSpouseJob() {
		return spouseJob;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Personal [name=" + name + ", birthDate=" + birthDate
				+ ", birthPlace=" + birthPlace + ", maritalStatus="
				+ maritalStatus + ", addressByKtp=" + addressByKtp
				+ ", addressByCurrent=" + addressByCurrent + ", spouseName="
				+ spouseName + ", spouseJob=" + spouseJob + ", id=" + id
				+ ", ktpNumber=" + ktpNumber + ", gender=" + gender + ", age="
				+ age + ", homeStatus=" + homeStatus + ", aliasName="
				+ aliasName + ", motherMaidenName=" + motherMaidenName
				+ ", numberOfDependents=" + numberOfDependents
				+ ", phoneNumber=" + phoneNumber + ", handphoneNumber="
				+ handphoneNumber + ", children=" + children + "]";
	}

	public static class Builder {
		private String name = "";
		private String birthDate = "";
		private String birthPlace = "";
		private String maritalStatus = "";
		private String addressByKtp = "";
		private String addressByCurrent = "";
		private String spouseName = "";
		private String spouseJob = "";
		private String pattern = "dd/MM/yyyy";
		private int id = -1;
		private String ktpNumber = "";
		private String gender = "";
		private int age = -1;
		private String homeStatus = "";
		private String aliasName = "";
		private String motherMaidenName = "";
		private int numberOfDependents = -1;
		private String phoneNumber = "";
		private String handphoneNumber = "";
		private List<Children> children = new ArrayList<>();

		public Builder numberOfDependents(int n) {
			this.numberOfDependents = n;
			return this;
		}

		public Builder children(List<Children> c) {
			this.children.addAll(c);
			return this;
		}

		public Builder homeStatus(String s) {
			this.homeStatus = s;
			return this;
		}

		public Builder nameAlias(String n) {
			this.aliasName = n;
			return this;
		}

		public Builder motherMaidenName(String m) {
			this.motherMaidenName = m;
			return this;
		}

		public Builder phoneNumber(String p) {
			this.phoneNumber = p;
			return this;
		}

		public Builder handphoneNumber(String p) {
			this.handphoneNumber = p;
			return this;
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder ktpNumber(String n) {
			this.ktpNumber = n;
			return this;
		}

		public Builder gender(String g) {
			this.gender = g;
			return this;
		}

		public Builder age(int a) {
			this.age = a;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder birthDate(String birthdate) {
			this.birthDate = birthdate;
			return this;
		}

		public Builder birthPlace(String place) {
			this.birthPlace = place;
			return this;
		}

		public Builder statusOfMarital(String status) {
			this.maritalStatus = status;
			return this;
		}

		public Builder addressByKtp(String addressKtp) {
			this.addressByKtp = addressKtp;
			return this;
		}

		public Builder addressByCurrent(String addrCurrent) {
			this.addressByCurrent = addrCurrent;
			return this;
		}

		public Builder spouseName(String nameOfCouples) {
			this.spouseName = nameOfCouples;
			return this;
		}

		public Builder spouseJob(String workOfCouples) {
			this.spouseJob = workOfCouples;
			return this;
		}

		public Personal build() {
			return new Personal(this);
		}
	}
}

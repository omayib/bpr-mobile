package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class PhotoFolder implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id;
	private String name;

	private Collection<Photo> photoGalleries = new ArrayList<Photo>();// a.k.a
																		// galleries

	private PhotoFolder(Builder b) {
		this.id = b.id;
		this.photoGalleries = b.photoGalleries;
		this.name = b.name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPhotoCount() {
		return photoGalleries.size();
	}

	public Collection<Photo> getPhotoGalleries() {
		return photoGalleries;
	}

	public void setPhotoCollection(Collection<Photo> photoCollection) {
		this.photoGalleries.addAll(photoCollection);
	}

	public void setPhotoCollection(Photo p) {
		this.photoGalleries.add(p);
	}

	@Override
	public String toString() {
		return "PhotoFolder [id=" + id + ", name=" + name + ", photoGalleries="
				+ photoGalleries + "]";
	}

	public static class Builder {
		private Collection<Photo> photoGalleries = new ArrayList<Photo>();
		private String name;
		private int id;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder name(String n) {
			this.name = n;
			return this;
		}

		public Builder photo(Collection<Photo> p) {
			this.photoGalleries.addAll(p);
			return this;
		}

		public Builder photo(Photo p) {
			this.photoGalleries.add(p);
			return this;
		}

		public PhotoFolder build() {
			return new PhotoFolder(this);
		}

	}

}

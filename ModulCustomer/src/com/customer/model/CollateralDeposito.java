package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class CollateralDeposito extends Model implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String bilyetNumber;
	private String accountNumber;
	private String name;
	private BigDecimal amount;

	private CollateralDeposito(Builder b) {
		this.id = b.id;
		this.bilyetNumber = b.bilyetNumber;
		this.accountNumber = b.accountNumber;
		this.name = b.name;
		this.amount = b.amount;
	}

	public void setBilyetNumber(String bilyetNumber) {
		this.bilyetNumber = bilyetNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setAmount(String amount) {
		this.amount = new BigDecimal(amount);
	}

	public int getId() {
		return id;
	}

	public String getBilyetNumber() {
		return bilyetNumber;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getAmountAsRupiah() {
		return RupiahConverter.convert(this.amount);
	}

	@Override
	public String toString() {
		return "CollateralDeposito [id=" + id + ", bilyetNumber="
				+ bilyetNumber + ", accountNumber=" + accountNumber + ", name="
				+ name + ", amount=" + amount + "]";
	}

	public static class Builder {
		private int id;
		private String bilyetNumber;
		private String accountNumber;
		private String name;
		private BigDecimal amount;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder bilyetNumber(String bilyet) {
			this.bilyetNumber = bilyet;
			return this;
		}

		public Builder accountNumber(String account) {
			this.accountNumber = account;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder amount(String amount) {
			String cleaned = "0";
			if (amount.contains(".")) {
				cleaned = amount.substring(0, amount.indexOf("."));
			} else {
				cleaned = amount;
			}
			this.amount = new BigDecimal(cleaned);
			return this;
		}

		public CollateralDeposito build() {
			return new CollateralDeposito(this);
		}
	}

}

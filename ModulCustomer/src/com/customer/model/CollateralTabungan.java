package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class CollateralTabungan implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String bookNumber;
	private String accountNumber;
	private String name;
	private BigDecimal amount;

	private CollateralTabungan(Builder b) {
		this.id = b.id;
		this.bookNumber = b.bookNumber;
		this.accountNumber = b.accountNumber;
		this.name = b.name;
		this.amount = b.amount;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CollateralTabungan other = (CollateralTabungan) obj;
		if (id != other.id)
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setBookNumber(String bookNumber) {
		this.bookNumber = bookNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public void setAmount(String amount) {
		this.amount = new BigDecimal(amount);
	}

	public String getBookNumber() {
		return bookNumber;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public String getName() {
		return name;
	}

	public String getAmountAsRupiah() {
		return RupiahConverter.convert(amount);
	}

	@Override
	public String toString() {
		return "CollateralTabungan [id=" + id + ", bookNumber=" + bookNumber
				+ ", accountNumber=" + accountNumber + ", name=" + name
				+ ", amount=" + amount + "]";
	}

	public static class Builder {
		private int id;
		private String bookNumber;
		private String accountNumber;
		private String name;
		private BigDecimal amount;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder bookNumber(String number) {
			this.bookNumber = number;
			return this;
		}

		public Builder accountNumber(String account) {
			this.accountNumber = account;
			return this;
		}

		public Builder ammount(String amount) {
			String cleaned = "0";
			if (amount.contains(".")) {
				cleaned = amount.substring(0, amount.indexOf("."));
			} else {
				cleaned = amount;
			}
			this.amount = new BigDecimal(cleaned);
			return this;
		}

		public CollateralTabungan build() {
			return new CollateralTabungan(this);
		}

	}
}

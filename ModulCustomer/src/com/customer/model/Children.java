package com.customer.model;

import java.io.Serializable;

public class Children implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int id = -1;
	private String name;
	private int age;

	private Children(Builder b) {
		this.id = b.id;
		this.name = b.name;
		this.age = b.age;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getAge() {
		return age;
	}

	@Override
	public String toString() {
		return "Children [id=" + id + ", name=" + name + ", age=" + age + "]";
	}

	public static class Builder {
		private int id = -1;
		private String name;
		private int age;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder name(String n) {
			this.name = n;
			return this;
		}

		public Builder age(int age) {
			this.age = age;
			return this;
		}

		public Children build() {
			return new Children(this);
		}
	}
}

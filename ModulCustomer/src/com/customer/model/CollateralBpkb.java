package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class CollateralBpkb extends Model implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id = -1;
	private String nameOfBpkb;
	private String priceOnMarket = "0";
	private String priceOnAssessed = "0";
	private String carName;
	private String carType;
	private String carYear = "1680";
	private String numberBpkb;
	private String numberStnk;
	private String carBrand;
	private String binding;
	private String policeNumber;
	private String info;

	private CollateralBpkb(Builder b) {
		this.info = b.info;
		this.id = b.id;
		this.nameOfBpkb = b.nameOfBpkb;
		this.priceOnMarket = b.priceOnMarket;
		this.priceOnAssessed = b.priceOnAssessed;
		this.carName = b.carName;
		this.carType = b.carType;
		this.carYear = b.carYear;
		this.numberBpkb = b.numberBpkb;
		this.numberStnk = b.numberStnk;
		this.carBrand = b.carBrand;
		this.binding = b.binding;
		this.policeNumber = b.policeNumber;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	public int getId() {
		return id;
	}

	public void setPoliceNumber(String policeNumber) {
		this.policeNumber = policeNumber;
	}

	public String getBinding() {
		return binding;
	}

	public void setBinding(String binding) {
		this.binding = binding;
	}

	public String getNameOfBpkb() {
		return nameOfBpkb;
	}

	public void setNameOfBpkb(String nameOfBpkb) {
		this.nameOfBpkb = nameOfBpkb;
	}

	public String getPriceOnMarket() {
		return priceOnMarket;
	}

	public String getPriceOnMarketAsRupiah() {
		return RupiahConverter.convert(new BigDecimal(priceOnMarket));
	}

	public void setPriceOnMarket(String priceOnMarket) {
		this.priceOnMarket = priceOnMarket;
	}

	public String getPriceOnAssessed() {
		return priceOnAssessed;
	}

	public String getPriceOnAssessedAsRupiah() {
		return RupiahConverter.convert(new BigDecimal(priceOnAssessed));
	}

	public void setPriceOnAssessed(String priceOnAssessed) {
		this.priceOnAssessed = priceOnAssessed;
	}

	public String getCarName() {
		return carName;
	}

	public void setCarName(String nameOfVehicle) {
		this.carName = nameOfVehicle;
	}

	public String getCarType() {
		return carType;
	}

	public void setCarType(String typeOfVehicle) {
		this.carType = typeOfVehicle;
	}

	public String getCarYear() {
		return carYear;
	}

	public void setCarYear(String yearOfVehicle) {
		this.carYear = yearOfVehicle;
	}

	public String getNumberBpkb() {
		return numberBpkb;
	}

	public void setNumberBpkb(String numberBpkb) {
		this.numberBpkb = numberBpkb;
	}

	public String getNumberStnk() {
		return numberStnk;
	}

	public void setNumberStnk(String numberStnk) {
		this.numberStnk = numberStnk;
	}

	public String getCarBrand() {
		return carBrand;
	}

	public void setCarBrand(String brand) {
		this.carBrand = brand;
	}

	public String getPoliceNumber() {
		return policeNumber;
	}

	public String getInfo() {
		return info;
	}

	@Override
	public String toString() {
		return "CollateralBpkb [id=" + id + ", nameOfBpkb=" + nameOfBpkb
				+ ", priceOnMarket=" + priceOnMarket + ", priceOnAssessed="
				+ priceOnAssessed + ", carName=" + carName + ", carType="
				+ carType + ", carYear=" + carYear + ", numberBpkb="
				+ numberBpkb + ", numberStnk=" + numberStnk + ", carBrand="
				+ carBrand + ", binding=" + binding + ", policeNumber="
				+ policeNumber + ", info=" + info + "]";
	}

	public static class Builder {
		private String info;
		private int id = -1;
		private String nameOfBpkb;
		private String priceOnMarket = "0";
		private String priceOnAssessed = "0";
		private String carName;
		private String carType;
		private String carYear = "1680";
		private String numberBpkb;
		private String numberStnk;
		private String carBrand;
		private String binding;
		private String policeNumber;

		public Builder info(String in) {
			this.info = in;
			return this;
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder policeNumber(String p) {
			this.policeNumber = p;
			return this;
		}

		public Builder binding(String b) {
			this.binding = b;
			return this;
		}

		public Builder nameOfBpkb(String n) {
			this.nameOfBpkb = n;
			return this;
		}

		public Builder priceOnMarket(String p) {
			String cleaned = "0";
			if (p.contains(".")) {
				cleaned = p.substring(0, p.indexOf("."));
			} else {
				cleaned = p;
			}
			this.priceOnMarket = cleaned;
			return this;
		}

		public Builder priceOnAssessed(String p) {
			String cleaned = "0";
			if (p.contains(".")) {
				cleaned = p.substring(0, p.indexOf("."));
			} else {
				cleaned = p;
			}
			this.priceOnAssessed = cleaned;
			return this;
		}

		public Builder carName(String n) {
			this.carName = n;
			return this;
		}

		public Builder carType(String y) {
			this.carType = y;
			return this;
		}

		public Builder carYear(String y) {
			this.carYear = y;
			return this;
		}

		public Builder numberBpkb(String number) {
			this.numberBpkb = number;
			return this;
		}

		public Builder numberStnk(String s) {
			this.numberStnk = s;
			return this;
		}

		public Builder carBrand(String s) {
			this.carBrand = s;
			return this;
		}

		public CollateralBpkb build() {
			return new CollateralBpkb(this);
		}

	}

}

package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class CollateralShm extends Model implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id = -1;
	private String certificateNumber;
	private String certificateName;
	private double landOfArea = 0;
	private String priceOnMarket = "0";
	private String priceOnAssessed = "0";
	private String statusOfLand;
	private String binding;
	private String info;
	private String type;

	private CollateralShm(Builder b) {
		this.id = b.id;
		this.info = b.info;
		this.certificateName = b.certificateName;
		this.certificateNumber = b.certificateNumber;
		this.landOfArea = b.landOfArea;
		this.priceOnAssessed = b.priceOnAssessed;
		this.priceOnMarket = b.priceOnMarket;
		this.statusOfLand = b.statusOfLand;
		this.binding = b.binding;
		this.type = b.type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type.toUpperCase();
	}

	public int getId() {
		return id;
	}

	public String getBinding() {
		return binding;
	}

	public void setBinding(String binding) {
		this.binding = binding.toUpperCase();
	}

	public String getStatusOfLand() {
		return statusOfLand.toUpperCase();
	}

	public void setStatusOfLand(String statusOfLand) {
		this.statusOfLand = statusOfLand;
	}

	public String getCertificateNumber() {
		return certificateNumber;
	}

	public void setCertificateNumber(String certificateNumber) {
		this.certificateNumber = certificateNumber;
	}

	public String getCertificateName() {
		return certificateName;
	}

	public void setCertificateName(String certificateName) {
		this.certificateName = certificateName;
	}

	public double getLandOfArea() {
		return landOfArea;
	}

	public void setLandOfArea(double landOfArea) {
		this.landOfArea = landOfArea;
	}

	public String getPriceOnAssessed() {
		return priceOnAssessed;
	}

	public String getPriceOnAssessedAsRupiah() {
		return RupiahConverter.convert(new BigDecimal(priceOnAssessed));
	}

	public void setPriceOnAssessed(String price) {
		this.priceOnAssessed = price;
	}

	public String getPriceOnMarket() {
		return priceOnMarket;
	}

	public String getPriceOnMarketAsRupiah() {
		return RupiahConverter.convert(new BigDecimal(priceOnMarket));
	}

	public void setPriceOnMarket(String priceOnMarket) {
		this.priceOnMarket = priceOnMarket;
	}

	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}

	@Override
	public String toString() {
		return "CollateralShm [id=" + id + ", certificateNumber="
				+ certificateNumber + ", certificateName=" + certificateName
				+ ", landOfArea=" + landOfArea + ", priceOnMarket="
				+ priceOnMarket + ", priceOnAssessed=" + priceOnAssessed
				+ ", statusOfLand=" + statusOfLand + ", binding=" + binding
				+ ", info=" + info + "]";
	}

	public static class Builder {
		private String info;
		private String certificateNumber;
		private String certificateName;
		private double landOfArea = 0;
		private String priceOnMarket = "0";
		private String priceOnAssessed = "0";
		private String statusOfLand;
		private String binding;
		private int id = -1;
		private String type;

		public Builder type(String t) {
			this.type = t.toUpperCase();
			System.out.println("type on builder " + type);
			return this;
		}

		public Builder info(String i) {
			this.info = i;
			return this;
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder binding(String b) {
			this.binding = b.toUpperCase();
			return this;
		}

		public Builder statusOfLand(String s) {
			this.statusOfLand = s.toUpperCase().replace("_", " ");
			return this;
		}

		public Builder certificateNumber(String n) {
			this.certificateNumber = n;
			return this;
		}

		public Builder certificateName(String name) {
			this.certificateName = name;
			return this;
		}

		public Builder loadOfArea(String area) {
			this.landOfArea = Double.parseDouble(area);
			return this;
		}

		public Builder priceOnMarket(String p) {
			String cleaned = "0";
			if (p.contains(".")) {
				cleaned = p.substring(0, p.indexOf("."));
			} else {
				cleaned = p;
			}
			this.priceOnMarket = cleaned;
			return this;
		}

		public Builder priceOnAssessed(String p) {
			String cleaned = "0";
			if (p.contains(".")) {
				cleaned = p.substring(0, p.indexOf("."));
			} else {
				cleaned = p;
			}
			this.priceOnAssessed = cleaned;
			return this;
		}

		public CollateralShm build() {
			return new CollateralShm(this);
		}

	}
}

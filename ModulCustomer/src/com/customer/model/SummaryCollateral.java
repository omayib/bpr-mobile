package com.customer.model;

import java.math.BigDecimal;

public class SummaryCollateral {
	private String collateralName;
	private String collateralRemark;
	private String collateralPriceMarket;
	private String collateralPriceAssesed;
	private String colletralType;

	public SummaryCollateral(Builder b) {
		this.colletralType = b.colletralType;
		this.collateralName = b.collateralName;
		this.collateralPriceAssesed = b.collateralPriceAssesed;
		this.collateralPriceMarket = b.collateralPriceMarket;
		this.collateralRemark = b.collateralRemark;
	}

	public String getColletralType() {
		return colletralType;
	}

	public String getCollateralName() {
		return collateralName;
	}

	public String getCollateralRemark() {
		return collateralRemark;
	}

	public String getCollateralPriceMarket() {
		return collateralPriceMarket;
	}

	public String getCollateralPriceMarketAsRupiah() {
		return RupiahConverter.convert(new BigDecimal(collateralPriceMarket));
	}
	public String getCollateralPriceAssesed() {
		return collateralPriceAssesed;
	}
	public String getCollateralPriceAssesedAsRupiah() {
		return RupiahConverter.convert(new BigDecimal(collateralPriceAssesed));
	}

	@Override
	public String toString() {
		return "SummaryCollateral [collateralName=" + collateralName
				+ ", collateralRemark=" + collateralRemark
				+ ", collateralPriceMarket=" + collateralPriceMarket
				+ ", collateralPriceAssesed=" + collateralPriceAssesed
				+ ", colletralType=" + colletralType + "]";
	}

	public static class Builder {
		private String collateralName;
		private String collateralRemark;
		private String collateralPriceMarket;
		private String collateralPriceAssesed;
		private String colletralType;

		public Builder type(String type) {
			this.colletralType = type;
			return this;
		}

		public Builder name(String n) {
			this.collateralName = n;
			return this;
		}

		public Builder remark(String n) {
			this.collateralRemark = n;
			return this;
		}

		public Builder priceMarket(String n) {
			this.collateralPriceMarket = n;
			return this;
		}

		public Builder priceAssesed(String n) {
			this.collateralPriceAssesed = n;
			return this;
		}

		public SummaryCollateral build() {
			return new SummaryCollateral(this);
		}
	}
}

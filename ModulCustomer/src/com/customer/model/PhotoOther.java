package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class PhotoOther implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private Collection<PhotoFolder> folders = new ArrayList<PhotoFolder>();

	private PhotoOther(Builder b) {
		this.folders = b.folders;
		this.name = b.name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getCount() {
		return folders.size();
	}

	public Collection<PhotoFolder> getFolders() {
		return folders;
	}

	public void setFolders(Collection<PhotoFolder> photoCollection) {
		this.folders.addAll(photoCollection);
	}

	public void setFolder(PhotoFolder p) {
		this.folders.add(p);
	}

	public static class Builder {
		private Collection<PhotoFolder> folders = new ArrayList<PhotoFolder>();
		private String name;

		public Builder name(String n) {
			this.name = n;
			return this;
		}

		public Builder folders(Collection<PhotoFolder> p) {
			this.folders.addAll(p);
			return this;
		}

		public Builder folder(PhotoFolder p) {
			this.folders.add(p);
			return this;
		}

		public PhotoOther build() {
			return new PhotoOther(this);
		}

	}

}

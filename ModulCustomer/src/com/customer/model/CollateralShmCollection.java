package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class CollateralShmCollection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<CollateralShm> guaranteeShm = new ArrayList<CollateralShm>();

	private CollateralShmCollection(Builder b) {
		this.guaranteeShm = b.shm;
	}

	public Collection<CollateralShm> getGuaranteeShm() {
		return guaranteeShm;
	}

	public void setGuaranteeShm(Collection<CollateralShm> guaranteeShm) {
		this.guaranteeShm.addAll(guaranteeShm);
	}

	public void setGuaranteeShm(CollateralShm guaranteeShm) {
		this.guaranteeShm.add(guaranteeShm);
	}

	@Override
	public String toString() {
		return "CollateralShmCollection [guaranteeShm=" + guaranteeShm + "]";
	}

	public static class Builder {

		private Collection<CollateralShm> shm = new ArrayList<CollateralShm>();

		public Builder shm(CollateralShm s) {
			shm.add(s);
			return this;
		}

		public Builder shm(Collection<CollateralShm> s) {
			shm.addAll(s);
			return this;
		}

		public CollateralShmCollection build() {
			return new CollateralShmCollection(this);
		}
	}

}

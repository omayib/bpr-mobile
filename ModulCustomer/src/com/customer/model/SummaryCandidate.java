package com.customer.model;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

public class SummaryCandidate {
	private String summaryCandidate;
	private int idCustomer;

	private SummaryCandidate(Builder b) {
		this.idCustomer = b.idCustomer;
		// create "proposal_recommendations_attributes"
		JSONArray recommendationArrObj = new JSONArray();
		JSONArray noteArrObj = new JSONArray();
		JSONObject recommendationObj = new JSONObject();
		recommendationObj.put("accepted", b.accepted);
		recommendationObj.put("signature", b.signature);
		noteArrObj.put(b.note);
		recommendationObj.put("notes", noteArrObj);
		recommendationArrObj.put(recommendationObj);

		// create "bpkb_collaterals_attributes"
		JSONArray bpkbArrObj = new JSONArray();
		List<CollateralBpkb> listBpkb = (List<CollateralBpkb>) b.summary
				.getBpkb().getGuaranteeBpkb();
		for (CollateralBpkb bpkb : listBpkb) {
			JSONObject bpkbObj = new JSONObject();
			bpkbObj.put("id", bpkb.getId());
			bpkbObj.put("binding", bpkb.getBinding());
			bpkbArrObj.put(bpkbObj);
		}
		// create "shm_collaterals_attributes"
		JSONArray shmArrObj = new JSONArray();
		List<CollateralShm> listShm = (List<CollateralShm>) b.summary.getShm()
				.getGuaranteeShm();
		for (CollateralShm shm : listShm) {
			JSONObject shmObj = new JSONObject();
			shmObj.put("id", shm.getId());
			shmObj.put("binding", shm.getBinding());
			shmArrObj.put(shmObj);
		}

		// create "survey_submission_credit_attributes"
		JSONObject submsObj = new JSONObject();
		submsObj.put("id", b.summary.getSubmission().getId());
		submsObj.put("plafon", b.summary.getSubmission().getPlafon() + "");
		submsObj.put("interest", b.summary.getSubmission().getInterest());

		JSONObject sumObj = new JSONObject();
		sumObj.put("id", b.summary.getId());
		sumObj.put("purchase", b.summary.getP());
		sumObj.put("assumption", b.summary.getA());
		sumObj.put("av_referral", b.summary.isAvReferral());
		sumObj.put("survey_submission_credit_attributes", submsObj);
		sumObj.put("shm_collaterals_attributes", shmArrObj);
		sumObj.put("bpkb_collaterals_attributes", bpkbArrObj);

		JSONObject summaryAttributes=new JSONObject();
		summaryAttributes.put("survey_summary_attributes", sumObj);
		summaryAttributes.put("id", b.idCurrentProposal);
		
		JSONObject summaryDataObj = new JSONObject();
		summaryDataObj.put("proposal_recommendations_attributes",
				recommendationArrObj);
		summaryDataObj.put("proposal_surveys_attributes", summaryAttributes);

		JSONObject summaryCandidateObj = new JSONObject();
		summaryCandidateObj.put("customer_data", summaryDataObj);
		summaryCandidateObj.put("token", b.token);
		summaryCandidate = summaryCandidateObj.toString();
	}

	@Override
	public String toString() {
		return "SummaryCandidate [summaryCandidate=" + summaryCandidate
				+ ", idCustomer=" + idCustomer + "]";
	}

	public int getIdCustomer() {
		return idCustomer;
	}

	public String getSummaryCandidate() {
		return summaryCandidate;
	}

	public static class Builder {
		private String token;
		private int idCurrentProposal;
		private boolean accepted;
		private String signature, note;
		private Summary summary;
		private int idCustomer;

		public Builder(String token, int idCurrentProposal, boolean accepted,
				String signature, String note, Summary summary) {
			super();
			this.token = token;
			this.idCurrentProposal = idCurrentProposal;
			this.accepted = accepted;
			this.signature = signature;
			this.note = note;
			this.summary = summary;
		}

		public Builder idCustomer(int idcustomer) {
			this.idCustomer = idcustomer;
			return this;
		}

		@Override
		public String toString() {
			return "Builder [token=" + token + ", idCurrentProposal="
					+ idCurrentProposal + ", accepted=" + accepted
					+ ", signature=" + signature + ", note=" + note
					+ ", summary=" + summary + "]";
		}

		public SummaryCandidate build() {
			return new SummaryCandidate(this);
		}
	}
}

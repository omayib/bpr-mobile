package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SidCollection implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private List<Sid> sidCollection;

	public SidCollection(Builder b) {
		if (b.listSid.isEmpty()) {
			sidCollection = new ArrayList<>();
			return;
		}
		this.sidCollection = b.listSid;
	}

	public List<Sid> getSidCollection() {
		return sidCollection;
	}

	public static class Builder {
		private List<Sid> listSid = new ArrayList<Sid>();

		public Builder sid(List<Sid> sid) {
			this.listSid.addAll(sid);
			return this;
		}

		public Builder sid(Sid sid) {
			this.listSid.add(sid);
			return this;
		}

		public SidCollection build() {
			return new SidCollection(this);
		}
	}
}

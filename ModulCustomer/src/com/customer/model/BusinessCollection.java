package com.customer.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

public class BusinessCollection implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Collection<Business> businessCollection = new ArrayList<>();

	private BusinessCollection(Builder b) {
		businessCollection = b.businessCollection;
	}

	public Collection<Business> getBusinessCollection() {
		return businessCollection;
	}

	public void setBusinessCollection(Collection<Business> businessCollection) {
		this.businessCollection.addAll(businessCollection);
	}

	public void setBusinessCollection(Business businessCollection) {
		this.businessCollection.add(businessCollection);
	}

	@Override
	public String toString() {
		return "BusinessCollection [businessCollection=" + businessCollection
				+ "]";
	}

	public static class Builder {
		private Collection<Business> businessCollection = new ArrayList<>();

		public Builder business(Business b) {
			businessCollection.add(b);
			return this;
		}

		public Builder business(Collection<Business> b) {
			businessCollection.addAll(b);
			return this;
		}

		public BusinessCollection build() {
			return new BusinessCollection(this);
		}

	}

}

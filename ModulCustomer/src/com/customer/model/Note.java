package com.customer.model;

import java.io.Serializable;

import com.user.modul.User;

public class Note implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String messages;
	private User creator;

	private Note(Builder b) {
		this.messages = b.messages;
		this.creator = b.creator;
	}

	public String getMessages() {
		return messages;
	}

	public void setMessages(String messages) {
		this.messages = messages;
	}

	public String getCreator() {
		return creator.getName();
	}

	public void setCreator(User creator) {
		this.creator = creator;
	}

	public static class Builder {
		private String messages;
		private User creator;

		public Builder messages(String m) {
			this.messages = m;
			return this;
		}

		public Builder creator(User c) {
			this.creator = c;
			return this;
		}

		public Note build() {
			return new Note(this);
		}
	}
}

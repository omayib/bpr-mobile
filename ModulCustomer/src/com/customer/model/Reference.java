//package com.customer.model;
//
//import java.io.Serializable;
//
//public class Reference implements Serializable {
//	/**
//	 * 
//	 */
//	private static final long serialVersionUID = 1L;
//
//
//
//	private String name;
//	private Ref reference;
//
//	private Reference(Builder b) {
//		super();
//		this.name = b.name;
//		this.reference = b.reference;
//	}
//
//	public String getName() {
//		return name;
//	}
//
//	public void setName(String name) {
//		this.name = name;
//	}
//
//	public Ref getReference() {
//		return reference;
//	}
//
//	public void setReference(Ref reference) {
//		this.reference = reference;
//	}
//
//	public class Builder {
//		private String name;
//		private Ref reference;
//
//		public Builder reference(Ref r) {
//			this.reference = r;
//			return this;
//		}
//
//		public Builder name(String n) {
//			this.name = n;
//			return this;
//		}
//
//		public Reference build() {
//			return new Reference(this);
//		}
//
//	}
//
// }

package com.customer.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class RupiahConverter {
	public static String convert(BigDecimal value) {

		DecimalFormat rupiahIndo = (DecimalFormat) DecimalFormat
				.getCurrencyInstance();
		DecimalFormatSymbols formatRp = new DecimalFormatSymbols();
		formatRp.setCurrencySymbol("Rp. ");
		formatRp.setMonetaryDecimalSeparator(',');
		formatRp.setGroupingSeparator('.');
		rupiahIndo.setMaximumFractionDigits(0);
		rupiahIndo.setDecimalFormatSymbols(formatRp);

		return rupiahIndo.format(value);
	}
}

package com.customer.model;

import com.user.modul.User;

public class ProposalLite {
	private int idProposal;
	private String checkedDate;
	private User user;

	private ProposalLite(Builder b) {
		super();
		this.idProposal = b.idProposal;
		this.checkedDate = b.checkedDate;
		this.user = b.user;
	}

	public int getIdProposal() {
		return idProposal;
	}

	public String getCheckedDate() {
		return checkedDate;
	}

	public User getUser() {
		return user;
	}

	@Override
	public String toString() {
		return "ProposalLite [idProposal=" + idProposal + ", checkedDate="
				+ checkedDate + ", user=" + user + "]";
	}

	public static class Builder {
		private int idProposal;
		private String checkedDate;
		private User user;

		public Builder id(int id) {
			this.idProposal = id;
			return this;
		}

		public Builder date(String d) {
			this.checkedDate = d;
			return this;
		}

		public Builder Checkedby(User u) {
			this.user = u;
			return this;
		}

		public ProposalLite build() {
			return new ProposalLite(this);
		}
	}
}

package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class SubmissionCredit extends Model implements Serializable {
	private static final long serialVersionUID = 1L;

	private BigDecimal plafon = new BigDecimal(0);
	private String creditProduct;
	private int periode = 0;
	private double interest;
	private int id;

	private SubmissionCredit(Builder b) {
		super();
		this.plafon = b.plafon;
		this.creditProduct = b.creditProduct;
		this.periode = b.periode;
		this.interest = b.interest;
		this.id = b.id;
	}

	public int getId() {
		return id;
	}

	public void setInterest(double interest) {
		this.interest = interest;
	}

	public String getCreditProduct() {
		return creditProduct;
	}

	public void setCreditProduct(String creditProduct) {
		this.creditProduct = creditProduct;
	}

	public double getInterest() {
		return interest;
	}

	public void setInterest(float interest) {
		this.interest = interest;
	}

	public BigDecimal getPlafon() {
		System.out.println("get plafond 1 :" + plafon.setScale(0));
		return plafon;
	}

	public String getPlafonAsRupiah() {
		System.out.println("get plafond to rupiah :" + plafon);
		System.out.println("get plafond to rupiah :" + plafon.setScale(0));
		return RupiahConverter.convert(plafon);
	}

	public void setPlafon(String plafon) {
		this.plafon = new BigDecimal(plafon);
	}

	public int getPeriode() {
		return periode;
	}

	public void setPeriode(int periode) {
		this.periode = periode;
	}

	@Override
	public String toString() {
		return "SubmissionCredit [plafon=" + plafon + ", creditProduct="
				+ creditProduct + ", periode=" + periode + ", interest="
				+ interest + ", id=" + id + "]";
	}

	public static class Builder {
		private BigDecimal plafon;
		private String creditProduct;
		private int periode;
		private double interest;
		private int id;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder interest(double i) {
			this.interest = i;
			return this;
		}

		public Builder plafon(String plafon2) {
			String cleaned = "0";
			if (plafon2.contains(".")) {
				cleaned = plafon2.substring(0, plafon2.indexOf("."));
			} else {
				cleaned = plafon2;
			}
			System.out.println("plafon " + plafon2 + " vs " + cleaned);
			this.plafon = new BigDecimal(cleaned);
			return this;
		}

		public Builder creditProduct(String creditProduct) {
			this.creditProduct = creditProduct;
			return this;
		}

		public Builder periode(int p) {
			this.periode = p;
			return this;
		}

		public SubmissionCredit build() {
			return new SubmissionCredit(this);
		}
	}
}

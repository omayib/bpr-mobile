package com.customer.model;

import java.io.Serializable;
import java.math.BigDecimal;

public class Job extends Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public enum statusOfWork {
		TETAP, KONTRAK
	}

	private int id = -1;
	private String name;
	private String officeName = "";
	private String officeAddr = "";
	private String jobField = "";
	private String statusOfWorker = "";
	private String positionByCurrent = "";
	private int yearOfJoined = -1;
	private String positionByInitial = "";
	private BigDecimal salary = new BigDecimal(0);
	private String achievement = "";

	private Job(Builder b) {
		this.id = b.id;
		this.officeName = b.officeName;
		this.officeAddr = b.officeAddr;
		this.statusOfWorker = b.statusOfWorker;
		this.positionByCurrent = b.positionByCurrent;
		this.yearOfJoined = b.yearOfJoined;
		this.positionByInitial = b.positionByInitial;
		this.salary = b.salary;
		this.achievement = b.achievement;
		this.jobField = b.jobField;
		this.name = b.name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getJobField() {
		return jobField;
	}

	public int getId() {
		return id;
	}

	public String getOfficeName() {
		return officeName;
	}

	public void setOfficeName(String officeName) {
		this.officeName = officeName;
	}

	public String getOfficeAddr() {
		return officeAddr;
	}

	public void setOfficeAddr(String officeAddr) {
		this.officeAddr = officeAddr;
	}

	public String getStatusOfWorker() {
		return statusOfWorker;
	}

	public void setStatusOfWorker(String statusOfWorker) {
		this.statusOfWorker = statusOfWorker;
	}

	public String getPositionByCurrent() {
		return positionByCurrent;
	}

	public void setPositionByCurrent(String positionByCurrent) {
		this.positionByCurrent = positionByCurrent;
	}

	public Integer getYearOfJoined() {
		return yearOfJoined;
	}

	public void setYearOfJoined(Integer yearOfJoined) {
		this.yearOfJoined = yearOfJoined;
	}

	public String getPositionByInitial() {
		return positionByInitial;
	}

	public void setPositionByInitial(String positionByFirst) {
		this.positionByInitial = positionByFirst;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public String getAchievement() {
		return achievement;
	}

	public void setAchievement(String achievement) {
		this.achievement = achievement;
	}

	public static class Builder {
		private int id = -1;
		private String officeName = "";
		private String officeAddr = "";
		private String statusOfWorker = "";
		private String positionByCurrent = "";
		private int yearOfJoined = -1;
		private String positionByInitial = "";
		private BigDecimal salary = new BigDecimal(0);
		private String achievement = "";
		private String jobField = "";
		private String name = "";

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder field(String f) {
			this.jobField = f;
			return this;
		}

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder officeName(String name) {
			this.officeName = name;
			return this;
		}

		public Builder officeAddress(String addr) {
			this.officeAddr = addr;
			return this;
		}

		public Builder statusOfWorker(String status) {
			this.statusOfWorker = status;
			return this;
		}

		public Builder positionByCurrent(String pos) {
			this.positionByCurrent = pos;
			return this;
		}

		public Builder yearOfJoined(Integer year) {
			this.yearOfJoined = year;
			return this;
		}

		public Builder positionByInitial(String postition) {
			this.positionByInitial = postition;
			return this;
		}

		public Builder salary(String value) {
			String cleaned = "0";
			if (value.contains(".")) {
				cleaned = value.substring(0, value.indexOf("."));
			} else {
				cleaned = value;
			}
			this.salary = new BigDecimal(cleaned);
			return this;
		}

		public Builder achievment(String achiev) {
			this.achievement = achiev;
			return this;
		}

		public Job build() {
			return new Job(this);
		}
	}
}

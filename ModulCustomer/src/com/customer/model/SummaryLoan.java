package com.customer.model;

import java.math.BigDecimal;

public class SummaryLoan {
	private int loansTo;
	private String plafon;
	private String debitBalance;

	private SummaryLoan(Builder b) {
		this.loansTo = b.loansTo;
		this.plafon = b.plafon;
		this.debitBalance = b.debitBalance;
	}

	public int getLoansTo() {
		return loansTo;
	}

	public String getPlafonAsRupiah(){
		return RupiahConverter.convert(new BigDecimal(plafon));
	}
	public String getPlafon() {
		return plafon;
	}

	public String getDebitBalance() {
		return debitBalance;
	}

	@Override
	public String toString() {
		return "SummaryLoan [loansTo=" + loansTo + ", plafon=" + plafon
				+ ", debitBalance=" + debitBalance + "]";
	}

	public static class Builder {
		private int loansTo;
		private String plafon;
		private String debitBalance;

		public Builder loansTo(int i) {
			this.loansTo = i;
			return this;
		}

		public Builder plafon(String p) {
			this.plafon = p;
			return this;
		}

		public Builder debitBalance(String b) {
			this.debitBalance = b;
			return this;
		}

		public SummaryLoan build() {
			return new SummaryLoan(this);
		}
	}
}

package com.customer.model;

import java.io.Serializable;

public class EnvironmentalCheck extends Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String name;
	private String relation;
	private String impression;
	private int id = -1;

	private EnvironmentalCheck(Builder b) {
		this.name = b.name;
		this.relation = b.relation;
		this.impression = b.impression;
		this.id = b.id;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getImpression() {
		return impression;
	}

	public void setImpression(String impression) {
		this.impression = impression;
	}

	public static class Builder {
		private String name;
		private String relation;
		private String impression;
		private int id = -1;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder relation(String re) {
			this.relation = re;
			return this;
		}

		public Builder impression(String im) {
			this.impression = im;
			return this;
		}

		public EnvironmentalCheck build() {
			return new EnvironmentalCheck(this);
		}

	}
}

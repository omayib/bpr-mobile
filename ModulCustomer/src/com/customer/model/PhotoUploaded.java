package com.customer.model;

public class PhotoUploaded {
	private String galleryName, photoName, photoInfo, photoUrl, uniqueId;
	private int idGallery, idFolder, idPhoto;

	private PhotoUploaded(Builder b) {
		this.galleryName = b.galleryName;
		this.photoName = b.photoName;
		this.photoInfo = b.photoInfo;
		this.photoUrl = b.photoUrl;
		this.idFolder = b.idCategory;
		this.idGallery = b.idGallery;
		this.idPhoto = b.idPhoto;
		this.uniqueId = b.uniqueId;
	}

	public String getUniqueId() {
		return uniqueId;
	}

	public int getIdPhoto() {
		return idPhoto;
	}

	public String getGalleryName() {
		return galleryName;
	}

	public String getPhotoName() {
		return photoName;
	}

	public String getPhotoInfo() {
		return photoInfo;
	}

	public String getPhotoUrl() {
		return photoUrl;
	}

	public int getIdGallery() {
		return idGallery;
	}

	public int getIdCategory() {
		return idFolder;
	}

	public static class Builder {
		private String galleryName, photoName, photoInfo, photoUrl, uniqueId;
		private int idGallery, idCategory, idPhoto;

		public Builder uniqueId(String id) {
			this.uniqueId = id;
			return this;
		}

		public Builder idPhoto(int id) {
			this.idPhoto = id;
			return this;
		}

		public Builder galeryName(String n) {
			this.galleryName = n;
			return this;
		}

		public Builder photoName(String n) {
			this.photoName = n;
			return this;
		}

		public Builder photoInfo(String n) {
			this.photoInfo = n;
			return this;
		}

		public Builder photoUrl(String p) {
			this.photoUrl = p;
			return this;
		}

		public Builder idGallery(int i) {
			this.idGallery = i;
			return this;
		}

		public Builder idCategory(int c) {
			this.idCategory = c;
			return this;
		}

		public PhotoUploaded build() {
			return new PhotoUploaded(this);
		}
	}

}

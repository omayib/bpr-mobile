package com.bpr.photo.viewer.adapter;

import java.util.List;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bpr.photo.viewer.R;
import com.customer.model.Photo;
import com.loopj.android.image.SmartImageView;

public class PhotoViewerAdapter extends ArrayAdapter<Photo> {
	private int inflatedLayout;
	// private ArrayList<Photo> photo;
	private Context context;
	private Photo currentPhoto;
	private Holder holder;
	private LayoutInflater inflater;

	public PhotoViewerAdapter(Context context, int layout, List<Photo> objects) {
		super(context, layout, objects);
		this.inflatedLayout = layout;
		this.context = context;
		// this.photo = (ArrayList<Photo>) objects;

	}

	public View getView(int position, android.view.View convertView,
			android.view.ViewGroup parent) {

		currentPhoto = getItem(position);
		if (convertView == null) {
			holder = new Holder();
			inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(inflatedLayout, parent, false);
			holder.warning = (ImageView) convertView.findViewById(R.id.warning);
			holder.img = (SmartImageView) convertView
					.findViewById(R.id.picture);
			holder.name = (TextView) convertView.findViewById(R.id.text);

			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		System.out.println("CURRENT PHOTO::" + currentPhoto.toString());
		holder.name.setText(currentPhoto.getName());
		if (currentPhoto.getUrl() != null) {
			holder.warning.setVisibility(View.GONE);
			try {
				// ((BitmapDrawable)
				// holder.img.getDrawable()).getBitmap().recycle();
				holder.img.setImageUrl(currentPhoto.getUrl());
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			holder.warning.setVisibility(View.VISIBLE);
			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inSampleSize = 2;
			try {
				holder.img.setImageBitmap(BitmapFactory.decodeFile(
						currentPhoto.getPath(), options));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return convertView;
	};

	static class Holder {
		SmartImageView img;
		TextView name;
		ImageView warning;
	}
}

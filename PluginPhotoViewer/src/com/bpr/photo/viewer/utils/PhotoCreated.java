package com.bpr.photo.viewer.utils;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Photo;

public class PhotoCreated implements BprEvent {
	public Photo photo;

	public PhotoCreated(Photo photo) {
		super();
		this.photo = photo;
	}

}

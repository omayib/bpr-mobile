package com.bpr.photo.viewer.utils;

import com.customer.model.Photo;

public interface PhotoAction {
	void uploadPhoto(String token, Photo p);
}

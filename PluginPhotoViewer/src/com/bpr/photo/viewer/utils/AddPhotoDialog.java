package com.bpr.photo.viewer.utils;

import java.util.UUID;

import android.os.Bundle;
import android.support.annotation.IntegerRes;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bpr.photo.viewer.R;
import com.customer.model.Photo;

public class AddPhotoDialog extends DialogFragment {
	private TakePicture takePicture;
	private String path;
	private String photoName = "";
	private String photoInfo = "";
	private PhotoDialogListener listener;

	public interface PhotoDialogListener {
		void onCreated(Photo photo);
	}

	public AddPhotoDialog(String selectedImagePath, TakePicture takePicture,
			PhotoDialogListener listener) {
		this.path = selectedImagePath;
		this.takePicture = takePicture;
		this.listener = listener;
	}

	@Override
	public View onCreateView(LayoutInflater inflater,
			@Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		getDialog().setTitle("Menambah aset");
		View rootView = inflater.inflate(R.layout.dialog_photo_properties,
				container, false);

		ImageView img = (ImageView) rootView.findViewById(R.id.photo_imgview);
		img.setImageBitmap(takePicture.decodeFile(path));

		final EditText editPhotoName = (EditText) rootView
				.findViewById(R.id.photo_name);
		final EditText editPhotoInfo = (EditText) rootView
				.findViewById(R.id.photo_info);

		Button b = (Button) rootView.findViewById(R.id.photo_button_save);
		b.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				photoInfo = editPhotoInfo.getText().toString();
				photoName = editPhotoName.getText().toString();
				if (photoName.isEmpty()) {
					Toast.makeText(getActivity(), "nama foto wajib diisi",
							Toast.LENGTH_SHORT).show();
					return;
				}
				int random=RandomInteger.randInt(1, 1000);
				Photo photo = new Photo.Builder().pathLocal(path).id(random)
						.info(photoInfo).name(photoName)
						.uniqueId(UUID.randomUUID().toString()).build();
				listener.onCreated(photo);
				getDialog().dismiss();
			}
		});
		return rootView;
	}

}

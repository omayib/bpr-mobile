package com.bpr.photo.viewer.activity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.bpr.photo.viewer.R;
import com.bpr.photo.viewer.adapter.PhotoViewerAdapter;
import com.bpr.photo.viewer.utils.AddPhotoDialog;
import com.bpr.photo.viewer.utils.AddPhotoDialog.PhotoDialogListener;
import com.bpr.photo.viewer.utils.TakePicture;
import com.customer.model.Photo;
import com.customer.model.PhotoFolder;

public class PhotoViewerActivity extends FragmentActivity implements
		PhotoDialogListener {
	private String selectedImagePath = "";
	private GridView gridView;
	private PhotoViewerAdapter photoAdapter;
	private List<Photo> listPhoto = new ArrayList<Photo>();
	private TakePicture takePicture;
	private PhotoFolder folder;
	private UploadPhotoReceiver uploadPhotoReceiver;
	private String galleryName;
	private int proposalId;
	private boolean isReadOnly;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_grid_photoviewer);

		takePicture = new TakePicture(this);
		photoAdapter = new PhotoViewerAdapter(this, R.layout.item_photoviewer,
				listPhoto);

		gridView = (GridView) findViewById(R.id.grid_picture);
		gridView.setAdapter(photoAdapter);
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				final Photo photo = (Photo) arg0.getAdapter().getItem(arg2);
				System.out.println("data photo " + photo.toString());
				AlertDialog.Builder ad = new AlertDialog.Builder(
						PhotoViewerActivity.this);
				ad.setMessage("hapus photo?");
				ad.setPositiveButton("ya", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int arg1) {
						Bundle b = new Bundle();
						b.putSerializable("photoDeleted", photo);

						Intent returnIntent = new Intent();
						returnIntent.setAction("com.photo.viewer.broadcast");
						returnIntent.putExtra("status", "photo_deleted");
						returnIntent.putExtras(b);

						sendBroadcast(returnIntent);
						dialog.dismiss();
					}
				});
				ad.setNegativeButton("tidak", new OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				ad.show();

			}
		});

		Bundle b = getIntent().getExtras();
		if (b != null) {
			if (b.containsKey("pendingPhotos")) {
				isReadOnly = true;
				System.out.println("containt pendingPHotos");
				List<Photo> pendingPhotos = (List<Photo>) b
						.getSerializable("pendingPhotos");
				listPhoto.addAll(pendingPhotos);
				photoAdapter.notifyDataSetChanged();
			} else {
				isReadOnly = false;
				System.out.println("NOT containt pendingPHotos");
				folder = (PhotoFolder) b.getSerializable("currentFolder");
				galleryName = b.getString("galleryName");
				proposalId = b.getInt("proposalId");
				listPhoto.addAll(folder.getPhotoGalleries());
				photoAdapter.notifyDataSetChanged();
			}
		} else {
			Log.d("viewer", "bundle is null");
		}

	}

	@Override
	protected void onStart() {
		super.onStart();
		// register receiver
		uploadPhotoReceiver = new UploadPhotoReceiver();
		IntentFilter intentFilter = new IntentFilter("com.photo.uploaded");
		registerReceiver(uploadPhotoReceiver, intentFilter);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != Activity.RESULT_CANCELED) {
			if (requestCode == TakePicture.PICK_IMAGE) {
				selectedImagePath = takePicture.getAbsolutePath(data.getData());
			} else if (requestCode == TakePicture.CAPTURE_IMAGE) {
				selectedImagePath = takePicture.getImagePath();
			}
			if (selectedImagePath == null) {
				Toast.makeText(getApplicationContext(),
						"maaf ada kesalahan pada aplikasi", Toast.LENGTH_SHORT)
						.show();
				return;
			}
			System.out.println(selectedImagePath);
			new Handler().post(new Runnable() {
				@Override
				public void run() {
					AddPhotoDialog photoDialog = new AddPhotoDialog(
							selectedImagePath, takePicture,
							PhotoViewerActivity.this);
					photoDialog.show(getSupportFragmentManager(), "photo");
				}
			});
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onBackPressed() {
		System.out.println("on back pressed");
		// Bundle b = new Bundle();
		// b.putSerializable("listPhoto", (Serializable) listPhoto);
		// b.putString("coba", "isi coba");
		// returnIntent.putExtra("bundle", b);

		Intent returnIntent = getIntent();
		returnIntent.putExtra("cobaa", "isi cobaa");
		setResult(RESULT_OK, returnIntent);
		// finish();
		super.onBackPressed();
	}

	@Override
	protected void onStop() {
		super.onStop();
		try {
			unregisterReceiver(uploadPhotoReceiver);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!isReadOnly) {
			getMenuInflater().inflate(R.menu.take_picture, menu);
		} else {
			getMenuInflater().inflate(R.menu.main_menu_with_sync, menu);
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.take_picture) {
			takePicture.selectDialog();
		} else if (item.getItemId() == R.id.upload) {
			Intent returnIntent = new Intent();
			returnIntent.setAction("com.photo.viewer.broadcast");
			returnIntent.putExtra("status", "upload");
			sendBroadcast(returnIntent);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onCreated(Photo photo) {
		System.out.println("PHOTO Created");
		System.out.println("PHOTO Created "+photo.toString());
		folder.setPhotoCollection(photo);
		listPhoto.add(photo);
		photoAdapter.notifyDataSetChanged();

		Bundle b = new Bundle();
		b.putSerializable("photoCreated", photo);
		b.putInt("proposalId", proposalId);
		b.putInt("folderId", folder.getId());
		b.putString("folderName", folder.getName());
		b.putString("galleryName", galleryName);

		Intent returnIntent = new Intent();
		returnIntent.setAction("com.photo.viewer.broadcast");
		returnIntent.putExtra("status", "photo_created");
		returnIntent.putExtras(b);

		sendBroadcast(returnIntent);
	}

	private class UploadPhotoReceiver extends BroadcastReceiver {
		Photo photo;

		@Override
		public void onReceive(Context context, Intent arg1) {
			Bundle b = arg1.getExtras();
			if (b == null)
				return;
			if (b.containsKey("photoCreated")) {
				photo = (Photo) b.getSerializable("photoCreated");
				runOnUiThread(new Runnable() {

					@Override
					public void run() {
						photoAdapter.notifyDataSetChanged();
					}
				});
				System.out.println("UPLOAD--" + photo.toString());
			} else if (b.containsKey("photoDeleted")) {
				photo = (Photo) b.getSerializable("photoDeleted");
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						for (Photo p : listPhoto) {
							if (photo.getId() == p.getId()) {
								listPhoto.remove(p);
							}
						}
						System.out.println(this.getClass().getName()
								+ " deleted");
						photoAdapter.notifyDataSetChanged();
					}
				});
			}
		}

	}
}

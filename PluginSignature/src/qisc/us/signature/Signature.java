package qisc.us.signature;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

public class Signature extends View {

	private static final float STROKE_WIDTH = 1f;
	private static final float HALF_STROKE_WIDTH = STROKE_WIDTH / 2;
	private Paint paint = new Paint();
	private Path path = new Path();

	private float lastTouchX;
	private float lastTouchY;
	private final RectF dirtyRect = new RectF();
	private JSONArray pointSig;
	private JSONArray memberLine;
	private JSONArray line;
	private JSONObject obSignature;
	int count;
	int numLine;
	DecimalFormat dc = new DecimalFormat("##.##");
	private boolean newLine = false;
	private boolean isEnabled = true;
	private static float scale = new Float(0.15);
	private static float move = 20;

	public Signature(Context context, AttributeSet attrs) {
		super(context, attrs);
		paint.setAntiAlias(true);
		paint.setColor(Color.BLACK);
		paint.setStyle(Paint.Style.STROKE);
		paint.setStrokeJoin(Paint.Join.ROUND);
		paint.setStrokeWidth(STROKE_WIDTH);

		System.out.println("signature container");

		prepareSignature();
		// decodeSignature(signature);

	}

	public void injectView(View v) {
		System.out.println("view :" + v.getWidth() + "," + v.getHeight());
	}

	private void prepareSignature() {
		// TODO Auto-generated method stub
		numLine = -1;
		memberLine = new JSONArray();
		line = new JSONArray();
		isEnabled = true;
	}

	public void setEnabled(boolean en) {
		isEnabled = en;
	}

	public void createSignature(String signature) {

		try {
			JSONObject obSig = new JSONObject(signature);
			JSONArray arrSig = obSig.getJSONArray("lines");
			for (int i = 0; i < arrSig.length(); i++) {
				JSONArray sig2 = arrSig.getJSONArray(i);
				System.out.println("sig2:: " + sig2.length());// 25
				for (int j = 0; j < sig2.length(); j++) {
					JSONArray point = sig2.getJSONArray(j);// 2
					if (j == 0)
						path.moveTo(scaleDownWidth(point.getLong(0)),
								scaleDownWidth(point.getLong(1)));
					path.lineTo(scaleDownWidth(point.getLong(0)),
							scaleDownHeight(point.getLong(1)));

				}

			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * scale with 0.15 then move to right by 20 unit
	 */

	private float scaleDownWidth(float input) {
		float output = (float) (input * scale) + move;
		return output;
	}

	/**
	 * scale with 0.15 then move to right by 20 unit
	 */
	private float scaleDownHeight(float input) {
		float output = (float) (input * scale) + move;
		return output;
	}

	public void clear() {
		path.reset();
		invalidate();
		prepareSignature();
	}

	@Override
	protected void onDraw(Canvas canvas) {
		canvas.drawPath(path, paint);
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (!isEnabled)
			return false;
		float eventX = event.getX();
		float eventY = event.getY();
		// mGetSign.setEnabled(true);

		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			newLine = true;
			count = 0;
			path.moveTo(eventX, eventY);
			lastTouchX = eventX;
			lastTouchY = eventY;
			generateLine(count, eventX, eventY, newLine);
			return true;

		case MotionEvent.ACTION_MOVE:
		case MotionEvent.ACTION_UP:
			newLine = false;
			count += 1;

			resetDirtyRect(eventX, eventY);
			int historySize = event.getHistorySize();
			for (int i = 0; i < historySize; i++) {
				float historicalX = event.getHistoricalX(i);
				float historicalY = event.getHistoricalY(i);
				expandDirtyRect(historicalX, historicalY);
				path.lineTo(historicalX, historicalY);

			}
			path.lineTo(eventX, eventY);
			generateLine(count, eventX, eventY, newLine);
			break;

		default:
			debug("Ignored touch event: " + event.toString());
			return false;
		}

		invalidate((int) (dirtyRect.left - HALF_STROKE_WIDTH),
				(int) (dirtyRect.top - HALF_STROKE_WIDTH),
				(int) (dirtyRect.right + HALF_STROKE_WIDTH),
				(int) (dirtyRect.bottom + HALF_STROKE_WIDTH));

		lastTouchX = eventX;
		lastTouchY = eventY;

		return true;
	}

	private void generateLine(int count2, float eventX, float eventY,
			boolean newLine2) {
		try {
			if (newLine2) {
				numLine += 1;
				memberLine = new JSONArray();
			}
			DecimalFormatSymbols symbols = new DecimalFormatSymbols();
			symbols.setDecimalSeparator('.');
			dc.setDecimalFormatSymbols(symbols);
			
			pointSig = new JSONArray();
			pointSig.put(0, Double.parseDouble(dc.format(eventX)));
			pointSig.put(1, Double.parseDouble(dc.format(eventY)));
			memberLine.put(count2, pointSig);
			line.put(numLine, memberLine);

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getJson() {
		String data = "";
		try {
			obSignature = new JSONObject();
			obSignature.put("lines", line);
			System.out.println(obSignature);
			data = obSignature.toString();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return data;
	}
	public boolean isEmpty(){
		return line.length()>0?false:true;
	}

	private void debug(String string) {
	}

	private void expandDirtyRect(float historicalX, float historicalY) {

		if (historicalX < dirtyRect.left) {
			dirtyRect.left = historicalX;
		} else if (historicalX > dirtyRect.right) {
			dirtyRect.right = historicalX;
		}

		if (historicalY < dirtyRect.top) {
			dirtyRect.top = historicalY;
		} else if (historicalY > dirtyRect.bottom) {
			dirtyRect.bottom = historicalY;
		}
	}

	private void resetDirtyRect(float eventX, float eventY) {
		dirtyRect.left = Math.min(lastTouchX, eventX);
		dirtyRect.right = Math.max(lastTouchX, eventX);
		dirtyRect.top = Math.min(lastTouchY, eventY);
		dirtyRect.bottom = Math.max(lastTouchY, eventY);
	}
}
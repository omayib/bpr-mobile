package com.bpr.event.manager;

public interface EventPublisher {
	void publish(BprEvent event);

	<T extends BprEvent> void registerSubscriber(EventSubscriber<T> subscriber);

	<T extends BprEvent> void unregisterSubsriber(EventSubscriber<T> subscriber);
}

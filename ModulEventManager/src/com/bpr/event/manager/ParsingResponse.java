package com.bpr.event.manager;

public interface ParsingResponse<T> {
	void onSuccess(T result);

	void onFailure(String failedResult);
}
package com.bpr.event.manager;

public interface EventSubscriber<T extends BprEvent> {
	void handleEvent(T event);
}

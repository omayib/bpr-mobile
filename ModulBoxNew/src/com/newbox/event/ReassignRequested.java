package com.newbox.event;

import com.bpr.event.manager.BprEvent;

public class ReassignRequested implements BprEvent {
	public String token, note;
	public int idcustomer, idAo;

	public ReassignRequested(String token, int idcustomer, int idAo, String note) {
		this.token = token;
		this.idcustomer = idcustomer;
		this.idAo = idAo;
		this.note = note;
	}
}

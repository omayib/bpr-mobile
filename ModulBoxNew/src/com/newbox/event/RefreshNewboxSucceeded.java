package com.newbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.event.BoxData;

public class RefreshNewboxSucceeded implements BprEvent {
	public List<BoxData> response;

	public RefreshNewboxSucceeded(List<BoxData> response) {
		super();
		this.response = response;
	}

}

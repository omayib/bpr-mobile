package com.newbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.user.modul.User;

public class LoadListAoSucceded implements BprEvent {
	public List<User> user;

	public LoadListAoSucceded(List<User> user) {
		super();
		this.user = user;
	}

}

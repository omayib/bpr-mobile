package com.newbox.event;

import com.bpr.event.manager.BprEvent;

public class RefreshNewboxFailed implements BprEvent {

	public String exception;

	public RefreshNewboxFailed(String exception) {
		super();
		this.exception = exception;
	}

}

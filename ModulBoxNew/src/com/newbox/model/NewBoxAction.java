package com.newbox.model;

public interface NewBoxAction {
	void newBoxrefresh(String token);

	void assign(String token, int idCustomer, int idAo, String note);

	void getListAo(String token,int customerId);

}

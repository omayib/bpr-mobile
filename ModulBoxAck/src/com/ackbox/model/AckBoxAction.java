package com.ackbox.model;

import com.customer.model.SummaryCandidate;

public interface AckBoxAction {
	void ackBoxRefresh(String token);

	void ackBoxGetSummary(String token, int idCustomer);

	void ackBoxAcknowladgement(String token, int idCustomer,
			SummaryCandidate summaryCandidate);

	void ackBoxReject(String token, int idCustomer,
			SummaryCandidate summaryCandidate);
}

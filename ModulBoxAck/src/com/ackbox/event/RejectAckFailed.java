package com.ackbox.event;

import com.bpr.event.manager.BprEvent;

public class RejectAckFailed implements BprEvent {
	public String exception;

	public RejectAckFailed(String exception) {
		super();
		this.exception = exception;
	}

}

package com.ackbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.event.BoxData;

public class RefreshAckBoxSucceded implements BprEvent {
	public List<BoxData> response;

	public RefreshAckBoxSucceded(List<BoxData> response) {
		super();
		this.response = response;
	}

}

package com.ackbox.event;

import com.bpr.event.manager.BprEvent;

public class RefreshAckBoxFailed implements BprEvent {
	public String exception;

	public RefreshAckBoxFailed(String exception) {
		super();
		this.exception = exception;
	}

}

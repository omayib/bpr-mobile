package com.ackbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.SummaryCandidate;

public class RejectAckRequested implements BprEvent {
	public SummaryCandidate summaryCandidate;

	public RejectAckRequested(SummaryCandidate summaryCandidate) {
		super();
		// TODO Auto-generated constructor stub
		this.summaryCandidate = summaryCandidate;
	}

}

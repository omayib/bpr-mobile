package com.ackbox.event;

import com.bpr.event.manager.BprEvent;

public class GetSummaryFailed implements BprEvent {
	public String exception;

	public GetSummaryFailed(String exception) {
		super();
		this.exception = exception;
	}

}

package com.ackbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.SummaryCandidate;

public class ApproveAckRequested implements BprEvent {
	public SummaryCandidate summaryCandidate;

	public ApproveAckRequested(SummaryCandidate summaryCandidate) {
		super();
		this.summaryCandidate = summaryCandidate;
	}

}

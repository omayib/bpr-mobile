package com.ackbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.Summary;

public class GetSummarySucceeded implements BprEvent {
	public Summary summary;

	public GetSummarySucceeded(Summary summary) {
		super();
		this.summary = summary;
	}

}

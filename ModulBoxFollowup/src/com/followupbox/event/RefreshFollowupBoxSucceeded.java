package com.followupbox.event;

import java.util.List;

import com.bpr.event.manager.BprEvent;
import com.customer.event.BoxData;

public class RefreshFollowupBoxSucceeded implements BprEvent {
	public List<BoxData> response;

	public RefreshFollowupBoxSucceeded(List<BoxData> response) {
		super();
		this.response = response;
	}
}

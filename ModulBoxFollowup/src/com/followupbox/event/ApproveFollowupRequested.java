package com.followupbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.SummaryCandidate;

public class ApproveFollowupRequested implements BprEvent {
	public SummaryCandidate summaryCandidate;

	public ApproveFollowupRequested(SummaryCandidate summaryCandidate) {
		super();
		this.summaryCandidate = summaryCandidate;
	}

}

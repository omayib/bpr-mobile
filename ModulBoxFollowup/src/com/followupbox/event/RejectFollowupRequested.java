package com.followupbox.event;

import com.bpr.event.manager.BprEvent;
import com.customer.model.SummaryCandidate;

public class RejectFollowupRequested implements BprEvent {
	public SummaryCandidate summaryCandidate;

	public RejectFollowupRequested(SummaryCandidate summaryCandidate) {
		super();
		this.summaryCandidate = summaryCandidate;
	}

}

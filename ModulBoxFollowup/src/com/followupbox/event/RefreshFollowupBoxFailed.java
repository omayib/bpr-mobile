package com.followupbox.event;

import com.bpr.event.manager.BprEvent;

public class RefreshFollowupBoxFailed implements BprEvent {

	public String exception;

	public RefreshFollowupBoxFailed(String exception) {
		super();
		this.exception = exception;
	}

}

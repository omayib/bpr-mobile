package com.followupbox.model;

import com.customer.model.SummaryCandidate;

public interface FollowupBoxAction {
	void refreshFollowupBox(String token);

	void reassign(String token, int idCustomer, int idAo, String note);

	void approveFollowupBox(String token, int idCustomer,
			SummaryCandidate summaryCandidate);

	void rejectFollowupBox(String token, int idCustomer,
			SummaryCandidate summaryCandidate);
}

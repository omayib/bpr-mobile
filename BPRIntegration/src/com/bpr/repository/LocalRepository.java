package com.bpr.repository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.activeandroid.ActiveAndroid;
import com.bpr.activerecord.model.AssetActiveRecord;
import com.bpr.activerecord.model.BpkbActiveRecord;
import com.bpr.activerecord.model.BusinessActiveRecord;
import com.bpr.activerecord.model.ChildActiveRecord;
import com.bpr.activerecord.model.DepositoActiveRecord;
import com.bpr.activerecord.model.EnvironmentActiveRecord;
import com.bpr.activerecord.model.ExpenditureActiveRecord;
import com.bpr.activerecord.model.IncomeActiveRecord;
import com.bpr.activerecord.model.JobActiveRecord;
import com.bpr.activerecord.model.PersonalActiveRecord;
import com.bpr.activerecord.model.PhotoActiveRecord;
import com.bpr.activerecord.model.PhotoFolderActiveRecord;
import com.bpr.activerecord.model.ProposalActiveRecord;
import com.bpr.activerecord.model.PurposeActiveRecord;
import com.bpr.activerecord.model.QueryAction;
import com.bpr.activerecord.model.ShmActiveRecord;
import com.bpr.activerecord.model.SubmissionActiveRecord;
import com.bpr.activerecord.model.TabunganActiveRecord;
import com.bpr.integration.utils.RandomInteger;
import com.customer.model.Assets;
import com.customer.model.AssetsCollection;
import com.customer.model.Business;
import com.customer.model.BusinessCollection;
import com.customer.model.CapacityCollection;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import com.customer.model.Children;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralBpkbCollection;
import com.customer.model.CollateralDeposito;
import com.customer.model.CollateralDepositoCollection;
import com.customer.model.CollateralShm;
import com.customer.model.CollateralShmCollection;
import com.customer.model.CollateralTabungan;
import com.customer.model.CollateralTabunganCollection;
import com.customer.model.EnvironmentalCheck;
import com.customer.model.EnvironmentalCheckCollection;
import com.customer.model.Job;
import com.customer.model.Personal;
import com.customer.model.Photo;
import com.customer.model.PhotoBusiness;
import com.customer.model.PhotoCandidate;
import com.customer.model.PhotoCollateral;
import com.customer.model.PhotoFolder;
import com.customer.model.PhotoHome;
import com.customer.model.PhotoOther;
import com.customer.model.Proposal;
import com.customer.model.Purposes;
import com.customer.model.PurposesCollection;
import com.customer.model.SubmissionCredit;

public class LocalRepository implements Repository {

	public Proposal loadProposal(int idProposal) {
		System.out.println("id proposal " + idProposal);
		ProposalActiveRecord p = ProposalActiveRecord
				.getProposalCandidate(idProposal);
		if (p != null) {

			Proposal proposal = new Proposal.Builder().id(p.id)
					.assets(getAssets(idProposal))
					.tabungan(getCollateralTabungan(idProposal))
					.deposito(getCollateralDeposito(idProposal))
					.bpkb(getCollateralBpkb(idProposal))
					.business(getBusiness(idProposal))
					.capacity(getCapacity(idProposal))
					.environmentalCheck(getEnvironmentalCheckup(idProposal))
					.job(getJob(idProposal)).personal(getPersonal(idProposal))
					.photoBusiness(getPhotoBusiness(idProposal))
					.photoGuarantee(getPhotoCollateral(idProposal))
					.photoHome(getPhotoHome(idProposal))
					.photoOther(getPhotoOther(idProposal))
					.purpose(getPurposes(idProposal))
					.shm(getCollateralShm(idProposal))
					.submissionOfCredit(getSubmission(idProposal)).build();
			System.out.println("id proposal " + proposal.toString());
			return proposal;
		}
		return null;
	}

	public Personal getPersonal(int idProposal) {
		System.out.println("geting personal from loca.... with id "
				+ idProposal);
		PersonalActiveRecord p = PersonalActiveRecord.getPersonal(idProposal);
		if (p == null) {
			return null;
		}
		List<ChildActiveRecord> c = ChildActiveRecord.getChildren(p.id);
		List<Children> children = new ArrayList<Children>();
		if (!c.isEmpty()) {
			for (ChildActiveRecord childActiveRecord : c) {
				Children child = new Children.Builder()
						.age(childActiveRecord.age).id(childActiveRecord.id)
						.name(childActiveRecord.name).build();
				children.add(child);
			}
		}
		Personal personal = new Personal.Builder()
				.addressByCurrent(p.addressByCurrent)
				.addressByKtp(p.addressByKtp).age(p.age).birthDate(p.birthDate)
				.birthPlace(p.birthPlace).children(children).gender(p.gender)
				.handphoneNumber(p.handphoneNumber).homeStatus(p.homeStatus)
				.id(p.id).ktpNumber(p.ktpNumber)
				.motherMaidenName(p.motherMaidenName).name(p.name)
				.nameAlias(p.aliasName)
				.numberOfDependents(p.numberOfDependents)
				.phoneNumber(p.phoneNumber).spouseJob(p.spouseJob)
				.spouseName(p.spouseName).statusOfMarital(p.maritalStatus)
				.build();
		return personal;
	}

	public AssetsCollection getAssets(int idProposal) {
		List<AssetActiveRecord> assets = AssetActiveRecord
				.getAssets(idProposal);
		List<Assets> listAsset = new ArrayList<Assets>();
		if (assets.isEmpty()) {
			AssetsCollection asc = new AssetsCollection.Builder().asset(
					listAsset).build();
			return asc;
		}
		for (AssetActiveRecord a : assets) {
			Assets asset = new Assets.Builder().asset(a.id, a.name,
					a.value.toString()).build();
			listAsset.add(asset);
		}
		AssetsCollection asc = new AssetsCollection.Builder().asset(listAsset)
				.build();
		return asc;
	}

	public Job getJob(int idProposal) {
		JobActiveRecord j = JobActiveRecord.getJob(idProposal);
		if (j != null) {
			Job job = new Job.Builder().achievment(j.achievement).name(j.name)
					.field(j.jobField).id(j.id).officeAddress(j.officeAddr)
					.officeName(j.officeName)
					.positionByCurrent(j.positionByCurrent)
					.positionByInitial(j.positionByInitial)
					.salary(j.salary.toString())
					.statusOfWorker(j.statusOfWorker)
					.yearOfJoined(j.yearOfJoined).build();
			return job;
		} else {
			return null;
		}
	}

	public BusinessCollection getBusiness(int idProposal) {
		List<BusinessActiveRecord> businesses = BusinessActiveRecord
				.getBusiness(idProposal);
		List<Business> listBusiness = new ArrayList<Business>();
		if (businesses.isEmpty()) {
			BusinessCollection bc = new BusinessCollection.Builder().business(
					listBusiness).build();
			return bc;
		}
		for (BusinessActiveRecord ba : businesses) {
			Business business = new Business.Builder()
					.companyAddress(ba.address).companyInfo(ba.additionalInfo)
					.companyLegallity(ba.companyLegallity).companyName(ba.name)
					.companyProject(ba.projectExperiences)
					.companySiup(ba.siupNumber).companyTdp(ba.tdpNumber)
					.companyTenure(ba.officeOwnership)
					.fieldOfBusiness(ba.field).foundedYear(ba.foundedYear)
					.id(ba.id).numberOfEmployee(ba.numberOfEmployees).build();
			listBusiness.add(business);
			System.out.println("from repo insert busines...");
		}
		BusinessCollection bc = new BusinessCollection.Builder().business(
				listBusiness).build();
		return bc;
	}

	public SubmissionCredit getSubmission(int idProposal) {
		SubmissionActiveRecord subm = SubmissionActiveRecord
				.getSubmission(idProposal);
		if (subm == null)
			return null;

		SubmissionCredit submissionCredit = new SubmissionCredit.Builder()
				.creditProduct(subm.product).id(subm.id)
				.interest(subm.interest).periode(subm.period)
				.plafon(subm.plafon.toString()).build();

		return submissionCredit;
	}

	public PurposesCollection getPurposes(int idProposal) {
		List<PurposeActiveRecord> pa = PurposeActiveRecord
				.getPurposes(idProposal);
		List<Purposes> listPurposes = new ArrayList<Purposes>();
		if (pa.isEmpty()) {
			PurposesCollection pc = new PurposesCollection.Builder().purpose(
					listPurposes).build();
			return pc;
		}
		for (PurposeActiveRecord p : pa) {
			Purposes purpose = new Purposes.Builder().purpose(p.id, p.general,
					p.detail).build();
			listPurposes.add(purpose);
		}
		PurposesCollection pc = new PurposesCollection.Builder().purpose(
				listPurposes).build();
		return pc;
	}

	public CapacityCollection getCapacity(int idProposal) {
		List<CapacityIncome> listIncomes = new ArrayList<CapacityIncome>();
		List<CapacityExpenditure> listEpenditures = new ArrayList<CapacityExpenditure>();

		List<IncomeActiveRecord> incomes = IncomeActiveRecord
				.getIncomes(idProposal);
		List<ExpenditureActiveRecord> expenditures = ExpenditureActiveRecord
				.getExpenditures(idProposal);

		SubmissionActiveRecord subm = SubmissionActiveRecord
				.getSubmission(idProposal);
		BigDecimal plafon;
		if (subm == null) {
			plafon = new BigDecimal(0);
		} else {
			plafon = subm.plafon;
		}

		int idCapacity = -1;
		for (IncomeActiveRecord inc : incomes) {
			CapacityIncome i = new CapacityIncome.Builder().incomes(inc.id,
					inc.name, inc.value.toString()).build();
			idCapacity = inc.idCapacity;
			listIncomes.add(i);
		}

		for (ExpenditureActiveRecord exp : expenditures) {
			CapacityExpenditure e = new CapacityExpenditure.Builder()
					.expenditures(exp.id, exp.name, exp.value.toString())
					.build();
			idCapacity = exp.idCapacity;
			listEpenditures.add(e);
		}

		CapacityCollection capcoll = new CapacityCollection.Builder(idCapacity,
				listIncomes, listEpenditures).submission(plafon).build();
		System.out.println("incom " + listIncomes.toString());
		System.out.println("exp" + listEpenditures.toString());
		System.out.println("capcol" + capcoll.toString());
		return capcoll;
	}

	public CollateralBpkbCollection getCollateralBpkb(int idProposal) {
		List<BpkbActiveRecord> bpkb = BpkbActiveRecord.getBpkb(idProposal);
		List<CollateralBpkb> listBpkb = new ArrayList<CollateralBpkb>();
		if (bpkb.isEmpty()) {
			CollateralBpkbCollection bc = new CollateralBpkbCollection.Builder()
					.bpkb(listBpkb).build();
			return bc;
		}

		for (BpkbActiveRecord b : bpkb) {
			CollateralBpkb collateralBpkb = new CollateralBpkb.Builder()
					.binding(b.binding).carBrand(b.vehicleBrand).id(b.id)
					.info(b.info).nameOfBpkb(b.name).carName(b.vehicleName)
					.numberBpkb(b.bpkbNumber).numberStnk(b.stnkNumber)
					.policeNumber(b.policeNumber)
					.priceOnAssessed(b.priceAssessed)
					.priceOnMarket(b.priceMarket).carType(b.vehicleType)
					.carYear(b.vehicleYear).build();
			listBpkb.add(collateralBpkb);
		}
		CollateralBpkbCollection bc = new CollateralBpkbCollection.Builder()
				.bpkb(listBpkb).build();
		return bc;
	}

	public CollateralShmCollection getCollateralShm(int idProposal) {
		List<ShmActiveRecord> shm = ShmActiveRecord.getShm(idProposal);
		List<CollateralShm> listShm = new ArrayList<CollateralShm>();
		if (shm.isEmpty()) {
			CollateralShmCollection sc = new CollateralShmCollection.Builder()
					.shm(listShm).build();
			return sc;
		}
		for (ShmActiveRecord s : shm) {
			CollateralShm guaranteeShm = new CollateralShm.Builder()
					.binding(s.binding).certificateName(s.name)
					.certificateNumber(s.certificateNumber).id(s.id)
					.info(s.info).loadOfArea(s.size + "")
					.priceOnAssessed(s.priceAssessed)
					.priceOnMarket(s.priceMarket).statusOfLand(s.status)
					.type((s.type == null) ? "shm" : s.type).build();
			listShm.add(guaranteeShm);
		}
		CollateralShmCollection sc = new CollateralShmCollection.Builder().shm(
				listShm).build();
		return sc;
	}

	public CollateralTabunganCollection getCollateralTabungan(int idProposal) {
		List<TabunganActiveRecord> tabungans = TabunganActiveRecord
				.getAllTabungan(idProposal);
		List<CollateralTabungan> listTabungan = new ArrayList<CollateralTabungan>();

		if (tabungans.isEmpty()) {
			CollateralTabunganCollection tc = new CollateralTabunganCollection(
					listTabungan);
			return tc;
		}

		System.out.println("tabungan from loal");
		for (TabunganActiveRecord t : tabungans) {
			CollateralTabungan tabungan = new CollateralTabungan.Builder()
					.accountNumber(t.accountNumber)
					.ammount(t.amount.toString()).bookNumber(t.bookNumber)
					.id(t.id).name(t.name).build();
			listTabungan.add(tabungan);
		}
		CollateralTabunganCollection tc = new CollateralTabunganCollection(
				listTabungan);
		return tc;
	}

	public CollateralDepositoCollection getCollateralDeposito(int idProposal) {
		List<DepositoActiveRecord> depositos = DepositoActiveRecord
				.getAllDeposito(idProposal);
		List<CollateralDeposito> listDeposito = new ArrayList<CollateralDeposito>();

		if (depositos.isEmpty()) {
			CollateralDepositoCollection dc = new CollateralDepositoCollection(
					listDeposito);
			return dc;
		}
		System.out.println("deposito from loal");
		for (DepositoActiveRecord t : depositos) {
			CollateralDeposito tabungan = new CollateralDeposito.Builder()
					.amount(t.amount.toString()).bilyetNumber(t.bilyetNumber)
					.accountNumber(t.accountNumber).id(t.id).name(t.name)
					.build();
			listDeposito.add(tabungan);
		}
		CollateralDepositoCollection dc = new CollateralDepositoCollection(
				listDeposito);
		return dc;
	}

	public EnvironmentalCheckCollection getEnvironmentalCheckup(int idProposal) {
		List<EnvironmentActiveRecord> check = EnvironmentActiveRecord
				.getEnv(idProposal);
		List<EnvironmentalCheck> listCheck = new ArrayList<EnvironmentalCheck>();
		if (check.isEmpty()) {
			EnvironmentalCheckCollection ev = new EnvironmentalCheckCollection.Builder()
					.environmentalCheck(listCheck).build();
			return ev;
		}
		for (EnvironmentActiveRecord e : check) {
			EnvironmentalCheck env = new EnvironmentalCheck.Builder().id(e.id)
					.impression(e.impression).name(e.name).relation(e.realtion)
					.build();
			listCheck.add(env);
		}
		EnvironmentalCheckCollection ev = new EnvironmentalCheckCollection.Builder()
				.environmentalCheck(listCheck).build();
		return ev;
	}

	public PhotoBusiness getPhotoBusiness(int idProposal) {

		List<PhotoFolderActiveRecord> folder = PhotoFolderActiveRecord
				.getFolders(idProposal, "business");
		List<PhotoFolder> listFolders = new ArrayList<PhotoFolder>();
		for (PhotoFolderActiveRecord f : folder) {
			List<PhotoActiveRecord> photos = f.photos();
			List<Photo> listPhoto = new ArrayList<Photo>();
			for (PhotoActiveRecord p : photos) {
				Photo photo = new Photo.Builder().id(p.id).info(p.info)
						.name(p.name).pathLocal(p.path).uniqueId(p.uniqueId)
						.url(p.url).build();
				listPhoto.add(photo);
			}
			PhotoFolder fd = new PhotoFolder.Builder().id(f.idFolder)
					.name(f.name).photo(listPhoto).build();
			listFolders.add(fd);
		}
		PhotoBusiness pb = new PhotoBusiness.Builder().folders(listFolders)
				.build();
		return pb;
	}

	public PhotoCollateral getPhotoCollateral(int idProposal) {
		List<PhotoFolderActiveRecord> folder = PhotoFolderActiveRecord
				.getFolders(idProposal, "collateral");
		List<PhotoFolder> listFolders = new ArrayList<PhotoFolder>();
		for (PhotoFolderActiveRecord f : folder) {
			List<PhotoActiveRecord> photos = f.photos();
			List<Photo> listPhoto = new ArrayList<Photo>();
			for (PhotoActiveRecord p : photos) {
				System.out.println("local repo::" + p.toString());
				Photo photo = new Photo.Builder().id(p.id).info(p.info)
						.name(p.name).pathLocal(p.path).uniqueId(p.uniqueId)
						.url(p.url).build();
				listPhoto.add(photo);
			}
			PhotoFolder fd = new PhotoFolder.Builder().id(f.idFolder)
					.name(f.name).photo(listPhoto).build();
			listFolders.add(fd);
		}
		PhotoCollateral pb = new PhotoCollateral.Builder().foldes(listFolders)
				.build();
		return pb;
	}

	public PhotoHome getPhotoHome(int idProposal) {
		List<PhotoFolderActiveRecord> folder = PhotoFolderActiveRecord
				.getFolders(idProposal, "home");
		List<PhotoFolder> listFolders = new ArrayList<PhotoFolder>();
		for (PhotoFolderActiveRecord f : folder) {
			List<PhotoActiveRecord> photos = f.photos();
			List<Photo> listPhoto = new ArrayList<Photo>();
			for (PhotoActiveRecord p : photos) {
				Photo photo = new Photo.Builder().id(p.id).info(p.info)
						.name(p.name).pathLocal(p.path).uniqueId(p.uniqueId)
						.url(p.url).build();
				listPhoto.add(photo);
			}
			PhotoFolder fd = new PhotoFolder.Builder().id(f.idFolder)
					.name(f.name).photo(listPhoto).build();
			listFolders.add(fd);
		}
		PhotoHome ph = new PhotoHome.Builder().folders(listFolders).build();
		return ph;
	}

	public PhotoOther getPhotoOther(int idProposal) {
		List<PhotoFolderActiveRecord> folder = PhotoFolderActiveRecord
				.getFolders(idProposal, "other");
		List<PhotoFolder> listFolders = new ArrayList<PhotoFolder>();
		for (PhotoFolderActiveRecord f : folder) {
			List<PhotoActiveRecord> photos = f.photos();
			List<Photo> listPhoto = new ArrayList<Photo>();
			for (PhotoActiveRecord p : photos) {
				Photo photo = new Photo.Builder().id(p.id).info(p.info)
						.name(p.name).pathLocal(p.path).uniqueId(p.uniqueId)
						.url(p.url).build();
				listPhoto.add(photo);
			}
			PhotoFolder fd = new PhotoFolder.Builder().id(f.idFolder)
					.name(f.name).photo(listPhoto).build();
			listFolders.add(fd);
		}
		PhotoOther po = new PhotoOther.Builder().folders(listFolders).build();
		return po;
	}

	public boolean isProposalExist(int idProposal) {
		return ProposalActiveRecord.isProposalAvailable(idProposal);
	}

	public void saveProposal(int proposalId) {
		System.out.println("========= save proposal " + proposalId);
		ProposalActiveRecord proposalActiveRecord = new ProposalActiveRecord();
		proposalActiveRecord.id = proposalId;
		proposalActiveRecord.save();
	}

	public void savePersonal(Personal personal, int proposalId) {
		PersonalActiveRecord pr = PersonalActiveRecord
				.getCurrentPersoanl(personal.getId());
		if (pr == null) {
			pr = new PersonalActiveRecord();
		}
		pr.addressByCurrent = personal.getAddressByCurrent();
		pr.addressByKtp = personal.getAddressByKtp();
		pr.age = personal.getAge();
		pr.aliasName = personal.getAliasName();
		pr.birthDate = personal.getBirthDate();
		pr.birthPlace = personal.getBirthPlace();
		pr.gender = personal.getGender();
		pr.handphoneNumber = personal.getHandphoneNumber();
		pr.homeStatus = personal.getHomeStatus();
		pr.id = personal.getId();
		pr.ktpNumber = personal.getKtpNumber();
		pr.maritalStatus = personal.getMaritalStatus();
		pr.motherMaidenName = personal.getMotherMaidenName();
		pr.name = personal.getName();
		pr.numberOfDependents = personal.getNumberOfDependents();
		pr.phoneNumber = personal.getPhoneNumber();
		pr.spouseJob = personal.getSpouseJob();
		pr.spouseName = personal.getSpouseName();
		pr.proposal = proposalId;
		pr.action = QueryAction.UPDATE;
		pr.save();

		List<Children> children = personal.getChildren();

		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < children.size(); i++) {
				ChildActiveRecord ch;
				ch = ChildActiveRecord.getCurrentChild(children.get(i).getId());
				if (ch == null) {
					ch = new ChildActiveRecord();
				}
				ch.personal = personal.getId();
				ch.age = children.get(i).getAge();
				ch.name = children.get(i).getName();
				if (children.get(i).getId() == -1) {
					ch.action = QueryAction.INSERT;
				} else {
					ch.id = children.get(i).getId();
					ch.action = QueryAction.UPDATE;
				}
				ch.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}

	}

	public void saveAssets(List<Assets> listAsset, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listAsset.size(); i++) {
				AssetActiveRecord ast;
				ast = AssetActiveRecord.getCurrentAssets(listAsset.get(i)
						.getId());
				if (ast == null) {
					ast = new AssetActiveRecord();
				}
				ast.name = listAsset.get(i).getName();
				ast.value = listAsset.get(i).getValue();
				ast.proposal = proposalId;
				if (listAsset.get(i).getId() == -1) {
					ast.action = QueryAction.INSERT;
				} else {
					ast.id = listAsset.get(i).getId();
					ast.action = QueryAction.UPDATE;
				}
				ast.save();
			}
			ActiveAndroid.setTransactionSuccessful();

		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveJob(Job job, int proposalId) {
		JobActiveRecord j;
		if (job.getName().isEmpty()) {
			return;
		}
		j = JobActiveRecord.getCurrentJob(job.getId());
		if (j == null) {
			j = new JobActiveRecord();
			j.action = QueryAction.INSERT;
		} else {
			j.action = QueryAction.UPDATE;
		}
		j.name = job.getName();
		j.achievement = job.getAchievement();
		j.id = job.getId();
		j.jobField = job.getJobField();
		j.officeAddr = job.getOfficeAddr();
		j.officeName = job.getOfficeName();
		j.positionByCurrent = job.getPositionByCurrent();
		j.positionByInitial = job.getPositionByInitial();
		j.salary = job.getSalary();
		j.statusOfWorker = job.getStatusOfWorker();
		j.yearOfJoined = job.getYearOfJoined();
		j.proposal = proposalId;
		j.save();
	}

	public void saveBusiness(Business business, int proposalId) {
		BusinessActiveRecord bis = BusinessActiveRecord
				.getCurrentBusiness(business.getId());
		if (bis == null)
			bis = new BusinessActiveRecord();
		bis.additionalInfo = business.getAdditionalInfo();
		bis.address = business.getAddress();
		bis.companyLegallity = business.getCompanyLegallity();
		bis.field = business.getField();
		bis.foundedYear = business.getFoundedYear();
		bis.id = business.getId();
		if (business.getId() < 0) {
			bis.action = QueryAction.INSERT;
		} else {
			bis.action = QueryAction.UPDATE;
		}

		bis.name = business.getName();
		bis.numberOfEmployees = business.getNumberOfEmployees();
		bis.officeOwnership = business.getOfficeOwnership();
		bis.projectExperiences = business.getProjectExperiences();
		bis.siupNumber = business.getSiupNumber();
		bis.tdpNumber = business.getTdpNumber();
		bis.yearsOfOperation = business.getYearsOfOperation();
		bis.proposal = proposalId;
		bis.save();
	}

	public void saveSubmission(SubmissionCredit submission, int proposalId) {
		SubmissionActiveRecord subm;
		subm = SubmissionActiveRecord.getSubmission(proposalId);
		if (subm == null)
			subm = new SubmissionActiveRecord();

		subm.id = submission.getId();
		subm.plafon = submission.getPlafon();
		subm.product = submission.getCreditProduct();
		subm.interest = submission.getInterest();
		subm.period = submission.getPeriode();
		subm.proposal = proposalId;
		subm.action = QueryAction.UPDATE;
		subm.save();
	}

	public void savePurposes(List<Purposes> listPurpose, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listPurpose.size(); i++) {
				System.out.println("id... :" + listPurpose.get(i).getId());
				PurposeActiveRecord pr;
				pr = PurposeActiveRecord.getCurrentPurposes(listPurpose.get(i)
						.getId());
				if (pr == null)
					pr = new PurposeActiveRecord();
				if (listPurpose.get(i).getId() <= 0) {
					pr.action = QueryAction.INSERT;
				} else {
					pr.action = QueryAction.UPDATE;
				}
				pr.id = listPurpose.get(i).getId();
				pr.general = listPurpose.get(i).getGeneral();
				pr.detail = listPurpose.get(i).getDetail();
				pr.proposal = proposalId;
				pr.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveCapacity(List<CapacityIncome> incomeCollection,
			List<CapacityExpenditure> expenditureCollection, int idCapacity,
			int proposalId) {
		ActiveAndroid.beginTransaction();

		try {
			for (int i = 0; i < incomeCollection.size(); i++) {
				IncomeActiveRecord inc;
				inc = IncomeActiveRecord.getCurrentIncomes(incomeCollection
						.get(i).getId());
				if (inc == null)
					inc = new IncomeActiveRecord();
				inc.name = incomeCollection.get(i).getName();
				inc.value = incomeCollection.get(i).getValue();
				inc.idCapacity = idCapacity;
				inc.proposal = proposalId;
				inc.id = incomeCollection.get(i).getId();
				if (incomeCollection.get(i).getId() <= 0) {
					inc.action = QueryAction.INSERT;
				} else {
					inc.action = QueryAction.UPDATE;
				}
				inc.save();
			}

			for (int i = 0; i < expenditureCollection.size(); i++) {
				ExpenditureActiveRecord exp;
				exp = ExpenditureActiveRecord
						.getCurrentExpenditures(expenditureCollection.get(i)
								.getId());
				if (exp == null)
					exp = new ExpenditureActiveRecord();
				exp.name = expenditureCollection.get(i).getName();
				exp.value = expenditureCollection.get(i).getValue();
				exp.idCapacity = idCapacity;
				exp.proposal = proposalId;
				exp.id = expenditureCollection.get(i).getId();
				if (expenditureCollection.get(i).getId() <= 0) {
					exp.action = QueryAction.INSERT;
				} else {
					exp.action = QueryAction.UPDATE;
				}
				exp.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveCollateralShm(List<CollateralShm> listShm, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listShm.size(); i++) {
				ShmActiveRecord shm;
				shm = ShmActiveRecord.getCurrentShm(listShm.get(i).getId());

				if (shm == null)
					shm = new ShmActiveRecord();

				shm.binding = listShm.get(i).getBinding().replace(" ", "_")
						.toUpperCase();
				shm.certificateName = listShm.get(i).getCertificateName();
				shm.certificateNumber = listShm.get(i).getCertificateNumber();
				shm.id = listShm.get(i).getId();

				if (listShm.get(i).getId() <= 0) {
					shm.action = QueryAction.INSERT;
				} else {
					shm.action = QueryAction.UPDATE;
				}

				shm.info = listShm.get(i).getInfo();
				shm.name = listShm.get(i).getCertificateName();
				shm.priceAssessed = listShm.get(i).getPriceOnAssessed()
						.isEmpty() ? "0" : listShm.get(i).getPriceOnAssessed();
				shm.priceMarket = listShm.get(i).getPriceOnMarket().isEmpty() ? "0"
						: listShm.get(i).getPriceOnMarket();
				shm.size = listShm.get(i).getLandOfArea();
				shm.status = listShm.get(i).getStatusOfLand();
				shm.type = listShm.get(i).getType();
				shm.proposal = proposalId;
				shm.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveCollateralBpkb(List<CollateralBpkb> listBpkb, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {

			for (int i = 0; i < listBpkb.size(); i++) {
				BpkbActiveRecord bpkb;
				bpkb = BpkbActiveRecord.getCurrentBpkb(listBpkb.get(i).getId());
				if (bpkb == null) {
					System.out.println("insert new");
					bpkb = new BpkbActiveRecord();
				}
				System.out.println("update");
				bpkb.binding = listBpkb.get(i).getBinding().replace(" ", "_")
						.toUpperCase();
				bpkb.bpkbName = listBpkb.get(i).getNameOfBpkb();
				bpkb.bpkbNumber = listBpkb.get(i).getNumberBpkb();
				bpkb.id = listBpkb.get(i).getId();

				if (listBpkb.get(i).getId() <= 0) {
					bpkb.action = QueryAction.INSERT;
				} else {
					bpkb.action = QueryAction.UPDATE;
				}
				bpkb.info = listBpkb.get(i).getInfo();
				bpkb.name = listBpkb.get(i).getNameOfBpkb();
				bpkb.policeNumber = listBpkb.get(i).getPoliceNumber();
				bpkb.priceAssessed = listBpkb.get(i).getPriceOnAssessed()
						.isEmpty() ? "0" : listBpkb.get(i).getPriceOnAssessed();
				bpkb.priceMarket = listBpkb.get(i).getPriceOnMarket().isEmpty() ? "0"
						: listBpkb.get(i).getPriceOnMarket();
				bpkb.stnkNumber = listBpkb.get(i).getNumberStnk();
				bpkb.vehicleBrand = listBpkb.get(i).getCarBrand();
				bpkb.vehicleName = listBpkb.get(i).getCarName();
				bpkb.vehicleType = listBpkb.get(i).getCarType();
				bpkb.vehicleYear = listBpkb.get(i).getCarYear();
				bpkb.proposal = proposalId;
				bpkb.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveEnvCheckup(List<EnvironmentalCheck> listCheck,
			int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listCheck.size(); i++) {
				EnvironmentActiveRecord env;
				env = EnvironmentActiveRecord.getCurrentEnv(listCheck.get(i)
						.getId());
				if (env == null)
					env = new EnvironmentActiveRecord();

				env.impression = listCheck.get(i).getImpression();
				env.name = listCheck.get(i).getName();
				env.realtion = listCheck.get(i).getRelation();
				env.proposal = proposalId;
				env.id = listCheck.get(i).getId();
				if (listCheck.get(i).getId() <= 0) {
					env.action = QueryAction.INSERT;
				} else {
					env.action = QueryAction.UPDATE;
				}
				env.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveTabungan(List<CollateralTabungan> listTabungan,
			int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (CollateralTabungan t : listTabungan) {
				TabunganActiveRecord tabunganRecord;
				tabunganRecord = TabunganActiveRecord.getCurrentTabungan(t
						.getId());
				if (tabunganRecord == null)
					tabunganRecord = new TabunganActiveRecord();

				tabunganRecord.accountNumber = t.getAccountNumber();
				tabunganRecord.amount = t.getAmount();
				tabunganRecord.bookNumber = t.getBookNumber();
				tabunganRecord.id = t.getId();
				tabunganRecord.name = t.getName();
				tabunganRecord.proposal = proposalId;
				tabunganRecord.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveDeposito(List<CollateralDeposito> listDeposito,
			int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (CollateralDeposito t : listDeposito) {
				DepositoActiveRecord depositoRecord;
				depositoRecord = DepositoActiveRecord.getCurrentDeposito(t
						.getId());
				if (depositoRecord == null)
					depositoRecord = new DepositoActiveRecord();

				depositoRecord.accountNumber = t.getAccountNumber();
				depositoRecord.amount = t.getAmount();
				depositoRecord.bilyetNumber = t.getBilyetNumber();
				depositoRecord.id = t.getId();
				depositoRecord.name = t.getName();
				depositoRecord.proposal = proposalId;
				depositoRecord.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveBusinessCollection(List<Business> listBusiness,
			int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (Business business : listBusiness) {
				BusinessActiveRecord bis = BusinessActiveRecord
						.getCurrentBusiness(business.getId());
				if (bis == null)
					bis = new BusinessActiveRecord();
				bis.additionalInfo = business.getAdditionalInfo();
				bis.address = business.getAddress();
				bis.companyLegallity = business.getCompanyLegallity();
				bis.field = business.getField();
				bis.foundedYear = business.getFoundedYear();
				if (business.getId() <= 0) {
					bis.action = QueryAction.INSERT;
				} else {
					bis.id = business.getId();
					bis.action = QueryAction.UPDATE;
				}

				bis.name = business.getName();
				bis.numberOfEmployees = business.getNumberOfEmployees();
				bis.officeOwnership = business.getOfficeOwnership();
				bis.projectExperiences = business.getProjectExperiences();
				bis.siupNumber = business.getSiupNumber();
				bis.tdpNumber = business.getTdpNumber();
				bis.yearsOfOperation = business.getYearsOfOperation();
				bis.proposal = proposalId;
				bis.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void savePhotoBusiness(List<PhotoFolder> listFolder, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listFolder.size(); i++) {
				boolean isFolderExist = PhotoFolderActiveRecord
						.isFolderExist(listFolder.get(i).getId());
				PhotoFolderActiveRecord folder;
				if (!isFolderExist) {
					folder = new PhotoFolderActiveRecord();
				} else {
					folder = PhotoFolderActiveRecord
							.getCurrentFolder(listFolder.get(i).getId());
				}
				folder.galleryName = "business";
				folder.idFolder = listFolder.get(i).getId();
				folder.idProposal = proposalId;
				folder.name = listFolder.get(i).getName();
				folder.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}

		ActiveAndroid.beginTransaction();
		try {
			for (PhotoFolder f : listFolder) {
				List<Photo> listPhoto = (List<Photo>) f.getPhotoGalleries();
				for (Photo p : listPhoto) {
					System.out.println("inserting..");
					PhotoCandidate photoCandidate = new PhotoCandidate.Builder()
							.folderId(f.getId()).folderName(f.getName())
							.galleryName("business").photo(p)
							.proposalId(proposalId).build();
					PhotoActiveRecord photo;
					photo = PhotoActiveRecord.getCurrentPhoto(photoCandidate
							.getPhoto().getId());
					if (photo == null)
						photo = new PhotoActiveRecord();

					photo.action = QueryAction.INSERT;
					photo.idProposal = photoCandidate.getProposalId();
					photo.idFolder = photoCandidate.getFolderId();
					photo.info = photoCandidate.getPhoto().getInfo();
					photo.name = photoCandidate.getPhoto().getName();
					photo.id = (photoCandidate.getPhoto().getId() > 0) ? photoCandidate
							.getPhoto().getId() : RandomInteger
							.randInt(1, 1000);
					photo.path = photoCandidate.getPhoto().getPath();
					photo.uniqueId = photoCandidate.getPhoto().getUniqueId();
					photo.url = photoCandidate.getPhoto().getUrl();
					if (photoCandidate.getPhoto().getUrl() == null) {
						photo.uploaded = false;
					} else {
						photo.uploaded = photoCandidate.getPhoto().getUrl()
								.isEmpty() ? false : true;
						photo.synchornized = photoCandidate.getPhoto().getUrl()
								.isEmpty() ? false : true;
					}
					photo.save();
				}
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}

	}

	public void savePhotoHome(List<PhotoFolder> listFolder, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listFolder.size(); i++) {
				boolean isFolderExist = PhotoFolderActiveRecord
						.isFolderExist(listFolder.get(i).getId());
				PhotoFolderActiveRecord folder;
				if (!isFolderExist) {
					folder = new PhotoFolderActiveRecord();
				} else {
					folder = PhotoFolderActiveRecord
							.getCurrentFolder(listFolder.get(i).getId());
				}
				folder.galleryName = "home";
				folder.idFolder = listFolder.get(i).getId();
				folder.idProposal = proposalId;
				folder.name = listFolder.get(i).getName();
				folder.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}

		ActiveAndroid.beginTransaction();
		try {
			for (PhotoFolder f : listFolder) {
				List<Photo> listPhoto = (List<Photo>) f.getPhotoGalleries();
				for (Photo p : listPhoto) {
					System.out.println("inserting..");
					PhotoCandidate photoCandidate = new PhotoCandidate.Builder()
							.folderId(f.getId()).folderName(f.getName())
							.galleryName("home").photo(p)
							.proposalId(proposalId).build();
					PhotoActiveRecord photo;
					photo = PhotoActiveRecord.getCurrentPhoto(photoCandidate
							.getPhoto().getId());
					if (photo == null)
						photo = new PhotoActiveRecord();

					photo.action = QueryAction.INSERT;
					photo.idProposal = photoCandidate.getProposalId();
					photo.idFolder = photoCandidate.getFolderId();
					photo.info = photoCandidate.getPhoto().getInfo();
					photo.name = photoCandidate.getPhoto().getName();
					photo.id = (photoCandidate.getPhoto().getId() > 0) ? photoCandidate
							.getPhoto().getId() : RandomInteger
							.randInt(1, 1000);
					photo.path = photoCandidate.getPhoto().getPath();
					photo.uniqueId = photoCandidate.getPhoto().getUniqueId();
					photo.url = photoCandidate.getPhoto().getUrl();
					if (photoCandidate.getPhoto().getUrl() == null) {
						photo.uploaded = false;
					} else {
						photo.uploaded = photoCandidate.getPhoto().getUrl()
								.isEmpty() ? false : true;
						photo.synchornized = photoCandidate.getPhoto().getUrl()
								.isEmpty() ? false : true;
					}
					photo.save();
				}
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void savePhotoCollateral(List<PhotoFolder> listFolder, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listFolder.size(); i++) {
				boolean isFolderExist = PhotoFolderActiveRecord
						.isFolderExist(listFolder.get(i).getId());
				PhotoFolderActiveRecord folder;
				if (!isFolderExist) {
					folder = new PhotoFolderActiveRecord();
				} else {
					folder = PhotoFolderActiveRecord
							.getCurrentFolder(listFolder.get(i).getId());
				}
				folder.galleryName = "collateral";
				folder.idFolder = listFolder.get(i).getId();
				folder.idProposal = proposalId;
				folder.name = listFolder.get(i).getName();
				folder.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}

		ActiveAndroid.beginTransaction();
		try {
			for (PhotoFolder f : listFolder) {
				List<Photo> listPhoto = (List<Photo>) f.getPhotoGalleries();
				for (Photo p : listPhoto) {
					System.out.println("inserting..");
					PhotoCandidate photoCandidate = new PhotoCandidate.Builder()
							.folderId(f.getId()).folderName(f.getName())
							.galleryName("collateral").photo(p)
							.proposalId(proposalId).build();
					System.out.println("inserting.. "+photoCandidate.getPhoto().toString());
					PhotoActiveRecord photo;
					photo = PhotoActiveRecord.getCurrentPhoto(photoCandidate
							.getPhoto().getId());
					if (photo == null)
						photo = new PhotoActiveRecord();

					photo.action = QueryAction.INSERT;
					photo.idProposal = photoCandidate.getProposalId();
					photo.idFolder = photoCandidate.getFolderId();
					photo.info = photoCandidate.getPhoto().getInfo();
					photo.name = photoCandidate.getPhoto().getName();
					photo.id = photoCandidate.getPhoto().getId();
					// photo.id = (photoCandidate.getPhoto().getId() > 0) ?
					// photoCandidate
					// .getPhoto().getId() : RandomInteger
					// .randInt(1, 1000);
					photo.path = photoCandidate.getPhoto().getPath();
					photo.uniqueId = photoCandidate.getPhoto().getUniqueId();
					photo.url = photoCandidate.getPhoto().getUrl();
					// null handle
					if (photoCandidate.getPhoto().getUrl() == null) {
						photo.uploaded = false;
					} else {
						photo.uploaded = photoCandidate.getPhoto().getUrl()
								.isEmpty() ? false : true;
						photo.synchornized = photoCandidate.getPhoto().getUrl()
								.isEmpty() ? false : true;
					}
					photo.save();
				}
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void savePhotoOther(List<PhotoFolder> listFolder, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listFolder.size(); i++) {
				boolean isFolderExist = PhotoFolderActiveRecord
						.isFolderExist(listFolder.get(i).getId());
				PhotoFolderActiveRecord folder;
				if (!isFolderExist) {
					folder = new PhotoFolderActiveRecord();
				} else {
					folder = PhotoFolderActiveRecord
							.getCurrentFolder(listFolder.get(i).getId());
				}
				folder.galleryName = "other";
				folder.idFolder = listFolder.get(i).getId();
				folder.idProposal = proposalId;
				folder.name = listFolder.get(i).getName();
				folder.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}

		ActiveAndroid.beginTransaction();
		try {
			for (PhotoFolder f : listFolder) {
				List<Photo> listPhoto = (List<Photo>) f.getPhotoGalleries();
				for (Photo p : listPhoto) {
					System.out.println("inserting..");
					PhotoCandidate photoCandidate = new PhotoCandidate.Builder()
							.folderId(f.getId()).folderName(f.getName())
							.galleryName("other").photo(p)
							.proposalId(proposalId).build();
					PhotoActiveRecord photo;
					photo = PhotoActiveRecord.getCurrentPhoto(photoCandidate
							.getPhoto().getId());
					if (photo == null)
						photo = new PhotoActiveRecord();

					photo.action = QueryAction.INSERT;
					photo.idProposal = photoCandidate.getProposalId();
					photo.idFolder = photoCandidate.getFolderId();
					photo.info = photoCandidate.getPhoto().getInfo();
					photo.name = photoCandidate.getPhoto().getName();
					photo.id = (photoCandidate.getPhoto().getId() > 0) ? photoCandidate
							.getPhoto().getId() : RandomInteger
							.randInt(1, 1000);
					photo.path = photoCandidate.getPhoto().getPath();
					photo.uniqueId = photoCandidate.getPhoto().getUniqueId();
					photo.url = photoCandidate.getPhoto().getUrl();
					if (photoCandidate.getPhoto().getUrl() == null) {
						photo.uploaded = false;
					} else {
						photo.uploaded = photoCandidate.getPhoto().getUrl()
								.isEmpty() ? false : true;
						photo.synchornized = photoCandidate.getPhoto().getUrl()
								.isEmpty() ? false : true;
					}
					photo.save();
				}
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public List<PhotoFolder> getPhotoFolder(int idProposal, String galleryName) {

		List<PhotoFolderActiveRecord> folders = PhotoFolderActiveRecord
				.getFolders(idProposal, galleryName);
		List<PhotoFolder> listFolder = new ArrayList<PhotoFolder>();
		if (folders.isEmpty()) {
			return listFolder;
		}
		for (PhotoFolderActiveRecord f : folders) {

			List<PhotoActiveRecord> photos = PhotoActiveRecord
					.getPhotos(f.idFolder);
			List<Photo> listPhoto = new ArrayList<Photo>();
			if (!photos.isEmpty()) {
				for (PhotoActiveRecord p : photos) {
					Photo photo = new Photo.Builder().id(p.id).info(p.info)
							.name(p.name).pathLocal(p.path)
							.uniqueId(p.uniqueId).url(p.url).build();
					System.out.println("photo :" + photo.toString());
					listPhoto.add(photo);
				}
			}

			PhotoFolder folder = new PhotoFolder.Builder().id(f.idFolder)
					.name(f.name).photo(listPhoto).build();
			listFolder.add(folder);
		}
		System.out.println("folder " + galleryName + " loaded::"
				+ folders.toString());
		System.out.println("folder loaded::" + listFolder.toString());
		return listFolder;
	}

	public List<Photo> getGalleryPending() {
		List<PhotoActiveRecord> photos = PhotoActiveRecord.getPendingPhotos();
		List<Photo> listPhoto = new ArrayList<Photo>();
		if (photos.isEmpty()) {
			return listPhoto;
		}
		for (PhotoActiveRecord p : photos) {
			Photo photo = new Photo.Builder().id(p.id).info(p.info)
					.name(p.name).pathLocal(p.path).uniqueId(p.uniqueId)
					.url(p.url).build();
			System.out.println("photo :" + photo.toString());
			listPhoto.add(photo);
		}
		return listPhoto;
	}

	public void setProposalUploadSucceeded(int idProposal) {
		ProposalActiveRecord proposal = ProposalActiveRecord
				.getProposalCandidate(idProposal);
		proposal.sync = true;
		proposal.save();
	}

	public boolean isProposalUploaded(int idProposal) {
		return ProposalActiveRecord.isProposalUploaded(idProposal);
	}

	public boolean isPhotoHasLocalPath(Photo photo) {
		return PhotoActiveRecord.photoHasLocalPath(photo);
	}

	public void deletePhoto(Photo photo) {
		System.out.println("deleting..");
		PhotoActiveRecord.deleteById(photo.getId());
	}

}

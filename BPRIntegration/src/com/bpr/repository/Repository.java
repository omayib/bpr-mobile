package com.bpr.repository;

import java.util.List;

import com.customer.model.Assets;
import com.customer.model.AssetsCollection;
import com.customer.model.Business;
import com.customer.model.BusinessCollection;
import com.customer.model.CapacityCollection;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralBpkbCollection;
import com.customer.model.CollateralDeposito;
import com.customer.model.CollateralDepositoCollection;
import com.customer.model.CollateralShm;
import com.customer.model.CollateralShmCollection;
import com.customer.model.CollateralTabungan;
import com.customer.model.CollateralTabunganCollection;
import com.customer.model.EnvironmentalCheck;
import com.customer.model.EnvironmentalCheckCollection;
import com.customer.model.Job;
import com.customer.model.Personal;
import com.customer.model.Photo;
import com.customer.model.PhotoBusiness;
import com.customer.model.PhotoCollateral;
import com.customer.model.PhotoFolder;
import com.customer.model.PhotoHome;
import com.customer.model.PhotoOther;
import com.customer.model.Proposal;
import com.customer.model.Purposes;
import com.customer.model.PurposesCollection;
import com.customer.model.SubmissionCredit;

public interface Repository {

	boolean isProposalExist(int idProposal);

	boolean isProposalUploaded(int idProposal);

	void setProposalUploadSucceeded(int idProposal);

	/**
	 * get data from local storage
	 * */
	Proposal loadProposal(int idProposal);

	Personal getPersonal(int idProposal);

	AssetsCollection getAssets(int idProposal);

	Job getJob(int idProposal);

	BusinessCollection getBusiness(int idProposal);

	SubmissionCredit getSubmission(int idProposal);

	PurposesCollection getPurposes(int idProposal);

	CapacityCollection getCapacity(int idProposal);

	CollateralBpkbCollection getCollateralBpkb(int idProposal);

	CollateralShmCollection getCollateralShm(int idProposal);

	CollateralTabunganCollection getCollateralTabungan(int idProposal);

	CollateralDepositoCollection getCollateralDeposito(int idProposal);

	EnvironmentalCheckCollection getEnvironmentalCheckup(int idProposal);

	List<PhotoFolder> getPhotoFolder(int idProposal, String galleryName);

	PhotoBusiness getPhotoBusiness(int idProposal);

	PhotoCollateral getPhotoCollateral(int idProposal);

	PhotoHome getPhotoHome(int idProposal);

	PhotoOther getPhotoOther(int idProposal);

	List<Photo> getGalleryPending();

	/**
	 * save data to local storage
	 * */

	void saveProposal(int proposalId);

	void savePersonal(Personal personal, int proposalId);

	void saveAssets(List<Assets> listAssets, int proposalId);

	void saveJob(Job job, int proposalId);

	void saveBusiness(Business business, int proposalId);

	void saveBusinessCollection(List<Business> business, int proposalId);

	void saveSubmission(SubmissionCredit subm, int proposalId);

	void savePurposes(List<Purposes> listPurpose, int proposalId);

	void saveCapacity(List<CapacityIncome> income,
			List<CapacityExpenditure> expenditure, int idCapacity,
			int proposalId);

	void saveCollateralShm(List<CollateralShm> shm, int proposalId);

	void saveCollateralBpkb(List<CollateralBpkb> bpkb, int proposalId);

	void saveEnvCheckup(List<EnvironmentalCheck> listCheck, int proposalId);

	void saveTabungan(List<CollateralTabungan> listTabungan, int proposalId);

	void saveDeposito(List<CollateralDeposito> listDeposito, int proposalId);

	void savePhotoBusiness(List<PhotoFolder> listFolder, int proposalId);

	void savePhotoHome(List<PhotoFolder> listFolder, int proposalId);

	void savePhotoCollateral(List<PhotoFolder> listFolder, int proposalId);

	void savePhotoOther(List<PhotoFolder> listFolder, int proposalId);

	boolean isPhotoHasLocalPath(Photo photo);

	void deletePhoto(Photo photo);

}

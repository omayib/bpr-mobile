package com.bpr.integration.utils;

import com.amazonaws.regions.Regions;

public class AwsUtils {
	public final static String AWS_ACCOUNT_ID = "826848193288";
	public final static String COGNITO_IDENTITY_POOL = "us-east-1:c7dc28ff-fb04-4b95-a008-15ae09d2269a";
	public final static String AUTHENTICATED_ROLE = "arn:aws:iam::826848193288:role/Cognito_BPRAndroidAuth_DefaultRole";
	public final static String UNAUTHENTICATED_ROLE = "arn:aws:iam::826848193288:role/android-dev";
	public final static Regions REGIONS = Regions.US_EAST_1;
	public final static String BUCKET_NAME = "1618-sandbox";

	public final static String URL_GENERATOR(String butcketName, String key) {
		return "https://s3-ap-southeast-1.amazonaws.com/" + butcketName + "/"
				+ key;
	}

}

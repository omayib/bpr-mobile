package com.bpr.integration.utils;

public class UrlApi {
	private static String production = "https://bprebs-engine.herokuapp.com/";
	private static String staging = "https://bprebs-staging.herokuapp.com/";
	private static String baseUrl = production;

	public static String SIGNIN = baseUrl + "sign_in";

	public static String SIGNOUT(String token) {
		String url = baseUrl + "signout?token=" + token;
		return url;
	}

	public static String REFRESH_NEWBOX(String token) {
		return baseUrl + "customer_data?status=new&token=" + token;
	}

	public static String REFRESH_PROPOSALBOX(String token) {
		return baseUrl + "customer_data?status=proposal&token=" + token;
	}

	public static String REFRESH_FOLLOWUPBOX(String token) {
		return baseUrl + "customer_data?status=followup&token=" + token;
	}

	public static String REFRESH_ALLBOX(String token) {
		return baseUrl + "customer_data?status=all&token=" + token;
	}

	public static String REFRESH_ACKBOX(String token) {
		return baseUrl + "customer_data?status=acknowledgement&token=" + token;
	}

	public static String ACKBOX_GET_SUMMARY(String token, int idcustomer) {
		return baseUrl + "customer_data/" + idcustomer + "/summary?token="
				+ token;
	}
	public static String GET_SUMMARY(String token, int idProposal) {
		return baseUrl + "proposals/" + idProposal + "/summary?token="
				+ token;
	}

	public static String ACKBOX_ACKNOWLADGEMENT(String token, int idcustomer) {
		return baseUrl + "customer_data/" + idcustomer + "/acknowledge?token="
				+ token;
	}

	public static String APPROVE_FOLLOWUP_BOX(String token, int idcustomer) {
		return baseUrl + "customer_data/" + idcustomer + "/approve?token="
				+ token;
	}

	public static String REJECT_PROPOSAL(String token, int idcustomer) {
		return baseUrl + "customer_data/" + idcustomer + "/reject?token="
				+ token;
	}

	public static String LIST_OF_PROPOSAL(String token, int idcustomer) {
		return baseUrl + "customer_data/" + idcustomer + "?token=" + token;
	}

	public static String DETAIL_OF_PROPOSAL(String token, int idproposal) {
		return baseUrl + "proposals/" + idproposal + "?token=" + token;
	}

	public static String UPLOAD_PROPOSAL(int idproposal) {
		return baseUrl + "proposals/" + idproposal + "/complete";
	}

	public static String UPLOAD_SUMMARY(int idcustomer) {
		return baseUrl + "customer_data/" + idcustomer + "/complete";
	}

	public static String LIST_AO(String token,int idCustomer) {
		return baseUrl + "users/?ao=true&token=" + token+"&customer_data_id="+idCustomer;
	}

	public static String LIST_SID(String token, int idCustomer) {
		return baseUrl + "customers/" + idCustomer + "/sids?token=" + token;
	}

	public static String ASSIGN(String token, int idCustomer) {
		return baseUrl + "customer_data/" + idCustomer + "/assign";
	}

	public static String REASSIGN(String token, int idCustomer) {
		return baseUrl + "customer_data/" + idCustomer + "/reassign";
	}

	/**
	 * @param token
	 * @param idProposal
	 * @param gallery
	 *            home / collateral / business / other
	 * */
	public static String CREATE_FOLDER_GALLERY(int idProporsal, String gallery) {
		return baseUrl + "proposals/" + idProporsal + "/survey_galleries/"
				+ gallery;
	}

	public static String UPLOAD_PHOTO(int categoryId) {
		return baseUrl + "survey_galleries/" + categoryId + "/gallery_photos";
	}
	public static String DELETE_PHOTO(int photoId,String token) {
		return baseUrl + "gallery_photos/" + photoId+"?token="+token;
	}
}

package com.bpr.integration.client;

import java.io.IOException;

import android.os.AsyncTask;

public class AssigntoAsync extends AsyncTask<String, Integer, String> {

	@Override
	protected String doInBackground(String... params) {
		System.out.println("url:" + params[0]);
		System.out.println("data:" + params[1]);
		String data = "";
		try {
			data = ClientToServer.putRequest(params[0], params[1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

}

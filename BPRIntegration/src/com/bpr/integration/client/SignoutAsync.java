package com.bpr.integration.client;

import java.util.ArrayList;

import org.apache.http.NameValuePair;

import android.os.AsyncTask;

public class SignoutAsync extends AsyncTask<String, Integer, String> {

	protected String doInBackground(String... params) {
		String url = params[0];
		String data = "";
		ArrayList<NameValuePair> paramsPair = new ArrayList<NameValuePair>();
		try {
			data = ClientToServer.postRequest(url, paramsPair);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}

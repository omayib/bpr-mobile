package com.bpr.integration.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.params.ConnManagerParams;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.bpr.integration.client.CustomMultiPartEntity.ProgressListener;

public class ClientToServer {
	public static final int HTTP_TIMEOUT = 30 * 1000;
	private static HttpClient client;
	static FileInputStream fileInputStream = null;
	static String sResponse;

	private static HttpClient getHttpClient() {
		if (client == null) {
			client = new DefaultHttpClient();
			ClientConnectionManager mgr = client.getConnectionManager();
			HttpParams params = client.getParams();
			client = new DefaultHttpClient(new ThreadSafeClientConnManager(
					params, mgr.getSchemeRegistry()), params);
			final HttpParams parameterHttp = client.getParams();

			HttpConnectionParams.setConnectionTimeout(parameterHttp,
					HTTP_TIMEOUT);
			ConnManagerParams.setTimeout(parameterHttp, HTTP_TIMEOUT);
		}
		return client;

	}

	public static String putRequest(String url, String data) throws IOException {
		System.out.println("url :" + url);
		String response = "";
		BufferedReader in = null;
		try {
			HttpClient client = getHttpClient();
			HttpPut req = new HttpPut(url);
			req.setHeader("Content-Type", "application/json");
			HttpEntity entity = new StringEntity(data);
			req.setEntity(entity);
			HttpResponse respon = client.execute(req);
			in = new BufferedReader(new InputStreamReader(respon.getEntity()
					.getContent()));
			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			String hasil = sb.toString();
			return hasil;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (in != null) {
				in.close();
			}

		}
		return response;
	}
	public static String deleteRequest(String url){

		HttpClient client = getHttpClient();
		HttpDelete del=new HttpDelete(url);
		try {
			HttpResponse respon = client.execute(del);
			int status=respon.getStatusLine().getStatusCode();
			System.out.println("status code "+status);
			if(status==204){
				return "";
			}else{
				return "sorry something wrong";
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}
	public static String postRequest(String url,
			ArrayList<NameValuePair> postParameters) throws Exception {
		System.out.println("url :" + url);
		BufferedReader in = null;
		try {
			HttpClient client = getHttpClient();
			HttpPost req = new HttpPost(url);
			req.setHeader("User-Agent", "Android-Mobile");
			if (!postParameters.isEmpty()) {
				UrlEncodedFormEntity formEntity = new UrlEncodedFormEntity(
						postParameters);
				req.setEntity(formEntity);
			}
			HttpResponse respon = client.execute(req);
			in = new BufferedReader(new InputStreamReader(respon.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			String hasil = sb.toString();
			return hasil;
		} finally {
			if (in != null) {
				in.close();
			}

		}

	}

	public static String getRequest(String url) throws Exception {
		System.out.println("url :" + url);
		BufferedReader in = null;
		try {
			HttpClient hc = getHttpClient();
			HttpGet req = new HttpGet();
			req.setURI(new URI(url));
			HttpResponse resp = hc.execute(req);
			in = new BufferedReader(new InputStreamReader(resp.getEntity()
					.getContent()));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = in.readLine()) != null) {
				sb.append(line + NL);
			}
			in.close();
			String hasil = sb.toString();
			return hasil;
		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	static long totalSize;

	public interface ProgressUploader {
		void transfered(long transfersize, long totalsize);
	}

	public static String uploadRequest(String url,
			ArrayList<NameValuePair> postParameters,
			NameValuePair picParameters, final ProgressUploader progress) {
		try {

			HttpClient client = getHttpClient();
			HttpPost req = new HttpPost(url);
			HttpContext localContext = new BasicHttpContext();

			// MultipartEntity entity = new MultipartEntity(
			// HttpMultipartMode.BROWSER_COMPATIBLE);
			CustomMultiPartEntity multipartContent = new CustomMultiPartEntity(
					new ProgressListener() {

						public void transferred(long num) {
							// int sizeUploaded = (int) (num / totalSize * 100);
							progress.transfered(num, totalSize);
						}

					});
			// byte[] data = bos.toByteArray();

			for (NameValuePair value : postParameters) {
				multipartContent.addPart(value.getName(),
						new StringBody(value.getValue()));
			}

			if (picParameters != null) {
				String picParamName = picParameters.getName();
				String filepath = picParameters.getValue();
				File f = new File(filepath);
				FileBody fbin = new FileBody(f);

				multipartContent.addPart(picParamName, fbin);
				totalSize = fbin.getContentLength();
				System.out.println(totalSize);
			}

			// req.setEntity(entity);
			req.setEntity(multipartContent);
			HttpResponse response = client.execute(req, localContext);
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent(), "UTF-8"));

			StringBuffer sb = new StringBuffer("");
			String line = "";
			String NL = System.getProperty("line.separator");
			while ((line = reader.readLine()) != null) {
				sb.append(line + NL);
			}
			reader.close();

			sResponse = sb.toString();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalStateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return sResponse;

	}

	public static boolean isNetworkAvailable(Context con) {
		ConnectivityManager connectivityManager = (ConnectivityManager) con
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}

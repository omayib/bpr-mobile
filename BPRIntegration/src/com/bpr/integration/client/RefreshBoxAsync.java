package com.bpr.integration.client;

import android.os.AsyncTask;

public class RefreshBoxAsync extends AsyncTask<String, Integer, String> {

	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String url = params[0];
		String data = "";
		try {
			data = ClientToServer.getRequest(url);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}

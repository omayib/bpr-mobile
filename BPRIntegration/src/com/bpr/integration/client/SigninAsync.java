package com.bpr.integration.client;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;

public class SigninAsync extends AsyncTask<String, Integer, String> {

	protected String doInBackground(String... params) {
		// TODO Auto-generated method stub
		String url = params[0];
		String username = params[1];
		String password = params[2];
		String data = "";
		ArrayList<NameValuePair> paramsPair = new ArrayList<NameValuePair>();
		paramsPair.add(new BasicNameValuePair("username", username));
		paramsPair.add(new BasicNameValuePair("password", password));
		try {
			data = ClientToServer.postRequest(url, paramsPair);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}

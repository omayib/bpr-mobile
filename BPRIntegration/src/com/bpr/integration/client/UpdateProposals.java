package com.bpr.integration.client;

import java.io.IOException;

import android.os.AsyncTask;

public class UpdateProposals extends AsyncTask<String, Integer, String> {

	@Override
	protected String doInBackground(String... params) {
		String data = "";
		try {
			data = ClientToServer.putRequest(params[0], params[1]);
			System.out.println("data response:" + data);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

}

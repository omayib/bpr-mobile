package com.bpr.integration.client;

import android.os.AsyncTask;

public class GetSidAsync extends AsyncTask<String, Integer, String> {

	@Override
	protected String doInBackground(String... params) {
		System.out.println("url:" + params[0]);
		String data = "";
		try {
			data = ClientToServer.getRequest(params[0]);
			System.out.println(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}

package com.bpr.integration.client;

import java.util.ArrayList;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import android.os.AsyncTask;

public class CreateFolderGalleryAsync extends
		AsyncTask<String, Integer, String> {

	@Override
	protected String doInBackground(String... params) {
		System.out.println("url" + params[0]);
		String data = "";
		ArrayList<NameValuePair> listParams = new ArrayList<NameValuePair>();
		NameValuePair token = new BasicNameValuePair("token", params[1]);
		NameValuePair name = new BasicNameValuePair("survey_gallery[name]",
				params[2]);
		listParams.add(name);
		listParams.add(token);
		try {
			data = ClientToServer.postRequest(params[0], listParams);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}
}

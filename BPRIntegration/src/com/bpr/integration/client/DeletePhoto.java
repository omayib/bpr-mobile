package com.bpr.integration.client;

import android.os.AsyncTask;

public class DeletePhoto extends AsyncTask<String, Integer, String> {

	@Override
	protected String doInBackground(String... arg0) {
		String data="";
		try {
			data=ClientToServer.deleteRequest(arg0[0]);
			System.out.println("url "+arg0[0]);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

}

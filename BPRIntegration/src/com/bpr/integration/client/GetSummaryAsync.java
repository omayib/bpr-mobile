package com.bpr.integration.client;

import android.os.AsyncTask;

public class GetSummaryAsync extends AsyncTask<String, Integer, String> {

	@Override
	protected String doInBackground(String... params) {
		System.out.println("url" + params[0]);
		String data = "";
		try {
			data = ClientToServer.getRequest(params[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

}

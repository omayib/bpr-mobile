package com.bpr.integration.client;

import android.os.AsyncTask;

public class ListAoAsync extends AsyncTask<String, Integer, String> {

	@Override
	protected String doInBackground(String... params) {
		String data = "";
		try {
			data = ClientToServer.getRequest(params[0]);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return data;
	}

}

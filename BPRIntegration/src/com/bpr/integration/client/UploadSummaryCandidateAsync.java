package com.bpr.integration.client;

import java.io.IOException;

import android.os.AsyncTask;

public class UploadSummaryCandidateAsync extends
		AsyncTask<String, Integer, String> {

	@Override
	protected String doInBackground(String... params) {
		String data = "";
		System.out.println("url:" + params[0]);
		try {
			data = ClientToServer.putRequest(params[0], params[1]);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return data;
	}

}

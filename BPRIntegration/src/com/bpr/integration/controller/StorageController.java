package com.bpr.integration.controller;

import java.util.List;

import com.activeandroid.ActiveAndroid;
import com.bpr.activerecord.model.AssetActiveRecord;
import com.bpr.activerecord.model.BpkbActiveRecord;
import com.bpr.activerecord.model.BusinessActiveRecord;
import com.bpr.activerecord.model.ChildActiveRecord;
import com.bpr.activerecord.model.DepositoActiveRecord;
import com.bpr.activerecord.model.EnvironmentActiveRecord;
import com.bpr.activerecord.model.ExpenditureActiveRecord;
import com.bpr.activerecord.model.IncomeActiveRecord;
import com.bpr.activerecord.model.JobActiveRecord;
import com.bpr.activerecord.model.PersonalActiveRecord;
import com.bpr.activerecord.model.PurposeActiveRecord;
import com.bpr.activerecord.model.QueryAction;
import com.bpr.activerecord.model.ShmActiveRecord;
import com.bpr.activerecord.model.SubmissionActiveRecord;
import com.bpr.activerecord.model.TabunganActiveRecord;
import com.customer.model.Assets;
import com.customer.model.Business;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import com.customer.model.Children;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralDeposito;
import com.customer.model.CollateralShm;
import com.customer.model.CollateralTabungan;
import com.customer.model.EnvironmentalCheck;
import com.customer.model.Job;
import com.customer.model.Personal;
import com.customer.model.Purposes;
import com.customer.model.SubmissionCredit;

public class StorageController implements StorageAction {

	public StorageController() {
		super();
	}

	public void savePersonal(Personal personal, int proposalId) {
		PersonalActiveRecord pr = PersonalActiveRecord
				.getCurrentPersoanl(personal.getId());
		if (pr == null) {
			pr = new PersonalActiveRecord();
		}
		pr.addressByCurrent = personal.getAddressByCurrent();
		pr.addressByKtp = personal.getAddressByKtp();
		pr.age = personal.getAge();
		pr.aliasName = personal.getAliasName();
		pr.birthDate = personal.getBirthDate();
		pr.birthPlace = personal.getBirthPlace();
		pr.gender = personal.getGender();
		pr.handphoneNumber = personal.getHandphoneNumber();
		pr.homeStatus = personal.getHomeStatus();
		pr.id = personal.getId();
		pr.ktpNumber = personal.getKtpNumber();
		pr.maritalStatus = personal.getMaritalStatus();
		pr.motherMaidenName = personal.getMotherMaidenName();
		pr.name = personal.getName();
		pr.numberOfDependents = personal.getNumberOfDependents();
		pr.phoneNumber = personal.getPhoneNumber();
		pr.spouseJob = personal.getSpouseJob();
		pr.spouseName = personal.getSpouseName();
		pr.proposal = proposalId;
		pr.action = QueryAction.UPDATE;
		pr.save();

		List<Children> children = personal.getChildren();

		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < children.size(); i++) {
				ChildActiveRecord ch = new ChildActiveRecord();
				ch.personal = personal.getId();
				ch.age = children.get(i).getAge();
				ch.name = children.get(i).getName();
				if (children.get(i).getId() == -1) {
					ch.action = QueryAction.INSERT;
				} else {
					ch.id = children.get(i).getId();
					ch.action = QueryAction.UPDATE;
				}
				ch.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveAssets(List<Assets> listAsset, int proposalId) {

		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listAsset.size(); i++) {
				AssetActiveRecord ast;
				ast = AssetActiveRecord.getCurrentAssets(listAsset.get(i)
						.getId());
				if (ast == null) {
					ast = new AssetActiveRecord();
				}
				ast.name = listAsset.get(i).getName();
				ast.value = listAsset.get(i).getValue();
				ast.proposal = proposalId;
				if (listAsset.get(i).getId() == -1) {
					ast.action = QueryAction.INSERT;
				} else {
					ast.id = listAsset.get(i).getId();
					ast.action = QueryAction.UPDATE;
				}
				ast.save();
			}
			ActiveAndroid.setTransactionSuccessful();

		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveJob(Job job, int proposalId) {
		JobActiveRecord j;
		j = JobActiveRecord.getCurrentJob(job.getId());
		if (j == null) {
			j = new JobActiveRecord();
			j.action = QueryAction.INSERT;
		} else {
			j.action = QueryAction.UPDATE;
		}
		j.achievement = job.getAchievement();
		j.id = job.getId();
		j.jobField = job.getJobField();
		j.officeAddr = job.getOfficeAddr();
		j.officeName = job.getOfficeName();
		j.positionByCurrent = job.getPositionByCurrent();
		j.positionByInitial = job.getPositionByInitial();
		j.salary = job.getSalary();
		j.statusOfWorker = job.getStatusOfWorker();
		j.yearOfJoined = job.getYearOfJoined();
		j.proposal = proposalId;
		j.save();
	}

	public void saveBusiness(Business business, int proposalId) {
		BusinessActiveRecord bis = BusinessActiveRecord
				.getCurrentBusiness(business.getId());
		if (bis == null)
			bis = new BusinessActiveRecord();
		bis.additionalInfo = business.getAdditionalInfo();
		bis.address = business.getAddress();
		bis.companyLegallity = business.getCompanyLegallity();
		bis.field = business.getField();
		bis.foundedYear = business.getFoundedYear();
		if (business.getId() <= 0) {
			bis.action = QueryAction.INSERT;
		} else {
			bis.id = business.getId();
			bis.action = QueryAction.UPDATE;
		}

		bis.name = business.getName();
		bis.numberOfEmployees = business.getNumberOfEmployees();
		bis.officeOwnership = business.getOfficeOwnership();
		bis.projectExperiences = business.getProjectExperiences();
		bis.siupNumber = business.getSiupNumber();
		bis.tdpNumber = business.getTdpNumber();
		bis.yearsOfOperation = business.getYearsOfOperation();
		bis.proposal = proposalId;
		bis.save();
	}

	public void saveSubmission(SubmissionCredit submission, int proposalId) {
		SubmissionActiveRecord subm;
		subm = SubmissionActiveRecord.getSubmission(proposalId);
		if (subm == null)
			subm = new SubmissionActiveRecord();

		subm.id = submission.getId();
		subm.plafon = submission.getPlafon();
		subm.product = submission.getCreditProduct();
		subm.interest = submission.getInterest();
		subm.period = submission.getPeriode();
		subm.proposal = proposalId;
		subm.action = QueryAction.UPDATE;
		subm.save();
	}

	public void savePurposes(List<Purposes> listPurpose, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listPurpose.size(); i++) {
				System.out.println("id... :" + listPurpose.get(i).getId());
				PurposeActiveRecord pr;
				pr = PurposeActiveRecord.getCurrentPurposes(listPurpose.get(i)
						.getId());
				if (pr == null)
					pr = new PurposeActiveRecord();
				if (listPurpose.get(i).getId() <= 0) {
					pr.action = QueryAction.INSERT;
				} else {
					pr.action = QueryAction.UPDATE;
				}
				pr.id = listPurpose.get(i).getId();
				pr.general = listPurpose.get(i).getGeneral();
				pr.detail = listPurpose.get(i).getDetail();
				pr.proposal = proposalId;
				pr.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveCapacity(List<CapacityIncome> incomeCollection,
			List<CapacityExpenditure> expenditureCollection, int idCapacity,
			int proposalId) {
		ActiveAndroid.beginTransaction();

		try {
			for (int i = 0; i < incomeCollection.size(); i++) {
				IncomeActiveRecord inc;
				inc = IncomeActiveRecord.getCurrentIncomes(incomeCollection
						.get(i).getId());
				if (inc == null)
					inc = new IncomeActiveRecord();
				inc.name = incomeCollection.get(i).getName();
				inc.value = incomeCollection.get(i).getValue();
				inc.idCapacity = idCapacity;
				inc.proposal = proposalId;
				inc.id = incomeCollection.get(i).getId();
				if (incomeCollection.get(i).getId() <= 0) {
					inc.action = QueryAction.INSERT;
				} else {
					inc.action = QueryAction.UPDATE;
				}
				inc.save();
			}

			for (int i = 0; i < expenditureCollection.size(); i++) {
				ExpenditureActiveRecord exp;
				exp = ExpenditureActiveRecord
						.getCurrentExpenditures(expenditureCollection.get(i)
								.getId());
				if (exp == null)
					exp = new ExpenditureActiveRecord();
				exp.name = expenditureCollection.get(i).getName();
				exp.value = expenditureCollection.get(i).getValue();
				exp.idCapacity = idCapacity;
				exp.proposal = proposalId;
				exp.id = expenditureCollection.get(i).getId();
				if (expenditureCollection.get(i).getId() <= 0) {
					exp.action = QueryAction.INSERT;
				} else {
					exp.action = QueryAction.UPDATE;
				}
				exp.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveCollateralShm(List<CollateralShm> listShm, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listShm.size(); i++) {
				ShmActiveRecord shm;
				shm = ShmActiveRecord.getCurrentShm(listShm.get(i).getId());

				if (shm == null)
					shm = new ShmActiveRecord();

				shm.binding = listShm.get(i).getBinding().replace(" ", "_")
						.toUpperCase();
				shm.certificateName = listShm.get(i).getCertificateName();
				shm.certificateNumber = listShm.get(i).getCertificateNumber();

				if (listShm.get(i).getId() <= 0) {
					shm.action = QueryAction.INSERT;
				} else {
					shm.action = QueryAction.UPDATE;
					shm.id = listShm.get(i).getId();
				}

				shm.info = listShm.get(i).getInfo();
				shm.name = listShm.get(i).getCertificateName();
				shm.priceAssessed = listShm.get(i).getPriceOnAssessed()
						.isEmpty() ? "0" : listShm.get(i).getPriceOnAssessed();
				shm.priceMarket = listShm.get(i).getPriceOnMarket().isEmpty() ? "0"
						: listShm.get(i).getPriceOnMarket();
				shm.size = listShm.get(i).getLandOfArea();
				shm.status = listShm.get(i).getStatusOfLand();
				shm.proposal = proposalId;
				shm.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveCollateralBpkb(List<CollateralBpkb> listBpkb, int proposalId) {
		ActiveAndroid.beginTransaction();
		try {

			for (int i = 0; i < listBpkb.size(); i++) {
				BpkbActiveRecord bpkb;
				bpkb = BpkbActiveRecord.getCurrentBpkb(listBpkb.get(i).getId());
				if (bpkb == null) {
					System.out.println("insert new");
					bpkb = new BpkbActiveRecord();
				}
				System.out.println("update");
				bpkb.binding = listBpkb.get(i).getBinding().replace(" ", "_")
						.toUpperCase();
				bpkb.bpkbName = listBpkb.get(i).getNameOfBpkb();
				bpkb.bpkbNumber = listBpkb.get(i).getNumberBpkb();
				bpkb.id = listBpkb.get(i).getId();

				if (listBpkb.get(i).getId() <= 0) {
					bpkb.action = QueryAction.INSERT;
				} else {
					bpkb.action = QueryAction.UPDATE;
				}
				bpkb.info = listBpkb.get(i).getInfo();
				bpkb.name = listBpkb.get(i).getNameOfBpkb();
				bpkb.policeNumber = listBpkb.get(i).getPoliceNumber();
				bpkb.priceAssessed = listBpkb.get(i).getPriceOnAssessed()
						.isEmpty() ? "0" : listBpkb.get(i).getPriceOnAssessed();
				bpkb.priceMarket = listBpkb.get(i).getPriceOnMarket().isEmpty() ? "0"
						: listBpkb.get(i).getPriceOnMarket();
				bpkb.stnkNumber = listBpkb.get(i).getNumberStnk();
				bpkb.vehicleBrand = listBpkb.get(i).getCarBrand();
				bpkb.vehicleName = listBpkb.get(i).getCarName();
				bpkb.vehicleType = listBpkb.get(i).getCarType();
				bpkb.vehicleYear = listBpkb.get(i).getCarYear();
				bpkb.proposal = proposalId;
				bpkb.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveEnvCheckup(List<EnvironmentalCheck> listCheck,
			int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (int i = 0; i < listCheck.size(); i++) {
				EnvironmentActiveRecord env;
				env = EnvironmentActiveRecord.getCurrentEnv(listCheck.get(i)
						.getId());
				if (env == null)
					env = new EnvironmentActiveRecord();

				env.impression = listCheck.get(i).getImpression();
				env.name = listCheck.get(i).getName();
				env.realtion = listCheck.get(i).getRelation();
				env.proposal = proposalId;
				env.id = listCheck.get(i).getId();
				if (listCheck.get(i).getId() <= 0) {
					env.action = QueryAction.INSERT;
				} else {
					env.action = QueryAction.UPDATE;
				}
				env.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveTabungan(List<CollateralTabungan> listTabungan,
			int proposalId) {
		ActiveAndroid.beginTransaction();
		try {
			for (CollateralTabungan t : listTabungan) {
				TabunganActiveRecord tabunganRecord;
				tabunganRecord = TabunganActiveRecord.getCurrentTabungan(t
						.getId());
				if (tabunganRecord == null)
					tabunganRecord = new TabunganActiveRecord();

				tabunganRecord.accountNumber = t.getAccountNumber();
				tabunganRecord.amount = t.getAmount();
				tabunganRecord.bookNumber = t.getBookNumber();
				tabunganRecord.id = t.getId();
				tabunganRecord.name = t.getName();
				tabunganRecord.proposal = proposalId;
				tabunganRecord.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveDeposito(List<CollateralDeposito> listDeposito,
			int proposalId) {

		ActiveAndroid.beginTransaction();
		try {
			for (CollateralDeposito t : listDeposito) {
				DepositoActiveRecord depositoRecord;
				depositoRecord = DepositoActiveRecord.getCurrentDeposito(t
						.getId());
				if (depositoRecord == null)
					depositoRecord = new DepositoActiveRecord();

				depositoRecord.accountNumber = t.getAccountNumber();
				depositoRecord.amount = t.getAmount();
				depositoRecord.bilyetNumber = t.getBilyetNumber();
				depositoRecord.id = t.getId();
				depositoRecord.name = t.getName();
				depositoRecord.proposal = proposalId;
				depositoRecord.save();
			}
			ActiveAndroid.setTransactionSuccessful();
		} finally {
			ActiveAndroid.endTransaction();
		}
	}

	public void saveProposal(int proposalId) {
		// ProposalActiveRecord proposalActiveRecord = new
		// ProposalActiveRecord();
		// proposalActiveRecord.id = proposalId;
		// proposalActiveRecord.save();
	}
}

package com.bpr.integration.controller;

import java.util.List;

import com.customer.model.Assets;
import com.customer.model.Business;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralDeposito;
import com.customer.model.CollateralShm;
import com.customer.model.CollateralTabungan;
import com.customer.model.EnvironmentalCheck;
import com.customer.model.Job;
import com.customer.model.Personal;
import com.customer.model.Purposes;
import com.customer.model.SubmissionCredit;

public interface StorageAction {
	void saveProposal(int proposalId);

	void savePersonal(Personal personal, int proposalId);

	void saveAssets(List<Assets> listAssets, int proposalId);

	void saveJob(Job job, int proposalId);

	void saveBusiness(Business business, int proposalId);

	void saveSubmission(SubmissionCredit subm, int proposalId);

	void savePurposes(List<Purposes> listPurpose, int proposalId);

	void saveCapacity(List<CapacityIncome> income,
			List<CapacityExpenditure> expenditure, int idCapacity,
			int proposalId);

	void saveCollateralShm(List<CollateralShm> shm, int proposalId);

	void saveCollateralBpkb(List<CollateralBpkb> bpkb, int proposalId);

	void saveEnvCheckup(List<EnvironmentalCheck> listCheck, int proposalId);

	void saveTabungan(List<CollateralTabungan> listTabungan, int proposalId);

	void saveDeposito(List<CollateralDeposito> listDeposito, int proposalId);
}

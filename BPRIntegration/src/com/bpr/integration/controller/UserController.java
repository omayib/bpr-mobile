package com.bpr.integration.controller;

import java.io.File;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;

import com.ackbox.event.GetSummaryFailed;
import com.ackbox.event.GetSummarySucceeded;
import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.event.ProgressEvent;
import com.amazonaws.event.ProgressListener;
import com.amazonaws.mobileconnectors.s3.transfermanager.TransferManager;
import com.amazonaws.mobileconnectors.s3.transfermanager.Upload;
import com.amazonaws.mobileconnectors.s3.transfermanager.model.UploadResult;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.ParsingResponse;
import com.bpr.integration.client.CreateFolderGalleryAsync;
import com.bpr.integration.client.DeletePhoto;
import com.bpr.integration.client.GetSummaryAsync;
import com.bpr.integration.client.ListOfProposalAsync;
import com.bpr.integration.client.ProposalDetailAsync;
import com.bpr.integration.client.SigninAsync;
import com.bpr.integration.client.UpdatePhotoAsync;
import com.bpr.integration.client.UploadProposalCandidateAsync;
import com.bpr.integration.client.UploadSummaryCandidateAsync;
import com.bpr.integration.parser.BprParser;
import com.bpr.integration.utils.AwsUtils;
import com.bpr.integration.utils.UrlApi;
import com.bpr.repository.Repository;
import com.customer.event.ProposalCollectionLoadFailed;
import com.customer.event.ProposalCollectionLoadSucceeded;
import com.customer.event.ProposalDetailLoadFailed;
import com.customer.event.ProposalDetailLoaded;
import com.customer.event.ProposalDetailToSave;
import com.customer.model.Assets;
import com.customer.model.Business;
import com.customer.model.BusinessCollection;
import com.customer.model.CapacityCollection;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralDeposito;
import com.customer.model.CollateralShm;
import com.customer.model.CollateralTabungan;
import com.customer.model.EnvironmentalCheck;
import com.customer.model.Job;
import com.customer.model.Personal;
import com.customer.model.Photo;
import com.customer.model.PhotoCandidate;
import com.customer.model.PhotoFolder;
import com.customer.model.Proposal;
import com.customer.model.Purposes;
import com.customer.model.SubmissionCredit;
import com.customer.model.Summary;
import com.customer.model.SummaryCandidate;
import com.proposalbox.event.FolderGalleryCreateFailed;
import com.proposalbox.event.FolderGalleryCreated;
import com.proposalbox.event.LocalAssetsLoaded;
import com.proposalbox.event.LocalBpkbLoaded;
import com.proposalbox.event.LocalBusinessLoadded;
import com.proposalbox.event.LocalCapacityLoaded;
import com.proposalbox.event.LocalDepositoLoaded;
import com.proposalbox.event.LocalEnvLoaded;
import com.proposalbox.event.LocalExpenditureLoaded;
import com.proposalbox.event.LocalFolderLoaded;
import com.proposalbox.event.LocalGalleryLoaded;
import com.proposalbox.event.LocalIncomeLoaded;
import com.proposalbox.event.LocalJobLoaded;
import com.proposalbox.event.LocalPersonalLoaded;
import com.proposalbox.event.LocalPurposesLoaded;
import com.proposalbox.event.LocalShmLoaded;
import com.proposalbox.event.LocalSubmissionLoaded;
import com.proposalbox.event.LocalTabunganLoaded;
import com.proposalbox.event.PhotoDeleteFailed;
import com.proposalbox.event.PhotoDeleteSucceeded;
import com.proposalbox.event.PhotoUpdateSucceded;
import com.proposalbox.event.PhotoUploadFailed;
import com.proposalbox.event.PhotoUploadProgress;
import com.proposalbox.event.PhotoUploadS3Succeded;
import com.proposalbox.event.ProposalUploadFailed;
import com.proposalbox.event.ProposalUploadSucceeded;
import com.proposalbox.event.SummaryUploadFailed;
import com.proposalbox.event.SummaryUploadSucceded;
import com.proposalbox.model.ProposalAction;
import com.user.event.SigninFailed;
import com.user.event.SigninSucceeded;
import com.user.event.SignoutSucceeded;
import com.user.modul.UserAction;

public class UserController implements UserAction, ProposalAction {
	private BprParser parser;
	private EventPublisher event;
	private Context context;
	private Repository repository;

	public UserController(Context context, BprParser parser,
			EventPublisher event, Repository bprRepository) {
		super();
		this.parser = parser;
		this.event = event;
		this.context = context;
		this.repository = bprRepository;
	}

	public void signin(String username, String password) {
		SigninAsync signin = new SigninAsync() {
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				if (result.isEmpty()) {
					event.publish(new SigninFailed("sorry something went wrong"));
					return;
				}
				parser.signin(result, new ParsingResponse<SigninSucceeded>() {

					public void onSuccess(SigninSucceeded result) {
						event.publish(result);
					}

					public void onFailure(String failedResult) {
						event.publish(new SigninFailed(failedResult));
					}
				});
			}
		};
		signin.execute(UrlApi.SIGNIN, username, password);
	}

	public void signout(String token) {
		event.publish(new SignoutSucceeded());
	}

	public void requestListOfProposal(String token, int idCustomer) {
		ListOfProposalAsync listProposals = new ListOfProposalAsync() {
			protected void onPostExecute(String result) {
				System.out.println("proposals::" + result);
				if (result.isEmpty())
					event.publish(new ProposalCollectionLoadFailed());

				parser.listOfProposal(result,
						new ParsingResponse<ProposalCollectionLoadSucceeded>() {

							public void onSuccess(
									ProposalCollectionLoadSucceeded result) {
								System.out.println("SUCCEEDED::"
										+ result.toString());
								event.publish(result);
							}

							public void onFailure(String failedResult) {
								// TODO Auto-generated method stub
								event.publish(new ProposalCollectionLoadFailed());

							}
						});
			};
		};
		listProposals.execute(UrlApi.LIST_OF_PROPOSAL(token, idCustomer));
	}

	// perlu dicek! yen read only , publishnya pake ProposalDetailToSave()
	public void requestProposalDetail(String token, int idproposal,
			final boolean isReadonly) {
		if (!repository.isProposalExist(idproposal)) {
			ProposalDetailAsync proposal = new ProposalDetailAsync() {
				@Override
				protected void onPostExecute(String result) {
					super.onPostExecute(result);
					System.out.println("respon:" + result);
					parser.proposalData(result,
							new ParsingResponse<Proposal>() {

								public void onSuccess(Proposal result) {
									// if read only
									System.out.println("inside onsuccess: "
											+ isReadonly);
									event.publish(new ProposalDetailLoaded(
											result));
									System.out
											.println("publish to detail loaded");
									if (!isReadonly) {
										System.out.println("publish to sav");
										event.publish(new ProposalDetailToSave(
												result));
									}

									// else
								}

								public void onFailure(String failedResult) {
									event.publish(new ProposalDetailLoadFailed(
											failedResult));
								}
							});
				}
			};
			proposal.execute(UrlApi.DETAIL_OF_PROPOSAL(token, idproposal));
		} else {
			Proposal a = repository.loadProposal(idproposal);
			System.out.println("========== local proposal:" + a.toString());
			event.publish(new ProposalDetailLoaded(a));
		}
	}

	public void uploadProposal(String dataToUpload, int idproposal,
			final String stringRecommendation) {
		if (dataToUpload.isEmpty()) {
			event.publish(new ProposalUploadFailed("proposal sudah diupload"));
			return;
		}
		System.out.println("data  proposal to upload:" + dataToUpload);
		UploadProposalCandidateAsync uploadProposalCandidate = new UploadProposalCandidateAsync() {
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				System.out.println("RESPONSE::" + result);
				parser.proposalUploadResponse(result,
						new ParsingResponse<Summary>() {

							public void onSuccess(Summary result) {
								event.publish(new ProposalUploadSucceeded(
										result));
							}

							public void onFailure(String failedResult) {
								event.publish(new ProposalUploadFailed(
										failedResult));
							}
						});
			}
		};
		uploadProposalCandidate.execute(UrlApi.UPLOAD_PROPOSAL(idproposal),
				dataToUpload);
	}

	public void uploadSummary(final int idproposal,
			SummaryCandidate summaryCandidate) {
		System.out.println("summaryCandidate::"
				+ summaryCandidate.getSummaryCandidate());
		UploadSummaryCandidateAsync uploadSummaryCandidate = new UploadSummaryCandidateAsync() {
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				System.out.println("RESPONSE!::" + result);
				parser.summaryUploadedResponse(result, idproposal,
						new ParsingResponse<Integer>() {

							public void onSuccess(Integer result) {
								event.publish(new SummaryUploadSucceded(result));
							}

							public void onFailure(String failedResult) {
								event.publish(new SummaryUploadFailed(
										failedResult));
							}
						});
			}
		};
		uploadSummaryCandidate.execute(
				// summaryCandidate.getIdCustomer()
				UrlApi.UPLOAD_SUMMARY(summaryCandidate.getIdCustomer()),
				summaryCandidate.getSummaryCandidate());
	}

	public void createFolderGallery(String token, String name, int idProposal,
			String galleryName) {
		CreateFolderGalleryAsync folderGalleryAsync = new CreateFolderGalleryAsync() {
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				parser.folderGalleryCreatedResponse(result,
						new ParsingResponse<PhotoFolder>() {

							public void onSuccess(PhotoFolder result) {
								event.publish(new FolderGalleryCreated(result));
							}

							public void onFailure(String failedResult) {
								event.publish(new FolderGalleryCreateFailed(
										failedResult));
							}
						});
			}
		};
		folderGalleryAsync.execute(
				UrlApi.CREATE_FOLDER_GALLERY(idProposal, galleryName), token,
				name);
	}

	public void uploadPhoto(final String token, final int folderId,
			final PhotoCandidate photo) {
		CognitoCachingCredentialsProvider credentialsProvider = new CognitoCachingCredentialsProvider(
				context, AwsUtils.AWS_ACCOUNT_ID,
				AwsUtils.COGNITO_IDENTITY_POOL, AwsUtils.UNAUTHENTICATED_ROLE,
				AwsUtils.AUTHENTICATED_ROLE, AwsUtils.REGIONS);
		TransferManager transferManager = new TransferManager(
				credentialsProvider);

		File fileToUpload = new File(photo.getPhoto().getPath());
		String key = photo.getPhoto().getUniqueId();
		String fileName = fileToUpload.getName();
		String extention = fileName.substring(fileName.lastIndexOf(".") + 1);
		System.out.println(extention);

		PutObjectRequest obj = new PutObjectRequest(AwsUtils.BUCKET_NAME, key,
				fileToUpload).withCannedAcl(CannedAccessControlList.PublicRead);

		Upload upload = transferManager.upload(obj);
		upload.addProgressListener(new ProgressListener() {

			public void progressChanged(ProgressEvent arg0) {
				// System.out.println("progress upload "
				// + arg0.getBytesTransferred());
				// System.out.println("event code " + arg0.getEventCode());
				event.publish(new PhotoUploadProgress(arg0
						.getBytesTransferred(), photo.getPhoto().getName()));
				if (arg0.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE) {

				}
			}

		});

		UploadResult res;
		try {
			res = upload.waitForUploadResult();
			String url = AwsUtils.URL_GENERATOR(res.getBucketName(),
					res.getKey());
			event.publish(new PhotoUploadS3Succeded(res.getKey(), url));

			photo.getPhoto().setUrl(url);

			updatePhoto(token, photo);
		} catch (AmazonServiceException e) {
			e.printStackTrace();
			event.publish(new PhotoUploadFailed(e.toString()));
		} catch (AmazonClientException e) {
			e.printStackTrace();
			event.publish(new PhotoUploadFailed(e.toString()));
		} catch (InterruptedException e) {
			e.printStackTrace();
			event.publish(new PhotoUploadFailed(e.toString()));
		}

	}

	public void updatePhoto(String token, final PhotoCandidate photo) {

		try {

			JSONObject objPhotoToUpload = new JSONObject();
			objPhotoToUpload.put("token", token);
			objPhotoToUpload.put("proposal_survey", photo.getCanditateAsJson());

			String dataToUpload = objPhotoToUpload.toString();
			System.out.println(dataToUpload);
			UpdatePhotoAsync updatePhotoAsync = new UpdatePhotoAsync() {
				@Override
				protected void onPostExecute(String result) {
					super.onPostExecute(result);
					System.out.println("hasil update::" + result);
					parser.updatePhoto(result, new ParsingResponse<Void>() {

						public void onSuccess(Void result) {
							event.publish(new PhotoUpdateSucceded(photo));
						}

						public void onFailure(String failedResult) {
							event.publish(new PhotoUploadFailed(failedResult));
						}
					});
				}
			};
			updatePhotoAsync
					.execute(UrlApi.UPLOAD_PROPOSAL(photo.getProposalId()),
							dataToUpload);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public void getPersonalFromLocal(int idPersonal) {

		Personal personal = repository.getPersonal(idPersonal);
		if (personal != null)
			event.publish(new LocalPersonalLoaded(personal));
	}

	public void getAssetsFromLocal(int idProposal) {

		List<Assets> listAssets = (List<Assets>) repository.getAssets(
				idProposal).getAsset();
		if (!listAssets.isEmpty())
			event.publish(new LocalAssetsLoaded(listAssets));
	}

	public void getJobFromLocal(int idProposal) {
		Job job = repository.getJob(idProposal);
		if (job != null)
			event.publish(new LocalJobLoaded(job));

	}

	public void getBusinessFromLocal(int idProposal) {
		BusinessCollection bCollection = repository.getBusiness(idProposal);
		if (bCollection != null)
			event.publish(new LocalBusinessLoadded(bCollection));
	}

	public void getSubmissionFromLocal(int idProposal) {

		SubmissionCredit submissionCredit = repository
				.getSubmission(idProposal);
		if (submissionCredit != null)
			event.publish(new LocalSubmissionLoaded(submissionCredit));
	}

	public void getPurposeFromLocal(int idProposal) {

		List<Purposes> listPurposes = (List<Purposes>) repository.getPurposes(
				idProposal).getPurposesCollection();
		if (!listPurposes.isEmpty())
			event.publish(new LocalPurposesLoaded(listPurposes));
	}

	public void getCapacityFromLocal(int idProposal) {
		CapacityCollection capcoll = repository.getCapacity(idProposal);
		if (!capcoll.getExpenditure().isEmpty()) {
			event.publish(new LocalExpenditureLoaded(
					(List<CapacityExpenditure>) capcoll.getExpenditure()));
		}
		if (!capcoll.getIncome().isEmpty()) {
			event.publish(new LocalIncomeLoaded((List<CapacityIncome>) capcoll
					.getIncome()));
		}
		// if (capcoll != null)
		// event.publish(new LocalCapacityLoaded(capcoll));

	}

	public void getBpkbFromLocal(int idProposal) {

		List<CollateralBpkb> listBpkb = (List<CollateralBpkb>) repository
				.getCollateralBpkb(idProposal).getGuaranteeBpkb();
		if (!listBpkb.isEmpty())
			event.publish(new LocalBpkbLoaded(listBpkb));
	}

	public void getShmFromLocal(int idProposal) {

		List<CollateralShm> listShm = (List<CollateralShm>) repository
				.getCollateralShm(idProposal).getGuaranteeShm();
		if (!listShm.isEmpty())
			event.publish(new LocalShmLoaded(listShm));
	}

	public void getEnvFromLocal(int idProposal) {

		List<EnvironmentalCheck> listCheck = (List<EnvironmentalCheck>) repository
				.getEnvironmentalCheckup(idProposal).getCheckCollection();
		if (!listCheck.isEmpty())
			event.publish(new LocalEnvLoaded(listCheck));
	}

	public void getFoldersFromLocal(int idProposal, String galleryName) {
		List<PhotoFolder> listFolder = repository.getPhotoFolder(idProposal,
				galleryName);
		if (!listFolder.isEmpty())
			event.publish(new LocalFolderLoaded(listFolder));
	}

	public void getGalleryPending() {
		List<Photo> listPhoto = repository.getGalleryPending();
		if (!listPhoto.isEmpty())
			event.publish(new LocalGalleryLoaded(listPhoto));
	}

	public void getTabunganFromLocal(int idProposal) {
		List<CollateralTabungan> listTabungan = repository
				.getCollateralTabungan(idProposal).listTabungan;
		if (!listTabungan.isEmpty())
			event.publish(new LocalTabunganLoaded(listTabungan));

	}

	public void getDepositoFromLocal(int idProposal) {
		List<CollateralDeposito> listDeposito = repository
				.getCollateralDeposito(idProposal).listDeposito;
		if (!listDeposito.isEmpty())
			event.publish(new LocalDepositoLoaded(listDeposito));
	}

	public void putProposal(int proposalId) {
		repository.saveProposal(proposalId);
	}

	public void putPersonal(Personal personal, int proposalId) {
		repository.savePersonal(personal, proposalId);
	}

	public void putAssets(List<Assets> listAssets, int proposalId) {
		repository.saveAssets(listAssets, proposalId);
	}

	public void putJob(Job job, int proposalId) {
		repository.saveJob(job, proposalId);
	}

	public void putBusiness(Business business, int proposalId) {
		repository.saveBusiness(business, proposalId);
	}

	public void putBusinessCollection(List<Business> business, int proposalId) {
		repository.saveBusinessCollection(business, proposalId);
	}

	public void putSubmission(SubmissionCredit subm, int proposalId) {
		repository.saveSubmission(subm, proposalId);
	}

	public void putPurposes(List<Purposes> listPurpose, int proposalId) {
		repository.savePurposes(listPurpose, proposalId);
	}

	public void putCapacity(List<CapacityIncome> income,
			List<CapacityExpenditure> expenditure, int idCapacity,
			int proposalId) {
		repository.saveCapacity(income, expenditure, idCapacity, proposalId);
	}

	public void putCollateralShm(List<CollateralShm> shm, int proposalId) {
		repository.saveCollateralShm(shm, proposalId);
	}

	public void putCollateralBpkb(List<CollateralBpkb> bpkb, int proposalId) {
		repository.saveCollateralBpkb(bpkb, proposalId);
	}

	public void puteEnvCheckup(List<EnvironmentalCheck> listCheck,
			int proposalId) {
		repository.saveEnvCheckup(listCheck, proposalId);
	}

	public void putTabungan(List<CollateralTabungan> listTabungan,
			int proposalId) {
		repository.saveTabungan(listTabungan, proposalId);
	}

	public void putDeposito(List<CollateralDeposito> listDeposito,
			int proposalId) {
		repository.saveDeposito(listDeposito, proposalId);
	}

	public void putPhotoBusiness(List<PhotoFolder> listFolder, int proposalId) {
		repository.savePhotoBusiness(listFolder, proposalId);
	}

	public void putPhotoHome(List<PhotoFolder> listFolder, int proposalId) {
		repository.savePhotoHome(listFolder, proposalId);
	}

	public void putPhotoCollateral(List<PhotoFolder> listFolder, int proposalId) {
		repository.savePhotoCollateral(listFolder, proposalId);
	}

	public void putPhotoOther(List<PhotoFolder> listFolder, int proposalId) {
		repository.savePhotoOther(listFolder, proposalId);
	}

	public void getSummary(String token, int idProposal) {
		System.out.println("get summary khusu proopal ini");
		GetSummaryAsync getSummaryAsync = new GetSummaryAsync() {
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				parser.proposalUploadResponse(result,
						new ParsingResponse<Summary>() {

							public void onSuccess(Summary result) {
								event.publish(new GetSummarySucceeded(result));
							}

							public void onFailure(String failedResult) {
								event.publish(new GetSummaryFailed(failedResult));
							}
						});
			}
		};
		getSummaryAsync.execute(UrlApi.GET_SUMMARY(token, idProposal));
	}

	public void deletePhoto(final Photo photo,String token) {
		System.out.println("photo has local path "+repository.isPhotoHasLocalPath(photo));
		if(repository.isPhotoHasLocalPath(photo)){
			// delete local
			//publish deleted
			repository.deletePhoto(photo);
			event.publish(new PhotoDeleteSucceeded(photo));
		}else{
			DeletePhoto deletePhoto=new DeletePhoto(){
				@Override
				protected void onPostExecute(String result) {
					super.onPostExecute(result);
					System.out.println("delete onpostexecuted");
					if(result.isEmpty()){
						//success
						System.out.println("delete berhasil");
						repository.deletePhoto(photo);
						event.publish(new PhotoDeleteSucceeded(photo));
					}else{
						//error
						System.out.println("delete tidak berhasil");
						event.publish(new PhotoDeleteFailed());
					}
				}
			};	
			deletePhoto.execute(UrlApi.DELETE_PHOTO(photo.getId(),token));
		}
	}
}

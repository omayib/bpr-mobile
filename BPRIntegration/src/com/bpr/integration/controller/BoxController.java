package com.bpr.integration.controller;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ackbox.event.ApproveAckFailed;
import com.ackbox.event.ApproveAckSucceded;
import com.ackbox.event.GetSummaryFailed;
import com.ackbox.event.GetSummarySucceeded;
import com.ackbox.event.RefreshAckBoxFailed;
import com.ackbox.event.RefreshAckBoxSucceded;
import com.ackbox.event.RejectAckFailed;
import com.ackbox.event.RejectAckSucceeded;
import com.ackbox.model.AckBoxAction;
import com.allbox.event.RefreshAllBoxFailed;
import com.allbox.event.RefreshAllBoxSucceeded;
import com.allbox.model.AllBoxAction;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.ParsingResponse;
import com.bpr.integration.client.ApproveAsync;
import com.bpr.integration.client.GetSidAsync;
import com.bpr.integration.client.GetSummaryAsync;
import com.bpr.integration.client.AssigntoAsync;
import com.bpr.integration.client.ListAoAsync;
import com.bpr.integration.client.RefreshBoxAsync;
import com.bpr.integration.client.RejectProposal;
import com.bpr.integration.parser.BprParser;
import com.bpr.integration.utils.UrlApi;
import com.customer.event.BoxData;
import com.customer.model.Sid;
import com.customer.model.Summary;
import com.customer.model.SummaryCandidate;
import com.followupbox.event.ApproveFollowupFailed;
import com.followupbox.event.ApproveFollowupSucceded;
import com.followupbox.event.RefreshFollowupBoxFailed;
import com.followupbox.event.RefreshFollowupBoxSucceeded;
import com.followupbox.event.RejectFollowupFailed;
import com.followupbox.event.RejectFollowupSucceeded;
import com.followupbox.model.FollowupBoxAction;
import com.newbox.event.AssignFailed;
import com.newbox.event.AssignSucceeded;
import com.newbox.event.LoadListAoFailed;
import com.newbox.event.LoadListAoSucceded;
import com.newbox.event.RefreshNewboxFailed;
import com.newbox.event.RefreshNewboxSucceeded;
import com.newbox.model.NewBoxAction;
import com.proposalbox.event.RefreshProposalBoxFailed;
import com.proposalbox.event.RefreshProposalBoxSucceeded;
import com.proposalbox.event.SidLoadFailed;
import com.proposalbox.event.SidLoadSucceded;
import com.proposalbox.model.ProposalBoxAction;
import com.user.modul.User;

public class BoxController implements NewBoxAction, ProposalBoxAction,
		FollowupBoxAction, AllBoxAction, AckBoxAction {

	private BprParser parser;
	private EventPublisher event;

	public BoxController(BprParser parser, EventPublisher event) {
		super();
		this.parser = parser;
		this.event = event;
	}

	public void newBoxrefresh(String token) {
		RefreshBoxAsync refresh = new RefreshBoxAsync() {
			protected void onPostExecute(String result) {
				parser.refreshBox(result, new ParsingResponse<List<BoxData>>() {

					public void onSuccess(List<BoxData> result) {
						event.publish(new RefreshNewboxSucceeded(result));
					}

					public void onFailure(String failedResult) {
						event.publish(new RefreshNewboxFailed(failedResult));
					}
				});
			};
		};
		refresh.execute(UrlApi.REFRESH_NEWBOX(token));
	}

	public void assign(String token, int idcustomer, int idAo, String note) {
		// generate assign object
		JSONObject assignObj = new JSONObject();
		JSONObject recPropObj = new JSONObject();
		JSONArray recArrObj = new JSONArray();
		JSONObject recObj = new JSONObject();
		JSONArray noteArr = new JSONArray();
		try {

			noteArr.put(note);
			recObj.put("note", noteArr);
			recArrObj.put(recObj);
			recPropObj.put("proposal_recommendations_attributes", recArrObj);

			assignObj.put("token", token);
			assignObj.put("ao", idAo);
			assignObj.put("customer_data", recPropObj);
			System.out.println("ASSIGN OBJ" + assignObj.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		AssigntoAsync assingnAsync = new AssigntoAsync() {
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				System.out.println("RESPON" + result);
				parser.assigningResponse(result, new ParsingResponse<Void>() {

					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						event.publish(new AssignSucceeded());
					}

					public void onFailure(String failedResult) {
						// TODO Auto-generated method stub
						event.publish(new AssignFailed());
					}
				});
			}
		};
		assingnAsync.execute(UrlApi.ASSIGN(token, idcustomer),
				assignObj.toString());
	}

	public void proposalBoxRefresh(String token) {
		// TODO Auto-generated method stub
		RefreshBoxAsync refresh = new RefreshBoxAsync() {
			protected void onPostExecute(String result) {
				parser.refreshBox(result, new ParsingResponse<List<BoxData>>() {

					public void onSuccess(List<BoxData> result) {
						event.publish(new RefreshProposalBoxSucceeded(result));
					}

					public void onFailure(String failedResult) {
						event.publish(new RefreshProposalBoxFailed(failedResult));
					}
				});
			};
		};
		refresh.execute(UrlApi.REFRESH_PROPOSALBOX(token));
	}

	public void refreshFollowupBox(String token) {
		// TODO Auto-generated method stub
		RefreshBoxAsync refresh = new RefreshBoxAsync() {
			protected void onPostExecute(String result) {
				parser.refreshBox(result, new ParsingResponse<List<BoxData>>() {

					public void onSuccess(List<BoxData> result) {
						event.publish(new RefreshFollowupBoxSucceeded(result));
					}

					public void onFailure(String failedResult) {
						event.publish(new RefreshFollowupBoxFailed(failedResult));
					}
				});
			};
		};
		refresh.execute(UrlApi.REFRESH_FOLLOWUPBOX(token));
	}

	public void refreshAllBox(String token) {
		// TODO Auto-generated method stub
		RefreshBoxAsync refresh = new RefreshBoxAsync() {
			protected void onPostExecute(String result) {
				parser.refreshBox(result, new ParsingResponse<List<BoxData>>() {

					public void onSuccess(List<BoxData> result) {
						event.publish(new RefreshAllBoxSucceeded(result));
					}

					public void onFailure(String failedResult) {
						event.publish(new RefreshAllBoxFailed(failedResult));
					}
				});
			};
		};
		refresh.execute(UrlApi.REFRESH_ALLBOX(token));
	}

	public void getListAo(String token,int customerId) {

		ListAoAsync aoAsync = new ListAoAsync() {
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				parser.requestAoResponse(result,
						new ParsingResponse<List<User>>() {

							public void onSuccess(List<User> result) {
								event.publish(new LoadListAoSucceded(result));
							}

							public void onFailure(String failedResult) {
								event.publish(new LoadListAoFailed());
							}
						});
			}
		};
		aoAsync.execute(UrlApi.LIST_AO(token,customerId));
	}

	public void reassign(String token, int idCustomer, int idAo, String note) {
		// generate assign object
		JSONObject assignObj = new JSONObject();
		JSONObject recPropObj = new JSONObject();
		JSONArray recArrObj = new JSONArray();
		JSONObject recObj = new JSONObject();
		JSONArray noteArr = new JSONArray();
		try {

			noteArr.put(note);
			recObj.put("note", noteArr);
			recArrObj.put(recObj);
			recPropObj.put("proposal_recommendations_attributes", recArrObj);

			assignObj.put("token", token);
			assignObj.put("ao", idAo);
			assignObj.put("customer_data", recPropObj);
			System.out.println("ASSIGN OBJ" + assignObj.toString());
		} catch (JSONException e) {
			e.printStackTrace();
		}
		AssigntoAsync assingnAsync = new AssigntoAsync() {
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				System.out.println("RESPON" + result);
				parser.assigningResponse(result, new ParsingResponse<Void>() {

					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						event.publish(new AssignSucceeded());
					}

					public void onFailure(String failedResult) {
						// TODO Auto-generated method stub
						event.publish(new AssignFailed());
					}
				});
			}
		};
		assingnAsync.execute(UrlApi.REASSIGN(token, idCustomer),
				assignObj.toString());
	}

	public void ackBoxRefresh(String token) {
		RefreshBoxAsync refreshAsync = new RefreshBoxAsync() {
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				parser.refreshBox(result, new ParsingResponse<List<BoxData>>() {

					public void onSuccess(List<BoxData> result) {
						event.publish(new RefreshAckBoxSucceded(result));
					}

					public void onFailure(String failedResult) {
						event.publish(new RefreshAckBoxFailed(failedResult));
					}
				});
			}
		};
		refreshAsync.execute(UrlApi.REFRESH_ACKBOX(token));
	}

	public void ackBoxGetSummary(String token, int idCustomer) {
		GetSummaryAsync getSummaryAsync = new GetSummaryAsync() {
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				parser.proposalUploadResponse(result,
						new ParsingResponse<Summary>() {

							public void onSuccess(Summary result) {
								event.publish(new GetSummarySucceeded(result));
							}

							public void onFailure(String failedResult) {
								event.publish(new GetSummaryFailed(failedResult));
							}
						});
			}
		};
		getSummaryAsync.execute(UrlApi.ACKBOX_GET_SUMMARY(token, idCustomer));
	}

	public void ackBoxAcknowladgement(String token, int idCustomer,
			SummaryCandidate summaryCandidate) {
		ApproveAsync approveAsync = new ApproveAsync() {
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				parser.acknowladgementResponse(result,
						new ParsingResponse<Void>() {

							public void onSuccess(Void result) {
								// TODO Auto-generated method stub
								event.publish(new ApproveAckSucceded());
							}

							public void onFailure(String failedResult) {
								// TODO Auto-generated method stub
								event.publish(new ApproveAckFailed());
							}
						});
			}
		};

		approveAsync.execute(UrlApi.ACKBOX_ACKNOWLADGEMENT(token, idCustomer),
				summaryCandidate.getSummaryCandidate());
	}

	public void approveFollowupBox(String token, int idCustomer,
			SummaryCandidate summaryCandidate) {
		ApproveAsync approveAsync = new ApproveAsync() {
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				parser.approveResponse(result, new ParsingResponse<Void>() {

					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						event.publish(new ApproveFollowupSucceded());
					}

					public void onFailure(String failedResult) {
						// TODO Auto-generated method stub
						event.publish(new ApproveFollowupFailed());
					}
				});
			}
		};
		approveAsync.execute(UrlApi.APPROVE_FOLLOWUP_BOX(token, idCustomer),
				summaryCandidate.getSummaryCandidate());

	}

	public void ackBoxReject(String token, int idCustomer,
			SummaryCandidate summaryCandidate) {
		// TODO Auto-generated method stub
		RejectProposal rejectAsync = new RejectProposal() {
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				parser.rejectResponse(result, new ParsingResponse<Void>() {

					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						event.publish(new RejectAckSucceeded());
					}

					public void onFailure(String failedResult) {
						// TODO Auto-generated method stub
						event.publish(new RejectAckFailed(failedResult));

					}
				});
			}
		};
		rejectAsync.execute(UrlApi.REJECT_PROPOSAL(token, idCustomer),
				summaryCandidate.getSummaryCandidate());
	}

	public void rejectFollowupBox(String token, int idCustomer,
			SummaryCandidate summaryCandidate) {
		// TODO Auto-generated method stub
		RejectProposal rejectAsync = new RejectProposal() {
			@Override
			protected void onPostExecute(String result) {
				// TODO Auto-generated method stub
				super.onPostExecute(result);
				parser.rejectResponse(result, new ParsingResponse<Void>() {

					public void onSuccess(Void result) {
						// TODO Auto-generated method stub
						event.publish(new RejectFollowupSucceeded());
					}

					public void onFailure(String failedResult) {
						// TODO Auto-generated method stub
						event.publish(new RejectFollowupFailed(failedResult));

					}
				});
			}
		};
		rejectAsync.execute(UrlApi.REJECT_PROPOSAL(token, idCustomer),
				summaryCandidate.getSummaryCandidate());
	}

	public void getSids(String token, int idCustomer) {
		System.out.println("sync sid execute");
		GetSidAsync sidAsync = new GetSidAsync() {
			@Override
			protected void onPostExecute(String result) {
				super.onPostExecute(result);
				parser.sidResponse(result, new ParsingResponse<List<Sid>>() {

					public void onSuccess(List<Sid> result) {
						event.publish(new SidLoadSucceded(result));
					}

					public void onFailure(String failedResult) {
						event.publish(new SidLoadFailed());

					}
				});
			}
		};
		sidAsync.execute(UrlApi.LIST_SID(token, idCustomer));
	}

}

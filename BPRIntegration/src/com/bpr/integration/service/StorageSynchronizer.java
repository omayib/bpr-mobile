package com.bpr.integration.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.activeandroid.query.Delete;
import com.bpr.activerecord.model.AssetActiveRecord;
import com.bpr.activerecord.model.BpkbActiveRecord;
import com.bpr.activerecord.model.BusinessActiveRecord;
import com.bpr.activerecord.model.ChildActiveRecord;
import com.bpr.activerecord.model.DepositoActiveRecord;
import com.bpr.activerecord.model.EnvironmentActiveRecord;
import com.bpr.activerecord.model.ExpenditureActiveRecord;
import com.bpr.activerecord.model.IncomeActiveRecord;
import com.bpr.activerecord.model.JobActiveRecord;
import com.bpr.activerecord.model.PersonalActiveRecord;
import com.bpr.activerecord.model.PhotoActiveRecord;
import com.bpr.activerecord.model.PhotoFolderActiveRecord;
import com.bpr.activerecord.model.ProposalActiveRecord;
import com.bpr.activerecord.model.PurposeActiveRecord;
import com.bpr.activerecord.model.QueryAction;
import com.bpr.activerecord.model.ShmActiveRecord;
import com.bpr.activerecord.model.SubmissionActiveRecord;
import com.bpr.activerecord.model.TabunganActiveRecord;
import com.bpr.event.manager.EventPublisher;
import com.bpr.event.manager.EventSubscriber;
import com.bpr.integration.controller.UserController;
import com.bpr.repository.Repository;
import com.customer.event.ProposalDetailToSave;
import com.customer.model.Assets;
import com.customer.model.Business;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;
import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralShm;
import com.customer.model.EnvironmentalCheck;
import com.customer.model.Photo;
import com.customer.model.PhotoCandidate;
import com.customer.model.PhotoFolder;
import com.customer.model.Purposes;
import com.proposalbox.event.MarkProposalAsCompleted;
import com.proposalbox.event.PhotoCandidateToUpload;
import com.proposalbox.event.PhotoDeleteRequest;
import com.proposalbox.event.PhotoReupload;
import com.proposalbox.event.PhotoUpdateSucceded;
import com.proposalbox.event.PhotoUploadRequested;
import com.proposalbox.event.PhotoUploadS3Succeded;
import com.proposalbox.event.ProposalCandidateIsNotValid;
import com.proposalbox.event.ProposalUploadSucceeded;
import com.proposalbox.event.SummaryUploadSucceded;
import com.user.event.SignoutSucceeded;

public class StorageSynchronizer {
	private EventPublisher eventPublisher;
	private final List<EventSubscriber<?>> subscribers = new ArrayList<EventSubscriber<?>>();
	private UserController userController;
	private Repository repository;

	public StorageSynchronizer(EventPublisher eventPublisher,
			UserController userController, Repository bprRepository) {
		super();
		this.userController = userController;
		this.eventPublisher = eventPublisher;
		this.repository = bprRepository;
	}

	public void init() {
		System.out.println("sync initialized");
		registeringSubscriber();
	}

	public void tearDown() {
		removeEventSubscribers();
	}

	private void registeringSubscriber() {
		addEventSubscriber(new EventSubscriber<ProposalUploadSucceeded>() {

			public void handleEvent(ProposalUploadSucceeded event) {
				repository.setProposalUploadSucceeded(event.summary
						.getProposalId());
			}
		});
		addEventSubscriber(new EventSubscriber<ProposalDetailToSave>() {

			public void handleEvent(ProposalDetailToSave event) {
				System.out.println("event ProposalDetailToSaved");
				repository.saveAssets((List<Assets>) event.proposal.getAssets()
						.getAsset(), event.proposal.getId());
				repository.saveBusinessCollection(
						(List<Business>) event.proposal.getBusinessCollection()
								.getBusinessCollection(), event.proposal
								.getId());
				repository.saveCapacity((List<CapacityIncome>) event.proposal
						.getCapacity().getIncome(),
						(List<CapacityExpenditure>) event.proposal
								.getCapacity().getExpenditure(), event.proposal
								.getCapacity().getId(), event.proposal.getId());
				repository.saveCollateralBpkb(
						(List<CollateralBpkb>) event.proposal
								.getBpkbCollection().getGuaranteeBpkb(),
						event.proposal.getId());
				repository.saveCollateralShm(
						(List<CollateralShm>) event.proposal.getShmCollection()
								.getGuaranteeShm(), event.proposal.getId());
				repository.saveDeposito(
						event.proposal.getDeposito().listDeposito,
						event.proposal.getId());
				repository.saveTabungan(
						event.proposal.getTabungan().listTabungan,
						event.proposal.getId());
				repository.saveEnvCheckup(
						(List<EnvironmentalCheck>) event.proposal
								.getEnviromental().getCheckCollection(),
						event.proposal.getId());
				repository.saveJob(event.proposal.getJob(),
						event.proposal.getId());
				repository.savePersonal(event.proposal.getPersonal(),
						event.proposal.getId());
				repository.saveProposal(event.proposal.getId());
				repository.savePurposes((List<Purposes>) event.proposal
						.getPurposeCollection().getPurposesCollection(),
						event.proposal.getId());
				repository.saveSubmission(event.proposal.getSubmission(),
						event.proposal.getId());
				repository.savePhotoBusiness((List<PhotoFolder>) event.proposal
						.getPhotoBusiness().getFolders(), event.proposal
						.getId());
				repository.savePhotoCollateral(
						(List<PhotoFolder>) event.proposal.getPhotoGuarantee()
								.getFolders(), event.proposal.getId());
				repository.savePhotoHome((List<PhotoFolder>) event.proposal
						.getPhotoHome().getFolders(), event.proposal.getId());
				repository.savePhotoOther((List<PhotoFolder>) event.proposal
						.getPhotoOther().getFolders(), event.proposal.getId());
			}
		});
		addEventSubscriber(new EventSubscriber<MarkProposalAsCompleted>() {

			public void handleEvent(MarkProposalAsCompleted event) {
				if (repository.isProposalUploaded(event.idProposal)) {
					System.out.println("getting summary...");
					userController.getSummary(event.token, event.idProposal);
				} else {
					System.out
							.println("upload proposal then getting summary...");
					getProposalCandidate(event.idProposal, event.token);
					getPhotosCandidate(event.idProposal, event.token);
				}
			}
		});

		addEventSubscriber(new EventSubscriber<PhotoReupload>() {

			public void handleEvent(PhotoReupload event) {
				System.out.println("sync called photoreupload event");
				getAllPhotosCandidate();
			}
		});

		addEventSubscriber(new EventSubscriber<SignoutSucceeded>() {

			public void handleEvent(SignoutSucceeded event) {
				removeAllProposalCandidate();

			}
		});

		addEventSubscriber(new EventSubscriber<SummaryUploadSucceded>() {

			public void handleEvent(SummaryUploadSucceded event) {
				removeProposalCandidate(event.idProposal);

			}
		});
		addEventSubscriber(new EventSubscriber<PhotoUploadRequested>() {

			public void handleEvent(PhotoUploadRequested event) {
				System.out.println("inserting..");
				PhotoCandidate photoCandidate = event.photoCandidate;
				PhotoActiveRecord photo;
				photo = PhotoActiveRecord.getCurrentPhoto(photoCandidate
						.getPhoto().getId());
				if (photo == null)
					photo = new PhotoActiveRecord();

				photo.action = QueryAction.INSERT;
				photo.idProposal = photoCandidate.getProposalId();
				photo.idFolder = photoCandidate.getFolderId();
				photo.info = photoCandidate.getPhoto().getInfo();
				photo.name = photoCandidate.getPhoto().getName();
				photo.id = photoCandidate.getPhoto().getId();
				photo.path = photoCandidate.getPhoto().getPath();
				photo.uniqueId = photoCandidate.getPhoto().getUniqueId();
				photo.uploaded = false;
				photo.synchornized = false;
				photo.save();
			}
		});
		addEventSubscriber(new EventSubscriber<PhotoDeleteRequest>() {

			public void handleEvent(PhotoDeleteRequest event) {
				userController.deletePhoto(event.photo,event.token);
			}
		});
		addEventSubscriber(new EventSubscriber<PhotoUploadS3Succeded>() {

			public void handleEvent(PhotoUploadS3Succeded event) {
				System.out.println("PHOTO UPLOADED");
				PhotoActiveRecord.photoUploaded(event.uniqueId, event.url);
			}
		});
		addEventSubscriber(new EventSubscriber<PhotoUpdateSucceded>() {

			public void handleEvent(PhotoUpdateSucceded event) {
				System.out.println("PHOTO UPDATE"
						+ event.latestCandidate.getPhoto().getUniqueId());

				PhotoActiveRecord.deleteByUniqueId(event.latestCandidate
						.getPhoto().getUniqueId());

			}
		});
	}

	protected void getAllPhotosCandidate() {
		List<PhotoActiveRecord> photos = PhotoActiveRecord.getPendingPhotos();
		List<PhotoCandidate> listPhotoCandidate = new ArrayList<PhotoCandidate>();
		if (photos.isEmpty())
			return;
		for (PhotoActiveRecord p : photos) {
			System.out.println("photos:" + p.name);
			PhotoFolderActiveRecord folder = PhotoFolderActiveRecord
					.getCurrentFolder(p.idFolder);

			Photo photo = new Photo.Builder().id(p.id).info(p.info)
					.name(p.name).pathLocal(p.path).uniqueId(p.uniqueId)
					.build();

			PhotoCandidate candidate = new PhotoCandidate.Builder()
					.folderId(p.idFolder).photo(photo)
					.galleryName(folder.galleryName).folderName(folder.name)
					.proposalId(folder.idProposal).build();
			listPhotoCandidate.add(candidate);
		}
		eventPublisher.publish(new PhotoCandidateToUpload(listPhotoCandidate));
	}

	protected void getPhotosCandidate(int idProposal, String token) {
		System.out.println("photos candidate");
		List<PhotoActiveRecord> photos = PhotoActiveRecord
				.getPhotosCandidateUploadToS3(idProposal);
		List<PhotoCandidate> listPhotoCandidate = new ArrayList<PhotoCandidate>();
		if (!photos.isEmpty()) {
			System.out.println("getPhotosCandidateUploadToS3 not empty");
			for (PhotoActiveRecord p : photos) {
				System.out.println("photos:" + p.name);

				PhotoFolderActiveRecord folder = PhotoFolderActiveRecord
						.getCurrentFolder(p.idFolder);

				Photo photo = new Photo.Builder().id(p.id).info(p.info)
						.name(p.name).pathLocal(p.path).uniqueId(p.uniqueId)
						.build();

				PhotoCandidate candidate = new PhotoCandidate.Builder()
						.folderId(p.idFolder).photo(photo)
						.galleryName(folder.galleryName)
						.folderName(folder.name).proposalId(folder.idProposal)
						.build();
				listPhotoCandidate.add(candidate);
			}
			eventPublisher.publish(new PhotoCandidateToUpload(
					listPhotoCandidate));
		} else {
			System.out.println("getPhotosCandidateUploadToBpr");
			List<PhotoActiveRecord> pa = PhotoActiveRecord
					.getPhotosCandidateUploadToBpr(idProposal);
			System.out.println("getPhotosCandidateUploadToBpr not empty "
					+ pa.size());
			if (!pa.isEmpty()) {
				for (PhotoActiveRecord p : pa) {
					System.out.println("photos:" + p.name);

					PhotoFolderActiveRecord folder = PhotoFolderActiveRecord
							.getCurrentFolder(p.idFolder);

					Photo photo = new Photo.Builder().id(p.id).info(p.info)
							.name(p.name).pathLocal(p.path).url(p.url)
							.uniqueId(p.uniqueId).build();

					PhotoCandidate candidate = new PhotoCandidate.Builder()
							.folderId(p.idFolder).photo(photo)
							.galleryName(folder.galleryName)
							.folderName(folder.name)
							.proposalId(folder.idProposal).build();
					listPhotoCandidate.add(candidate);
					userController.updatePhoto(token, candidate);
				}
			}
		}
	}

	protected void removeAllProposalCandidate() {
		System.out.println("deleting....");
		new Thread(new Runnable() {

			public void run() {

				new Delete().from(ProposalActiveRecord.class).execute();
				new Delete().from(AssetActiveRecord.class).execute();
				new Delete().from(BusinessActiveRecord.class).execute();
				new Delete().from(EnvironmentActiveRecord.class).execute();
				new Delete().from(IncomeActiveRecord.class).execute();
				new Delete().from(JobActiveRecord.class).execute();
				new Delete().from(PersonalActiveRecord.class).execute();
				new Delete().from(ProposalActiveRecord.class).execute();
				new Delete().from(PurposeActiveRecord.class).execute();
				new Delete().from(ShmActiveRecord.class).execute();
				new Delete().from(SubmissionActiveRecord.class).execute();
				new Delete().from(TabunganActiveRecord.class).execute();
				new Delete().from(DepositoActiveRecord.class).execute();
				new Delete().from(PhotoFolderActiveRecord.class).execute();
				new Delete().from(PhotoActiveRecord.class).execute();
			}
		}).start();
	}

	protected void removeProposalCandidate(final int id) {
		new Thread(new Runnable() {

			public void run() {

				AssetActiveRecord.delete(id);
				BpkbActiveRecord.delete(id);
				BusinessActiveRecord.delete(id);
				EnvironmentActiveRecord.delete(id);
				ExpenditureActiveRecord.delete(id);
				IncomeActiveRecord.delete(id);
				JobActiveRecord.delete(id);
				PersonalActiveRecord.delete(id);
				ProposalActiveRecord.delete(id);
				PurposeActiveRecord.delete(id);
				ShmActiveRecord.delete(id);
				SubmissionActiveRecord.delete(id);
			}
		}).start();
	}

	private void addEventSubscriber(EventSubscriber<?> subscriber) {
		eventPublisher.registerSubscriber(subscriber);
		subscribers.add(subscriber);
	}

	private void removeEventSubscribers() {
		for (EventSubscriber<?> subscriber : subscribers) {
			eventPublisher.unregisterSubsriber(subscriber);
		}
		subscribers.clear();
	}

	protected void getProposalCandidate(int idProposal, String token) {
		ProposalActiveRecord proposal = ProposalActiveRecord
				.getProposalCandidate(idProposal);
		if (proposal == null) {
			userController.uploadProposal("", -1, "");
			return;
		}
		/*
		 * get all data from SQLite
		 */
		PersonalActiveRecord personal = PersonalActiveRecord
				.getPersonal(idProposal);

		List<ChildActiveRecord> child = new ArrayList<ChildActiveRecord>();
		if (personal != null)
			child = ChildActiveRecord.getChildren(personal.id);

		JobActiveRecord job = JobActiveRecord.getJob(idProposal);

		List<BusinessActiveRecord> business = new ArrayList<BusinessActiveRecord>();
		business = BusinessActiveRecord.getBusiness(idProposal);

		List<PurposeActiveRecord> purposes = new ArrayList<PurposeActiveRecord>();
		purposes = PurposeActiveRecord.getPurposes(idProposal);

		SubmissionActiveRecord submission = SubmissionActiveRecord
				.getSubmission(idProposal);
		List<ExpenditureActiveRecord> expenditures = new ArrayList<ExpenditureActiveRecord>();
		expenditures = ExpenditureActiveRecord.getExpenditures(idProposal);

		List<IncomeActiveRecord> incomes = new ArrayList<IncomeActiveRecord>();
		incomes = IncomeActiveRecord.getIncomes(idProposal);

		List<ShmActiveRecord> shm = new ArrayList<ShmActiveRecord>();
		shm = ShmActiveRecord.getShm(idProposal);

		List<BpkbActiveRecord> bpkb = new ArrayList<BpkbActiveRecord>();
		bpkb = BpkbActiveRecord.getBpkb(idProposal);

		List<TabunganActiveRecord> tabungan = new ArrayList<TabunganActiveRecord>();
		tabungan = TabunganActiveRecord.getAllTabungan(idProposal);
		System.out.println("tabungannnn:" + tabungan.toString());

		List<DepositoActiveRecord> deposito = new ArrayList<DepositoActiveRecord>();
		deposito = DepositoActiveRecord.getAllDeposito(idProposal);

		List<EnvironmentActiveRecord> env = new ArrayList<EnvironmentActiveRecord>();
		env = EnvironmentActiveRecord.getEnv(idProposal);

		List<AssetActiveRecord> assets = new ArrayList<AssetActiveRecord>();
		assets = AssetActiveRecord.getAssets(idProposal);

		/*
		 * now, generate to JSON format
		 */
		// FIRST, create PROPOSAL CANDIDATE object

		JSONObject proposalObj = new JSONObject();

		// lets start from Children
		JSONArray childArr = new JSONArray();
		if (child != null) {
			for (ChildActiveRecord c : child) {
				JSONObject childAttr = new JSONObject();
				try {
					childAttr.put("name", c.name);
					childAttr.put("age", c.age);
					if (c.id >= 0) {
						childAttr.put("id", c.id);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				childArr.put(childAttr);
			}
		}
		// next, create for personal
		JSONObject personalAttr = new JSONObject();
		if (personal != null) {
			try {
				personalAttr.put("id", personal.id);
				personalAttr.put("name", personal.name);
				personalAttr.put("ktp_number", personal.ktpNumber);
				personalAttr.put("birth_date", personal.birthDate);
				personalAttr.put("birth_place", personal.birthPlace);
				personalAttr.put("age", personal.age);
				personalAttr.put("gender", personal.gender);
				personalAttr.put("marital_status", personal.maritalStatus);
				personalAttr.put("ktp_address", personal.addressByKtp);
				personalAttr.put("current_address", personal.addressByCurrent);
				personalAttr.put("spouse_name", personal.spouseName);
				personalAttr.put("spouse_job", personal.spouseJob);
				personalAttr.put("alias_name", personal.aliasName);
				personalAttr.put("home_status", personal.homeStatus);
				personalAttr.put("mother_maiden_name",
						personal.motherMaidenName);
				personalAttr.put("number_of_dependents",
						personal.numberOfDependents);
				personalAttr.put("phone_number", personal.phoneNumber);
				personalAttr.put("handphone_number", personal.handphoneNumber);
				personalAttr.put("personal_children_attributes", childArr);
				// insert into proposal candidate obj
				proposalObj.put("survey_personal_attributes", personalAttr);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		// next, we will found for JOB ! hehee..
		JSONObject jobAttr = new JSONObject();
		if (job != null) {
			if (job.name.isEmpty()) {
				System.out.println("NOT VALID");
				eventPublisher.publish(new ProposalCandidateIsNotValid(
						"nama pekerjaan wajib diisi"));
				return;
			}
			try {
				jobAttr.put("name", job.name);
				jobAttr.put("field", job.jobField);
				jobAttr.put("office_name", job.officeName);
				jobAttr.put("office_address", job.officeAddr);
				jobAttr.put("status", job.statusOfWorker);
				jobAttr.put("current_position", job.positionByCurrent);
				jobAttr.put("initial_position", job.positionByInitial);
				jobAttr.put("joined_year", job.yearOfJoined);
				jobAttr.put("monthly_salary", job.salary);
				jobAttr.put("achievement", job.achievement);
				if (job.id > 0) {
					jobAttr.put("id", job.id);
				}
				// =====================
				proposalObj.put("survey_job_attributes", jobAttr);

			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

		// next, we continue with BUSINESS
		if (!business.isEmpty()) {
			JSONArray businessArr = new JSONArray();
			for (BusinessActiveRecord b : business) {
				JSONObject o = new JSONObject();
				try {
					o.put("name", b.name);
					o.put("address", b.address);
					o.put("field", b.field);
					o.put("legality", b.companyLegallity);
					o.put("office_ownership_status", b.officeOwnership);
					o.put("siup_number", b.siupNumber);
					o.put("tdp_number", b.tdpNumber);
					o.put("number_of_employees", b.numberOfEmployees);
					o.put("founded_year", b.foundedYear);
					o.put("years_of_operation", b.yearsOfOperation);
					o.put("project_experience", b.projectExperiences);
					o.put("additional_info", b.additionalInfo);
					if (b.id >= 0) {
						o.put("id", b.id);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				businessArr.put(o);
			}
			try {
				proposalObj.put("survey_businesses_attributes", businessArr);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		// continue again!, we re arange purpose
		if (!purposes.isEmpty()) {
			JSONArray purposeArr = new JSONArray();
			for (PurposeActiveRecord p : purposes) {
				JSONObject o = new JSONObject();
				try {
					o.put("general", p.general);
					o.put("detail", p.detail);
					if (p.id >= 0) {
						o.put("id", p.id);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				purposeArr.put(o);
			}
			try {
				proposalObj.put("survey_purposes_attributes", purposeArr);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		// create SUBMISSION object
		JSONObject submObj = new JSONObject();
		if (submission != null) {
			try {
				submObj.put("id", submission.id);
				submObj.put("plafon", submission.plafon);
				submObj.put("product", submission.product);
				submObj.put("period", submission.period);
				submObj.put("interest", submission.interest);
				// ///////////////
				proposalObj.put("survey_submission_credit_attributes", submObj);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		// create INCOME object
		JSONArray incArr = new JSONArray();
		int idCapacity = -1;
		for (IncomeActiveRecord inc : incomes) {
			JSONObject o = new JSONObject();
			try {
				o.put("name", inc.name);
				o.put("value", inc.value);
				if (inc.id >= 0) {
					o.put("id", inc.id);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			idCapacity = inc.idCapacity;
			incArr.put(o);
		}
		// create EXPENDITURE object
		JSONArray expenditureArr = new JSONArray();
		for (ExpenditureActiveRecord exp : expenditures) {
			JSONObject o = new JSONObject();
			try {
				o.put("name", exp.name);
				o.put("value", exp.value);
				if (exp.id >= 0) {
					o.put("id", exp.id);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			idCapacity = exp.idCapacity;
			expenditureArr.put(o);
		}
		// create CAPACITY object
		JSONObject capacityObj = new JSONObject();
		try {
			capacityObj.put("capacity_expenditures_attributes", expenditureArr);
			capacityObj.put("capacity_incomes_attributes", incArr);
			if (idCapacity != -1) {
				capacityObj.put("id", idCapacity);
			}
			// ///////////////////
			proposalObj.put("survey_capacity_attributes", capacityObj);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		// create SHM collateral
		JSONArray shmArray = new JSONArray();
		if (!shm.isEmpty()) {
			for (ShmActiveRecord s : shm) {
				JSONObject o = new JSONObject();
				try {
					o.put("name", s.name);
					o.put("certificate_number", s.certificateNumber);
					o.put("certificate_name", s.certificateName);
					o.put("size", s.size);
					o.put("market_price", s.priceMarket);
					o.put("assessed_price", s.priceAssessed);
					o.put("status", s.status.replace(" ", "_"));
					o.put("shm_type", s.type);
					o.put("binding", s.binding);
					o.put("remarks", s.info);
					if (s.id >= 0) {
						o.put("id", s.id);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				shmArray.put(o);
			}
			try {
				proposalObj.put("shm_collaterals_attributes", shmArray);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}
		// create BPKB collateral
		JSONArray bpkbArray = new JSONArray();
		if (!bpkb.isEmpty()) {
			for (BpkbActiveRecord b : bpkb) {
				JSONObject o = new JSONObject();
				try {
					o.put("name", b.name);
					o.put("bpkb_name", b.bpkbName);
					o.put("bpkb_number", b.bpkbNumber);
					o.put("stnk_number", b.stnkNumber);
					o.put("market_price", b.priceMarket);
					o.put("assessed_price", b.priceAssessed);
					o.put("vehicle_name", b.vehicleName);
					o.put("vehicle_type", b.vehicleType);
					o.put("vehicle_year", b.vehicleYear);
					o.put("vehicle_brand", b.vehicleBrand);
					o.put("binding", b.binding);
					o.put("police_number", b.policeNumber);
					o.put("remarks", b.info);
					if (b.id >= 0) {
						o.put("id", b.id);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				bpkbArray.put(o);
			}
			try {
				proposalObj.put("bpkb_collaterals_attributes", bpkbArray);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}

		JSONArray tabunganArray = new JSONArray();
		if (!tabungan.isEmpty()) {
			for (TabunganActiveRecord t : tabungan) {
				JSONObject o = new JSONObject();
				try {
					o.put("book_number", t.bookNumber);
					o.put("account_number", t.accountNumber);
					o.put("name", t.name);
					o.put("amount", t.amount);
					if (t.id >= 0) {
						o.put("id", t.id);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				tabunganArray.put(o);
			}
			try {
				proposalObj.put("tabungan_collaterals_attributes",
						tabunganArray);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		JSONArray depositoArray = new JSONArray();
		if (!deposito.isEmpty()) {
			for (DepositoActiveRecord d : deposito) {
				JSONObject o = new JSONObject();
				try {
					o.put("bilyet_number", d.bilyetNumber);
					o.put("account_number", d.accountNumber);
					o.put("name", d.name);
					o.put("amount", d.amount);
					if (d.id >= 0) {
						o.put("id", d.id);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				depositoArray.put(o);
			}
			try {
				proposalObj.put("deposito_collaterals_attributes",
						depositoArray);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		// ENVIRONMENTAL CHECKUP
		JSONArray envArray = new JSONArray();
		if (!env.isEmpty()) {
			for (EnvironmentActiveRecord e : env) {
				JSONObject o = new JSONObject();
				try {
					o.put("name", e.name);
					o.put("relation", e.realtion);
					o.put("impression", e.impression.toUpperCase());
					if (e.id >= 0) {
						o.put("id", e.id);
					}
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				envArray.put(o);
			}

			try {// survey_environments_attributes
				proposalObj.put("survey_environments_attributes", envArray);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}

		// ASSET
		JSONArray assetArr = new JSONArray();
		if (!assets.isEmpty()) {
			for (AssetActiveRecord a : assets) {
				JSONObject o = new JSONObject();
				try {
					o.put("name", a.name);
					o.put("value", a.value);
					if (a.id >= 0) {
						o.put("id", a.id);
					}
				} catch (JSONException e1) {
					e1.printStackTrace();
				}
				assetArr.put(o);
			}
			try {
				proposalObj.put("survey_assets_attributes", assetArr);
			} catch (JSONException e1) {
				e1.printStackTrace();
			}
		}

		JSONObject proposalCandidateObj = new JSONObject();
		try {
			proposalCandidateObj.put("token", token);
			proposalCandidateObj.put("proposal_survey", proposalObj);
		} catch (JSONException e1) {
			e1.printStackTrace();
		}
		System.out.println(proposalObj.toString());
		userController.uploadProposal(proposalCandidateObj.toString(),
				idProposal, "");
	}

}

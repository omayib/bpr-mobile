package com.bpr.integration.parser;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.Assets;
import com.customer.model.AssetsCollection;

public class AssetsParser {
	private Collection<Assets> assets;

	public AssetsParser(JSONArray arr) {
		assets = new ArrayList<Assets>();
		try {
			for (int i = 0; i < arr.length(); i++) {
				JSONObject ob = arr.getJSONObject(i);
				Assets a = new Assets.Builder().asset(ob.getInt("id"),
						ob.getString("name"), ob.getString("value")).build();
				assets.add(a);
			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public AssetsCollection getAssets() {
		return new AssetsCollection.Builder().asset(assets).build();
	}

}

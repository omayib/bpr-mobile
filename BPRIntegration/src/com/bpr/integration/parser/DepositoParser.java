package com.bpr.integration.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.CollateralDeposito;

public class DepositoParser {
	public List<CollateralDeposito> listDeposito = new ArrayList<CollateralDeposito>();

	public DepositoParser(JSONArray enDeposito) {
		if (enDeposito.length() == 0) {
			return;
		}
		for (int i = 0; i < enDeposito.length(); i++) {
			try {
				JSONObject ob = enDeposito.getJSONObject(i);
				CollateralDeposito deposito = new CollateralDeposito.Builder()
						.accountNumber(ob.getString("account_number"))
						.bilyetNumber(ob.getString("bilyet_number"))
						.id(ob.getInt("id")).name(ob.getString("name"))
						.amount(ob.getString("ammount")).build();
				listDeposito.add(deposito);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

}

package com.bpr.integration.parser;

import java.util.List;

import com.bpr.event.manager.ParsingResponse;
import com.customer.event.BoxData;
import com.customer.event.ProposalCollectionLoadSucceeded;
import com.customer.model.PhotoFolder;
import com.customer.model.PhotoUploaded;
import com.customer.model.Proposal;
import com.customer.model.Sid;
import com.customer.model.Summary;
import com.user.event.SigninSucceeded;
import com.user.modul.User;

public interface DataParser {
	void signin(String data, ParsingResponse<SigninSucceeded> callback);

	void signout(String data, ParsingResponse<Void> callback);

	void refreshBox(String data, ParsingResponse<List<BoxData>> callback);

	void listOfProposal(String data,
			ParsingResponse<ProposalCollectionLoadSucceeded> callback);

	void proposalData(String data, ParsingResponse<Proposal> callback);

	void proposalUploadResponse(String data, ParsingResponse<Summary> callback);

	void summaryUploadedResponse(String data, int idProposal,
			ParsingResponse<Integer> callback);

	void requestAoResponse(String data, ParsingResponse<List<User>> callback);

	void assigningResponse(String data, ParsingResponse<Void> callback);

	void approveResponse(String data, ParsingResponse<Void> callback);

	void acknowladgementResponse(String data, ParsingResponse<Void> callback);

	void rejectResponse(String data, ParsingResponse<Void> callback);

	void sidResponse(String data, ParsingResponse<List<Sid>> callback);

	void folderGalleryCreatedResponse(String data,
			ParsingResponse<PhotoFolder> callback);

	void photoUploadedResoponse(String data,
			ParsingResponse<PhotoUploaded> callback);

	void updatePhoto(String data, ParsingResponse<Void> callback);
}

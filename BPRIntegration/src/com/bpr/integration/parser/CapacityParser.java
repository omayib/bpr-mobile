package com.bpr.integration.parser;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.CapacityCollection;
import com.customer.model.CapacityExpenditure;
import com.customer.model.CapacityIncome;

public class CapacityParser {
	private CapacityCollection capColl;

	public CapacityParser(JSONObject cap, BigDecimal bigDecimal) {

		try {
			int id = cap.getInt("id");
			JSONArray iob = cap.getJSONArray("income");
			JSONArray eob = cap.getJSONArray("expenditure");

			Collection<CapacityIncome> inColl = new ArrayList<CapacityIncome>();
			Collection<CapacityExpenditure> exColl = new ArrayList<CapacityExpenditure>();

			for (int i = 0; i < iob.length(); i++) {
				JSONObject ob = iob.getJSONObject(i);
				CapacityIncome in = new CapacityIncome.Builder().incomes(
						ob.getInt("id"), ob.getString("name"),
						ob.getString("value")).build();
				inColl.add(in);
			}
			for (int i = 0; i < eob.length(); i++) {
				JSONObject ob = eob.getJSONObject(i);
				CapacityExpenditure in = new CapacityExpenditure.Builder()
						.expenditures(ob.getInt("id"), ob.getString("name"),
								ob.getString("value")).build();
				exColl.add(in);
			}

			capColl = new CapacityCollection.Builder(id, inColl, exColl)
					.submission(bigDecimal).build();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public CapacityCollection getCapacityCollection() {
		return capColl;
	}
}

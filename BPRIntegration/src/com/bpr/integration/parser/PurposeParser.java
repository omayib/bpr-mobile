package com.bpr.integration.parser;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.Purposes;
import com.customer.model.PurposesCollection;

public class PurposeParser {
	private PurposesCollection pc;

	public PurposeParser(JSONArray ppob) {
		try {
			Collection<Purposes> puposes = new ArrayList<Purposes>();
			for (int i = 0; i < ppob.length(); i++) {
				JSONObject ob = ppob.getJSONObject(i);
				int id = ob.getInt("id");
				String general = ob.getString("general");
				String detail = ob.getString("detail");
				Purposes purpose = new Purposes.Builder().purpose(id, general,
						detail).build();
				puposes.add(purpose);
			}
			pc = new PurposesCollection.Builder().purpose(puposes).build();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public PurposesCollection getPurposeCollection() {
		return pc;
	}

}

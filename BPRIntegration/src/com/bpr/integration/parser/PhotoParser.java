package com.bpr.integration.parser;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.Photo;
import com.customer.model.PhotoBusiness;
import com.customer.model.PhotoFolder;
import com.customer.model.PhotoCollateral;
import com.customer.model.PhotoHome;
import com.customer.model.PhotoOther;

public class PhotoParser {
	private PhotoBusiness photoBusiness;
	private PhotoHome photoHome;
	private PhotoCollateral photoGuarantee;
	private PhotoOther photoOther;

	public PhotoParser(JSONObject photoob) {
		try {
			JSONArray homeob = photoob.getJSONArray("home");
			JSONArray businessob = photoob.getJSONArray("business");
			JSONArray guaranteeob = photoob.getJSONArray("collateral");
			JSONArray otherob = photoob.getJSONArray("other");

			Collection<PhotoFolder> homeCollection = new ArrayList<PhotoFolder>();
			Collection<PhotoFolder> businessCollection = new ArrayList<PhotoFolder>();
			Collection<PhotoFolder> guaranteeCollection = new ArrayList<PhotoFolder>();
			Collection<PhotoFolder> otherCollection = new ArrayList<PhotoFolder>();

			for (int i = 0; i < homeob.length(); i++) {
				JSONObject ob = homeob.getJSONObject(i);
				int id = ob.getInt("id");
				String category = ob.getString("category");
				JSONArray galleries = ob.getJSONArray("galleries");
				Collection<Photo> photos = new ArrayList<Photo>();
				for (int j = 0; j < galleries.length(); j++) {
					JSONObject p = galleries.getJSONObject(j);
					String url = p.getString("url");
					String name = p.getString("name");
					String info = p.getString("info");
					int idC = p.getInt("id");
					Photo photo = new Photo.Builder().id(idC).url(url)
							.name(name).info(info).build();
					photos.add(photo);
				}

				PhotoFolder pc = new PhotoFolder.Builder().id(id)
						.photo(photos).name(category).build();
				homeCollection.add(pc);
			}
			photoHome = new PhotoHome.Builder().folders(homeCollection)
					.build();
			for (int i = 0; i < businessob.length(); i++) {
				JSONObject ob = businessob.getJSONObject(i);
				int id = ob.getInt("id");
				String category = ob.getString("category");
				JSONArray galleries = ob.getJSONArray("galleries");
				Collection<Photo> photos = new ArrayList<Photo>();
				for (int j = 0; j < galleries.length(); j++) {
					JSONObject p = galleries.getJSONObject(j);
					String url = p.getString("url");
					String name = p.getString("name");
					String info = p.getString("info");
					int idC = p.getInt("id");
					Photo photo = new Photo.Builder().id(idC).url(url)
							.name(name).info(info).build();
					photos.add(photo);
				}

				PhotoFolder pc = new PhotoFolder.Builder().id(id)
						.photo(photos).name(category).build();
				businessCollection.add(pc);
			}
			photoBusiness = new PhotoBusiness.Builder().folders(
					businessCollection).build();
			for (int i = 0; i < guaranteeob.length(); i++) {
				JSONObject ob = guaranteeob.getJSONObject(i);
				int id = ob.getInt("id");
				String category = ob.getString("category");
				JSONArray galleries = ob.getJSONArray("galleries");
				Collection<Photo> photos = new ArrayList<Photo>();
				for (int j = 0; j < galleries.length(); j++) {
					JSONObject p = galleries.getJSONObject(j);
					String url = p.getString("url");
					String name = p.getString("name");
					String info = p.getString("info");
					int idC = p.getInt("id");
					Photo photo = new Photo.Builder().url(url).name(name)
							.info(info).id(idC).build();
					photos.add(photo);
				}

				PhotoFolder pc = new PhotoFolder.Builder().id(id)
						.photo(photos).name(category).build();
				guaranteeCollection.add(pc);
			}
			photoGuarantee = new PhotoCollateral.Builder().foldes(
					guaranteeCollection).build();
			for (int i = 0; i < otherob.length(); i++) {
				JSONObject ob = otherob.getJSONObject(i);
				int id = ob.getInt("id");
				String category = ob.getString("category");
				JSONArray galleries = ob.getJSONArray("galleries");
				Collection<Photo> photos = new ArrayList<Photo>();
				for (int j = 0; j < galleries.length(); j++) {
					JSONObject p = galleries.getJSONObject(j);
					String url = p.getString("url");
					String name = p.getString("name");
					String info = p.getString("info");
					int idC = p.getInt("id");
					Photo photo = new Photo.Builder().id(idC).url(url)
							.name(name).info(info).build();
					photos.add(photo);
				}

				PhotoFolder pc = new PhotoFolder.Builder().id(id)
						.photo(photos).name(category).build();
				otherCollection.add(pc);
			}
			photoOther = new PhotoOther.Builder().folders(otherCollection)
					.build();

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public PhotoBusiness getPhotoBusiness() {
		return photoBusiness;
	}

	public PhotoHome getPhotoHome() {
		return photoHome;
	}

	public PhotoCollateral getPhotoGuarantee() {
		return photoGuarantee;
	}

	public PhotoOther getPhotoOther() {
		return photoOther;
	}

}

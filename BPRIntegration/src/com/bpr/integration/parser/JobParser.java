package com.bpr.integration.parser;

import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.Job;

public class JobParser {
	private Job job;

	public JobParser() {
		super();
	}

	public JobParser(JSONObject job) {
		try {
			int id = job.getInt("id");
			String jobName = job.isNull("name") ? "" : job.getString("name");
			String jobField = job.isNull("field") ? "" : job.getString("field");
			String officeName = job.isNull("office_name") ? "" : job
					.getString("office_name");
			String officeAddr = job.isNull("office_address") ? "" : job
					.getString("office_address");
			String statusOfWorker = job.isNull("status") ? "" : job
					.getString("status");
			String positionByCurrent = job.isNull("current_position") ? ""
					: job.getString("current_position");
			int yearOfJoined = job.isNull("joined_year") ? 0 : job
					.getInt("joined_year");
			String positionByFirst = job.isNull("initial_position") ? "" : job
					.getString("initial_position");
			String salary = job.isNull("monthly_salary") ? "0" : job
					.getString("monthly_salary");
			String achievement = job.isNull("achievement") ? "" : job
					.getString("achievement");

			this.job = new Job.Builder().achievment(achievement).name(jobName)
					.field(jobField).officeAddress(officeAddr)
					.officeName(officeName)
					.positionByCurrent(positionByCurrent)
					.positionByInitial(positionByFirst)
					.statusOfWorker(statusOfWorker).id(id).salary(salary)
					.yearOfJoined(yearOfJoined).build();

		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public Job getJob() {
		if (job != null) {
			return job;
		} else {
			return new Job.Builder().achievment("").field("").officeAddress("")
					.officeName("").positionByCurrent("").positionByInitial("")
					.statusOfWorker("").id(-9991).salary("0").yearOfJoined(0)
					.build();
		}
	}

}

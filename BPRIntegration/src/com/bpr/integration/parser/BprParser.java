package com.bpr.integration.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.bpr.event.manager.ParsingResponse;
import com.customer.event.BoxData;
import com.customer.event.ProposalCollectionLoadSucceeded;
import com.customer.model.AssetsCollection;
import com.customer.model.BusinessCollection;
import com.customer.model.CapacityCollection;
import com.customer.model.CollateralBpkbCollection;
import com.customer.model.CollateralDepositoCollection;
import com.customer.model.CollateralShmCollection;
import com.customer.model.CollateralTabunganCollection;
import com.customer.model.EnvironmentalCheckCollection;
import com.customer.model.Job;
import com.customer.model.Personal;
import com.customer.model.PhotoBusiness;
import com.customer.model.PhotoCollateral;
import com.customer.model.PhotoFolder;
import com.customer.model.PhotoHome;
import com.customer.model.PhotoOther;
import com.customer.model.PhotoUploaded;
import com.customer.model.Proposal;
import com.customer.model.ProposalLite;
import com.customer.model.PurposesCollection;
import com.customer.model.Recommendation;
import com.customer.model.Sid;
import com.customer.model.SubmissionCredit;
import com.customer.model.Summary;
import com.user.event.SigninSucceeded;
import com.user.modul.User;

public class BprParser implements DataParser {

	public void signin(String data, ParsingResponse<SigninSucceeded> callback) {
		System.out.println(data);
		try {
			JSONObject o = new JSONObject(data);

			if (!o.getBoolean("success")) {
				callback.onFailure("failed");
				return;
			}
			JSONObject json = o.getJSONObject("data");

			JSONArray arrBox = json.getJSONArray("box");
			if (arrBox.length() == 0) {
				callback.onFailure("have no permission");
				return;
			}

			String name = json.getString("username");
			int id = json.getInt("id");
			String level = json.getString("level");
			String token = json.getString("auth_token");
			String box = json.getString("box");

			User u = new User.Builder().id(id).level(level).name(name).build();

			SigninSucceeded succeeded = new SigninSucceeded(u, token, box);
			callback.onSuccess(succeeded);
			return;

		} catch (JSONException e) {
			e.printStackTrace();
		}
		callback.onFailure("");
	}

	public void signout(String data, ParsingResponse<Void> callback) {
		try {
			JSONObject json = new JSONObject(data);
			if (!json.getBoolean("success")) {
				callback.onFailure("failed");
				return;
			}
			callback.onSuccess(null);
		} catch (JSONException e) {
			e.printStackTrace();
			callback.onFailure(e.toString());
		}
	}

	public void refreshBox(String data, ParsingResponse<List<BoxData>> callback) {
		List<BoxData> listResponse = new ArrayList<BoxData>();
		try {
			JSONObject job = new JSONObject(data);
			if (!job.getBoolean("success")) {
				callback.onFailure("sorry, something wrong");
				return;
			}

			JSONArray jar = job.getJSONArray("data");
			if (jar.length() == 0) {
				callback.onFailure("no data");
				return;
			}
			for (int i = 0; i < jar.length(); i++) {
				JSONObject ob = jar.getJSONObject(i);

				BoxData response = new BoxData.Builder()
						.box(ob.getString("status")).id(ob.getInt("id"))
						.idCustomer(ob.getInt("customer_id"))
						.name(ob.getString("name"))
						.plafon(ob.getString("plafon")).build();
				listResponse.add(response);
			}
			callback.onSuccess(listResponse);
		} catch (JSONException e) {
			e.printStackTrace();
			callback.onFailure("sorry,temporary server error");
		}
	}

	public void listOfProposal(String data,
			ParsingResponse<ProposalCollectionLoadSucceeded> callback) {
		try {
			JSONObject job = new JSONObject(data);
			if (!job.getBoolean("success")) {
				callback.onFailure("");
				return;
			}
			JSONObject d = job.getJSONObject("data");
			String name = d.getString("name");
			String plafon = d.getString("plafon");
			List<ProposalLite> lite = new ArrayList<ProposalLite>();
			List<Recommendation> reco = new ArrayList<Recommendation>();

			JSONArray p = d.getJSONArray("proposals");
			for (int i = 0; i < p.length(); i++) {
				JSONObject pob = p.getJSONObject(i);
				int idp = pob.getInt("id");
				String date = pob.getString("checked_date");
				User user = null;
				if (!pob.isNull("checked_by")) {
					JSONObject u = pob.getJSONObject("checked_by");
					user = new User.Builder().id(u.getInt("id"))
							.name(u.getString("username")).build();
				} else {
					user = new User.Builder().id(-1).name("ORIGINAL").build();

				}
				ProposalLite pl = new ProposalLite.Builder().Checkedby(user)
						.date(date).id(idp).build();
				lite.add(pl);

			}

			JSONArray r = d.getJSONArray("recommendations");
			for (int i = 0; i < r.length(); i++) {

				JSONObject rob = r.getJSONObject(i);
				if (rob.isNull("user")) {
					continue;
				}
				int idr = rob.getInt("id");
				String date = rob.getString("date");
				String status = rob.getString("status");
				String sign = rob.getString("signature");

				JSONObject uob = rob.getJSONObject("user");
				User user = new User.Builder().id(uob.getInt("id"))
						.name(uob.getString("username"))
						.level(uob.getString("level")).build();

				JSONArray anote = rob.getJSONArray("notes");
				List<String> notes = new ArrayList<String>();
				for (int j = 0; j < anote.length(); j++) {
					String note = anote.getString(j);
					notes.add(note);
				}

				Recommendation recom = new Recommendation.Builder()
						.signature(sign).user(user).date(date).build();
				recom.setNotes(notes);

				reco.add(recom);
			}
			ProposalCollectionLoadSucceeded loaded = new ProposalCollectionLoadSucceeded(
					lite, reco, name, plafon);
			loaded.recommendationAsStringObject = r.toString();
			callback.onSuccess(loaded);
		} catch (JSONException e) {
			e.printStackTrace();
			callback.onFailure("");
		}
	}

	public void proposalData(String data, ParsingResponse<Proposal> callback) {
		try {
			JSONObject obj = new JSONObject(data);

			if (!obj.getBoolean("success")) {
				callback.onFailure("sory, failed to get detail proposal");
				return;
			}
			JSONObject dob = obj.getJSONObject("data");
			int id = dob.getInt("id");
			JSONObject pob = dob.getJSONObject("personal");
			PersonalParser personalParser = new PersonalParser(pob);
			Personal personal = personalParser.getPersonal();

			JSONArray aob = dob.getJSONArray("assets");
			AssetsParser assetParser = new AssetsParser(aob);
			AssetsCollection assets = assetParser.getAssets();

			Job job;
			JobParser jobParser;
			if (!dob.isNull("job")) {
				JSONObject jobObj = dob.getJSONObject("job");
				jobParser = new JobParser(jobObj);
			} else {
				jobParser = new JobParser();
			}
			job = jobParser.getJob();

			JSONArray bob = dob.getJSONArray("business");
			BusinessParser businessParser = new BusinessParser(bob);
			BusinessCollection bcoll = businessParser.getBusinessCollection();

			JSONObject sob = dob.getJSONObject("submission");
			SubmissionParser submissionParser = new SubmissionParser(sob);
			SubmissionCredit submissionCredit = submissionParser
					.getSubmissionCredit();

			JSONObject cap = dob.getJSONObject("capacity");
			CapacityParser capacityParser = new CapacityParser(cap,
					submissionCredit.getPlafon());
			CapacityCollection capColl = capacityParser.getCapacityCollection();

			JSONArray ppob = dob.getJSONArray("purpose");
			PurposeParser purposeParser = new PurposeParser(ppob);
			PurposesCollection pc = purposeParser.getPurposeCollection();

			JSONArray envob = dob.getJSONArray("environment_check");
			EnvironmentalParser envParser = new EnvironmentalParser(envob);
			EnvironmentalCheckCollection envChekCollection = envParser
					.getCheckCollection();

			JSONObject gob = dob.getJSONObject("collateral");
			GuaranteeParser guaranteeParser = new GuaranteeParser(gob);
			CollateralBpkbCollection bpkbCollection = guaranteeParser
					.getBpkbCollection();
			CollateralShmCollection shmCollection = guaranteeParser
					.getShmCollection();
			CollateralTabunganCollection tabunganCollection = guaranteeParser
					.getTabunganCollection();
			CollateralDepositoCollection depositoCollection = guaranteeParser
					.getDepositoCollection();

			JSONObject photoob = dob.getJSONObject("photo");
			PhotoParser photoParser = new PhotoParser(photoob);

			PhotoHome photoHome = photoParser.getPhotoHome();
			PhotoCollateral photoGuarantee = photoParser.getPhotoGuarantee();
			PhotoBusiness photoBusiness = photoParser.getPhotoBusiness();
			PhotoOther photoOther = photoParser.getPhotoOther();

			Proposal proposal = new Proposal.Builder().id(id).assets(assets)
					.tabungan(tabunganCollection).deposito(depositoCollection)
					.bpkb(bpkbCollection).business(bcoll).capacity(capColl)
					.environmentalCheck(envChekCollection).job(job)
					.personal(personal).photoBusiness(photoBusiness)
					.photoGuarantee(photoGuarantee).photoHome(photoHome)
					.photoOther(photoOther).purpose(pc).shm(shmCollection)
					.submissionOfCredit(submissionCredit).build();
			callback.onSuccess(proposal);
		} catch (JSONException e) {
			e.printStackTrace();
			callback.onFailure("problem with your connection or server");
		}
	}

	public void proposalUploadResponse(String data,
			ParsingResponse<Summary> callback) {
		try {
			JSONObject responseObj = new JSONObject(data);
			if (!responseObj.getBoolean("success")) {
				callback.onFailure("request failed");
				return;
			}
			if (responseObj.isNull("data")) {
				callback.onFailure("dont have summary");
				return;
			}
			JSONObject dataObj = responseObj.getJSONObject("data");
			JSONObject submissionObj = dataObj.getJSONObject("submission");
			JSONObject collateralObj = dataObj.getJSONObject("collaterals");
			JSONArray recoArrObj = dataObj.getJSONArray("recommendations");
			String coRatio = dataObj.getString("ratio");
			// ============================================
			int proposal_id = dataObj.getInt("proposal_id");
			String name = dataObj.getString("name");
			boolean avReferral = dataObj.getBoolean("av_referral");
			String pValue = dataObj.getString("P");
			String aValue = dataObj.getString("A");
			int summaryId = dataObj.getInt("id");
			// ============================================
			SubmissionParser submissionParser = new SubmissionParser(
					submissionObj);
			SubmissionCredit submissionCredit = submissionParser
					.getSubmissionCredit();
			// ============================================
			String coTotalValues = collateralObj.getString("total_values");

			GuaranteeParser collateralParser = new GuaranteeParser(
					collateralObj);
			CollateralBpkbCollection bpkbCollection = collateralParser
					.getBpkbCollection();
			CollateralShmCollection shmCollection = collateralParser
					.getShmCollection();
			// ==================================================

			List<Recommendation> listReco = new ArrayList<Recommendation>();
			if (recoArrObj.length() > 0) {
				for (int i = 0; i < recoArrObj.length(); i++) {
					JSONObject rob = recoArrObj.getJSONObject(i);
					if (rob.isNull("user")) {
						continue;
					}
					System.out.println("terate " + i + "-"
							+ rob.getString("recommend_as"));
					if (rob.getString("recommend_as").equalsIgnoreCase(
							"assigner")
							|| rob.getString("recommend_as").equalsIgnoreCase(
									"reassigner")) {
						System.out.println("break");
						continue;
					}
					System.out.println("not break");
					int idr = rob.getInt("id");
					String date =rob.isNull("data")?"":rob.getString("date");
					String status = rob.getString("status");
					String sign = rob.getString("signature");
					String recommendAs = rob.getString("recommend_as");

					JSONObject uob = rob.getJSONObject("user");
					User user = new User.Builder().id(uob.getInt("id"))
							.name(uob.getString("username"))
							.level(uob.getString("level")).build();

					JSONArray anote = rob.getJSONArray("notes");
					List<String> notes = new ArrayList<String>();
					for (int j = 0; j < anote.length(); j++) {
						String note = anote.getString(j);
						notes.add(note);
					}

					Recommendation recom = new Recommendation.Builder().setAccepted(status)
							.recommendAs(recommendAs).signature(sign)
							.user(user).date(date).build();
					recom.setNotes(notes);

					listReco.add(recom);
				}
			}
			Summary summary = new Summary.Builder().ratio(coRatio)
					.summaryId(summaryId).proposalId(proposal_id)
					.totalValues(coTotalValues).a(Float.parseFloat(aValue))
					.avReferral(avReferral).bpkbCollection(bpkbCollection)
					.name(name).p(Float.parseFloat(pValue))
					.shmCollection(shmCollection).submission(submissionCredit)
					.recommendations(listReco).build();
			callback.onSuccess(summary);
		} catch (JSONException e) {
			e.printStackTrace();
			callback.onFailure("upload failed");
		}

	}

	public void requestAoResponse(String data,
			ParsingResponse<List<User>> callback) {
		try {
			JSONObject dataObj = new JSONObject(data);
			if (!dataObj.getBoolean("success")) {
				callback.onFailure("");
				return;
			}
			JSONArray userArrObj = dataObj.getJSONArray("data");
			List<User> listAo = new ArrayList<User>();
			for (int i = 0; i < userArrObj.length(); i++) {
				JSONObject userObj = userArrObj.getJSONObject(i);
				User user = new User.Builder().id(userObj.getInt("id"))
						.name(userObj.getString("username"))
						.level(userObj.getString("level")).build();
				listAo.add(user);
			}
			callback.onSuccess(listAo);
		} catch (JSONException e) {
			e.printStackTrace();
			callback.onFailure("");
		}
	}

	public void assigningResponse(String data, ParsingResponse<Void> callback) {
		try {
			JSONObject dataObj = new JSONObject(data);
			if (!dataObj.getBoolean("success")) {
				callback.onFailure("");
				return;
			}
			callback.onSuccess(null);

		} catch (JSONException e) {
			callback.onFailure("");
			e.printStackTrace();
		}

	}

	public void summaryUploadedResponse(String data, int idProposal,
			ParsingResponse<Integer> callback) {
		try {
			JSONObject dataObj = new JSONObject(data);
			if (!dataObj.getBoolean("success")) {
				callback.onFailure("something went wrong");
				return;// failed
			}
			callback.onSuccess(idProposal);
		} catch (JSONException e) {
			e.printStackTrace();
			callback.onFailure("something went wrong");
		}

	}

	public void approveResponse(String data, ParsingResponse<Void> callback) {
		// TODO Auto-generated method stub

		System.out.println("response:" + data);
		try {
			JSONObject dataObj = new JSONObject(data);
			if (!dataObj.getBoolean("success")) {
				callback.onFailure("upss..,something wrong");
				return;
			}
			callback.onSuccess(null);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void acknowladgementResponse(String data,
			ParsingResponse<Void> callback) {
		// TODO Auto-generated method stub
		System.out.println("response:" + data);
		try {
			JSONObject dataObj = new JSONObject(data);
			if (!dataObj.getBoolean("success")) {
				callback.onFailure("upss..,something wrong");
				return;
			}
			callback.onSuccess(null);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void rejectResponse(String data, ParsingResponse<Void> callback) {
		System.out.println("response:" + data);
		try {
			JSONObject dataObj = new JSONObject(data);
			if (!dataObj.getBoolean("success")) {
				callback.onFailure("upss..,something wrong");
				return;
			}
			callback.onSuccess(null);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			callback.onFailure("upss..,something wrong");
			e.printStackTrace();
		}
	}

	public void sidResponse(String data, ParsingResponse<List<Sid>> callback) {
		System.out.println("data sid" + data);
		try {
			JSONObject dataObj = new JSONObject(data);
			if (!dataObj.getBoolean("success")) {
				callback.onFailure("upss..,something wrong");
				return;
			}
			JSONArray dataArrObj = dataObj.getJSONArray("data");
			List<Sid> listSid = new ArrayList<Sid>();
			for (int i = 0; i < dataArrObj.length(); i++) {
				JSONObject sidObj = dataArrObj.getJSONObject(i);
				Sid sid = new Sid.Builder().id(sidObj.getInt("id"))
						.photo(sidObj.getString("url"))
						.thumbnail(sidObj.getString("thumb")).build();
				listSid.add(sid);
			}
			callback.onSuccess(listSid);

		} catch (JSONException e) {
			callback.onFailure("upss..,something wrong");
			e.printStackTrace();
		}
	}

	public void folderGalleryCreatedResponse(String data,
			ParsingResponse<PhotoFolder> callback) {
		System.out.println(data);
		try {
			JSONObject responseObj = new JSONObject(data);
			if (!responseObj.getBoolean("success")) {
				callback.onFailure("sorry missing parameters");
				return;
			}
			JSONObject dataObj = responseObj.getJSONObject("data");
			int id = dataObj.getInt("id");
			String catagoryName = dataObj.getString("category");
			PhotoFolder gallery = new PhotoFolder.Builder().id(id)
					.name(catagoryName).build();
			callback.onSuccess(gallery);
		} catch (JSONException e) {
			callback.onFailure("sorry, error with your server or connection");
			e.printStackTrace();
		}
	}

	public void photoUploadedResoponse(String data,
			ParsingResponse<PhotoUploaded> callback) {
		System.out.println(data);
		try {
			JSONObject responseObj = new JSONObject(data);
			if (!responseObj.getBoolean("success")) {
				callback.onFailure("sorry missing parameters");
				return;
			}
			String uniqueId = responseObj.getString("unique_id");
			JSONObject dataObj = responseObj.getJSONObject("data");
			int id = dataObj.getInt("id");
			int idGallery = dataObj.getInt("gallery_id");
			int idCategory = dataObj.getInt("gallery_category_id");
			String name = dataObj.getString("name");
			String galleryName = dataObj.getString("gallery_category");
			String info = dataObj.getString("info");
			String url = dataObj.getString("url");

			PhotoUploaded photo = new PhotoUploaded.Builder().idPhoto(id)
					.galeryName(galleryName).idCategory(idCategory)
					.idGallery(idGallery).photoInfo(info).photoName(name)
					.photoUrl(url).uniqueId(uniqueId).build();
			callback.onSuccess(photo);
		} catch (JSONException e) {
			callback.onFailure("sorry, error with your server or connection");
			e.printStackTrace();
		}
	}

	public void updatePhoto(String data, ParsingResponse<Void> callback) {
		try {
			JSONObject objData = new JSONObject(data);
			if (!objData.getBoolean("success")) {
				callback.onFailure("update photo failed");
				return;
			}
			callback.onSuccess(null);
		} catch (JSONException e) {
			e.printStackTrace();
			callback.onFailure("server error failed");
		}
	}
}

package com.bpr.integration.parser;

import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.SubmissionCredit;

public class SubmissionParser {
	private SubmissionCredit submissionCredit;

	public SubmissionParser(JSONObject sob) {
		try {
			int id = sob.getInt("id");
			String creditProduct = sob.getString("product");
			String plafon = sob.getString("plafon");
			int periode = sob.getInt("period");
			double interest = sob.isNull("interest") ? 0 : sob
					.getDouble("interest");

			submissionCredit = new SubmissionCredit.Builder().id(id)
					.creditProduct(creditProduct).interest(interest)
					.periode(periode).plafon(plafon).build();

		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	public SubmissionCredit getSubmissionCredit() {
		return submissionCredit;
	}
}

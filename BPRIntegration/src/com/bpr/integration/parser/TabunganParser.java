package com.bpr.integration.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import com.customer.model.CollateralTabungan;

public class TabunganParser {
	public List<CollateralTabungan> listTabungan = new ArrayList<CollateralTabungan>();

	public TabunganParser(JSONArray enTabungan) {
		if (enTabungan.length() == 0) {
			return;
		}
		for (int i = 0; i < enTabungan.length(); i++) {
			org.json.JSONObject ob;
			try {
				ob = enTabungan.getJSONObject(i);
				CollateralTabungan tabungan = new CollateralTabungan.Builder()
						.accountNumber(ob.getString("account_number"))
						.ammount(ob.getString("ammount"))
						.bookNumber(ob.getString("book_number"))
						.id(ob.getInt("id")).build();
				listTabungan.add(tabungan);
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}

	}
}

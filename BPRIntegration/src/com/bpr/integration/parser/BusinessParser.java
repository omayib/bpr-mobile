package com.bpr.integration.parser;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.Business;
import com.customer.model.BusinessCollection;

public class BusinessParser {
	private BusinessCollection bissColl;

	public BusinessParser(JSONArray bob) {

		Collection<Business> bisColl = new ArrayList<Business>();
		try {
			for (int i = 0; i < bob.length(); i++) {
				JSONObject b = bob.getJSONObject(i);
				int id = b.getInt("id");
				String fieldOfBusiness = b.isNull("field") ? "" : b
						.getString("field");
				String companyName = b.isNull("name") ? "" : b
						.getString("name");
				String companyAddress = b.isNull("address") ? "" : b
						.getString("address");
				String companyTenure = b.isNull("office_ownership_status") ? ""
						: b.getString("office_ownership_status");
				String companyLegallity = b.isNull("legality") ? "" : b
						.getString("legality");
				String companySiup = b.isNull("siup_number") ? "" : b
						.getString("siup_number");
				String companyTdp = b.isNull("tdp_number") ? "" : b
						.getString("tdp_number");
				int numberOfEmployes = b.isNull("number_of_employees") ? 0 : b
						.getInt("number_of_employees");
				int year = b.isNull("years_of_operation") ? 0 : b
						.getInt("years_of_operation");
				String companyProject = b.isNull("project_experience") ? "" : b
						.getString("project_experience");
				String companyOtherInfo = b.isNull("additional_info") ? "" : b
						.getString("additional_info");
				int foundedYear = b.isNull("founded_year") ? 0 : b
						.getInt("founded_year");

				Business business = new Business.Builder().id(id)
						.foundedYear(foundedYear)
						.companyAddress(companyAddress)
						.companyInfo(companyOtherInfo)
						.companyLegallity(companyLegallity)
						.companyName(companyName)
						.companyProject(companyProject)
						.companySiup(companySiup).companyTdp(companyTdp)
						.companyTenure(companyTenure)
						.fieldOfBusiness(fieldOfBusiness)
						.numberOfEmployee(numberOfEmployes).year(year).build();
				bisColl.add(business);
			}
			bissColl = new BusinessCollection.Builder().business(bisColl)
					.build();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public BusinessCollection getBusinessCollection() {
		return bissColl;
	}

}

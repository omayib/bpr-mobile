package com.bpr.integration.parser;

import java.util.ArrayList;
import java.util.Collection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.EnvironmentalCheck;
import com.customer.model.EnvironmentalCheckCollection;

public class EnvironmentalParser {
	EnvironmentalCheckCollection ec;

	public EnvironmentalParser(JSONArray envob) {
		Collection<EnvironmentalCheck> check = new ArrayList<EnvironmentalCheck>();
		try {
			for (int i = 0; i < envob.length(); i++) {
				JSONObject ob = envob.getJSONObject(i);
				int id = ob.getInt("id");
				String name = ob.isNull("name") ? "" : ob.getString("name");
				String relations = ob.isNull("relation") ? "" : ob
						.getString("relation");
				String impression = ob.isNull("impression") ? "" : ob
						.getString("impression");
				EnvironmentalCheck ev = new EnvironmentalCheck.Builder().id(id)
						.name(name).relation(relations).impression(impression)
						.build();
				check.add(ev);
			}
			ec = new EnvironmentalCheckCollection.Builder().environmentalCheck(
					check).build();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public EnvironmentalCheckCollection getCheckCollection() {
		return ec;
	}
}

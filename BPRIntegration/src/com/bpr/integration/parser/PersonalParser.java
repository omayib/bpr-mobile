package com.bpr.integration.parser;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.Children;
import com.customer.model.Personal;

public class PersonalParser {
	public Personal personal;

	public PersonalParser(JSONObject p) {
		super();

		try {
			int id = p.getInt("id");
			String name = p.isNull("name") ? "" : p.getString("name");
			String ktpNumber = p.isNull("ktp_number") ? "" : p
					.getString("ktp_number");
			String birthOfDate = p.isNull("birth_date") ? "" : p
					.getString("birth_date");
			String birthOfPlace = p.isNull("birth_place") ? "" : p
					.getString("birth_place");
			String statusOfMarital = p.isNull("marital_status") ? "" : p
					.getString("marital_status");
			String addressByKtp = p.isNull("ktp_address") ? "" : p
					.getString("ktp_address");
			String addressByCurrent = p.isNull("current_address") ? "" : p
					.getString("current_address");
			String spouseName = p.isNull("spouse_name") ? "" : p
					.getString("spouse_name");
			String spouseJob = p.isNull("spouse_job") ? "" : p
					.getString("spouse_job");
			int age = p.isNull("age") ? 0 : p.getInt("age");
			String gender = p.isNull("gender") ? "" : p.getString("gender");
			String nameAlias = p.isNull("alias_name") ? "" : p
					.getString("alias_name");
			String homeStatus = p.isNull("home_status") ? "" : p
					.getString("home_status");
			String motherMaidenName = p.isNull("mother_maiden_name") ? "" : p
					.getString("mother_maiden_name");
			int numberOfDependents = p.isNull("number_of_dependents") ? 0 : p
					.getInt("number_of_dependents");
			String phone = p.isNull("phone_number") ? "" : p
					.getString("phone_number");
			String handphone = p.isNull("handphone_number") ? "" : p
					.getString("handphone_number");
			JSONArray childArrObj = p.getJSONArray("children");
			List<Children> childs = new ArrayList<Children>();
			if (childArrObj.length() > 0) {
				for (int i = 0; i < childArrObj.length(); i++) {
					JSONObject c = childArrObj.getJSONObject(i);
					Children child = new Children.Builder()
							.name(c.getString("name")).age(c.getInt("age"))
							.id(c.getInt("id")).build();
					childs.add(child);
				}
			}
			Personal ps = new Personal.Builder()
					.addressByCurrent(addressByCurrent)
					.addressByKtp(addressByKtp).age(age).birthDate(birthOfDate)
					.birthPlace(birthOfPlace).children(childs).gender(gender)
					.handphoneNumber(handphone).homeStatus(homeStatus).id(id)
					.ktpNumber(ktpNumber).motherMaidenName(motherMaidenName)
					.name(name).nameAlias(nameAlias)
					.numberOfDependents(numberOfDependents).phoneNumber(phone)
					.spouseJob(spouseJob).spouseName(spouseName)
					.statusOfMarital(statusOfMarital).build();

			personal = ps;

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public Personal getPersonal() {
		System.out.println("GET PERSONAL::" + personal.toString());
		return personal;
	}

}

package com.bpr.integration.parser;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.customer.model.CollateralBpkb;
import com.customer.model.CollateralBpkbCollection;
import com.customer.model.CollateralDeposito;
import com.customer.model.CollateralDepositoCollection;
import com.customer.model.CollateralShm;
import com.customer.model.CollateralShmCollection;
import com.customer.model.CollateralTabungan;
import com.customer.model.CollateralTabunganCollection;

public class GuaranteeParser {
	private CollateralBpkbCollection bpkbCollection;
	private CollateralShmCollection shmCollection;
	private CollateralDepositoCollection depositoCollection;
	private CollateralTabunganCollection tabunganCollection;

	public GuaranteeParser(JSONObject gob) {
		List<CollateralBpkb> listBpkb = new ArrayList<CollateralBpkb>();
		List<CollateralShm> listShm = new ArrayList<CollateralShm>();
		List<CollateralDeposito> listDeposito = new ArrayList<CollateralDeposito>();
		List<CollateralTabungan> listTabungan = new ArrayList<CollateralTabungan>();

		try {

			JSONArray shmob = gob.isNull("shm") ? new JSONArray() : gob
					.getJSONArray("shm");
			JSONArray bpkbob = gob.isNull("bpkb") ? new JSONArray() : gob
					.getJSONArray("bpkb");
			JSONArray tabunganbob = gob.isNull("tabungan") ? new JSONArray()
					: gob.getJSONArray("tabungan");
			JSONArray depositobob = gob.isNull("deposito") ? new JSONArray()
					: gob.getJSONArray("deposito");

			for (int i = 0; i < shmob.length(); i++) {
				JSONObject ob = shmob.getJSONObject(i);
				int id = ob.getInt("id");
				String certificateNumber = ob.isNull("certificate_number") ? ""
						: ob.getString("certificate_number");
				String certificateName = ob.isNull("certificate_name") ? ""
						: ob.getString("certificate_name");
				String size = ob.isNull("size") ? "0" : ob.getString("size");
				String priceOnMarket = ob.isNull("market_price") ? "0" : ob
						.getString("market_price");
				String priceOnAssessed = ob.isNull("assessed_price") ? "0" : ob
						.getString("assessed_price");
				String statusOfLand = ob.isNull("status")
						|| ob.getString("status").isEmpty() ? "sawah" : ob
						.getString("status");
				String type = ob.isNull("shm_type") ? "shm" : ob
						.getString("shm_type");
				String binding = ob.isNull("binding") ? "skmht" : ob
						.getString("binding");

				CollateralShm shm = new CollateralShm.Builder().id(id)
						.binding(binding).certificateName(certificateName)
						.certificateNumber(certificateNumber).loadOfArea(size)
						.priceOnAssessed(priceOnAssessed).type(type)
						.priceOnMarket(priceOnMarket)
						.statusOfLand(statusOfLand).build();
				listShm.add(shm);
			}
			shmCollection = new CollateralShmCollection.Builder().shm(listShm)
					.build();

			for (int i = 0; i < bpkbob.length(); i++) {
				JSONObject ob = bpkbob.getJSONObject(i);
				int id = ob.getInt("id");
				String nameOfBpkb = ob.isNull("bpkb_name") ? "" : ob
						.getString("bpkb_name");
				String priceOnMarket = ob.isNull("market_price") ? "0" : ob
						.getString("market_price");
				String priceOnAssessed = ob.isNull("assessed_price") ? "0" : ob
						.getString("assessed_price");
				String nameOfVehicle = ob.isNull("vehicle_name") ? "" : ob
						.getString("vehicle_name");
				String typeOfVehicle = ob.isNull("vehicle_type") ? "" : ob
						.getString("vehicle_type");
				String yearOfVehicle = ob.isNull("vehicle_year") ? "0" : ob
						.getString("vehicle_year");
				String numberBpkb = ob.isNull("bpkb_number") ? "" : ob
						.getString("bpkb_number");
				String numberStnk = ob.isNull("stnk_number") ? "" : ob
						.getString("stnk_number");
				String brand = ob.isNull("vehicle_brand") ? "" : ob
						.getString("vehicle_brand");
				String binding = ob.isNull("binding") ? "fidusia" : ob
						.getString("binding");
				String policeNumber = ob.isNull("police_number") ? "" : ob
						.getString("police_number");

				CollateralBpkb bpkb = new CollateralBpkb.Builder().id(id)
						.binding(binding).carBrand(brand)
						.nameOfBpkb(nameOfBpkb).carName(nameOfVehicle)
						.numberBpkb(numberBpkb).numberStnk(numberStnk)
						.policeNumber(policeNumber)
						.priceOnAssessed(priceOnAssessed)
						.priceOnMarket(priceOnMarket).carType(typeOfVehicle)
						.carYear(yearOfVehicle).build();
				listBpkb.add(bpkb);
			}
			bpkbCollection = new CollateralBpkbCollection.Builder().bpkb(
					listBpkb).build();

			for (int i = 0; i < tabunganbob.length(); i++) {
				org.json.JSONObject ob;
				try {
					ob = tabunganbob.getJSONObject(i);
					CollateralTabungan tabungan = new CollateralTabungan.Builder()
							.accountNumber(ob.getString("account_number"))
							.ammount(ob.getString("amount"))
							.name(ob.getString("name"))
							.bookNumber(ob.getString("book_number"))
							.id(ob.getInt("id")).build();
					listTabungan.add(tabungan);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			tabunganCollection = new CollateralTabunganCollection(listTabungan);

			// ======================
			for (int i = 0; i < depositobob.length(); i++) {
				try {
					JSONObject ob = depositobob.getJSONObject(i);
					CollateralDeposito deposito = new CollateralDeposito.Builder()
							.accountNumber(ob.getString("account_number"))
							.bilyetNumber(ob.getString("bilyet_number"))
							.id(ob.getInt("id")).name(ob.getString("name"))
							.amount(ob.getString("amount")).build();
					listDeposito.add(deposito);
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
			depositoCollection = new CollateralDepositoCollection(listDeposito);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	public CollateralDepositoCollection getDepositoCollection() {
		return depositoCollection;
	}

	public CollateralTabunganCollection getTabunganCollection() {
		return tabunganCollection;
	}

	public CollateralBpkbCollection getBpkbCollection() {
		return bpkbCollection;
	}

	public CollateralShmCollection getShmCollection() {
		return shmCollection;
	}

}

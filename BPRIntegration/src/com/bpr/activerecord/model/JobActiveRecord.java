package com.bpr.activerecord.model;

import java.math.BigDecimal;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "job")
public class JobActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "name")
	public String name;

	@Column(name = "office_name")
	public String officeName;

	@Column(name = "office_address")
	public String officeAddr;

	@Column(name = "field")
	public String jobField;

	@Column(name = "status")
	public String statusOfWorker;

	@Column(name = "current_position")
	public String positionByCurrent;

	@Column(name = "initial_position")
	public String positionByInitial;

	@Column(name = "joined_year")
	public int yearOfJoined;

	@Column(name = "monthly_salary")
	public BigDecimal salary;

	@Column(name = "achievement")
	public String achievement;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public static JobActiveRecord getJob(int idProposal) {
		return new Select().from(JobActiveRecord.class)
				.where("proposal=?", idProposal).executeSingle();
	}

	public static JobActiveRecord getCurrentJob(int idJob) {
		return new Select().from(JobActiveRecord.class).where("_id=?", idJob)
				.executeSingle();
	}

	public static void delete(int idProposal) {
		new Delete().from(JobActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}
}

package com.bpr.activerecord.model;

import java.math.BigDecimal;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "submission")
public class SubmissionActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "product")
	public String product;

	@Column(name = "plafon")
	public BigDecimal plafon;

	@Column(name = "priod")
	public int period;

	@Column(name = "interest")
	public double interest;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public static SubmissionActiveRecord getSubmission(int idProposal) {
		return new Select().from(SubmissionActiveRecord.class)
				.where("proposal=?", idProposal).executeSingle();
	}

	public static void delete(int idProposal) {
		new Delete().from(SubmissionActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}
}

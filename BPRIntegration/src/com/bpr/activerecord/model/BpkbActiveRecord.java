package com.bpr.activerecord.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "bpkb")
public class BpkbActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "bpkb_name")
	public String bpkbName;

	@Column(name = "bpkb_number")
	public String bpkbNumber;

	@Column(name = "stnk_number")
	public String stnkNumber;

	@Column(name = "market_price")
	public String priceMarket;

	@Column(name = "assessed_price")
	public String priceAssessed;

	@Column(name = "vehicle_name")
	public String vehicleName;

	@Column(name = "vehicle_type")
	public String vehicleType;

	@Column(name = "vehicle_year")
	public String vehicleYear;

	@Column(name = "vehicle_brand")
	public String vehicleBrand;

	@Column(name = "binding")
	public String binding;

	@Column(name = "police_number")
	public String policeNumber;

	@Column(name = "name")
	public String name;

	@Column(name = "info")
	public String info;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public static List<BpkbActiveRecord> getBpkb(int idProposal) {
		return new Select().from(BpkbActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}

	public static BpkbActiveRecord getCurrentBpkb(int idBpkb) {
		return new Select().from(BpkbActiveRecord.class).where("_id=?", idBpkb)
				.executeSingle();
	}

	public static void delete(long idProposal) {
		new Delete().from(BpkbActiveRecord.class)
				.where("proposal=?", idProposal).execute();

	}
}

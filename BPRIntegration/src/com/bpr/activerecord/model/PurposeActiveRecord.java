package com.bpr.activerecord.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "purpose")
public class PurposeActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "general")
	public String general;

	@Column(name = "detail")
	public String detail;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public static List<PurposeActiveRecord> getPurposes(int idProposal) {
		return new Select().from(PurposeActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}

	public static PurposeActiveRecord getCurrentPurposes(int idPurpose) {
		return new Select().from(PurposeActiveRecord.class)
				.where("_id=?", idPurpose).executeSingle();
	}

	public static void delete(int idProposal) {
		new Delete().from(PurposeActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}
}

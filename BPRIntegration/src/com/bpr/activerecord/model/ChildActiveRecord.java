package com.bpr.activerecord.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "child")
public class ChildActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "name")
	public String name;

	@Column(name = "age")
	public int age;

	@Column(name = "personal")
	public int personal;

	@Column(name = "user_action")
	public String action;

	public static List<ChildActiveRecord> getChildren(long idpersonal) {
		return new Select().from(ChildActiveRecord.class)
				.where("personal = ?", idpersonal).execute();
	}

	public static ChildActiveRecord getCurrentChild(long idChild) {
		return new Select().from(ChildActiveRecord.class)
				.where("_id = ?", idChild).executeSingle();
	}

	public static void delete(long idPersonal) {
		new Delete().from(ChildActiveRecord.class)
				.where("personal=?", idPersonal).execute();
	}
}

package com.bpr.activerecord.model;

import java.math.BigDecimal;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "asset")
public class AssetActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "name")
	public String name;

	@Column(name = "value")
	public BigDecimal value;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public static List<AssetActiveRecord> getAssets(int idProposal) {
		return new Select().from(AssetActiveRecord.class)
				.where("proposal = ?", idProposal).execute();
	}

	public static AssetActiveRecord getCurrentAssets(int idAsset) {
		return new Select().from(AssetActiveRecord.class)
				.where("_id = ?", idAsset).executeSingle();
	}

	public static void delete(long idProposal) {
		new Delete().from(AssetActiveRecord.class)
				.where("proposal = ?", idProposal).execute();
	}
}

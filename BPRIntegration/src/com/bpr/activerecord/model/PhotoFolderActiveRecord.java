package com.bpr.activerecord.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

@Table(name = "photo_folder")
public class PhotoFolderActiveRecord extends Model {

	@Column(name = "folder_id")
	public int idFolder;

	@Column(name = "name")
	public String name;

	// collateral/business/home/other
	@Column(name = "gallery_name")
	public String galleryName;

	@Column(name = "proposal_id")
	public int idProposal;

	@Override
	public String toString() {
		return "PhotoFolderActiveRecord [idFolder=" + idFolder + ", name="
				+ name + ", galleryName=" + galleryName + ", idProposal="
				+ idProposal + "]";
	}

	public List<PhotoActiveRecord> photos() {
		return getMany(PhotoActiveRecord.class, "folder_id");
	}

	public int photoCount() {
		List<PhotoActiveRecord> listPhoto = photos();
		return listPhoto.size();
	}

	public static boolean isFolderExist(int idFolder) {
		return new Select().from(PhotoFolderActiveRecord.class)
				.where("folder_id=?", idFolder).exists();
	}

	public static PhotoFolderActiveRecord getCurrentFolder(int idFolder) {
		return new Select().from(PhotoFolderActiveRecord.class)
				.where("folder_id=?", idFolder).executeSingle();
	}

	public static List<PhotoFolderActiveRecord> getFoldersAndPhotos(
			int idProposal, String galleryName) {
		From from = new Select()
				.all()
				.from(PhotoFolderActiveRecord.class)
				.as("folder")
				.join(PhotoActiveRecord.class)
				.using("folder_id")
				.as("photo")
				.where("proposal_id=? AND gallery_name=?", idProposal,
						galleryName);
		System.out.println("SQL::" + from.toSql());
		return from.execute();
	}

	public static List<PhotoFolderActiveRecord> getFolders(int idProposal,
			String galleryName) {
		From from = new Select().from(PhotoFolderActiveRecord.class).where(
				"proposal_id=? AND gallery_name=?", idProposal, galleryName);
		System.out.println("SQL::" + from.toSql());
		return from.execute();
	}
}

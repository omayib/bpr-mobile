package com.bpr.activerecord.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "environment")
public class EnvironmentActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "name")
	public String name;

	@Column(name = "impression")
	public String impression;

	@Column(name = "relation")
	public String realtion;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public static List<EnvironmentActiveRecord> getEnv(int idProposal) {
		return new Select().from(EnvironmentActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}

	public static EnvironmentActiveRecord getCurrentEnv(int idEnv) {
		return new Select().from(EnvironmentActiveRecord.class)
				.where("_id=?", idEnv).executeSingle();
	}

	public static void delete(long idProposal) {
		new Delete().from(EnvironmentActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}
}

package com.bpr.activerecord.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Column.ConflictAction;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "proposal")
public class ProposalActiveRecord extends Model {

	@Column(name = "_id", unique = true, onUniqueConflict = ConflictAction.IGNORE)
	public int id;

	@Column(name = "sync")
	public boolean sync;

	@Column(name = "user_action")
	public String action;

	public static ProposalActiveRecord getProposal(int idProposal) {
		return new Select().from(ProposalActiveRecord.class)
				.where("_id = ?", idProposal).executeSingle();
	}

	public static ProposalActiveRecord getProposalCandidate(int id) {
		// return new Select().from(ProposalActiveRecord.class)
		// .where("_id = ?", id).where("sync=?", "0").executeSingle();
		return new Select().from(ProposalActiveRecord.class)
				.where("_id = ?", id).executeSingle();
	}

	public static boolean isProposalAvailable(int id) {
		return new Select().from(ProposalActiveRecord.class)
				.where("_id = ?", id).exists();
	}

	public static boolean isProposalUploaded(int id) {
		return new Select().from(ProposalActiveRecord.class)
				.where("_id = ?", id).where("sync=?", "1").exists();
	}

	public static List<ProposalActiveRecord> getAllProposal() {
		return new Select().from(ProposalActiveRecord.class).execute();
	}

	public static void delete(long idProposal) {
		new Delete().from(ProposalActiveRecord.class)
				.where("_id = ?", idProposal).execute();
	}

	public static void truncate() {
		List<ProposalActiveRecord> listProposal = getAllProposal();
		for (ProposalActiveRecord proposalActiveRecord : listProposal) {
			proposalActiveRecord.delete();
		}
	}
}

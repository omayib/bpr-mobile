package com.bpr.activerecord.model;

import java.math.BigDecimal;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "deposito")
public class DepositoActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "bilyet_number")
	public String bilyetNumber;

	@Column(name = "account_number")
	public String accountNumber;

	@Column(name = "name")
	public String name;

	@Column(name = "amount")
	public BigDecimal amount;

	@Column(name = "proposal")
	public int proposal;

	public static List<DepositoActiveRecord> getAllDeposito(int idProposal) {
		return new Select().from(DepositoActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}

	public static DepositoActiveRecord getCurrentDeposito(int idTabungan) {
		return new Select().from(DepositoActiveRecord.class)
				.where("_id=?", idTabungan).executeSingle();
	}

	public static void delete(int idProposal) {
		new Delete().from(DepositoActiveRecord.class)
				.where("proposal = ?", idProposal).execute();
	}
}

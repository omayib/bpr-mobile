package com.bpr.activerecord.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "business")
public class BusinessActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "field")
	public String field;

	@Column(name = "name")
	public String name;

	@Column(name = "address")
	public String address;

	@Column(name = "office_ownership_status")
	public String officeOwnership;

	@Column(name = "siup_number")
	public String siupNumber;

	@Column(name = "tdp_number")
	public String tdpNumber;

	@Column(name = "number_of_employees")
	public int numberOfEmployees;

	@Column(name = "years_of_operation")
	public int yearsOfOperation;

	@Column(name = "founded_year")
	public int foundedYear;

	@Column(name = "project_experience")
	public String projectExperiences;

	@Column(name = "additional_info")
	public String additionalInfo;

	@Column(name = "legality")
	public String companyLegallity;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public static List<BusinessActiveRecord> getBusiness(int idProposal) {
		return new Select().from(BusinessActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}

	public static BusinessActiveRecord getCurrentBusiness(int idBusiness) {
		return new Select().from(BusinessActiveRecord.class)
				.where("_id=?", idBusiness).executeSingle();
	}

	public static void delete(long idProposal) {
		new Delete().from(BusinessActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}
}

package com.bpr.activerecord.model;

import java.math.BigDecimal;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "shm")
public class ShmActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "certificate_number")
	public String certificateNumber;

	@Column(name = "certificate_name")
	public String certificateName;

	@Column(name = "size")
	public double size;

	@Column(name = "market_price")
	public String priceMarket;

	@Column(name = "assessed_price")
	public String priceAssessed;

	@Column(name = "status")
	public String status;
	

	@Column(name = "type")
	public String type;

	@Column(name = "binding")
	public String binding;

	@Column(name = "name")
	public String name;

	@Column(name = "remarks")
	public String info;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public static List<ShmActiveRecord> getShm(int idProposal) {
		return new Select().from(ShmActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}

	public static ShmActiveRecord getCurrentShm(int idShm) {
		return new Select().from(ShmActiveRecord.class).where("_id=?", idShm)
				.executeSingle();
	}

	public static void delete(int idProposal) {
		new Delete().from(ShmActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}
}

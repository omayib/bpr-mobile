package com.bpr.activerecord.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.Select;

@Table(name = "personal")
public class PersonalActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "name")
	public String name;

	@Column(name = "ktp_number")
	public String ktpNumber;

	@Column(name = "birth_date")
	public String birthDate;

	@Column(name = "birth_place")
	public String birthPlace;

	@Column(name = "age")
	public int age;

	@Column(name = "gender")
	public String gender;

	@Column(name = "marital_status")
	public String maritalStatus;

	@Column(name = "ktp_address")
	public String addressByKtp;

	@Column(name = "current_address")
	public String addressByCurrent;

	@Column(name = "spouse_name")
	public String spouseName;

	@Column(name = "spouse_job")
	public String spouseJob;

	@Column(name = "alias_name")
	public String aliasName;

	@Column(name = "home_status")
	public String homeStatus;

	@Column(name = "mother_maiden_name")
	public String motherMaidenName;

	@Column(name = "number_of_dependents")
	public int numberOfDependents;

	@Column(name = "phone_number")
	public String phoneNumber;

	@Column(name = "handphone_number")
	public String handphoneNumber;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public List<ChildActiveRecord> children() {
		return getMany(ChildActiveRecord.class, "personal");
	}

	public static PersonalActiveRecord getPersonal(int idProposal) {
		return new Select().from(PersonalActiveRecord.class)
				.where("proposal = ?", idProposal).executeSingle();
	}

	public static PersonalActiveRecord getCurrentPersoanl(int idPersonal) {
		return new Select().from(PersonalActiveRecord.class)
				.where("_id = ?", idPersonal).executeSingle();
	}

	public static void delete(long idProposal) {
		PersonalActiveRecord p = new Select().from(PersonalActiveRecord.class)
				.where("proposal = ?", idProposal).executeSingle();
		if (p != null)
			ChildActiveRecord.delete(p.id);
		new Delete().from(PersonalActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}
}
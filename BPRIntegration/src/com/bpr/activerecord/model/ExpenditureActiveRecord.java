package com.bpr.activerecord.model;

import java.math.BigDecimal;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

@Table(name = "expenditure")
public class ExpenditureActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "capacity_id")
	public int idCapacity;

	@Column(name = "name")
	public String name;

	@Column(name = "value")
	public BigDecimal value;

	@Column(name = "proposal")
	public int proposal;

	@Column(name = "user_action")
	public String action;

	public static List<ExpenditureActiveRecord> getExpenditures(int idProposal) {
		From from = new Select().from(ExpenditureActiveRecord.class).where(
				"proposal=?", idProposal);
		System.out.println("SQL1:" + from.toSql());
		System.out.println("SQL2:" + from.toExistsSql());
		System.out.println("SQL3:" + idProposal);
		return from.execute();
	}

	public static ExpenditureActiveRecord getCurrentExpenditures(int idExp) {
		return new Select().from(ExpenditureActiveRecord.class)
				.where("_id=?", idExp).executeSingle();
	}

	public static void delete(int idProposal) {
		new Delete().from(ExpenditureActiveRecord.class)
				.where("proposal=?", idProposal).execute();
	}
}

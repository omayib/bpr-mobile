package com.bpr.activerecord.model;

import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.customer.model.Photo;

@Table(name = "photo")
public class PhotoActiveRecord extends Model {

	@Column(name = "id_photo")
	public int id;

	@Column(name = "folder_id")
	public int idFolder;

	@Column(name = "name")
	// photo name
	public String name;

	@Column(name = "info")
	public String info;

	@Column(name = "url")
	public String url;

	@Column(name = "path")
	public String path;

	@Column(name = "uploaded")
	// mean uploaded to s3
	public boolean uploaded;

	@Column(name = "synchornized")
	// mean synchronized to BPR server
	public boolean synchornized;

	@Column(name = "unique_id")
	public String uniqueId;

	@Column(name = "user_action")
	public String action;

	@Column(name = "proposal_id")
	public int idProposal;

	public static void deleteByUniqueId(String uniqueId) {
		System.out.println("deleteByUniqueId executed");
		PhotoActiveRecord p = new Select().from(PhotoActiveRecord.class)
				.where("unique_id=?", uniqueId).executeSingle();
		if (PhotoFolderActiveRecord.isFolderExist(p.idFolder)) {
			// update flag
			System.out.println("update flag");
			p.synchornized = true;
			p.save();
		} else {
			p.delete();
		}
	}

	public static void photoUploaded(String uniqueId, String url) {
		PhotoActiveRecord p = new Select().from(PhotoActiveRecord.class)
				.where("unique_id=?", uniqueId).executeSingle();
		p.uploaded = true;
		p.url = url;
		p.save();
	}

	public static List<PhotoActiveRecord> getPhotosCandidateUploadToS3(
			int idProposal) {
		System.out.println("idproposal:" + idProposal);
		From from = new Select().from(PhotoActiveRecord.class).where(
				"proposal_id=? AND uploaded=0", idProposal);
		System.out.println(from.toSql());
		return from.execute();
	}

	public static List<PhotoActiveRecord> getPhotosCandidateUploadToBpr(
			int idProposal) {
		System.out.println("idproposalxxx:" + idProposal);
		From from = new Select().from(PhotoActiveRecord.class).where(
				"proposal_id=? AND synchornized=0", idProposal);
		System.out.println(from.toSql());
		return from.execute();
	}

	public static PhotoActiveRecord getCurrentPhoto(int idPhoto) {
		return new Select().from(PhotoActiveRecord.class)
				.where("id_photo=?", idPhoto).executeSingle();
	}

	public static List<PhotoActiveRecord> getPhotos(int idFolder) {
		From from = new Select().from(PhotoActiveRecord.class).where(
				"folder_id=?", idFolder);
		System.out.println(from.toSql());
		return from.execute();
	}

	public static List<PhotoActiveRecord> getPhotos() {
		From from = new Select().from(PhotoActiveRecord.class);
		return from.execute();
	}

	public static List<PhotoActiveRecord> getPendingPhotos() {
		From from = new Select().from(PhotoActiveRecord.class).where(
				"synchornized=0");
		System.out.println(from.toSql());
		return from.execute();
	}

	public static boolean photoHasLocalPath(Photo photo) {
		From from = new Select().all().from(PhotoActiveRecord.class)
				.where("id_photo=?", photo.getId()).where("path != ''");
		System.out.println("id  " + photo.getId());
		System.out.println("id name " + from.toSql());
		System.out.println("id exists " + from.exists());
		return from.exists();
	}

	public static void deleteById(int id) {
		PhotoActiveRecord p = new Select().from(PhotoActiveRecord.class)
				.where("id_photo=?", id).executeSingle();
		p.delete();
	}

}

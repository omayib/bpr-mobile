package com.bpr.activerecord.model;

import java.math.BigDecimal;
import java.util.List;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Delete;
import com.activeandroid.query.From;
import com.activeandroid.query.Select;

@Table(name = "tabungan")
public class TabunganActiveRecord extends Model {

	@Column(name = "_id")
	public int id;

	@Column(name = "book_number")
	public String bookNumber;

	@Column(name = "account_number")
	public String accountNumber;

	@Column(name = "name")
	public String name;

	@Column(name = "ammount")
	public BigDecimal amount;

	@Column(name = "proposal")
	public int proposal;

	@Override
	public String toString() {
		return "TabunganActiveRecord [id=" + id + ", bookNumber=" + bookNumber
				+ ", accountNumber=" + accountNumber + ", name=" + name
				+ ", amount=" + amount + ", proposal=" + proposal + "]";
	}

	public static List<TabunganActiveRecord> getAllTabungan(int idProposal) {
		From from = new Select().from(TabunganActiveRecord.class).where(
				"proposal=?", idProposal);
		System.out.println("from :" + from.toSql() + ",id proposal "
				+ idProposal);
		return from.execute();
	}

	public static TabunganActiveRecord getCurrentTabungan(int idTabungan) {
		return new Select().from(TabunganActiveRecord.class)
				.where("_id=?", idTabungan).executeSingle();
	}

	public static void delete(int idProposal) {
		new Delete().from(TabunganActiveRecord.class)
				.where("proposal = ?", idProposal).execute();
	}
}
